(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'container-tablecell',
            condition: function(el) {
                return [
                    el.contentStyle === 'tablecell',
                    el.isContainer
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();