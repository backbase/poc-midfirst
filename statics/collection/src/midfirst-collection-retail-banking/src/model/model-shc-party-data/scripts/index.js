/**
 * @module model-shc-insurance-party-data
 *
 * @description
 * Model to fetch Policies
 *
 * @example
 * import modelInsurancePoliciesModuleKey, { modelInsurancePoliciesKey } from 'model-shc-insurance-party-data';
 *
 * angular
 *   .module('ExampleModule', [
 *     modelInsurancePoliciesModuleKey,
 *   ])
 *   .factory('someFactory', [
 *     modelInsurancePoliciesKey,
 *     // into
 *     function someFactory(insurancePoliciesModel) {
 *       // ...
 *     },
 *   ]);
 */
import angular from 'vendor-bb-angular';


import shcPartyDataModuleKey, {
    shcPartyDataKey,
} from 'data-shc-party-data-http-ng';


import Model from './party-data';

const shcPartyModelModuleKey = 'model-shc-party-data';
export const shcPartyModelKey = `${shcPartyModelModuleKey}:model`;

export default angular
  .module(shcPartyModelModuleKey, [shcPartyDataModuleKey])

  .factory(shcPartyModelKey, [
    '$q',
    shcPartyDataKey,
    /* into */
    Model,
  ])

  .name;
