# ui-bb-account-card

Component for displaying account card.

# Information
| Name       | ui-bb-account-card                   |
|------------|--------------------------------------|
| Version    | 1.1.0                                |
| Bundle     | N/A                                  |
| Author     | Armin Mehinovic <armin@backbase.com> |

## Usage

Install library

```bash
npm install ui-bb-account-card
```

Include script tag into HTML page:

```html
<script src="[features-path]/ui-bb-account-card/dist/uiBbAccountCard.js"></script>
```

- CommonJS

```javascript
var uiBbAccountCard = require('ui-bb-account-card');
```

- AMD

```javascript
define(['ui-bb-account-card'], function(uiBbAccountCard) {

});
```

- ES2015

```javascript
import uiBbAccountCard from 'ui-bb-account-card';
```

## [Api Reference](./docs/api-dev.md)

## Dependencies

## Options

## Events

## Requirements

## References

## Development

```bash
cd ui-bb-account-card
npm install && bblp start -e
```

### Development Dependencies
