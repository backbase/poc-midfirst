/**
 * Project level configuration of modules
 *
 * @module config-bb-project-ng
 *
 * @example
 * Export functions to be used in config phase. E.g.:
 *
 * import { dataSomeEndpointKey } from 'data-bb-some-endpoint-ng';
 *
 * export default [
 *   [`${dataSomeEndpointKey}Provider`, (endpoint) => {
 *     endpoint.setBaseUri('http://example.com/api');
 *   }],
 * ];
 *
 */

import intentModuleKey from 'lib-bb-intent-ng';
import {contactDataKey} from 'data-bb-contact-http-ng';
import {productSummaryDataKey} from 'data-bb-product-summary-http-ng';
import {transactionsDataKey} from 'data-bb-transactions-http-ng';
import {userDataKey} from 'data-bb-user-http-ng';
import {paymentsDataKey} from 'data-bb-payments-http-ng';
import {paymentOrdersDataKey} from 'data-bb-payment-orders-http-ng';
import {placesDataKey} from 'data-bb-places-http-ng';
//--
const serverRoot = window.b$.portal.config.remoteContextRoot || window.b$.portal.config.serverRoot;
const baseUri = serverRoot + '/services/rest/';

//--
const configProvider = (p) => p.setBaseUri(baseUri);
//--
export default [
    ['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.xsrfCookieName = 'BBXSRF';
        $httpProvider.defaults.xsrfHeaderName = 'X-BBXSRF';
    }],
    [`${userDataKey}Provider`, configProvider],
    [`${contactDataKey}Provider`, configProvider],
    [`${productSummaryDataKey}Provider`, configProvider],
    [`${transactionsDataKey}Provider`, configProvider],
    [`${paymentsDataKey}Provider`, configProvider],
    [`${placesDataKey}Provider`, configProvider],
    [`${paymentOrdersDataKey}Provider`, configProvider],
    [`${intentModuleKey}:intentProvider`, (intents) => {
        intents.setRoutes({
            'view.payment.account.select': 'bb.action.payment.account.select',
            'view.payment.form.show': 'bb.action.payment.form.show',
            'view.payment.summary.show': 'bb.action.payment.summary.show',
            'view.payment.pin.show': 'bb.action.payment.pin.show',
            'view.payment.review.show': 'bb.action.payment.review.show',
            'view.payment.schedule.select': 'bb.action.payment.schedule.select',
            'view.productsummary.overview.show': 'bb.action.productsummary.overview.show',
        });
    }],
];
