(function (blueriq, bqApp) {
    'use strict';

    /**
     * Register default templates for elements
     */
    blueriq.defaultTemplateFactory = function (viewModel, context) {

        if (viewModel.type === 'page' && viewModel instanceof blueriq.models.dashboard.PageModel) {
            return blueriq.templates.page.dashboardPage;
        } else if (viewModel.type === 'page' && jQuery.inArray('widget', context.modus) > -1) {
            return blueriq.templates.page.widgetPage;
        }else if(viewModel.type === 'page'){
            return blueriq.templates.page.page;
        }

        // tables
        if (viewModel.type === 'table') {
            return blueriq.templates.container.table.table;
        } else if (viewModel.type === 'tablesearch') {
            return blueriq.templates.container.table.tableSearch;
        } else if (viewModel.type === 'tablesortedheader') {
            return blueriq.templates.container.table.tableSortedHeader;
        } else if (viewModel.type === 'tablenavigation') {
            return blueriq.templates.container.table.tableNavigation;
        }

        //visualisation
        if (viewModel.type === 'visualization') {
            if(viewModel.showLegend){
                return blueriq.templates.statistic.visualizationWithLegend;
            }else {
                return blueriq.templates.statistic.visualization;
            }
        }else if(viewModel.contentStyle === 'statistic'){
            return blueriq.templates.statistic.statistic;
        }

        //containers
        if (viewModel.type === 'container') {
            if (viewModel.contentStyle.lastIndexOf('table', 0) === 0) {
                if (viewModel.model.contentStyle === 'tablerow') {
                    return blueriq.templates.container.table.tableRow;
                } else if (viewModel.model.contentStyle === 'tablecell') {
                    if (viewModel.displayName()) {
                        return blueriq.templates.container.table.tableTextCell;
                    } else {
                        return blueriq.templates.container.table.tableCell;
                    }
                }else if(viewModel.model.contentStyle === 'tableheader'){
                    return blueriq.templates.container.table.tableHeader;
                }
                if(console) {
                    console.warn('No template found for table-part with contenstyle: "'+viewModel.model.contentStyle+'"', viewModel);
                }
            } else if (viewModel.contentStyle === 'externalcontent') {
                return blueriq.templates.container.externalContent;
            } else
            if (viewModel.contentStyle === 'breadcrumbcontainer') {
                return blueriq.templates.container.breadcrumb;
            } else if (viewModel.contentStyle === 'timeline') {
                return blueriq.templates.container.timeline;
            } else if (viewModel.contentStyle === 'menubar') {
                return blueriq.templates.container.menubar;
            } else if (viewModel.contentStyle === 'instance_linker') {
                return blueriq.templates.container.instanceLinker;
            } else if (viewModel.contentStyle === 'fileupload') {
                return blueriq.templates.container.fileUpload;
            } else if (viewModel.contentStyle === 'filedownload') {
                return blueriq.templates.container.fileDownload;
            } else if (viewModel.contentStyle === 'tabs') {
                return blueriq.templates.container.tabs;
            } else if (viewModel.contentStyle === 'formfooter') {
                return blueriq.templates.container.formFooter;
            } else if (viewModel.contentStyle === 'chartEasyPie'){
                return blueriq.templates.container.easyPieChart;
            }else if (viewModel.presentationStyles().inlinegroup){
                return blueriq.templates.container.inlineGroup;
            }
            return blueriq.templates.container.container;
        }

        if (viewModel.type === 'link') {
        	return blueriq.templates.link.link;
        }

        if (viewModel.type === 'textitem') {
            if (viewModel.presentationStyles().authenticated_user) {
                return blueriq.templates.textItem.authenticatedUser;
            } else if (viewModel.presentationStyles().logout_link) {
                return blueriq.templates.textItem.logout;
            } else if (viewModel.presentationStyles().styled) {
                return blueriq.templates.textItem.styled;
            } else if(viewModel.presentationStyles().text_label){
                return blueriq.templates.textItem.label;
            } else if(viewModel.presentationStyles().text_success){
                return blueriq.templates.textItem.succes;
            } else if(viewModel.presentationStyles().text_danger){
                return blueriq.templates.textItem.danger;
            } else if(viewModel.presentationStyles().text_warn){
                return blueriq.templates.textItem.warning;
            } else if(viewModel.presentationStyles().text_info){
                return blueriq.templates.textItem.info;
            }

            return blueriq.templates.textItem.textitem;

            //todo: implement missing templates
            //plaintext?
            //node?
        }

        if (viewModel.type === 'instancelist') {
            return blueriq.templates.container.instanceList;
        }

        if (viewModel.type === 'filetype') {
            return blueriq.templates.field.filetypeField;
        }

        if (viewModel.type === 'field') {
            var formHorizontal = true;

            if (jQuery.inArray('fieldValueOnly', context.modus) > -1) {
                if (viewModel.readonly()) {
                    return blueriq.templates.field.readonlyValue;
                } else {
                    return blueriq.templates.field.value;
                }
            } else if (viewModel.readonly()) {
                if(formHorizontal){
                    return blueriq.templates.field.readonlyHorizontal;
                }else{
                    return blueriq.templates.field.readonly;
                }
            }

            if(formHorizontal) {
                return blueriq.templates.field.fieldHorizontal;
            }else {
                return blueriq.templates.field.field;
            }

        }

        if (viewModel.type === 'button') {
            /* jscs:disable requireCamelCaseOrUpperCaseIdentifiers */
            if (viewModel.presentationStyles().isIcon) {
                if (viewModel.presentationStyles().only_icon) {
                    return blueriq.templates.button.iconOnlyButton
                } else {
                    return blueriq.templates.button.iconButton;
                }
            } else {
                if (viewModel.presentationStyles().button_link) {
                    return blueriq.templates.button.buttonLink;
                } else if (viewModel.presentationStyles().button_primary) {
                    return blueriq.templates.button.buttonPrimary
                }
            }
            return blueriq.templates.button.button;
            /* jscs:enable requireCamelCaseOrUpperCaseIdentifiers */
        }

        if(viewModel.type === 'asset'){
            return blueriq.templates.asset.asset;
        }

        if(viewModel.type === 'contentitem'){
            return blueriq.templates.contentItem.contentItem;
        }

        if(viewModel.type === 'failedelement'){
            return blueriq.templates.failedElement.failedElement;
        }

        if(viewModel.type === 'image'){
            return blueriq.templates.image.image;
        }

        if(viewModel.type === 'unknown'){
            return blueriq.templates.unknown.unknown;
        }

        if(console){
            console.error('No template found for viewModel: ', viewModel)
            return blueriq.templates.error.noTemplate;
        }

    };

    /**
     * Register default templates for field values
     */
    blueriq.defaultFieldValueFactory = function (viewModel) { //,context
        /* jscs:disable requireCamelCaseOrUpperCaseIdentifiers */


        if (viewModel.multiValued) {
            if (viewModel.presentationStyles().options_horizontal) {
                return blueriq.templates.field.checkboxHorizontal;
            } else if (viewModel.presentationStyles().options_vertical) {
                return blueriq.templates.field.checkboxVertical;
            }
        } else {
            if (viewModel.presentationStyles().toggle && viewModel.domain() && viewModel.domain().length === 2) {
                return blueriq.templates.field.toggle;
            } else if (viewModel.presentationStyles().options_vertical) {
                return blueriq.templates.field.radioVertical;
            } else if (viewModel.presentationStyles().options_horizontal) {
                return blueriq.templates.field.radioHorizontal;
            }
        }

        if (viewModel.hasDomain) {
            if (viewModel.multiValued) {
                return blueriq.templates.field.domainMulti;
            }
            return blueriq.templates.field.domainSingle;
        }

        if (viewModel.dataType === 'text') {
            if (viewModel.presentationStyles().memo) {
                return blueriq.templates.field.memo
            } else if (viewModel.presentationStyles().password) {
                return blueriq.templates.field.password
            }
        }
        if(viewModel.dataType === 'text'){
            return blueriq.templates.field.text;
        } else if(viewModel.dataType === 'date'){
            return blueriq.templates.field.date;
        }else if(viewModel.dataType === 'datetime'){
            return blueriq.templates.field.dateTime;
        } else if(viewModel.dataType === 'integer'){
            return blueriq.templates.field.integer;
        }else if(viewModel.dataType === 'currency'){
            return blueriq.templates.field.currency;
        }else if(viewModel.dataType === 'number'){
            return blueriq.templates.field.number;
        }else if(viewModel.dataType === 'boolean'){
            return blueriq.templates.field.booleanfield;
        }else if(viewModel.dataType === 'percentage'){
            return blueriq.templates.field.percentage;
        }

        throw 'No template defined for field with dataType: '+viewModel.dataType;

        /* jscs:enable requireCamelCaseOrUpperCaseIdentifiers */
    };

    /**
     * Register automatically if bqApp is already created
     */
    if (bqApp) {
        bqApp.templateFactory.registerModelHandler(blueriq.defaultTemplateFactory);
        bqApp.templateFactory.registerFieldValueHandler(blueriq.defaultFieldValueFactory);
    }
}(window.blueriq, window.bqApp));
