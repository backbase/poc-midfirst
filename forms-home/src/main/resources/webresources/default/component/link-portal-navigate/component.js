(function() {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'link-portal-navigate',
            condition: function(el) {
                return [
                    el.isButton,
                    el.styles.indexOf('link_portal_navigate') > -1
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();
