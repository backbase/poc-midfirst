(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'field-custom-layout',
            condition: function(el) {
                return [
                    el.isField,
                    el.styles.indexOf("custom_layout") > -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();