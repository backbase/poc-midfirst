/* global document */
/**
 * @name Cookies
 * @type {object}
 * @description
 * Cookie utils
 */

const getCookies = () => {
  const cookies = {};
  for (const cookie of document.cookie.split('; ')) {
    const [name, value] = cookie.split('=');
    cookies[name] = decodeURIComponent(value);
  }

  return cookies;
};

export default {
  getCookies,
};
