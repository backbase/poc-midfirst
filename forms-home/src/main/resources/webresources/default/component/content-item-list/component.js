(function () {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'content-item-list',
            condition: function (el) {
                return [
                    el.type === 'contentitem',
                    el.contentStyle === 'List'
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();