/**
 * Created by nazario on 26/05/2017.
 */
/**
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : content.js
 *  Description:
 *
 *  Source code for CXP Universal Content Widget
 *  ----------------------------------------------------------------
 */

b$ = b$ || {};
b$.universal = b$.universal || {};
b$.universal.widgets = b$.universal.widgets || {};
b$.universal.widgets.contents = b$.universal.contents || {};
b$.universal.widgets.contents.bgContent = (function ($, bd) {
    'use strict';

    var widget;
    var targetSelector;
    var isActive;
    var imgContainer;


    return {
        /**
         * Initialize content widget
         * @param  {Object} oWidget Backbase widget object
         */

        init: function (oWidget, target) {
            imgContainer = oWidget.getPreference('bgImageContainer');
            if (!isActive) {
                widget = oWidget;
                targetSelector = target;
                if (be.ice && bd && bd.designMode === 'true') {
                    oWidget.model.removeEventListener('PrefModified', waitForChanges);
                    oWidget.model.addEventListener('PrefModified', waitForChanges, false, oWidget);
                }
            }
            if (imgContainer === 'inline') {
                dettachBG();
            } else {
                attachBG();
            }


        }
    };

    function waitForChanges(oEvent) {
        imgContainer = widget.getPreference('bgImageContainer');
        if (imgContainer && imgContainer === "inline") {
            dettachBG();
        } else if (imgContainer) {
            attachBG();
        }
    }

    function dettachBG() {
        $(widget.body).css({
            'background-image': 'none'
        });
    }

    function attachBG() {
        var target = widget.body,
            bg = target.querySelector("img"),
            bgSize = widget.getPreference('bgImageSize'),
            bgImageScroll = widget.getPreference('bgImageScroll'),
            bgCentering = widget.getPreference('bgCentering'),
		    bgCenteringMobile = widget.getPreference('bgCenteringMobile');

        if (imgContainer === 'fullscreen') {
            target = $('#main');
            target[0].classList.add('fullscreen-bg');
        }

        if (bg) {
            var url = bg.getAttribute("src");
            target = targetSelector ? targetSelector : target;
            if (url) setBG(target, url, bgSize, bgImageScroll, bgCentering,bgCenteringMobile);
        }

    }

    function setBG(pTarget, pURL, pBgSize, pImageScroll, pCentering,pCenteringMobile) {

        if (pTarget.length){
            pTarget = pTarget[0];
        }
        if (pTarget) {

            if (!pTarget.classList.contains('background-set')) {
                pTarget.classList.add("background-set");
            }

            var bgURL = 'url(' + pURL + ')';
            pTarget.style.setProperty('--background-image-url', bgURL);
            pTarget.style.setProperty('--background-image-size', pBgSize);
            pTarget.style.setProperty('--background-image-attachment', pImageScroll);
            pTarget.style.setProperty('--background-image-position', pCentering);
			pTarget.style.setProperty('--background-image-position--mobile', pCenteringMobile);
        }

    }

}(window.jQuery, window.bd));
