/**
 * This directive is designed to render Google Recaptcha
 * We use sitekey and secretkey for domain backbasecloud.com
 * Keys lay in the backbase.properties file
 */

(function() {
    angular.module('forms-ui').directive('fieldLicenseplate',  /*@ngInject*/ function () {
        return {
            restrict: 'A',
            require: {
                form: '^^bbForm'
            },
            link: function ($scope, element, attrs,formsController) {
                var context = (element.context)?element.context:element[0];
                $scope.$watch('element.description',function (pNuValue,pOldValue) {
                    $scope.splitDescription = $scope.element.description.split(",");
                    $scope.descriptions = [];
                    $scope.splitDescription.forEach(function (pItem) {
                        var split = pItem.split(";");
                        $scope.descriptions.push({brand:split[0],value:split[1]});
                    });
                    $scope.$evalAsync();
                });
            }
        };
    });

})();