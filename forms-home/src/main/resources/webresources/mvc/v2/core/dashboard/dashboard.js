(function (blueriq) {
    'use strict';

    if (!blueriq) {
        blueriq = {};
    }

    window.blueriq = blueriq;

    if (!blueriq.models) {
        blueriq.models = {};
    }

    if (!blueriq.services) {
        blueriq.services = {};
    }

    blueriq.models.dashboard = {};
    blueriq.services.dashboard = {};

    /**
     * Service for handling widget related actions on the server.
     * @constructor
     * @name blueriq.services.dashboard.WidgetService
     * @param {string} baseUri - Base URL of the server API.
     * @returns {@link blueriq.services.dashboard.WidgetService} widgetservice to handle widget-actions
     */
    blueriq.services.dashboard.WidgetService = function WidgetService(baseUri) {
        if (!baseUri) {
            throw new Error('baseUri is mandatory');
        }

        /**
         * Creates a subsession for the specified session id, that gets the specified token as identifier.
         * @function
         * @name blueriq.services.dashboard.WidgetService#createSession
         * @param {string} sessionId - The id of the session to create a subsession on.
         * @param {string} csrfToken - The CSRF token for the main session.
         * @param {string} token - The identifier for the subsession to be created.
         * @param {function} callback - Callback method which will be called after successfully creating the session an returning the session
         */
        this.createSession = function (sessionId, csrfToken, token, callback) {
            if (!sessionId) {
                throw new Error('sessionId is mandatory');
            }
            if (!csrfToken) {
            	throw new Error('csrfToken is mandatory');
            }
            if (!token) {
                throw new Error('token is mandatory');
            }
            $.ajax({
                type: 'POST',
                url: baseUri + sessionId + '/api/widget/' + token,
                headers: {
                	'X-CSRF-Token': csrfToken
                },
                success: function (data) {
                    if (callback) {
                        callback(data.sessionId);
                    }
                },
                contentType: 'application/json'
            });
        };
    };

}(window.blueriq));