/* global b$, gadgets */
(function () {
	'use strict';

	const DeckContainer = b$.bdom.getNamespace('launchpad').classes.DeckContainer;

	const TRANSACTIONS_PANEL_KEY = 'panel-transactions';
	const TRANSACTIONS_PANEL_URL = 'transactions';
	const MY_PRODUCTS_URL = 'my-products';

	var self = null;
	var lastKey = null;
	var isInProducts = false;

	//////
	// Map
	//////

	var firstPanelSelect = true;

	const getMap = function () {
		return JSON.parse(self.getPreference('map') || '{}');
	};

	const saveMap = function (map) {
		self.setPreference('map', JSON.stringify(map));
		self.model.save();
	};

	const hideElements = function (listOfIds) {
		listOfIds.forEach(function (id) {
			const panel = document.querySelector('#' + id);
			if (panel) {
				panel.classList.add('hidden');
				panel.classList.remove('active');
			}
		});
	};

	const showPanelByKey = function (key) {
		var inDesignMode = !!bd.designMode;
		const map = getMap();
		const listOfIds = Object.values(map);
		const id = map[key];

		if (lastKey !== key) {
			hideElements(listOfIds);
			if (inDesignMode) {
				const panel = document.querySelector('#' + id);
				if (panel) {
					panel.classList.remove('hidden');
					panel.classList.add('active');
				}
				gadgets.pubsub.publish('panel-selected', key);
			} else {
				const panelInstance = self.getPanel(id);
				panelInstance.refreshHTML(function (pItem) {

					const panel = document.querySelector('#' + id);
					if (panel) {
						panel.classList.remove('hidden');
						panel.classList.add('active');
					}

					var insideContainers = panel.querySelectorAll('[data-pid^="container-panel"]');
					insideContainers = [].slice.call(insideContainers);
					//--
					insideContainers.forEach(function (pItem) {
						pItem.viewController.refreshHTML();
					});
					//--
					gadgets.pubsub.publish('panel-selected', key);
				}, function (pItem) {

				});
			}
			lastKey = key;
		}

	};

	gadgets.pubsub.subscribe('panel-select', function (args) {
		//// --
		const key = args.key;
		const url = args.url;
		//// --
		isInProducts = (url === MY_PRODUCTS_URL) ;
		//// --
		const map = getMap();
		if (!map[key]) {
			map[key] = self.addPanel({}, function () {
				saveMap(map);
				gadgets.pubsub.publish('panel-select', args);
			});
		}
		else {
			showPanelByKey(key);
			const id = map[key];
			if (firstPanelSelect) {
				window.history.replaceState({id: id, key: key}, '', '#/' + url);
				firstPanelSelect = false;
			}
			else {
				window.history.pushState({id: id, key: key}, '', '#/' + url);
			}
		}
	});

	gadgets.pubsub.subscribe('panel-cleanup', function (uuids) {
		const map = getMap();
		const keys = Object.keys(map);
		keys.forEach(function (key) {
			const isLink = uuids.indexOf(key) > -1;
			const isTransaction = key === TRANSACTIONS_PANEL_KEY;
			if (!isLink && !isTransaction) {
				self.removePanel(map[key]);
				delete map[key];
				saveMap(map);
			}
		});
	});
	gadgets.pubsub.subscribe('bb.event.product.selected', function (pParam1, pParam2) {
		if (isInProducts) {
			gadgets.pubsub.publish(
				'panel-select',
				{key: TRANSACTIONS_PANEL_KEY, url: TRANSACTIONS_PANEL_URL}
			);
		}
	});

	////////////
	// Dom Ready
	////////////

	const domReady = function () {
		DeckContainer.prototype.DOMReady.call(this, arguments);
		const map = getMap();
		saveMap(map);
		const panels = Object.values(map);
		hideElements(panels);
	};

	/////////
	// Extend
	/////////

	DeckContainer.extend(
		function () {
			DeckContainer.apply(this, arguments);
			this.isPossibleDragTarget = true;
			self = this;
		},
		{
			localName: 'widgetShcBankingPanelsTemplate',
			namespaceURI: 'launchpad',
			DOMReady: domReady
		},
		{
			template: function (json) {
				var data = {item: json.model.originalItem};
				return window[this.namespaceURI][this.localName](data);
			},
			handlers: {
				preferencesSaved: function (event) {
					if (event.target === this) {
						this.refreshHTML(function () {
							//add code, HTML refreshed
						});
					}
				}
			}
		}
	);

	window.addEventListener('popstate', function (event) {
		const state = event.state;
		if (state) showPanelByKey(state.key);
	});
})();
