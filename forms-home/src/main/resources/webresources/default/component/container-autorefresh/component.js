(function () {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'container-autorefresh',
            condition: function(el) {
                return [el.isContainer,el.contentStyle === 'AutoRefresh'];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();