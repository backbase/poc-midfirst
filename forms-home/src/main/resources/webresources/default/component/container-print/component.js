(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'container-print',
            condition: function(el) {
                return [
                    el.isContainer,
                    el.styles.indexOf("print_button")>-1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();