(function() {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'car-damage-selector',
            condition: function(el) {
                return [
                    el.hasOptions,
                    el.multiple,
                    el.styles.indexOf("car_damage_selector") > -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();
