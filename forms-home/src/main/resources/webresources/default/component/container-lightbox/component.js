(function () {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'container-lightbox',
            condition: function(el) {
                return [el.isContainer,el.contentStyle === "Lightbox"];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();