(function () {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'container-video-bg',
            condition: function(el) {
                return [el.isContainer, el.contentStyle === 'video_background'];

            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();