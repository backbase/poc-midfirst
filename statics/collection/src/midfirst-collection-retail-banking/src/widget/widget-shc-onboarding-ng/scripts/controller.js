/**
 * @module widget-shc-onboarding-ng
 * @name OnboardingController
 *
 * @description
 * Onboarding
 */

export default function OnboardingController(bus, hooks, widget) {
    const $ctrl = this;

    let inputFilledName = "";
    let inputFilledSSN = "";
    let inputFilledEmail = "";

    let focussedQuestion = 1;
    let questions = [
        {
            id: 1,
            headerText: "Let's get down to business!",
            question: "What 's your name?",
            finished: false,
            focussed: true,
            inputType: "NAME",
        },
        {
            id: 2,
            headerText: "Nice to meet you {name}",
            question: "What's your Social Security Number (SSN)?",
            finished: false,
            focussed: false,
            inputType: "SSN",
        },
        {
            id: 3,
            headerText: "Cool, let's create a username for you",
            question: "What's your email address?",
            finished: false,
            focussed: false,
            inputType: "EMAIL",
        },
        {
            id: 4,
            headerText: "Almost there. To complete the application we need to verify your identity. We will need a copy of your State ID, drivers license or passport.",
            question: "How would you like to provide your ID card?",
            finished: false,
            focussed: false,
            inputType: "CONTINUE",
        }
    ];

    const continueOnPhone = () => {
        console.log('continue on phone');
        this.continueOnPhonePopup = true;
    }

    let continueOnPhonePopup = false;

    const focusQuestion = (questionId) => {
        focussedQuestion = questionId++;
        focussedQuestion = questionId;

        Object.values(questions).forEach(question => {
            if (question.id < questionId) {
                question.finished = true;
                question.focussed = false;
            } else if (question.id === questionId) {
                question.finished = false;
                question.focussed = true;
            } else {
                question.finished = false;
                question.focussed = false;
            }
        })
    }

    const keyUp = (keyCode) => {
        if (keyCode === 13) { // tab wont work, only use enter
            focusQuestion(focussedQuestion);
        }
    }

    const $onInit = () => {
        $ctrl.items = hooks.itemsFromModel([]);

        bus.publish('cxp.item.loaded', {
            id: widget.getId(),
        });

        bus.publish('bb.item.loaded', {
            id: widget.getId(),
        });
    };

    Object.assign($ctrl, {
        $onInit,
        focusQuestion,
        questions,
        inputFilledName,
        inputFilledSSN,
        inputFilledEmail,
        keyUp,
        continueOnPhone,
        continueOnPhonePopup,
    });
}
