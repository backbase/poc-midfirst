# Showcase Project

Make sure to follow the next steps in order to setup this project locally (make sure you are on VPN, or at the Backbase office)



#### Pre-Requisites Project

##### BBCLI  
```
This project is being tested using the BBCLI version 0.3.0
https://github.com/Backbase/bb-cli
npm install --global bb-cli@0.3.0
```

##### Bower Resolver  
```
Install the following globally
npm install --global filesystem-bower-resolver
npm install --global bb-bower-resolver
```

#### Clone Project

```
git clone ssh://git@stash.backbase.com:7999/shc/shc-project-2.0.git
```

#### Go into directory
```
cd shc-project-2.0
```

#### Build the project and install collections & portals
```
mvn clean install -Pclean-database
```

#### Run the portal & enjoy
```
sh run.sh -df
```

#### Run the portal & enjoy
```
cd statics/collection
```

#### Import Dashboard
```
npm run import-dashboard
```

#### Bower install
```
npm run bower-install
```

#### Import collection
```
npm run import-collection
```

#### Import Portals
```
npm run install-portals
```