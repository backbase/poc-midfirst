(function() {

    /**
     * @ngInject
     */
    function registerElement(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'file-upload',
            condition: function(el) {
                return [
                    el.contentStyle === 'fileupload-old',
                    el.isContainer
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerElement);

})();
