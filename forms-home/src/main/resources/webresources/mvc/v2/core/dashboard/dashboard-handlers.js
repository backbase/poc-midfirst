if (typeof window.blueriq === 'undefined') {
    window.blueriq = {};
}
(function (ko, blueriq) {
    'use strict';

    /**
     * Creates a widgetBox to place widgets in
     * @constructor
     * @return {@link blueriq.models.dashboard.widgetBox} Widgetbox
     */
    ko.bindingHandlers.widgetBox = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var box = $(element).next();
            // If next element is a box and hasn't .non-collapsible class
            if (box.hasClass('box') && !box.hasClass('non-collapsible')) {
                $(element).append('<a href="#" class="box-collapse pull-right" onclick="var self = $(this).hide(100, \'linear\'); self.parent(\'.box-header\').next(\'.box\').slideUp(400, function() { $(\'.box-expand\', self.parent(\'.box-header\')).show(100, \'linear\'); });">hide&nbsp;&nbsp;<i class="icon-caret-up"></i></a>')
                    .append('<a href="#" class="box-expand pull-right" style="display: none" onclick="var self = $(this).hide(100, \'linear\'); self.parent(\'.box-header\').next(\'.box\').slideDown(400, function() { $(\'.box-collapse\', self.parent(\'.box-header\')).show(100, \'linear\'); });">show&nbsp;&nbsp;<i class="icon-caret-down"></i></a>');
            }
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var innerBox = $(element).find('.box');

        }
    };
    /**
     * Creates a bleuriq menubar
     * @constructor
     * @return {@link ko.bindingHandlers.bqMenubar} blueriq menubar
     */
    ko.bindingHandlers.bqMenubar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

            // Add click handler for closing wrappers
            $('html').mousedown(function () {
                $('.menubar-submenu-wrapper.open').removeClass('open');
                $('.menubar-submenu-toggle.open').removeClass('open');
            }).find('.menubar-submenu-toggle').click(function (e) {
                return false;
            });

            $('.menubar-submenu-wrapper').mousedown(function () {
                return false;
            });

            // Add click handler for opening submenu's
            $('.menubar-submenu-toggle').click(
                function (event) {
                    $('.menubar-submenu-wrapper.open').removeClass('open');
                    $('.menubar-submenu-toggle.open').removeClass('open');

                    var id = $(this).parent().attr('id');
                    $(this).addClass('open');

                    var wrapper = $('.menubar-submenu-wrapper[data-dropdown-owner=' + id + ']');
                    wrapper.addClass('open');
                    wrapper.width(wrapper.find('li').length * 82);

                    // Position submenu wrapper
                    if (!wrapper.parent().is('.dashboard-menu')) {
                        wrapper.css({top: $(this).parent().offset().top, left: $('.dashboard-menu').width() + 10});
                        wrapper.appendTo('.dashboard-menu');
                    }
                }
            );
        }
    };

    ko.virtualElements.allowedBindings.bqMenubar = true;


    ko.bindingHandlers.bqDashboardMenu = {
        init: function (element) {
            var container = $(element).parent();
            $('.menubar-horizontal-active').unbind().click(function (event) {
                if (container.hasClass('open')) {
                    container.css({
                        height: 37
                    });
                } else {
                    container.css({
                        height: $('.menubar').innerHeight() + 37
                    });
                }
                container.toggleClass('open');
                return false;
            }).text(
                $(($('.active', container).length === 1) ? '.active' : '.active .active', container).text()
            );
        }
    };

    ko.virtualElements.allowedBindings.bqDashboardMenu = true;


})(window.ko, window.blueriq);