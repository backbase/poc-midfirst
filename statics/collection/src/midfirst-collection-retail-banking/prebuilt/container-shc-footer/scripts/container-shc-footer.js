/* global b$ */
(function () {
    'use strict';

    var Container = b$.bdom.getNamespace('http://backbase.com/2013/portalView').getClass('container');
 
    Container.extend(function() {
        Container.apply(this, arguments);
        this.isPossibleDragTarget = true;
    }, {
        localName: 'templateShcFooterContainer',
        namespaceURI: 'templates_templateShcFooterContainer'
    }, {
        template: function(json) {
            var data = {item: json.model.originalItem};
            return window[this.namespaceURI][this.localName](data);
        },
        handlers: {
            DOMReady: function(){
                //add code, DOM ready
            },
            preferencesSaved: function(event){
                if(event.target === this) {
                    this.refreshHTML(function(){
                        //add code, HTML refreshed
                    });
                }
            }
        }
    });
})();
