(function() {
    /**
     * @ngInject
     */
    function registerControl(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'stepped-slider',
            controller: 'SteppedSliderController as controller',

            condition: function (el) {
                return [
                    el.styles.indexOf('stepped_slider') > -1,
                    el.hasOptions,
                    !el.multiple
                ];
            }

        });
    }
    angular.module('forms-ui').run(registerControl);
})();
