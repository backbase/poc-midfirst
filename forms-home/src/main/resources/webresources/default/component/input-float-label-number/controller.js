angular.module('forms-ui').directive('stringToNumber', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            var context = (element.context)?element.context:element[0];
            context.addEventListener("focus",onFocus);
            context.addEventListener("blur",onBlur);
            function onBlur(pEvent) {
                //console.log("inputBlur:",pEvent);
                $scope.$emit("current-input-blur",{targetModel:$scope.element,target:context});
                //context.removeEventListener("focus",onFocus);
            }

            function onFocus(pEvent) {
                //console.log("onFocus:",pEvent);
                $scope.$emit("current-input-focus",{targetModel:$scope.element,target:context});
            }

            ngModel.$parsers.push(function(value) {
                return  ~~(value);
            });
            ngModel.$formatters.push(function(value) {
                return parseFloat(value, 10);
            });
        }
    };
});