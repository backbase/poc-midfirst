(function() {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'textarea',
            condition: function(el) {
                return [
                    el.dataType === 'text',
                    !el.hasDomain,
                    el.styles.indexOf('memo') > -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();
