package com.backbase.forms.services.proxy;

import java.io.InputStream;

import org.apache.http.client.methods.HttpUriRequest;

/**
 * Factory for Http Requests.
 * 
 * @author Walter Werner
 *
 */
public interface HttpRequestFactory {

	/**
	 * Creates a request.
	 * 
	 * @param path
	 *            the path to the resource. The server base URL should not be
	 *            passed
	 * @param method
	 *            the HTTP method
	 * @param contentType
	 *            the Content Type
	 * @param contentLength
	 *            the Content Length
	 * @param body
	 *            the Input Stream containing the body of the request
	 * @return the request created.
	 */
	HttpUriRequest createRequest(String path, String method,
			String contentType, int contentLength, InputStream body);

}