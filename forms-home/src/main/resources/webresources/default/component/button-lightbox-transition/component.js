(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'button-lightbox-transition',
            condition: function(el) {
                return [
                    el.isButton,
                    el.styles.indexOf('LightBox_Transition') > -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();