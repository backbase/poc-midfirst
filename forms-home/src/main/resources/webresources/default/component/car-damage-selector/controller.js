(function () {

    /**
     * Controller for checkbox-group components
     * Adds a mapper between options and values to local scope
     * @ngInject
     */

    angular.module('forms-ui').directive('carDamageSelector', /*@ngInject*/ function () {
        return {
            restrict: 'A',
            //  ---
            link: function ($scope, element, attrs, $filter) {
                var context = (element.context) ? element.context : element[0];

                //--
                var ctrl = $scope;
                //--
                ctrl.valueMap = {};
                $scope.element.options.forEach(mapValues);
                //  ---
                ctrl.updateValues = function () {
                    var options = $scope.element.options;
                    var index = options.length;
                    var values = [];

                    while (index--) {
                        if (ctrl.valueMap[index]) {
                            values.unshift(options[index].value);
                        }
                    }

                    if (values && !values.length) values = null;
                    $scope.element.value = values;
                };

                ctrl.mapName = function (pValue) {
                    var response = "";
                    $scope.element.options.forEach(function (pItem) {
                        if (pItem.value === pValue) {
                            response = pItem.displayValue;
                        }
                    });
                    return response;
                };

                function mapValues(option, index) {
                    if ($scope.element.value.indexOf(option.value) > -1) {
                        ctrl.valueMap[index] = true;
                    }
                }

            }
        };
    });
})();
