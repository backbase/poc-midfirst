(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'scenario-selector',
            condition: function(el) {
                return [
                    el.isContainer,
                    el.styles.indexOf('scenario_selector') > -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();