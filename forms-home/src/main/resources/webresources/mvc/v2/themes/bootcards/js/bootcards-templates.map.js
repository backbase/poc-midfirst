/**
 * Created by f.van.rijswijk on 7-9-2015.
 */

(function(blueriq){
    var templateRoot = 'themes/bootcards/template/';

    //blueriq.templates.page.page = templateRoot+'page/bootcardspage';
    blueriq.templates.page.dashboardPage = templateRoot+'page/bootcardspage';

    blueriq.templates.container.dashboard.dashboardBody = templateRoot+'container/bootcards-body';
    blueriq.templates.container.dashboard.dashboardMenu = templateRoot+'container/bootcards-menu';

    blueriq.templates.container.formFooter = templateRoot+'container/formfooter';
    blueriq.templates.container.buttonList = templateRoot+'container/buttonlist';

    blueriq.templates.flowWidget.flowWidget = templateRoot+'flowwidget/flowwidget';

	blueriq.templates.textItem.logout = templateRoot+'textitem/logout';
	blueriq.templates.textItem.authenticatedUser = templateRoot+'textitem/authenticateduser';

    //empty template for pagination search
    blueriq.templates.container.table.tableSearch  = blueriq.templates.unknown.unknown;

    //field types
    blueriq.templates.field.field = templateRoot+'field/field';
    blueriq.templates.field.fieldHorizontal = templateRoot+'field/fieldhorizontal';
    blueriq.templates.field.date = templateRoot+'field/date';
    blueriq.templates.field.dateTime = templateRoot+'field/datetime';
    blueriq.templates.field.percentage = templateRoot+'field/percentage';
    blueriq.templates.field.currency = templateRoot+'field/currency';
    blueriq.templates.field.domainSingle = templateRoot+'field/domainsingle';

    //tables as a list
    blueriq.templates.container.list = {
            container: templateRoot+"container/list/listcontainer",
            item: templateRoot+"container/list/listitem"
        };

})(window.blueriq);
