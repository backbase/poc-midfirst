import widget from './index';

describe('Login widget', () => {
  it('should export the module name', () => {
    expect(widget).toEqual('widget-shc-login-ng');
  });
});
