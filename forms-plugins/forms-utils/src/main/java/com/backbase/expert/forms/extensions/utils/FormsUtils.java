package com.backbase.expert.forms.extensions.utils;

import com.aquima.interactions.composer.model.TextElement;
import com.aquima.interactions.composer.model.TextItem;
import com.aquima.interactions.composer.model.TextNode;
import com.aquima.interactions.composer.model.TextValueNode;
import com.aquima.interactions.foundation.DataType;
import com.aquima.interactions.foundation.IPrimitiveValue;
import com.aquima.interactions.foundation.ISingleValue;
import com.aquima.interactions.foundation.IValue;
import com.aquima.interactions.foundation.types.DateValue;
import com.aquima.interactions.foundation.types.EntityValue;
import com.aquima.interactions.foundation.types.ListValue;
import com.aquima.interactions.portal.IContainerContext;
import com.aquima.interactions.portal.IServiceContext;
import com.aquima.interactions.profile.IEntityInstance;
import com.aquima.interactions.rule.IExpression;
import com.aquima.interactions.rule.IExpressionParser;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public final class FormsUtils {

    private static final Logger LOG = LoggerFactory.getLogger(FormsUtils.class);

    private static final String DEFAULT_LANGUAGE_CODE = "en-US";

    private FormsUtils() {
        // Empty - Singleton
    }

    public static IValue getExpressionAttrByParameter(IServiceContext context, String paramName) {

        String valueAttribute = context.getParameter(paramName);
        if (StringUtils.isBlank(valueAttribute)) {
            return null;
        }
        LOG.debug("validCodes: {}", valueAttribute.toString());

        IExpressionParser expressionParser = context.getExpressionParser();
        LOG.debug("expressionParser: {}", expressionParser.toString());

        IExpression expression = expressionParser.expressionFor(valueAttribute);
        LOG.debug("expression: {}", expression.toString());

        IValue iValue = expression.evaluateWith(context.getProfile());

        return iValue;
    }

    public static IValue evaluateContainerContextParameterExpression(IContainerContext containerContext,
                                                                     String parameter) {
        if (containerContext == null || StringUtils.isBlank(parameter)) {
            return null;
        }
        String parameterValue = containerContext.getParameter(parameter);
        if (StringUtils.isBlank(parameterValue)) {
            return null;
        }
        IExpressionParser expressionParser = containerContext.getExpressionParser();
        IExpression expression = expressionParser.expressionFor(parameterValue);

        return expression.evaluateWith(containerContext.getProfile());
    }

    public static String evaluateContainerContextParameterExpressionToString(IContainerContext containerContext,
                                                                             String parameter) {
        IValue parameterValue = evaluateContainerContextParameterExpression(containerContext, parameter);
        if (parameterValue == null || parameterValue.isUnknown()) {
            return null;
        }
        return parameterValue.stringValue();
    }

    public static Date evaluateContainerContextParameterExpressionToDate(IContainerContext containerContext,
                                                                             String parameter) {
        IValue parameterValue = evaluateContainerContextParameterExpression(containerContext, parameter);
        if (parameterValue == null || parameterValue.isUnknown()) {
            return null;
        }
        return parameterValue.dateValue();
    }

    /**
     * Extract a single entity instance from the context using a (dot seperated)
     * instance name.
     *
     * @param context
     * @param instanceName
     * @return
     */
    public static IEntityInstance extractEntityInstance(IServiceContext context,
                                                        String instanceName) {
        LOG.debug("extractEntityInstance");

        LOG.debug("context: {}", context.toString());
        LOG.debug("instanceName: {}", instanceName);

        IExpressionParser expressionParser = context.getExpressionParser();

        LOG.debug("expressionParser: {}", expressionParser.toString());

        IExpression expression = expressionParser.expressionFor(instanceName);

        LOG.debug("expression: {}", expression.toString());

        IValue iValue = expression.evaluateWith(context.getProfile());

        LOG.debug("iValue: {}", iValue.toString());

        EntityValue entityValue = (EntityValue) iValue.getValue();

        if (entityValue != null) {
            LOG.debug("entityValue: {}", entityValue.toString());

            IEntityInstance entityInstance = context.getProfile().getInstance(
                    entityValue);

            LOG.debug("entityInstance: {}", entityInstance.toString());

            return entityInstance;
        }

        return null;
    }

    public static IEntityInstance getEntityInstanceFromContext(
            IServiceContext serviceContext, String entityExpressionParameter) {
        if (serviceContext == null || entityExpressionParameter == null) {
            return null;
        }
        String entityExpression = serviceContext.getParameter(entityExpressionParameter);
        IEntityInstance entityInstance = null;
        if (!StringUtils.isBlank(entityExpression)) {
            entityInstance = extractEntityInstance(serviceContext, entityExpression);
        }
        return entityInstance;
    }

    /**
     * Evaluates an expression on the profile
     * instance name.
     *
     * @param context
     * @param instanceName
     * @return
     */
    public static IValue evaluateExpression(IContainerContext context,
                                            String instanceName) {
        LOG.debug("evaluateExpression");

        LOG.debug("context: {}", context.toString());
        LOG.debug("instanceName: {}", instanceName);

        IExpressionParser expressionParser = context.getExpressionParser();

        LOG.debug("expressionParser: {}", expressionParser.toString());

        IExpression expression = expressionParser.expressionFor(instanceName);

        LOG.debug("expression: {}", expression.toString());

        IValue iValue = expression.evaluateWith(context.getProfile());

        LOG.debug("iValue: {}", iValue.toString());


        return iValue;
    }

    /**
     * Extract a single entity instance from the context using a (dot seperated)
     * instance name.
     *
     * @param context
     * @param instanceName
     * @return
     */
    public static IEntityInstance extractEntityInstance(IContainerContext context,
                                                        String instanceName) {
        LOG.debug("extractEntityInstance");

        LOG.debug("context: {}", context.toString());
        LOG.debug("instanceName: {}", instanceName);

        IExpressionParser expressionParser = context.getExpressionParser();

        LOG.debug("expressionParser: {}", expressionParser.toString());

        IExpression expression = expressionParser.expressionFor(instanceName);

        LOG.debug("expression: {}", expression.toString());

        IValue iValue = expression.evaluateWith(context.getProfile());

        LOG.debug("iValue: {}", iValue.toString());

        EntityValue entityValue = (EntityValue) iValue.getValue();

        LOG.debug("entityValue: {}", entityValue.toString());

        IEntityInstance entityInstance = context.getProfile().getInstance(entityValue);

        LOG.debug("entityInstance: {}", entityInstance.toString());

        return entityInstance;
    }

    /**
     * Get attribute value of active entity instance
     *
     * @param context
     * @param entityName
     * @param attrName
     * @return valueStr
     */
    public static String getAttrDisplayValue(IServiceContext context,
                                             String entityName, String attrName, String languageCode) {
        String valueStr;
        IValue value = context.getProfile().getActiveInstance(entityName)
                .getValue(attrName);
        if (!value.isUnknown()) {
            ISingleValue singleValue = value == null ? null : value
                    .toSingleValue();
            if (singleValue != null) {
                valueStr = singleValue.getDisplayValue(StringUtils
                        .defaultString(languageCode, DEFAULT_LANGUAGE_CODE),
                        null);
            } else {
                valueStr = value == null ? null : value.stringValue();
            }
        } else {
            valueStr = null;
        }
        return valueStr;
    }

    /**
     * Get attribute value of active entity instance
     *
     * @param context
     * @param entityValue
     * @param attrName
     * @return valueStr
     */
    public static String getAttrDisplayValue(IServiceContext context,
                                             EntityValue entityValue, String attrName, String languageCode) {
        String valueStr;
        IValue value = context.getProfile().getInstance(entityValue)
                .getValue(attrName);
        if (!value.isUnknown()) {
            ISingleValue singleValue = value == null ? null : value
                    .toSingleValue();
            if (singleValue != null) {
                valueStr = singleValue.getDisplayValue(StringUtils
                        .defaultString(languageCode, DEFAULT_LANGUAGE_CODE),
                        null);
            } else {
                valueStr = value == null ? null : value.stringValue();
            }
        } else {
            valueStr = null;
        }
        return valueStr;
    }

    /**
     * Get attribute value of active entity instance
     *
     * @param context
     * @param entityName
     * @param attrName
     * @return valueStr
     */
    public static String getAttr(IServiceContext context, String entityName,
                                 String attrName) {
        String valueStr;
        IValue value = context.getProfile().getActiveInstance(entityName)
                .getValue(attrName);
        if (!value.isUnknown()) {
            valueStr = value == null ? null : value.stringValue();
        } else {
            valueStr = null;
        }
        return valueStr;
    }

    /**
     * Get attribute value of given entity instance
     *
     * @param context
     * @param entityValue
     * @param attrName
     * @return
     */
    public static String getAttr(IServiceContext context, EntityValue entityValue,
                                 String attrName) {
        IValue value = context.getProfile().getInstance(entityValue)
                .getValue(attrName);
        if (!value.isUnknown()) {
            return (value == null ? null : value.stringValue());
        }
        return null;
    }

    /**
     * Get attribute value of given entity instance
     *
     * @param entityInstance
     * @param attrName
     * @return
     */
    public static String getAttr(IEntityInstance entityInstance, String attrName) {
        if (entityInstance == null) {
            return null;
        }
        IValue value = entityInstance.getValue(attrName);
        if (value == null) {
            return null;
        }
        if (!value.isUnknown()) {
            return (value == null ? null : value.stringValue());
        }
        return null;
    }

    public static Boolean getBooleanAttr(IEntityInstance entityInstance, String attrName) {
        if (entityInstance == null) {
            return null;
        }
        IValue value = entityInstance.getValue(attrName);
        if (!value.isUnknown() && value.getDataType().equals(DataType.BOOLEAN)) {
            return (value == null ? null : value.booleanValue());
        }
        return null;
    }

    public static Integer getIntegerAttr(IEntityInstance entityInstance, String attrName) {
        if (entityInstance == null) {
            return null;
        }
        IValue value = entityInstance.getValue(attrName);
        if (value == null) {
            return null;
        }
        if (!value.isUnknown() && value.getDataType().equals(DataType.INTEGER)) {
            return (value == null ? null : value.integerValue());
        }
        return null;
    }

    public static Double getDoubleAttr(IEntityInstance entityInstance, String attrName) {
        if (entityInstance == null) {
            return null;
        }
        IValue value = entityInstance.getValue(attrName);
        if (!value.isUnknown() &&
                (value.getDataType().equals(DataType.CURRENCY) || value.getDataType().equals(DataType.NUMBER))) {
            return (value == null ? null : value.doubleValue());
        }
        return null;
    }

    public static Date getDateAttr(IEntityInstance entityInstance, String attrName) {
        if (entityInstance == null) {
            return null;
        }
        IValue value = entityInstance.getValue(attrName);
        if (value == null) {
            return null;
        }
        if (!value.isUnknown() &&
                (value.getDataType().equals(DataType.DATE) || value.getDataType().equals(DataType.DATETIME))) {
            return (value == null ? null : value.dateValue());
        }
        return null;
    }

    public static XMLGregorianCalendar getXmlCalendarAttr(IEntityInstance entityInstance, String attrName)
            throws Exception {
        Date policyDate = FormsUtils.getDateAttr(entityInstance, attrName);
        XMLGregorianCalendar xmlPolicyStart;
        try {
            xmlPolicyStart = FormsUtils.toXMLGregorianCalendar(policyDate);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw e;
        }
        return xmlPolicyStart;
    }

    /**
     * Get attribute values from ListValue of given entity instance.
     *
     * @param context
     * @param entityValue
     * @param attrName
     * @return
     */
    public static IPrimitiveValue[] getAttrMultiValue(IServiceContext context,
                                                      EntityValue entityValue, String attrName) {
        IValue multiValue = context.getProfile().getInstance(entityValue)
                .getValue(attrName);
        if (multiValue != null && !multiValue.isUnknown()
                && !multiValue.isSingleValue()) {
            return multiValue.toListValue().getValues();
        }
        return new IPrimitiveValue[] {};
    }

    /**
     * Get attribute values from ListValue of given entity instance.
     *
     * @param entityInstance
     * @param attrName
     * @return
     */
    public static IPrimitiveValue[] getAttrMultiValue(IEntityInstance entityInstance,
                                                      String attrName) {
        IValue multiValue = entityInstance.getValue(attrName);
        if (multiValue != null && !multiValue.isUnknown()
                && !multiValue.isSingleValue()) {
            return multiValue.toListValue().getValues();
        }
        return new IPrimitiveValue[] {};
    }

    /**
     * Set attribute in an entity instance in the Forms profile. Null values
     * will reset the attribute value in the profile.
     *
     * @param context
     * @param entityDotAttribute String of the form <code>Entity.Attribute</code>
     * @param value
     * @param persistNull        if false, null values are skipped
     * @param overwrite          if false, values already being persisted to the profile will
     *                           not be overwritten
     */
    public static void setAttr(IServiceContext context, String entityDotAttribute,
                               Object value, boolean persistNull, boolean overwrite) {
        String entityName = StringUtils
                .substringBefore(entityDotAttribute, ".");
        String attributeName = StringUtils.substringAfter(entityDotAttribute,
                ".");
        try {
            if (persistNull || value != null) {
                if (overwrite
                        || StringUtils.isBlank(getAttr(context, entityName,
                        attributeName))) {
                    context.getProfile().getActiveInstance(entityName)
                            .setValue(attributeName, value);
                }
            }
        } catch (Exception e) {
            LOG.error(
                    "Could not store Entity.Attribute value in Forms profile.",
                    e);
        }
    }

    /**
     * Persists a value of an attribute for the entity instance, by referring to the IServiceContext and the entity instance name.
     *
     * @param serviceContext     the IServiceContext instance
     * @param entityInstanceName the name of the entity instance
     * @param attrName           the name of the attribute
     * @param attrValue          the value of the attribute
     */
    public static void persistFormModelData(IServiceContext serviceContext, String entityInstanceName,
                                            String attrName, Object attrValue) {
        FormsUtils.persistFormModelData(FormsUtils.extractEntityInstance(serviceContext, entityInstanceName),
                attrName,
                attrValue
        );
    }

    /**
     * Persists a value of an attribute for the provided entity instance.
     *
     * @param entityInstance
     * @param attrName
     * @param attrValue
     */
    public static void persistFormModelData(IEntityInstance entityInstance,
                                            String attrName, Object attrValue) {
        boolean succes = entityInstance.setValue(attrName, attrValue);

        if (succes) {
            LOG.debug("Persisted into profile {}.{} = {}", new String[] {
                    entityInstance.getName(), attrName, attrValue.toString()});
        } else {
            LOG.debug("Could not persist into profile {}.{} = {}",
                    new String[] {entityInstance.getName(), attrName,
                            attrValue.toString()});
        }
    }

    public static XMLGregorianCalendar toXMLGregorianCalendar(Date date) throws Exception {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
    }

    public static DateValue xmlGregorianCalendarToDateValue(XMLGregorianCalendar xmlDate) {
        DateValue dateValue = new DateValue(
                xmlDate.getYear(), xmlDate.getMonth(), xmlDate.getDay());
        return dateValue;
    }

    /**
     * Read TextItem and get its value
     *
     * @param textItem
     * @return
     */
    public static String getTextItemValue(TextItem textItem, String languageCode) {
        String returnValue = "";

        TextElement[] textItems = textItem.getNodes(languageCode);
        // Loop through nodes of TextItem and return its concatenated value
        for (TextElement textElement : textItems) {
            if (textElement instanceof TextNode) {
                TextNode textNode = (TextNode) textElement;
                LOG.debug("Normal TextNode");
                returnValue += textNode.getText();
            }

            if (textElement instanceof TextValueNode) {
                TextValueNode textValueNode = (TextValueNode) textElement;
                IValue value = textValueNode.getValue();
                if (!value.isUnknown()) {
                    LOG.debug("TextValueNode datatype = {}", value.getDataType().getName());
                    if(value.isSingleValue()) {
                        returnValue += textValueNode.getValue().toSingleValue().getDisplayValue(languageCode, null);
                    }
                    else {
                        LOG.debug("TextValueNode is a ListValue");
                        IPrimitiveValue[] values = ((ListValue) value).getValues();
                        int valuesCount = values.length;
                        for(int i=0; i<valuesCount; i++) {
                            IPrimitiveValue val = values[i];
                            returnValue += val.toSingleValue().getDisplayValue(languageCode, null) + ",";
                        }
                    }
                }
            }
        }
        return returnValue;
    }

    /**
     * Activates an instance of type entityName which has the UUID uuid.
     * Returns true if the entity was found and got activated in the profile, and false otherwise.
     * 
     * @param context
     * @param entityName
     * @param uuid
     * @return
     */
    public static boolean activateInstanceByUUID(IServiceContext context, String entityName, String uuid) {
        IEntityInstance[] instances = context.getProfile().getAllInstancesForEntity(entityName, true);
        boolean activated = false;
        if(uuid != null && instances != null) {
            for(IEntityInstance instance : instances) {
                if(uuid.equals(instance.getId().toString())) {
                    context.pushActiveInstance(instance);
                    LOG.debug("Entity instances of type {} with UUID {} has been activated", entityName, uuid);
                    activated = true;
                    break;
                }
            }
        }
        if(!activated) {
            LOG.warn("Multiple instances of type {} were found, but none of them has UUID {}", entityName, uuid);
        }
        return activated;
    }



}
