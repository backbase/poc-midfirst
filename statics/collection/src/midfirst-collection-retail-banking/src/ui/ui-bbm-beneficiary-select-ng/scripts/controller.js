import {
    createSearchIndex,
    isNewBeneficiary,
    matches,
} from './utils';

/**
 * Field names enumeration
 * @type {object}
 */
const FieldName = {
    NAME: 'name',
    IDENTIFIER: 'identifier',
};

const InputTypes = {
    IBAN:'iban',
    TEXT:'text'
}

/**
 * Component controller
 */
export default function controller($element, $timeout, document, $templateCache) {
    const ctrl = this;
    const ngModelCtrl = $element.controller('ngModel');
    //--
    $templateCache.put(InputTypes.TEXT, '<input class="form-control bbm-input uppercase"  type="text"  name="iban"  ng-model="$ctrl.state.beneficiary.identifier" ng-change="$ctrl.onIdentifierInputChanged()" ng-model-options="{ allowInvalid: true }"  autocomplete="off"  autocorrect="off" autocapitalize="off"  ng-focus="$ctrl.onIdentifierFocus($event)" ng-blur="$ctrl.onBlur()" ng-required="true" data-role="beneficiary-select-identifier"  /><label>{{ $ctrl.messages.identifierPlaceholder }} <span class="error-info error-required">{{$ctrl.messages.errorRequired }}</span><span class="error-info error-invalid">{{$ctrl.messages.errorInvalid }}</span></label>');
    $templateCache.put(InputTypes.IBAN, '<input ui-bb-iban class="form-control bbm-input uppercase"  type="text"  name="iban"  ng-model="$ctrl.state.beneficiary.identifier"   ng-change="$ctrl.onIdentifierInputChanged()" ng-model-options="{ allowInvalid: true }"  autocomplete="off"  autocorrect="off" autocapitalize="off"  ng-focus="$ctrl.onIdentifierFocus($event)" ng-blur="$ctrl.onBlur()" ng-required="true" data-role="beneficiary-select-identifier"/><label>{{ $ctrl.messages.identifierPlaceholder }} <span class="error-info error-required">{{$ctrl.messages.errorRequired }}</span><span class="error-info error-invalid">{{$ctrl.messages.errorInvalid }}</span></label>');
    //--
    ctrl.inputTemplate = (ctrl.enableIban) ? InputTypes.IBAN :  InputTypes.TEXT;
    //--
    let searchIndex = [];
    let currentSearchValue = '';

    let blurTimeout;
    let hasFocus = false;

    /**
     * ------------------------------------------------------------------------------------
     * Public state
     * ------------------------------------------------------------------------------------
     */

    /**
     * UI state
     * @type {object}
     */
    const state = {};

    /**
     * Name of the active field, when component has focus.
     * Possible values are 'name', or 'identifier', or empty string if component doesn't have focus.
     * @type {FieldName}
     */
    state.activeField = '';

    /**
     * Whether the list is opened
     * @type {boolean}
     */
    state.isListOpened = false;

    /**
     * Selected beneficiary
     * @type {object}
     */
    state.beneficiary = {};

    /**
     * List of matching accounts
     * @type {Array<object>}
     */
    state.suggestions = [];

    /**
     * ------------------------------------------------------------------------------------
     * Inner functions
     * ------------------------------------------------------------------------------------
     */

    const closeList = () => {
        state.isListOpened = false;
    };

    const getActiveFieldValue = () => {
        let value = '';
        if (hasFocus) {
            value = currentSearchValue;
        }
        return value || '';
    };

    const openList = () => {
        state.isListOpened = true;
    };

    const setBeneficiary = (beneficiaryToSet) => {
        state.beneficiary = beneficiaryToSet;
        ngModelCtrl.$setViewValue(beneficiaryToSet);
    };

    const selectAccount = (account) => {
        setBeneficiary(Object.assign({}, account));
    };


    const setFocus = (fieldName) => {
        state.activeField = fieldName;
        hasFocus = true;
    };

    const unsetFocus = () => {
        state.activeField = '';
        hasFocus = false;
    };

    const updateListState = () => {
        if (getActiveFieldValue()) {
            openList();
        } else {
            closeList();
        }
    };

    const updateSuggestions = () => {
        const query = getActiveFieldValue();
        const searchQuery = query.toLowerCase().trim();
        const accounts = ctrl.accounts || [];

        state.suggestions = accounts.filter((account, idx) => {
            return matches(searchQuery, searchIndex[idx])
        });

        updateListState();
    };

    const validate = () => {
        const isReady = Boolean(ctrl.accounts && state.beneficiary);
        if (isReady) {
            const isNew = isNewBeneficiary(ctrl.accounts, state.beneficiary);
            const isSet = Boolean(state.beneficiary.identifier);

            let createBeneficiaryValidity = true;


            if (isSet && isNew && !ctrl.allowCreate) {
                createBeneficiaryValidity = false;
            }

            ngModelCtrl.$setValidity('createBeneficiary', createBeneficiaryValidity);
        }
    };

    /**
     * ------------------------------------------------------------------------------------
     * Angular component lifecycle hooks
     * ------------------------------------------------------------------------------------
     */

    const $onChanges = ({accounts, allowCreate, ngModel}) => {

        if (accounts && accounts.currentValue) {
            searchIndex = createSearchIndex(ctrl.accounts);
        }

        if (ngModel && ctrl.ngModel) {
            setBeneficiary(ctrl.ngModel);
            validate();
        }

        if (allowCreate) {
            validate();
        }
    };



    /**
     * ------------------------------------------------------------------------------------
     * Event handlers
     * ------------------------------------------------------------------------------------
     */

    const onAccountClick = (account) => {
        selectAccount(account);
        closeList();
    };

    const onIdentifierInputChanged = () => {
        currentSearchValue = ctrl.state.beneficiary.identifier;
        updateSuggestions();
    };

    const onNameInputChanged = () => {
        currentSearchValue = ctrl.state.beneficiary.name;
        updateSuggestions();
    };


    const onNameFocus = () => {
        $timeout.cancel(blurTimeout);
        setFocus(FieldName.NAME);
        updateSuggestions();
    };

    const onIdentifierFocus = () => {
        $timeout.cancel(blurTimeout);
        setFocus(FieldName.IDENTIFIER);
        updateSuggestions();
    };

    const onBlur = () => {
        // Zero timeout is used in order to prevent closing the list too early,
        // when a user taps a suggestion from the dropdown list
        blurTimeout = $timeout(() => {
            validate();
            unsetFocus();
            closeList();
        }, 100);
    };

    /**
     * ------------------------------------------------------------------------------------
     * Public API
     * ------------------------------------------------------------------------------------
     */

    Object.assign(ctrl, {
        $onChanges,
        onAccountClick,
        onBlur,
        onIdentifierFocus,
        onNameFocus,
        onNameInputChanged,
        onIdentifierInputChanged,
        state,
    });
}
