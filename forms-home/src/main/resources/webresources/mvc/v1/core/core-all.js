if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function () {
        'use strict';
        return $.inArray(this, arguments);
    };
}

(function (blueriq, ko, bqSessionId, bqConfiguration) {
    'use strict';

    if (typeof blueriq === 'undefined') {
        blueriq = {};
        window.blueriq = blueriq;
    }


    /**
	 * Contains all default viewmodel definitions.
	 *
	 * @namespace blueriq.models
	 */
    blueriq.models = {};


    /**
	 * The Application initializes the different ui components and prepares a server side subscription for the current session id.
	 *
	 * @constructor
	 * @name blueriq.Application
	 * @param {String} sessionId The session id of the application, must be defined.
	 * @param {Object} configuration The configuration data to use, must be defined.
	 * @returns An instance of the application.
	 */
    blueriq.Application = function Application(sessionId, configuration, initCallback) {

        if (!sessionId) {
            throw new Error('sessionId is mandatory');
        }
        if (!configuration) {
            throw new Error('configuration is mandatory');
        }

        var self = this;

        /**
		 * @member {Object} blueriq.Application#configuration
		 * @property {String} baseUri URI that references to the base URL of the current application.
		 * @property {String} resourceUri URI that references to the web resources base folder.
		 * @property {String} blueriqPath URI that references to the Blueriq folder which contains all standard components (used in all themes).
		 * @property {String} themePath URI that references to the current theme folder.
		 * @property {Boolean} developmentMode Defines whether or not the application is running in development mode.
		 */
        this.configuration = configuration;
        /** @member {blueriq.LogService} blueriq.Application#log */
        this.log = new blueriq.LogService(configuration.developmentMode);
        /** @member {blueriq.ModelFactory} blueriq.Application#modelFactory */
        this.modelFactory = new blueriq.ModelFactory();
        /** @member {blueriq.MessageBus} blueriq.Application#messageBus */
        this.messageBus = new blueriq.MessageBus(self.log);
        /** @member {blueriq.EventHandler} blueriq.Application#eventHandler */
        this.eventHandler = new blueriq.EventHandler(this.messageBus);
        /** @member {blueriq.TemplateFactory} blueriq.Application#templateFactory */
        this.templateFactory = new blueriq.TemplateFactory(configuration);
        /** @member {blueriq.SessionService} blueriq.Application#sessionService */
        this.sessionService = new blueriq.SessionService(configuration.baseUri, configuration.submitLock);
        /** @member {blueriq.UtilityService} blueriq.Application#utilityService */
        this.utilityService = new blueriq.UtilityService(configuration.baseUri);

        // Hook to execute extra initialization logic
        if (initCallback) {
            initCallback(self);
        }

        window.onerror = function onError(message) { //, url, lineNumber,error
            self.messageBus.notify('error', {
                type: 'error',
                title: 'Application Exception',
                message: message,
                blocking: false
            });
            return false;
        };

        this.context = {
            configuration: configuration,
            log: self.log,
            messageBus: self.messageBus,
            templateFactory: self.templateFactory,
            modelFactory: self.modelFactory,
            utilityService: self.utilityService,
            sessionService: self.sessionService,
            eventHandler: self.eventHandler,
            subscriptionId: sessionId
        };

        /**
		 * Internal function to inject the templates from configuration.templates
		 *
		 * @function
		 * @name blueriq.Application#_injectTemplates
		 */
        this._injectTemplates = function (callback, index) {
            if (!index) { index = 0; }
            if (!self.configuration.templates || index > self.configuration.templates.length - 1) {
                callback();
            } else {
                jQuery.get(self.configuration.templates[index], function (themeTemplates) {
                    if (themeTemplates) {
                        jQuery('body').append(themeTemplates);
                    }
                    self._injectTemplates(callback, index + 1);
                });
            }
        };

        /**
		 * Starts the application by creating a subscription for the current session id. Knockout bindings will me applied on success.
		 *
		 * @function
		 * @name blueriq.Application#start
		 */
        this.start = function start() {
            self.sessionService.createSubscription(sessionId, function () {
                self._injectTemplates(function () {
                    if (self.configuration.elementId) {
                        ko.applyBindings(new blueriq.models.AppModel(sessionId, self.context), document.getElementById(self.configuration.elementId));
                    } else {
                        ko.applyBindings(new blueriq.models.AppModel(sessionId, self.context));
                    }
                });
            });
        };

        jQuery(document).ready(function () {
            // Keep alive interval
            window.setInterval(function () {
                self.context.messageBus.notify('keepAlive');
            }, (self.context.configuration.sessionTimeout / 2) * 1000);

            // General error handling
            jQuery.ajaxPrefilter(function (options) { //,originalOptions, jqXHR
                // Accept JSON by default
            	options.headers = options.headers || {};
                options.headers['Accept'] = 'application/json; charset=UTF-8';

                var callback = options.error;
                options.error = function (data) {
                    if (data.responseJSON !== undefined && data.responseJSON.errorType !== undefined) {
                        // Capture JSON error messages
                        self.context.messageBus.notify('error', {
                            type: 'error',
                            title: data.responseJSON.title,
                            message: data.responseJSON.message,
                            blocking: true
                        });
                    } else {
                        if (callback) {
                            callback(data);
                        }
                    }
                };
            });

            self.start();
        });
    };

    /**
	 * Represents the MessageBus. With this bus interface components can interact
	 * with each other.
	 *
	 * @constructor
	 * @name blueriq.MessageBus
	 * @param {blueriq.LogService} logService The logService, must be defined.
	 * @returns A {@link blueriq.MessageBus|MessageBus} instance.
	 */
    blueriq.MessageBus = function MessageBus(logService) {

        if (!logService) {
            throw new Error('logService is mandatory');
        }
        var listenerId = 0;
        var listeners = {};

        /**
		 * Subscribes a listener to an event, using the specified event and callback
		 * function.
		 *
		 * @function
		 * @name blueriq.MessageBus#subscribe
		 * @param {Object} event The event to subscribe to.
		 * @param {Function} callback Function to be called when the listener is notified.
		 * @returns {Object} A disposable object which can be used to unsubscribe
		 *          from the event by calling dispose().
		 */
        this.subscribe = function subscribe(event, callback) {
            var id = ++listenerId;
            listeners[id] = {
                id: id,
                event: event,
                callback: callback
            };
            return {
                dispose: function dispose() {
                    // Removes the listener from the array
                    delete listeners[id];
                }
            };
        };

        /**
		 * Notifies all listeners subscribed to the specified event, passing the
		 * specified data as event body.
		 *
		 * @function
		 * @name blueriq.MessageBus#notify
		 * @param {Object} event The event to publish.
		 * @param {Object} data The data to pass to the listeners.
		 */
        this.notify = function notify(event, data) {
            //logService.debug('Sending Event', event, data);
            for (var id in listeners) {
                var listener = listeners[id];
                if (listener.event === event) {
                    listener.callback(event, data);
                }
            }
        };

        /**
		 * Gets all listeners registered.
		 *
		 * @function
		 * @name blueriq.MessageBus#getListeners
		 * @returns {Array} Containing all listeners mapped by their id.
		 */
        this.getListeners = function getListeners() {
            return listeners;
        };
    };

    /**
	 * Allows logging (wraps console.log)
	 *
	 * @constructor
	 * @name blueriq.LogService
	 * @param {Boolean} debugEnabled Tells whether or not debug logging is shown.
	 * @returns A LogService instance.
	 */
    blueriq.LogService = function LogService(debugEnabled) {
        /**
		 * Logs all arguments specified to the console when debugging is enabled.
		 *
		 * @function
		 * @name blueriq.LogService#debug
		 */

        this.debug = function () {
            if (debugEnabled && window.console) {
                window.console.log(arguments);
            }

        };

        /**
		 * Logs all arguments specified to the console on default logging level.
		 *
		 * @function
		 * @name blueriq.LogService#log
		 */
        this.log = function () {
            if (window.console) {
                window.console.log(arguments);
            }
        };
        /**
		 * Logs all arguments specified to the console on info logging level.
		 *
		 * @function
		 * @name blueriq.LogService#info
		 */
        this.info = function () {
            if (window.console) {
                window.console.info(arguments);
            }
        };
        /**
		 * Logs all arguments specified to the console on error logging level.
		 *
		 * @function
		 * @name blueriq.LogService#error
		 */
        this.error = function () {
            if (window.console) {
                window.console.error(arguments);
            }
        };
    };

    /**
	 * Represents a Blueriq portal session.
	 *
	 * @constructor
	 * @name blueriq.SessionController
	 * @param {String} sessionId The session id, must be defined.
	 * @param {String} subscriptionId The subscription id, must be defined.
	 * @param {blueriq.SessionService} sessionService The sessionService, must be defined.
	 * @param {blueriq.UtilityService} utilityService The utilityService, must be defined.
	 * @param {blueriq.MessageBus} messageBus Message bus instance, must be defined.
	 * @param {blueriq.EventHandler} eventHandler Event handler, must be defined.
	 * @returns A SessionController instance.
	 */
    blueriq.SessionController = function SessionController(sessionId, subscriptionId, sessionService, utilityService, messageBus, eventHandler) {

        if (!sessionId) {
            throw new Error('sessionId is mandatory');
        }
        if (!subscriptionId) {
            throw new Error('subscriptionId is mandatory');
        }
        if (!sessionService) {
            throw new Error('sessionService is mandatory');
        }
        if (!utilityService) {
            throw new Error('utilityService is mandatory');
        }
        if (!messageBus) {
            throw new Error('messageBus is mandatory');
        }
        if (!eventHandler) {
            throw new Error('eventHandler is mandatory');
        }

        var self = this;
        var listeners = {};
        var modelListeners = {};
        var listenerId = 0;

        this.id = sessionId;
        this.csrfToken = ko.observable(null);
        this.language = {};
        this.models = {};

        /**
		 * Creates a page event object using the specified element key and
		 * parameters.
		 *
		 * @function
		 * @name blueriq.SessionController#createPageEvent
		 * @param {String} key The key of the element that initiates the page event.
		 * @param {Object} parameters (Optional) Parameters to pass with the page event.
		 * @returns {Object} A page event object containing the element key, all field elements (key/value pairs) and the parameters.
		 */
        this.createPageEvent = function createPageEvent(key, parameters) {
            // Create a page event.
            var pageEvent = {};
            pageEvent.elementKey = key;
            pageEvent.parameters = parameters;

            pageEvent.fields = [];
            for (var modelKey in self.models) {
                var model = self.models[modelKey];
                if (model.type === 'field') {
                    var fieldValue = {};
                    fieldValue.key = model.key;
                    fieldValue.values = model.values;

                    pageEvent.fields.push(fieldValue);
                }
            }
            return pageEvent;
        };

        /**
		 * Performs a modification of the model by handling the specified set of
		 * changes.
		 *
		 * @function
		 * @name blueriq.SessionController#handleEvent
		 * @param {Object}
		 *            changes Array of changes to perform on the model, must be
		 *            defined.
		 */

        var notifyListeners = function __notifyListeners(changes) {
            for (var i = 0; i < changes.changes.length; ++i) {
                var change = changes.changes[i];
                var listener;
                for (var listenerId in listeners) {
                    listener = listeners[listenerId];
                    listener(change.type, change.key, change.model);
                }
                if (modelListeners[change.key]) {
                    for (var id in modelListeners[change.key]) {
                        listener = modelListeners[change.key][id];
                        listener(change.type, change.model);
                    }
                }
                if (change.type === 'delete') {
                    delete modelListeners[change.key];
                }
            }
        };

        this.handleChanges = function handleChanges(changes) {
            if (!changes) {
                throw new Error('changes is mandatory');
            }

            // update model
            for (var i = 0; i < changes.changes.length; ++i) {
                var change = changes.changes[i];
                if (change.type === 'update' || change.type === 'add') {
                    self.models[change.key] = change.model;
                } else {
                    delete self.models[change.key];
                }
            }

            // notify
            notifyListeners(changes);
        };

        /**
		 * Gets the page model.
		 *
		 * @function
		 * @name blueriq.SessionController#getPage
		 * @returns {Object} The page model or undefined if not found.
		 */
        this.getPage = function getPage() {
            for (var element in self.models) {
                var model = self.models[element];
                if (model.type === 'page') {
                    return model;
                }
            }
        };

        /**
		 * Returns the model of the specified key.
		 *
		 * @function
		 * @name blueriq.SessionController#getModel
		 * @param {String} key The key of the model, must be defined.
		 * @returns {Object} The model of the key.
		 */
        this.getModel = function getModel(key) {
            if (!key) {
                throw new Error('key is mandatory');
            }
            return self.models[key];
        };

        /**
		 * Initializes the session data.
		 *
		 * @function
		 * @name blueriq.SessionController#init
		 * @param {Function}  callback (Optional) Callback triggered after initialization.
		 */
        this.init = function init(callback) {
            sessionService.subscribe(subscriptionId, sessionId, function (data) {
                self.language = data.language;
                self.csrfToken(data.csrfToken);

                for (var i = 0; i < data.elements.length; ++i) {
                    var element = data.elements[i];
                    self.models[element.key] = element;
                }

                if (callback) {
                    callback();
                }
            });

            self.pageSubscription = messageBus.subscribe('page', function (event, data) {
                if (data.sessionId === self.id) {
                	if (data.csrfToken) {
                		self.csrfToken(data.csrfToken);
                	}
                    self.handleChanges(data.changes);
                }
            });
            
            self.projectSubscription = messageBus.subscribe('project', function(event, data) {
            	if (data.sessionId === self.id) {
            		sessionService.startNewSession(data.newSessionId, data.newTab);
            	}
            });
			
			self.keepAlive = true;

            self.keepAliveSubscription = messageBus.subscribe('keepAlive',
                function () {
                    if (self.keepAlive) {
                        utilityService.keepAlive(sessionId);
                    }
                });

            self.beforeSubmitSubscription = messageBus.subscribe('beforeSubmit',
                function () {
                    self.keepAlive = false;
                });

            self.afterSubmitSubscription = messageBus.subscribe('afterSubmit',
                function () {
                    self.keepAlive = true;
                });
        };

        /**
		 * Subscribes to all change events.
		 *
		 * @function
		 * @name blueriq.SessionController#subscribe
		 * @param {Function} listener Callback with signature changeType, key, model. Must be defined.
		 * @returns {Object} A disposable object which can be used to unsubscribe from the event by calling dispose().
		 */
        this.subscribe = function subscribe(listener) {
            if (!listener) {
                throw new Error('listener is mandatory');
            }
            var id = ++listenerId;
            listeners[id] = listener;
            return {
                dispose: function () {
                    delete listeners[id];
                }
            };
        };



        /**
		 * Subscribes to change events for models with a specific key.
		 * @function
		 * @name blueriq.SessionController#subscribe
		 * @param {String} key
		 * @param {Function} listener Callback with signature changeType, model. Must be defined.
		 * @returns {Object} A disposable object which can be used to unsubscribe from the event by calling dispose().
		 */
        this.subscribeModel = function subscribeModel(key, listener) {
            if (!key) {
                throw new Error('[subscribeModel] key is mandatory');
            }
            if (!listener) {
                throw new Error('[subscribeModel] listener is mandatory');
            }
            if (!modelListeners[key]) {
                modelListeners[key] = {};
            }
            var id = ++listenerId;
            modelListeners[key][id] = listener;
            return {
                dispose: function () {
                    delete modelListeners[key][id];
                }
            };
        };

        /**
		 * Submits the current page.
		 *
		 * @function
		 * @name blueriq.SessionController#submit
		 * @param {String} [key] to reference the element that triggers the submit
		 * @param {Object} [parameters] Extra parameters
		 * @param {Function} [callback] Function to call when submit is done.
		 */
        this.submit = function submit(key, parameters, callback) {
            var pageEvent = self.createPageEvent(key, parameters);
            sessionService.submit(sessionId, subscriptionId, self.csrfToken(), pageEvent, function (success, data) {
                eventHandler.handleEvents(success, data);
                if (callback) {
                    callback(success);
                }
            });
        };

        /**
		 * Recompose the current page.
		 *
		 * @function
		 * @name blueriq.SessionController#recompose
		 * @param {Function} [callback] Function to call when submit is done.
		 */
        this.recompose = function recompose(callback) {
            var pageEvent = {};
            sessionService.submit(sessionId, subscriptionId, self.csrfToken(), pageEvent, function (success, data) {
                eventHandler.handleEvents(success, data);
                if (callback) {
                    callback(success);
                }
            });
        };

        /**
		 * Starts a flow with the specified name.
		 *
		 * @function
		 * @name blueriq.SessionController#startFlow
		 * @param {String} flowName Name of the flow to start, must be specified.
		 */
        this.startFlow = function startFlow(flowName) {
            if (!flowName) {
                throw new Error('flowName is mandatory');
            }
            sessionService.startFlow(sessionId, subscriptionId, self.csrfToken(), flowName,eventHandler.handleEvents);
        };

        /**
		 * Disposes the session by closing all subscriptions.
		 *
		 * @function
		 * @name blueriq.SessionController#dispose
		 */
        this.dispose = function sessionControllerDispose() {
            if (self.pageSubscription) {
                self.pageSubscription.dispose();
            }
            if (self.projectSubscription) {
            	self.projectSubscription.dispose();
            }
            if (self.keepAliveSubscription) {
                self.keepAliveSubscription.dispose();
            }
            if (self.beforeSubmitSubscription) {
                self.beforeSubmitSubscription.dispose();
            }
            if (self.afterSubmitSubscription) {
                self.afterSubmitSubscription.dispose();
            }
        };
    };

    /**
	 * Responsible for retrieving template url's for viewmodels.
	 *
	 * @constructor
	 * @name blueriq.TemplateFactory
	 * @param {Object}configuration Configuration object, must be specified.
	 * @returns An instance of the template factory.
	 */
    blueriq.TemplateFactory = function (configuration) {

        if (!configuration) {
            throw new Error('configuration is mandatory');
        }

        var self = this;

        this.modelHandlers = [];
        this.fieldValueHandlers = [];

        /**
		 * Handler for custom templates for elements.
		 *
		 * @function
		 * @name blueriq.TemplateFactory.ElementHandler
		 * @param {Object}
		 *            model The model to retrieve a template for.
		 * @returns {String} The path to the template, or undefined if not
		 *          applicable.
		 */

        /**
		 * Registers a custom handler for a viewmodel.
		 *
		 * @function
		 * @name blueriq.TemplateFactory#registerModelHandler
		 * @param {blueriq.TemplateFactory.ElementHandler} handler (Optional) The handler implementation.
		 */
        this.registerModelHandler = function registerModelHandler(handler) {
            if (typeof (handler) !== 'function') {
                throw new Error('handler must be a function');
            }
            self.modelHandlers.push(handler);
        };

        /**
		 * Returns a template url for the viewmode.
		 *
		 * @function
		 * @name blueriq.TemplateFactory#getTemplate
		 * @param {blueriq.models.BaseModel} viewModel The model to retrieve a template for, must be specified.
		 * @param {Object} bindingContext binding context is an object that holds data that you can reference from your bindings
		 * @returns {String} The url of the template.
		 */
        this.getTemplate = function getTemplate(viewModel, bindingContext) {
            if (!viewModel) {
                throw new Error('viewModel is mandatory');
            }
            for (var i = self.modelHandlers.length - 1; i >= 0; i--) {
                var template = self.modelHandlers[i](viewModel, bindingContext);
                if (template) {
                    return template;
                }
            }

            throw new Error('Could not create template');
        };

        /**
		 * Registers a custom handler for a viewmodel.
		 *
		 * @function
		 * @name blueriq.TemplateFactory#registerFieldValueHandler
		 * @param {FieldHandler}  handler (Optional) The handler implementation.
		 */
        this.registerFieldValueHandler = function registerFieldValueHandler(handler) {
            if (handler) {
                self.fieldValueHandlers.push(handler);
            }
        };

        /**
		 * Returns the template url for the field value of a field viewmodel.
		 *
		 * @function
		 * @name blueriq.TemplateFactory#getFieldValueTemplate
		 * @param {blueriq.models.BaseModel} viewModel The field to retrieve a value template for, must be specified.
		 * @param {Object} bindingContext binding context is an object that holds data that you can reference from your bindings
		 * @returns {String} URL to the template for the specified field value viewmodel.
		 */
        this.getFieldValueTemplate = function getFieldValueTemplate(viewModel, bindingContext) {
            if (!viewModel) {
                throw new Error('viewModel is mandatory');
            }
            for (var i = self.fieldValueHandlers.length - 1; i >= 0; i--) {
                var template = self.fieldValueHandlers[i](viewModel, bindingContext);
                if (template) {
                    return template;
                }
            }
            throw new Error('Could not create a template for field value');
        };
    };

    /**
	 * Responsible for generating a viewmodel from a datamodel.
	 *
	 * @constructor
	 * @name blueriq.ModelFactory
	 * @returns An instance of the modelFactory.
	 */
    blueriq.ModelFactory = function ModelFactory() {

        var modelHandlers = [];

        /**
		 * Registers a model handler.
		 *
		 * @function
		 * @name blueriq.ModelFactory#register
		 * @param {blueriq.ModelFactory.ModelHandler} handler (Optional) The handler implementation.
		 */
        this.register = function register(handler) {
            if (typeof (handler) !== 'function') {
                throw new Error('handler must be a function');
            }
            modelHandlers.push(handler);
        };

        /**
		 * Returns a viewmodel for a datamodel.
		 *
		 * @function
		 * @name blueriq.ModelFactory#createViewModel
		 * @param {Object} model The concerning data model, must be specified.
		 * @param {Object} context Context for the viewmodel to be created, must be specified.
		 * @returns {blueriq.models.BaseModel} The generated viewmodel.
		 */
        this.createViewModel = function createViewModel(model, context) {
            if (!model) {
                throw new Error('model is mandatory');
            }
            if (!context) {
                throw new Error('context is mandatory');
            }

            for (var i = modelHandlers.length - 1; i >= 0; i--) {
                var viewModel = modelHandlers[i](model, context);
                if (viewModel) {
                    return viewModel;
                }
            }
        };
    };

    /**
	 * Responsible for handling requests to the utility service.
	 *
	 * @constructor
	 * @name blueriq.UtilityService
	 * @param {String} baseUri The base uri, must be specified.
	 * @returns An instance of the utility service.
	 */
    blueriq.UtilityService = function UtilityService(baseUri) {
        if (!baseUri) {
            throw new Error('baseUri is mandatory');
        }

        /**
		 * Heartbeat to keep the session active. Sends a request to the server and
		 * calls the specified callback function on response.
		 *
		 * @function
		 * @name blueriq.UtilityService#keepAlive
		 * @param {String} sessionId Id of the session to keep alive, must be specified.
		 * @param {Function} [callback] Function to call when the server responds.
		 */
        this.keepAlive = function keepAlive(sessionId, callback) {
            if (!sessionId) {
                throw new Error('sessionId is mandatory');
            }

            $.get(baseUri + sessionId + '/api/utility/keepAlive',
                function (response) {
                    if (callback) {
                        callback(response);
                    }
                }).fail(function() {
                    window.bqApp.log.error('KeepAlive failed, session expired?');
                });
        };
    };

    /**
     * Set or add parameters to a URL query string.
     * 
     * @function
     * @name blueriq.QueryStringBuilder
     * @param {String} url starting URL, must be specified
     */
    blueriq.QueryStringBuilder = function QueryStringBuilder(url) {
    	var self = this;
    	var elements = url.match(/([^\?#]+)(\?([^#]+))?(#.+)?/);
    	
    	this.head = elements[1] || '';
    	this.queryString = elements[3] || ''
    	this.tail = elements[4] || '';
    	
    	this.parameters = {};
    	$(this.queryString.split('&')).each(function(index, value) {
    		var tokens = value.split('=');
    		if (tokens.length == 2) {
    			self.parameters[tokens[0]] = tokens[1];
    		}
    	});
    	
    	
    	this.param = function(name, value) {
    		self.parameters[name] = value;
    		return self;
    	};
    	
    	var joinParameters = function() {
    		var result = '';
    		for (var name in self.parameters) {
    			if (result) result += '&';
    			result += name + '=' + encodeURIComponent(self.parameters[name]);
    		}
    		
    		return result ? '?' + result : '';
    	};
    	
    	this.toUrl = function() {
    		return self.head + joinParameters() + self.tail;
    	}
    	
    };
    
    /**
	 * Event handler for handling page events. The specified message bus is used to
	 * notify when an event is handled successfully or when an error occurs.
	 *
	 * @constructor
	 * @name blueriq.EventHandler
	 * @param {blueriq.MessageBus} messageBus Message bus instance to use for notifications on success or failure, must be specified.
	 * @returns An EventHandler instance.
	 */
    blueriq.EventHandler = function EventHandler(messageBus) {
        if (!messageBus) {
            throw new Error('messageBus is mandatory');
        }

        /**
		 * Handles the events present on the specified data object, by notifying on
		 * the message bus for each event.
		 *
		 * @function
		 * @name blueriq.EventHandler#handleEvents
		 * @param {Boolean} success Indicator whether or not the success path should be taken.
		 * @param {Object} data (Optional) Data object containing the events to handle.
		 */
        this.handleEvents = function handleEvent(success, data) {
            if (success) {
                if (data && data.events) {
                    for (var i = 0; i < data.events.length; ++i) {
                        var event = data.events[i];
                        if (event.type == 'project') {
                        	messageBus.notify('project', event);
                        } else if (event.type == 'page') {
                        	messageBus.notify('page', event);
                        }
                    }
                }
            } else {
                messageBus.notify('error', {
                    type: 'error',
                    title: 'Application Exception',
                    message: 'Server communication failed',
                    blocking: true
                });
            }
        };
    };

    /**
	 * Queue for events that handles events sequentially.
	 */
    blueriq.EventQueue = function EventQueue() {

        this.reqs = [];
        this.requesting = false;

        this.add = function add(req) {
            this.reqs.push(req);
            this.next();
        };
        this.next = function next() {
            if (this.reqs.length === 0) {
                return;
            }
            if (this.requesting === true) {
                return;
            }

            var req = this.reqs.splice(0, 1)[0];
            var complete = req.complete;
            var self = this;

            req.complete = function () {
                if (complete) {
                    complete.apply(this, arguments);
                }
                self.requesting = false;
                self.next();
            };
            this.requesting = true;
            $.ajax(req);
        };
    };

    /**
	 * Service that communicates with the server to perform session related actions,
	 * like flowing and event subscriptions.
	 *
	 * @constructor
	 * @name blueriq.SessionService
	 * @param {String} baseUri The base URL of the API, must be specified.
     * @param {Boolean} submitLock whether only on event should be handled. If this value is true or specified, the event queue is not used, which is the same behavior as in 9.3-.
     */
    blueriq.SessionService = function SessionService(baseUri, submitLock) {

        if (!baseUri) {
            throw new Error('baseUri is mandatory');
        }
        var self = this;
        self.eventQueue = new blueriq.EventQueue();
        self.submitLock = submitLock;

        /**
		 * Submits the pageEvent to the server.
		 *
		 * @function
		 * @name blueriq.SessionService#submit
		 * @param {String}
		 *            sessionId The id of the session submit for, must be specified.
		 * @param {String}
		 *            subscriptionId The id of the subscription, must be specified.
		 * @param {string}
		 *            csrfToken the CSRF token for this request, must be specified.
		 * @param {Object}
		 *            pageEvent The event to submit, must be specified.
		 * @param {Function}
		 *            (Optional) Callback function to call when submit is
		 *            successful.
		 */
        var submitting = false;
        this.submit = function submit(sessionId, subscriptionId, csrfToken, pageEvent, callback) {
            if (self.submitLock && self.submitLock === true && submitting) {
                return;
            }

            if (!sessionId) {
                throw new Error('sessionId is mandatory');
            }
            if (!subscriptionId) {
                throw new Error('subscriptionId is mandatory');
            }
            if (!pageEvent) {
                throw new Error('pageEvent is mandatory');
            }
            if (!csrfToken) {
            	throw new Error('csrfToken is mandatory');
            }
            submitting = true;
            self.eventQueue.add({
                type: 'POST',
                url: baseUri + sessionId + '/api/subscription/' + subscriptionId + '/handleEvent',
                data: JSON.stringify(pageEvent),
                contentType: 'application/json',
                headers: {
                	'X-CSRF-Token': csrfToken
                },
                success: function (data) {
                    if (callback) {
                        callback(true, data);
                    }
                    submitting = false;
                },
                error: function () {
                    if (callback) {
                        callback(false);
                    }
                    submitting = false;
                }
            });
        };

        /**
		 * Starts a new flow by calling the flow service on the REST API with a
		 * specified flow name.
		 *
		 * @function
		 * @name blueriq.SessionService#startFlow
		 * @param {String} sessionId The id of the session submit for, must be specified.
		 * @param {String} subscriptionId The id of the subscription, must be specified.
		 * @param {String} csrfToken The CSRF token for this request, must be specified.
		 * @param {String} flowName Name of the flow to start.
		 * @param {Function} [callback] Callback function for handling the success response.
		 */
        this.startFlow = function startFlow(sessionId, subscriptionId, csrfToken, flowName, callback) {
            if (!sessionId) {
                throw new Error('sessionId is mandatory');
            }
            if (!subscriptionId) {
                throw new Error('subscriptionId is mandatory');
            }
            if (!flowName) {
                throw new Error('flowName is mandatory');
            }
            if (!csrfToken) {
            	throw new Error('csrfToken is mandatory');
            }

            $.ajax({
                type: 'POST',
                url: baseUri + sessionId + '/api/subscription/' + subscriptionId + '/startFlow/' + flowName,
                contentType: 'application/json',
                headers: {
                	'X-CSRF-Token': csrfToken
                },
                success: function (data) {
                    if (callback) {
                        callback(true, data);
                    }
                },
                error: function () {
                    if (callback) {
                        callback(false);
                    }
                }
            });
        };

        /**
		 * Creates a subscription for the specified subscription id.
		 *
		 * @function
		 * @name blueriq.SessionService#createSubscription
		 * @param {String} subscriptionId The id subscription to be created, usually the main session id, must be specified.
		 * @param {Function} [callback]The function to call when the subscription is successful.
		 */
        this.createSubscription = function createSubscription(subscriptionId,
            callback) {
            if (!subscriptionId) {
                throw new Error('subscriptionId is mandatory');
            }
            $.ajax({
                type: 'POST',
                url: baseUri + subscriptionId + '/api/subscribe/',
                contentType: 'application/json',
                success: function () {
                    if (callback) {
                        callback();
                    }
                }
            });
        };

        /**
		 * Subscribes the specified session id to the specified subscription id.
		 *
		 * @function
		 * @name blueriq.SessionService#subscribe
		 * @param {String} subscriptionId The id of the subscription to subscribe to, must be specified.
		 * @param {String} sessionId The id of the session to subscribe to thesubscription, must be specified.
		 * @param {Function} [callback] Callback function to call when subscription is successful.
		 */
        this.subscribe = function subscribe(subscriptionId, sessionId, callback) {
            if (!subscriptionId) {
                throw new Error('subscriptionId is mandatory');
            }
            if (!sessionId) {
                throw new Error('sessionId is mandatory');
            }
            $.ajax({
                type: 'POST',
                url: baseUri + sessionId + '/api/subscribe/' + subscriptionId,
                contentType: 'application/json',
                success: function (data) {
                    if (callback) {
                        callback(data);
                    }
                }
            });
        };
        
        /**
         * Opens a new session. The session must already be created server-side.
         * 
         * @function
         * @name blueriq.SessionService#startNewSession
         * @param {String} newSessionId the ID of the new session that will be opened
         * @param {boolean} newTab whether the new session should be opened in a new tab or in the same tab
         */
        this.startNewSession = function startNewSession(newSessionId, newTab) {
        	var nextLocation = baseUri + newSessionId + '/mvc/index.html';
        	if (newTab) {
        		var win = window.open(nextLocation, '_blank');
        		if (win) {
        			win.focus();
        		}
        	} else {
        		window.open(nextLocation, '_self');
        	}
        };
    };

    /**
	 * Represents the main viewmodel of the application.
	 *
	 * @constructor
	 * @name blueriq.models.AppModel
	 * @param {String} sessionId The sessionId of the application.
	 * @param {Object} context The data context.
	 * @returns An AppModel instance.
	 */
    blueriq.models.AppModel = function AppModel(sessionId, context) {

        if (!sessionId) {
            throw new Error('sessionId is mandatory');
        }
        if (!context) {
            throw new Error('context is mandatory');
        }
        this.notification = new blueriq.models.NotificationModel(context.messageBus);
        this.session = new blueriq.models.SessionModel(sessionId, context);
    };

    /**
	 * Represents the notification viewmodel.
	 *
	 * @constructor
	 * @name blueriq.models.NotificationModel
	 * @param {blueriq.MessageBus} messageBus The messageBus to monitor.
	 * @returns An NotificationModel instance.
	 */
    blueriq.models.NotificationModel = function NotificationModel(messageBus) {

        if (!messageBus) {
            throw new Error('messageBus is mandatory');
        }

        var self = this;
        var queue = [];

        this.type = ko.observable(null);
        this.title = ko.observable(null);
        this.message = ko.observable(null);
        this.blocking = ko.observable(null);

        function setData(data) {
            if (data) {
                self.type(data.type);
                self.title(data.title);
                self.message(data.message);
                self.blocking(data.blocking);
            } else {
                self.type(null);
                self.title(null);
                self.message(null);
                self.blocking(null);
            }
        }

        /**
		 * Sets the state of this {@link blueriq.models.NotificationModel|NotificationModel} as handled and moves to the next {@link blueriq.models.NotificationModel} if possible.
		 *
		 * @function
		 * @name blueriq.models.NotificationModel#handled
		 */
        this.handled = function () {
            var next = queue.shift();
            if (next) {
                setData(next);
            } else {
                setData();
            }
        };

        messageBus.subscribe('error', function (event, data) {
            if (self.message()) {
                queue.push(data);
            } else {
                setData(data);
            }
        });
    };

    /**
	 * Represents a Blueriq Session Model
	 *
	 * @constructor
	 * @name blueriq.models.SessionModel
	 * @param {String} sessionId The session id, must be defined.
	 * @param {Object} context The data context, must be defined.
	 * @returns An instance of the session model.
	 */
    blueriq.models.SessionModel = function SessionModel(sessionId, context) {

        if (!sessionId) {
            throw new Error('sessionId is mandatory');
        }
        if (!context) {
            throw new Error('context is mandatory');
        }

        var self = this;
        this.context = {};

        this.page = ko.observable(null);
        ko.utils.extend(this.context, context);

        this.context.session = new blueriq.SessionController(sessionId, context.subscriptionId, context.sessionService, context.utilityService, context.messageBus, context.eventHandler);

        var changeSubscription = this.context.session.subscribe(function sessionChangeListener(type, key, model) {
            // update will be handled by the page viewmodel, delete without add should never occur..
            if (type === 'add' && model.type === 'page') {
                self.page(self.context.modelFactory.createViewModel(model, self.context));
            }
        });

        this.context.session.init(function () {
            var pageModel = self.context.session.getPage();
            self.page(self.context.modelFactory.createViewModel(pageModel, self.context));
        });

        /**
		 * Disposes this {@link blueriq.models.SessionModel|SessionModel} and all its subscriptions.
		 *
		 * @function
		 * @name blueriq.models.SessionModel#dispose
		 */
        this.dispose = function sessionModelDispose() {
            this.context.session.dispose();

            if (changeSubscription) {
                changeSubscription.dispose();
            }
        };
    };


    /* create the application */
    if ((typeof bqSessionId !== 'undefined') && (typeof bqConfiguration !== 'undefined')) {
        window.bqApp = new blueriq.Application(bqSessionId, bqConfiguration);
    }

}(window.blueriq, window.ko, window.bqSessionId, window.bqConfiguration));
//providing a scoped namesspaces to inject in the iife
var jsScopes = {
    //Defining Chart.js namespacing
    chartJs: {
        Chart: window.Chart,
        legend: window.legend
    },

    //defining odometer namespacing
    odometerJs: {
        odometerOptions: window.odometerOptions
    }
};

(function (ko, chartJs, odometerJs) {
    'use strict';

    ko.virtualElements.allowedBindings.bqMode = true;

    /**
	 * This handler adds a render modus to the context.
	 */
    ko.bindingHandlers.bqMode = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var mode = valueAccessor();

            var modus = bindingContext.modus ? bindingContext.modus.slice() : [];
            if (mode || 0 === mode.length) {
                modus = modus.concat(mode);
            }

            //var newProperties = valueAccessor(),
            var childBindingContext = bindingContext.createChildContext(viewModel);

            ko.utils.extend(childBindingContext, {
                modus: modus
            });
            ko.applyBindingsToDescendants(childBindingContext, element);

            return {
                controlsDescendantBindings: true
            };
        }
    };

    /**
	 * This handler creates datepicker.
	 */
    ko.bindingHandlers.bqDatePicker = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            $(element).datepicker({
                format: viewModel.context.session.language.patterns.date
            }).on('changeDate', function (ev) {
                var value = $(ev.currentTarget).find('input').val();
                valueAccessor()(value);
            }).on('show', function () {
                if (viewModel.readonly()) {
                    $(element).datepicker('hide');
                }
            });
        },
        update: function (element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            if (value) {
                $(element).datepicker('setValue', value);
            }
        }
    };

    /**
	 * This handler generates a tooltip.
	 */
    ko.bindingHandlers.bqTooltip = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var placement;
            if (!placement) {
                placement = 'right';
            }
            $(element).tooltip({
                title: viewModel.explainText,
                placement: placement
            });
        }
    };

    /**
	 * This handler generates a multiple select box.
	 */
    ko.bindingHandlers.bqMultiSelect = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            var obj = valueAccessor(), allBindings = allBindingsAccessor(), lookupKey = allBindings.lookupKey;
            $(element).select2(obj);
            if (lookupKey) {
                var value = ko.utils.unwrapObservable(allBindings.value);
                $(element).select2('data', ko.utils.arrayFirst(obj.data.results, function (item) {
                    return item[lookupKey] === value;
                }));
            }

            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $(element).select2('destroy');
            });
        },
        update: function (element) {
            $(element).trigger('change');
        }
    };

    /**
	 * This handler shows a modal based on its value accessor.
	 */
    ko.bindingHandlers.bqModal = {
        init: function () {
        },
        update: function (element, valueAccessor) {
            if (valueAccessor()) {
                $(element).modal('show');
            } else {
                $(element).modal('hide');
            }
        }
    };

    /**
	 * This handler converts the enters in a text to html enters(linebreaks).
	 */
    ko.bindingHandlers.bqHtml = {
        update: function (element, valueAccessor) {
            if(valueAccessor() !== null) {
                ko.utils.setHtml(element, valueAccessor().replace(/\r?\n/g, '<br />'));
            }
        }
    };

    /**
	* This handler handles the file upload container
	*/
    ko.bindingHandlers.bqFileUpload = {
        update: function (element, valueAccessor, allBindings, viewModel) {
            $(element).fileupload({
                url: new blueriq.QueryStringBuilder(allBindings().url()).param('X-CSRF-Token', viewModel.csrfToken()).toUrl()
            });
        },
        init: function (element, valueAccessor, allBindings, viewModel) {
            var attachFileUpload = function attachFileUpload(element, labelElement, inputElement, descriptionElement, configuration) {
                $(document).bind('drop dragover', function (e) {
                    e.preventDefault();
                });

                // Input checking
                if (!element) {
                    throw new Error('bqFileUpload: element is mandatory');
                }
                if (!labelElement) {
                    throw new Error('bqFileUpload: labelElement is mandatory');
                }
                if (!inputElement) {
                    throw new Error('bqFileUpload: inputElement is mandatory');
                }
                if (!configuration || !configuration.url) {
                    throw new Error('bqFileUpload: url is mandatory');
                }

                // Utility functions
                var formatFileSize = function formatFileSize(bytes) {
                    if (typeof bytes !== 'number') { return ''; }
                    if (bytes >= 1000000000) { return (bytes / 1000000000).toFixed(2) + ' GB'; }
                    if (bytes >= 1000000) { return (bytes / 1000000).toFixed(2) + ' MB'; }
                    return (bytes / 1000).toFixed(2) + ' KB';
                };

                var formatBitrate = function formatBitrate(bits) {
                    if (typeof bits !== 'number') { return ''; }
                    if (bits >= 1000000000) { return (bits / 1000000000).toFixed(2) + ' Gbit/s'; }
                    if (bits >= 1000000) { return (bits / 1000000).toFixed(2) + ' Mbit/s'; }
                    if (bits >= 1000) { return (bits / 1000).toFixed(2) + ' kbit/s'; }
                    return bits.toFixed(2) + ' bit/s';
                };

                var formatTime = function formatTime(seconds) {
                    var date = new Date(seconds * 1000), days = Math.floor(seconds / 86400);
                    days = days ? days + 'd ' : '';
                    return days + ('0' + date.getUTCHours()).slice(-2) + ':' + ('0' + date.getUTCMinutes()).slice(-2) + ':' + ('0' + date.getUTCSeconds()).slice(-2);
                };

                // var formatPercentage = function formatPercentage(floatValue) {
                //     return (floatValue * 100).toFixed(2);
                // };

                // Set text on UI elements
                if (configuration.singleFileMode) {
                    labelElement.text(configuration.singleUploadLabel);
                } else {
                    labelElement.text(configuration.multiUploadLabel);
                    inputElement.attr('multiple', 'multiple');
                }

                if (descriptionElement) {
                    var description = '';
                    if (configuration.fileSizeDescription && configuration.maxFileSize) {
                        description += configuration.fileSizeDescription.replace('{0}', formatFileSize(configuration.maxFileSize));
                    }
                    if (configuration.extensionDescription && configuration.allowedExtensions) {
                        if (configuration.fileSizeDescription && configuration.maxFileSize) {
                            description += ' / ';
                        }
                        description += configuration.extensionDescription.replace('{0}', configuration.allowedExtensions.split('|').join(', '));
                    }
                    descriptionElement.text(description);
                }

                // Set messages and settings for upload component
                var messages = {};
                if (configuration.extensionValidationMessage) {
                    messages.acceptFileTypes = configuration.extensionValidationMessage;
                }
                if (configuration.fileSizeValidationMessage) {
                    messages.maxFileSize = configuration.fileSizeValidationMessage;
                }

                var settings = {
                    url: new blueriq.QueryStringBuilder(configuration.url).param('X-CSRF-Token', viewModel.csrfToken()).toUrl(), 
                    singleFileUploads: false,
                    dropZone: element,
                    messages: messages,
                    formData: function() {//form
                        var pageEvent = viewModel.context.session.createPageEvent(viewModel.key, {});
                        return [{name: 'pageEvent', value: JSON.stringify(pageEvent)}];
                    }
                };

                if (configuration.allowedExtensions) {
                    settings.acceptFileTypes = new RegExp('(' + configuration.allowedExtensions + ')$');
                }
                if (configuration.maxFileSize) {
                    settings.maxFileSize = configuration.maxFileSize;
                }

                // Create upload component
                var fileUpload = element.fileupload(settings);

                // Set callback handlers
                fileUpload.on('fileuploadprocessalways', function (e, data) {
                    if (configuration.fileAddedHandler) {
                        var selectedFile = data.files[data.index];
                        var file = {
                            name: selectedFile.name,
                            size: formatFileSize(selectedFile.size),
                            isValid: !selectedFile.error,
                            errorMessage: selectedFile.error
                        };
                        configuration.fileAddedHandler(file);
                    }
                });

                fileUpload.on('fileuploadadd', function (e, data) {
                    // Callback on start of (multiple) file upload
                    if (configuration.uploadStartHandler) {
                        configuration.uploadStartHandler(data.response().result);
                    }
                });

                fileUpload.on('fileuploadsend', function (e, data) {
                    var doUpload = true;
                    for (var i = 0; i < data.files.length; i++) {
                        var file = data.files[i];
                        if (file.error) {
                            //upload = false; What is upload?
                            break;
                        }
                    }
                    return doUpload;
                });

                fileUpload.on('fileuploadprogressall', function (e, data) {
                    if (configuration.progressPercentageHandler) {
                        configuration.progressPercentageHandler(parseInt(data.loaded / data.total * 100, 10));
                    }
                    if (configuration.progressUploadSpeedHandler) {
                        configuration.progressUploadSpeedHandler(formatBitrate(data.bitrate));
                    }
                    if (configuration.progressRemainingTimeHandler) {
                        configuration.progressRemainingTimeHandler(formatTime((data.total - data.loaded) * 8 / data.bitrate));
                    }
                    if (configuration.progressRemainingFileSizeHandler) {
                        configuration.progressRemainingFileSizeHandler(formatFileSize(data.loaded));
                    }
                    if (configuration.progressUploadedFileSizeHandler) {
                        configuration.progressUploadedFileSizeHandler(formatFileSize(data.total));
                    }
                });

                fileUpload.on('fileuploaddone', function (e, data) {
                    if (configuration.uploadDoneHandler) {
                        // Check response type for IE support
                        var responseContentType = data.response().jqXHR.getResponseHeader('Content-type');

                        if (!responseContentType || responseContentType.indexOf('application/json') === -1) {
                            try {
                                // Browser doesn't support XHR request for file upload
                                var response = data.response();
                                var jsonText;

                                if (response.result instanceof Object) {
                                    // Get JSON text from inner node
                                    jsonText = response.result[0].childNodes[1].innerText;
                                }
                                else {
                                    jsonText = response.result;
                                }
                                // Pass data as true JSON
                                configuration.uploadDoneHandler(jQuery.parseJSON(jsonText));
                            }
                            catch (err) {
                                console.error('Error during file upload response handling: ' + err);
                            }
                        }
                        else {
                            configuration.uploadDoneHandler(data.response().result);
                        }
                    }
                });
            };

            // Initialization
            var bindings = allBindings();

            var fileUploadElement = $(element);
            var labelElement = fileUploadElement.find('.fileUploadLabel');
            var inputElement = fileUploadElement.find('.fileUploadInput');
            var descriptionElement = fileUploadElement.find('.fileUploadDescription');

            attachFileUpload(fileUploadElement, labelElement, inputElement, descriptionElement, {
                'url': bindings.url(),
                'allowedExtensions': bindings.allowedExtensions,
                'maxFileSize': bindings.maxFileSize,
                'singleFileMode': bindings.singleFileMode,
                'singleUploadLabel': bindings.singleUploadLabel,
                'multiUploadLabel': bindings.multiUploadLabel,
                'fileSizeDescription': bindings.fileSizeDescription,
                'extensionDescription': bindings.extensionDescription,
                'fileSizeValidationMessage': bindings.fileSizeValidationMessage,
                'extensionValidationMessage': bindings.extensionValidationMessage,
                'fileAddedHandler': function (file) {
                    valueAccessor().push(file);
                },
                'uploadStartHandler': function (data) {
                    if (bindings.uploadStartHandler) {
                        bindings.uploadStartHandler(data);
                    }
                },
                'uploadDoneHandler': function (data) {
                    if (bindings.uploadDoneHandler) {
                        bindings.uploadDoneHandler(data);
                    }
                },
                'progressPercentageHandler': function (data) {
                    if (bindings.progressPercentageHandler) {
                        bindings.progressPercentageHandler(data);
                    }
                }
            });
        }
    };

    ko.bindingHandlers.bqSearchField = {
        init: function (element, valueAccessor, allBindings, viewModel) {
            $(element).select2({
                width: 'resolve',
                tags: [],
                formatNoMatches: '',
                dropdownCssClass: 'select2-hidden',
                initSelection: function (element, callback) {
                    var data = [];
                    $(element.val().split(',')).each(function () {
                        if (this.trim().length > 0) { // Ignore empty strings
                            data.push({ id: this.valueOf(), text: this.valueOf() });
                        }
                    });
                    callback(data);
                }
            }).on('change', function (val) {
                // Handle add
                if (val.added) {
                    var newValue = valueAccessor()().concat();
                    if (val.added.text.trim().length > 0) {
                        newValue[valueAccessor()().length] = val.added.text.trim();
                        valueAccessor()(newValue);
                    }
                    else {
                        // Empty string detected
                        val.val.splice(val.val.length - 1); // Remove last inserted value
                    }
                }
                // Handle remove
                if (val.removed) {
                    var index = valueAccessor()().indexOf(val.removed.text);
                    if (index > -1) {
                        valueAccessor()().splice(index, 1);
                    }
                }
                viewModel.search();
            }).select2('val', valueAccessor()()); // Trigger initSelection callback

            $('.select2-search-field > input.select2-input').on('keyup', function (e) {
                if (e.target.value.trim().length === 0) { // TODO fix
                    element.value = '';
                    e.target.value = '';
                    // TODO also set value from source element to empty
                }
                return true;
            });
        }
    };

    /**
	 * This handler generates a switch box (http://www.bootstrap-switch.org/) for a domain with two values.
	 */
    ko.bindingHandlers.bqToggle = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            $(element).bootstrapSwitch()
                .on('switch-change', function (event, data) {
                if (data.value) {
                    valueAccessor()(viewModel.domain()[0].value);
                } else {
                    valueAccessor()(viewModel.domain()[1].value);
                }
            })
            .bootstrapSwitch('setOnLabel', viewModel.domain()[0].displayValue)
            .bootstrapSwitch('setOffLabel', viewModel.domain()[1].displayValue);

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            $(element).bootstrapSwitch('setState', viewModel.domain()[0].value === viewModel.value());
        }
    };

    /**
	 * This handler creates toggle functionality for stacktraces
	 */
    ko.bindingHandlers.bqStackTrace = {
        init: function (element, valueAccessor) {
            $(element).click(function () {
                $('#' + valueAccessor()).toggle();
                if ($('#' + valueAccessor()).is(':hidden')) {
                    $(element).removeClass('icon-chevron-down');
                    $(element).addClass('icon-chevron-right');
                } else {
                    $(element).removeClass('icon-chevron-right');
                    $(element).addClass('icon-chevron-down');
                }
            });
        }
    };

    /**
	 * This handler creates a tooltip for an iconButton with the caption
	 */
    ko.bindingHandlers.bqIconTooltip = {
        init: function (element, valueAccessor) {
            if (!valueAccessor()()) {
                return;
            }
            var placement;
            if (!placement) {
                placement = 'bottom';
            }
            var text = ko.unwrap(valueAccessor());
            $(element).tooltip({
                title: text,
                placement: placement
            });
        }
    };

    /**
	 * This handler creates a datetimepicker.
	 */
    ko.bindingHandlers.bqDateTimePicker = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var datetimeformat = viewModel.context.session.language.patterns.date.toUpperCase() + ' HH:mm:ss';
            $(element).datetimepicker({
                format: datetimeformat,
                pick12HourFormat: false,
                sideBySide: true,
                useSeconds: true
            }).on('change', function (ev) {
                var value = $(ev.currentTarget).find('input').val();
                valueAccessor()(value);
            }).on('show', function () {
                if (viewModel.readonly()) {
                    $(element).datetimepicker('hide');
                }
            });
        },
        update: function (element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            if (value) {
                $(element).data('DateTimePicker').setDate(value);
            }
        }
    };

    ko.bindingHandlers.visualizationColors =
    [
        '#3da0ea', // Blueriq-Blue
        '#f377ab', // Blueriq-Pink
        '#707070', // Blueriq-Dark-Gray
        '#A8A8A8', // Blueriq-Light-Gray
        '#e7912a', // Orange
        '#4ec9ce', // Teal
        '#ec7337', // Orange-Red
        '#bacf0b' // Green
    ];

    /**
	 * This handler creates a visualization.
	 */
    ko.bindingHandlers.bqVisualization ={
        init: function(element, valueAccessor, allBindingsAccessor){
            var incomingData = allBindingsAccessor().data;
            var statistics = element.getContext('2d');
            var chartType = allBindingsAccessor().chartType;
            var title = allBindingsAccessor().title;
            var animationEase = 'easeInOutQuad';

            // HTML-elem:   canvas, td,           tr,           td,        div
            var legendDiv = null;//element.parentElement.parentElement.children[1].children[0];

            // HTML-elem:  canvas, td,           tr,           table,        tr,         td,         div
            var titleDiv = null; //element.parentElement.parentElement.parentElement.children[0].children[0].children[0];

            // Retrieving elements
            var parentElement = element.parentElement;
            if ( parentElement !== undefined && parentElement !== null )
            {
                var grandParentElement = parentElement.parentElement;
                if ( grandParentElement !== undefined && grandParentElement !== null )
                {
                    if ( grandParentElement.children.length > 1 )
                    {
                        var uncleElement = grandParentElement.children[1];
                        if ( uncleElement !== undefined && uncleElement !== null ) {
                            if (uncleElement.children.length > 0) {
                                legendDiv = uncleElement.children[0];
                            }
                        }
                    }

                    var greatGrandParentElement = grandParentElement.parentElement;
                    if ( greatGrandParentElement !== undefined && greatGrandParentElement !== null ){
                        if ( greatGrandParentElement.children.length > 0 ){
                            var grandUncleElement = greatGrandParentElement.children[0];
                            if ( grandUncleElement !== undefined && grandUncleElement !== null ){
                                if ( grandUncleElement.children.length > 0 ) {
                                    var secondUncle = grandUncleElement.children[0];
                                    if (secondUncle !== undefined && secondUncle !== null) {
                                        if (secondUncle.children.length > 0) {
                                            titleDiv = secondUncle.children[0];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (titleDiv !== null) {
                if (title !== null) {
                    titleDiv.innerHTML = title;
                } else {
                    titleDiv.innerHTML = 'Chart';
                }
            }

            // Creating data sets
            var data = null;
            switch ( chartType ){
                case 'bar':
                case 'line':
                case 'radar':
                    var incomingLabels = [];
                    var incomingPoints = [];
                    for ( var j = 0; j < incomingData.length ; j += 1){
                        incomingLabels.push(incomingData[j][0]);
                        incomingPoints.push(incomingData[j][1]);
                    }
                    data = {
                        labels : incomingLabels,
                        datasets : [
                        {
                            fillColor : ko.bindingHandlers.visualizationColors[0],
                            strokeColor : ko.bindingHandlers.visualizationColors[2],
                            pointColor : ko.bindingHandlers.visualizationColors[5],
                            pointStrokeColor : ko.bindingHandlers.visualizationColors[3],
                            data : incomingPoints
                        }]
                    };
                    break;
                case 'pie':
                case 'polar':
                case 'doughnut':
                    data = [];
                    for ( var i = 0 ; i < incomingData.length ; i += 1){
                        data.push(
                        {
                            color : ko.bindingHandlers.visualizationColors[i % 6],
                            label : incomingData[i][0],
                            value : incomingData[i][1]
                        });
                    }
                    break;
            }

            // Creating charts
            var myChart = null;
            switch ( chartType ){
                case 'line':
                    myChart = new chartJs.Chart(statistics).Line(data);
                    break;
                case 'pie':
                    myChart = new chartJs.Chart(statistics).Pie(data, {
                        animateScale: true,
                        responsive: true,
                        animationEasing: animationEase
                    });
                    if (legendDiv !== null) {
                        chartJs.legend(legendDiv, data, myChart);
                    }
                    break;
                case 'bar':
                    myChart = new chartJs.Chart(statistics).Bar(data);
                    break;
                case 'polar':
                    myChart = new chartJs.Chart(statistics).PolarArea(data, {
                        animationEasing: animationEase
                    });
                    if (legendDiv !== null) {
                        chartJs.legend(legendDiv, data, myChart);
                    }
                    break;
                case 'radar':
                    myChart = new chartJs.Chart(statistics).Radar(data);
                    break;
                case 'doughnut':
                    myChart = new chartJs.Chart(statistics).Doughnut(data, {
                        animateScale: true,
                        animationEasing: animationEase });
                    if (legendDiv !== null) {
                        chartJs.legend(legendDiv, data, myChart);
                        var container = greatGrandParentElement.parentElement.parentElement;
                        if (container.offsetWidth < greatGrandParentElement.offsetWidth) {
                        	//move legend bellow the chart
                        	$(greatGrandParentElement.insertRow(2)).append(legendDiv.parentElement);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    };

    /**
     * This handler creates a odometer.
     */
    ko.bindingHandlers.bqStatistic =
    {
        init: function(element, valueAccessor, allBindingsAccessor){
            odometerJs.odometerOptions =
            {
                auto: false
            };
            var odometerElement = element;
            var statistic = allBindingsAccessor().data;

            new odometerJs.Odometer(
                {
                    el: odometerElement,
                    value: statistic,
                    format: 'd',
                    theme: 'plaza',
                    duration: 3000,
                    animation: 'count'
                });
        }
    };

    /**
	 * Drag/drop target for fileupload
	 *
	 * source: https://github.com/blueimp/jQuery-File-Upload/wiki/Drop-zone-effects
	 */
    $(document).bind('dragover', function (e) {
        var dropZone = $('.dropzone'), foundDropzone, timeout = window.dropZoneTimeout;
        if (!timeout) {
            dropZone.addClass('in');
        } else {
            clearTimeout(timeout);
        }
        var found = false, node = e.target;

        do {
            if ($(node).hasClass('dropzone')) {
                found = true;
                foundDropzone = $(node);
                break;
            }
            node = node.parentNode;
        } while (node !== null);

        dropZone.removeClass('in hover');

        if (found) {
            foundDropzone.addClass('hover');
        }

        window.dropZoneTimeout = setTimeout(function () {
            window.dropZoneTimeout = null;
            dropZone.removeClass('in hover');
        }, 100);
    });

})(window.ko, jsScopes.chartJs, jsScopes.odometerJs);

(function (blueriq, bqApp) {
    'use strict';

    /**
     * Register default templates for elements
     */
    blueriq.defaultModelHandler = function (viewModel, context) {

        var prefix = (viewModel.context.configuration.baseId ? viewModel.context.configuration.baseId : 'core') + '/template/';
        var statisticPrefix = prefix + 'statistic/';
        var containerPrefix = prefix + 'container/';

        if (viewModel.type === 'table') {
            return containerPrefix + 'table/table';
        } else if (viewModel.model.contentStyle === 'tablecell') {
            if (viewModel.displayName()) {
                return containerPrefix + 'table/tabletextcell';
            } else {
                return containerPrefix + 'table/tablecell';
            }
        } else if (viewModel.type === 'tablesearch') {
            return containerPrefix + 'table/tablesearch';
        } else if (viewModel.type === 'tablesortedheader') {
            return containerPrefix + 'table/tablesortedheader';
        } else if (viewModel.type === 'tablenavigation') {
            return containerPrefix + 'table/tablenavigation';
        } else if (viewModel.type === 'visualization') {
            return statisticPrefix + 'visualization';
        }
        else if (viewModel.type === 'container') {
            if (viewModel.contentStyle.lastIndexOf('table', 0) === 0) {
                return containerPrefix + 'table/' + viewModel.contentStyle;
            } else if (viewModel.contentStyle === 'externalcontent') {
                return containerPrefix + 'externalcontent';
            } else if (viewModel.contentStyle === 'breadcrumbcontainer') {
                return containerPrefix + 'breadcrumb';
            } else if (viewModel.contentStyle === 'timeline') {
                return containerPrefix + 'timeline';
            }
            else if (viewModel.contentStyle === 'statistic') {
                return statisticPrefix + 'statistic';
            }
            else if (viewModel.contentStyle === 'menubar') {
                return containerPrefix + 'menubar';
            } else if (viewModel.contentStyle === 'instance_linker') {
                return containerPrefix + 'instancelinker';
            } else if (viewModel.contentStyle === 'fileupload') {
                return containerPrefix + 'fileupload';
            } else if (viewModel.contentStyle === 'filedownload') {
                return containerPrefix + 'filedownload';
            } else if (viewModel.contentStyle === 'tabs') {
                return containerPrefix + 'tabs';
            } else if (viewModel.contentStyle === 'formfooter') {
                return containerPrefix + 'footer';
            }

        } else if (viewModel.type === 'textitem') {
            if (jQuery.inArray('authenticated_user', viewModel.model.styles) > -1) {
                return prefix + 'textitem/authenticateduser';
            } else if (jQuery.inArray('logout_link', viewModel.model.styles) > -1) {
                return prefix + 'textitem/logout';
            } else if (jQuery.inArray('styled', viewModel.model.styles) > -1) {
                return prefix + 'textitem/textitemstyled';
            }
        }

        if (viewModel.type === 'instancelist') {
            return containerPrefix + 'instancelist';
        }

        if (viewModel.type === 'filetype') {
            return prefix + 'field/filetypefield';
        }

        if (viewModel.type === 'field') {
            if (jQuery.inArray('fieldValueOnly', context.modus) > -1) {
                if (viewModel.readonly()) {
                    return prefix + 'field/readonlyvalue';
                } else {
                    return prefix + 'field/value';
                }
            } else if (viewModel.readonly()) {
                return prefix + 'field/readonly';
            } else {
                return prefix + 'field/field';
            }
        }

        if (viewModel.type === 'button') {
            /* jscs:disable requireCamelCaseOrUpperCaseIdentifiers */
            if (viewModel.presentationStyles().isIcon) {
                if (viewModel.presentationStyles().only_icon) {
                    return prefix + 'button/onlyiconbutton';
                } else {
                    return prefix + 'button/iconbutton';
                }
            } else {
                if (viewModel.presentationStyles().button_link) {
                    return prefix + 'button/buttonlink';
                } else if (viewModel.presentationStyles().button_primary) {
                    return prefix + 'button/buttonprimary';
                }
            }

            /* jscs:enable requireCamelCaseOrUpperCaseIdentifiers */
        }

        if (viewModel.type === 'container') {
            if (!viewModel.displayName()) {
                return prefix + 'container/plaincontainer';
            }
        }

        //default element type as template
        return prefix + viewModel.type + '/' + viewModel.type;
    };

    /**
     * Register default templates for field values
     */
    blueriq.defaultFieldValueHandler = function (viewModel) { //,context
        /* jscs:disable requireCamelCaseOrUpperCaseIdentifiers */

        var prefix = (viewModel.context.configuration.baseId ? viewModel.context.configuration.baseId : 'core') + '/template/field/';

        if (viewModel.multiValued) {
            if (viewModel.presentationStyles().options_horizontal) {
                return prefix + 'checkboxhorizontal';
            } else if (viewModel.presentationStyles().options_vertical) {
                return prefix + 'checkboxvertical';
            }
        } else {
            if (viewModel.presentationStyles().toggle && viewModel.domain() && viewModel.domain().length === 2) {
                return prefix + 'toggle';
            } else if (viewModel.presentationStyles().options_vertical) {
                return prefix + 'radiovertical';
            } else if (viewModel.presentationStyles().options_horizontal) {
                return prefix + 'radiohorizontal';
            }
        }

        if (viewModel.hasDomain) {
            if (viewModel.multiValued) {
                return prefix + 'domainmulti';
            }
            return prefix + 'domainsingle';
        }

        if (viewModel.dataType === 'text') {
            if (viewModel.presentationStyles().memo) {
                return prefix + 'memo';
            } else if (viewModel.presentationStyles().password) {
                return prefix + 'password';
            }
        }

        return prefix + viewModel.dataType;


        /* jscs:enable requireCamelCaseOrUpperCaseIdentifiers */
    };

    /**
     * Register automatically if bqApp is already created
     */
    if (bqApp) {
        bqApp.templateFactory.registerModelHandler(blueriq.defaultModelHandler);
        bqApp.templateFactory.registerFieldValueHandler(blueriq.defaultFieldValueHandler);
    }
}(window.blueriq, window.bqApp));(function (blueriq, bqApp, ko) {
    'use strict';

    /**

Default modelFactory for the default Blueriq view models.
     */

    blueriq.defaultModelFactory = function (model, context) {

        /* 'special' element models */
        if (model.type === 'field' && jQuery.inArray('filetype', model.styles) > -1) {
            return new blueriq.models.FileTypeModel(model, context);
        }

        /* generic container models */
        if (model.contentStyle === 'table') {
            return new blueriq.models.TableModel(model, context);
        } else if (model.contentStyle === 'tablesearch') {
            return new blueriq.models.TableSearchModel(model, context);
        } else if (model.contentStyle === 'tablesortedheader') {
            return new blueriq.models.TableSortedHeaderModel(model, context);
        } else if (model.contentStyle === 'tablenavigation') {
            return new blueriq.models.TableNavigationModel(model, context);
        } else if (model.contentStyle === 'fileupload') {
            return new blueriq.models.FileUploadModel(model, context);
        } else if (model.contentStyle === 'filedownload') {
            return new blueriq.models.FileDownloadModel(model, context);
        } else if (model.contentStyle === 'instancelist') {
            return new blueriq.models.InstanceListModel(model, context);
        } else if (model.contentStyle === 'tabs') {
            return new blueriq.models.TabContainerModel(model, context);
        } else if(model.contentStyle === 'visualization') {
            return new blueriq.models.VisualizationModel(model, context);
        } else if(model.contentStyle === 'statistic' ) {
            return new blueriq.models.StatisticModel(model, context);
        } else if (model.type === 'container') {
            return new blueriq.models.ContainerModel(model, context);
        }

        /* generic element models*/
        if (model.type === 'page') {
            return new blueriq.models.PageModel(model, context);
        } else if (model.type === 'button') {
            return new blueriq.models.ButtonModel(model, context);
        } else if (model.type === 'contentitem') {
            return new blueriq.models.ContentItemModel(model, context);
        } else if (model.type === 'field') {
            return new blueriq.models.FieldModel(model, context);
        } else if (model.type === 'image') {
            return new blueriq.models.ImageModel(model, context);
        } else if (model.type === 'asset') {
            return new blueriq.models.AssetModel(model, context);
        } else if (model.type === 'textitem') {
            return new blueriq.models.TextitemModel(model, context);
        } else if (model.type === 'breadcrumb') {
            return new blueriq.models.BreadcrumbModel(model, context);
        } else if (model.type === 'link') {
            return new blueriq.models.LinkModel(model, context);
        } else if (model.type === 'failedelement') {
            return new blueriq.models.FailedElementModel(model, context);
        } else {
            return new blueriq.models.UnknownModel(model, context);
        }
    };

    /**
     * Register automatically if bqApp is already created
     */
    if (bqApp) {
        bqApp.modelFactory.register(blueriq.defaultModelFactory);
    }

    /**
     * Represents the base model for all blueriq model based viewmodels.
     *
     * @constructor
     * @name blueriq.models.BaseModel
     * @param {Object} model
     * @param {Object} context
     * @returns A BaseModel object.
     */
    blueriq.models.BaseModel = function BaseModel(model, context) {

        if (!model) {
            throw new Error('model is mandatory');
        }
        if (!context) {
            throw new Error('context is mandatory');
        }

        var self = this;

        function getIcon(styles) {
            for (var i = 0; i < styles.length; ++i) {
                if (styles[i].indexOf('icon_') === 0) {
                    return styles[i].replace(new RegExp('_', 'g'), '-');
                }
            }
        }

        function getStyles(styles) {
            var result = {};
            if (styles) {
                for (var i = 0; i < styles.length; ++i) {
                    result[styles[i]] = true;
                }
                result.icon = getIcon(styles);
                result.isIcon = result.icon ? true : false;
                result.joined = styles.join(' ');
            }
            return result;
        }

        this.key = model.key;
        this.model = model;
        this.context = context;

        this.presentationStyles = ko.observable(getStyles(model.styles));
        var subscription;
        if (self.key) {
            subscription = this.context.session.subscribeModel(self.key, function onChange(type, model) {
                if (type === 'update') {
                    if (self.update) {
                        self.update(model);
                    }
                    self.model = model;
                    self.presentationStyles(getStyles(model.styles));
                } else if (type === 'delete') {
                    if (self.dispose) {
                        self.dispose();
                    }
                    subscription.dispose();
                }
            });
        } else {
            subscription = this.context.session.subscribe(function onChange(type, key, model) {
                if (type === 'update' && self.key === key) {
                    if (self.update) {
                        self.update(model);
                    }
                    self.model = model;
                    self.presentationStyles(getStyles(model.styles));
                } else if (type === 'delete' && self.key === key) {
                    if (self.dispose) {
                        self.dispose();
                    }
                    subscription.dispose();
                }
            });
        }

        /**
         * Gets the API URI for the configuration present on this basemodel.
         *
         * @function
         * @name blueriq.models.BaseModel#getApiUri
         * @returns {String} API URI
         */
        this.getApiUri = function getApiUri() {
            return context.configuration.baseUri + context.session.id + '/api/';
        };

        /**
         * Submits the current page.
         *
         * @function
         * @name blueriq.models.BaseModel#submit
         * @param {String} trigger Key of triggering element.
         * @param {Object} parameters The optional set of parameters.
         */
        this.submit = function submit(trigger, parameters) {
            context.messageBus.notify('beforeSubmit', context.session.id);
            context.session.submit(trigger, parameters, function () {
                context.messageBus.notify('afterSubmit', context.session.id);
            });
        };

        /**
         * Recompose the current page.
         *
         * @function
         * @name blueriq.models.BaseModel#recompose
         */
        this.recompose = function recompose() {
            context.messageBus.notify('beforeRecompose', context.session.id);
            context.session.recompose(function () {
                context.messageBus.notify('afterRecompose', context.session.id);
            });
        };

        /**
         * Short method for the blueriq.TemplateFactory#getTemplate method that
         * returns the id/url of the template given the view model and context.
         *
         * @function
         * @name blueriq.model.BaseModel#template
         * @param {blueriq.models.BaseModel} data viewModel The model to retrieve a template for, must be specified.
         * @param {Object} context bindingContext binding context is an object that holds data that you can reference from your bindings
         * @returns {String} The url/d of the template.
         */
        this.template = function getTemplate(data, context) {
            return self.context.templateFactory.getTemplate(data, context);
        };

        /**
         * Short method for the blueriq.TemplateFactory#getFieldValueTemplate method that
         * returns the id/url of the template given the view model and context.
         *
         * @function
         * @name blueriq.model.BaseModel#fieldValueTemplate
         * @param {blueriq.models.BaseModel} data viewModel The field model to retrieve a value template for, must be specified.
         * @param {Object} context bindingContext binding context is an object that holds data that you can reference from your bindings
         * @returns {String} The url/d of the template.
         */
        this.fieldValueTemplate = function getFieldValueTemplate(data, context) {
            return self.context.templateFactory.getFieldValueTemplate(data, context);
        };

        /**
         * Checks whether or not the specified presentation style is present on this model.
         *
         * Use: presentationStyles().NAME instead for better performance
         *
         * @function
         * @name blueriq.models.BaseModel#hasPresentationStyle
         * @param {String} presentationStyle The presentation style to check for.
         */
        this.hasPresentationStyle = function hasPresentationStyle(presentationStyle) {
            if (presentationStyle) {
                for (var i = 0; i < this.models.styles.length; i++) {
                    if (presentationStyle === this.models.styles[i]) {
                        return true;
                    }
                }
            }
            return false;
        };
    };

    /**
     * Represents an unknown model.
     *
     * @constructor
     * @name blueriq.models.UnknownModel
     * @param {Object} model The unknown model.
     * @param {Object} context The data context.
     * @returns And UnknownModel instance.
     */
    blueriq.models.UnknownModel = function UnknownModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'unknown';
    };

    /**
     * Represents a container.
     *
     * @constructor
     * @name blueriq.models.ContainerModel
     * @param {Object} model The container model.
     * @param {Object} context The data context.
     * @returns An container viewmodel instance.
     */
    blueriq.models.ContainerModel = function ContainerModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(this, model, context);

        this.type = 'container';
        var childModels = {};

        this.contentStyle = model.contentStyle;
        this.displayName = ko.observable(model.displayName);
        this.children = ko.observableArray(null);
        this.properties = ko.observable(model.properties);
        { // initialize children
            var result = [];
            for (var i = 0; i < model.children.length; ++i) {
                var key = model.children[i];
                var dataModel = context.session.getModel(key);
                if (!dataModel) {
                    throw new Error('Illegal state: expected to have a model with key ' + key);
                }
                var viewModel = context.modelFactory.createViewModel(dataModel, context);
                result.push(viewModel);
                childModels[key] = viewModel;
            }
            this.children(result);
        }

        /**
         * Updates this container viewmodel properties.
         *
         * @function
         * @name blueriq.models.ContainerModel#update
         */
        this.update = function update(model) {
            self.displayName(model.displayName);
            self.properties(model.properties);

            var currentChildren = self.model.children;
            var newChildren = model.children;

            // Set/Order children
            var children = [];
            var key, viewModel, dataModel, i;
            for (i = 0; i < newChildren.length; ++i) {
                key = newChildren[i];
                viewModel = childModels[key];
                if (!viewModel) {
                    dataModel = context.session.getModel(key);
                    viewModel = context.modelFactory.createViewModel(dataModel, context);
                    childModels[key] = viewModel;
                }
                children.push(viewModel);
            }

            // Remove Obsolete children
            for (i = 0; i < currentChildren.length; ++i) {
                key = currentChildren[i];
                if (newChildren.indexOf(key) < 0) {
                    delete childModels[key];
                }
            }
            self.children(children); // single set (otherwise it will have multiple notifications);
        };
    };

    /**
     * Represents a page.
     *
     * @constructor
     * @name blueriq.models.PageModel
     * @param {Object} model The Page model.
     * @param {Object} context The data context.
     * @returns A Page Instance instance.
     */
    blueriq.models.PageModel = function PageModel(model, context) {

        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);

        this.type = 'page';

        this.messages = ko.observableArray(model.messages);
        var baseUpdate = self.update;
        this.update = function PageUpdate(model) {
            baseUpdate(model);
            self.messages(model.messages);
        };
    };

    /**
     * Represents a breadcrumb.
     *
     * @constructor
     * @name blueriq.models.BreadcrumbModel
     * @param {Object} model The breadcrumb model.
     * @param {Object} context The data context.
     * @returns A Breadcrumb instance.
     */
    blueriq.models.BreadcrumbModel = function BreadcrumbModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'breadcrumb';

        this.properties = ko.observable(model.properties);

        this.handleClick = function () {
            if (!self.properties().isPassed) {
                return false;
            }
            // Start the flow
            var flowName = self.properties().flowName;
            context.session.startFlow(flowName);
        };

        this.update = function BreadCrumbModelUpdate(model) {
            self.properties(model.properties);
        };
    };

    /**
     * Represents a button.
     *
     * @constructor
     * @name blueriq.models.ButtonModel
     * @param {Object} model The button model.
     * @param {Object} context The data context.
     * @returns A ButtonModel instance.
     */
    blueriq.models.ButtonModel = function ButtonModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'button';
        this.icon = ko.observable();
        this.caption = ko.observable(model.caption);

        this.disabled = ko.observable(model.disabled);

        this.handleClick = function ButtonModelClick() {
            if (!self.disabled()) {
                self.submit(model.key);
            }
        };
        this.update = function ButtonModelUpdate(model) {
            self.caption(model.caption);
            self.disabled(model.disabled);
        };
    };

    /**
     * Represents a content item.
     *
     * @constructor
     * @name blueriq.models.ContentItemModel
     * @param {Object} model The contentitem model.
     * @param {Object} context The data context.
     * @returns A ContentItem instance.
     */
    blueriq.models.ContentItemModel = function ContentItemModel(model, context) {

        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);
        this.type = 'contentitem';

        //strip empty textitems which pollute the model
        for(var i = this.children().length-1 ; i >= 0 ; i--){
            console.log(this.children()[i].type);
            if(this.children()[i].type === 'textitem' && this.children()[i].plainText() === null){
                this.children().splice(i,1);
            }
        }
    };
    /**
     * Represents a field.
     *
     * @constructor
     * @name blueriq.models.FieldModel
     * @param {Object} model The viewmodel.
     * @param {Object} context The data context.
     * @returns A FieldModel instance.
     */
    blueriq.models.FieldModel = function FieldModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'field';

        function hasError(model) {
            for (var i = 0; i < model.messages.length; ++i) {
                if (model.messages[i].type === 'ERROR') {
                    return true;
                }
            }
            return false;
        }

        function hasWarning(model) {
            for (var i = 0; i < model.messages.length; ++i) {
                if (model.messages[i].type === 'WARNING') {
                    return true;
                }
            }
            return false;
        }

        this.dataType = model.dataType;
        this.hasDomain = model.hasDomain;
        this.multiValued = model.multiValued;
        this.maxLength = model.displayLength > 1 ? model.displayLength : null;

        this.domain = ko.observableArray(model.domain);
        this.questionText = ko.observable(model.questionText);
        this.explainText = ko.observable(model.explainText);
        this.hasWarning = ko.observable(hasWarning(model));
        this.hasError = ko.observable(hasError(model));
        this.required = ko.observable(model.required);
        this.readonly = ko.observable(model.readonly);
        this.messages = ko.observableArray(model.messages);
        this.validations = ko.observableArray(model.validations);

        this.value = ko.observable(model.values[0]);

        function formatDisplayValue(value) {
            if (!value) {
                return '';
            }
            if (self.model.dataType === 'currency') {
                return '€ ' + value;
            } else if (self.model.dataType === 'percentage') {
                return value + ' %';
            } else {
                return value;
            }
        }
        function getDisplayValue() {
            var result;
            if (!self.multiValued) {
                if (self.hasDomain) {
                    return getDisplayValueFromDomain(self.model.values[0]);
                } else {
                    return formatDisplayValue(self.model.values[0]);
                }
            } else {
                result = '';
                for (var i = 0; i < self.model.values.length; i++) {
                    if (self.hasDomain) {
                        if(result !== ''){
                            result += ', ';
                        }
                        result += getDisplayValueFromDomain(self.model.values[i]);

                    } else {
                        if (result !== '') {
                            result += ', ';
                        }
                        result += formatDisplayValue(self.model.values[i]);
                    }
                }
            }
            return result;
        }
        function getDisplayValueFromDomain(value) {
            for (var j = 0; j < self.domain().length; j++) {
                if (self.domain()[j].value === value) {
                    var displayValue = self.domain()[j].displayValue;
                    if (displayValue) {
                        return self.domain()[j].displayValue;
                    } else {
                        return formatDisplayValue(self.domain()[j].value);
                    }
                }
            }
        }
        this.displayValue = ko.observable(getDisplayValue());

        var valueSubscription = this.value.subscribe(function (newValue) {
            self.model.values[0] = newValue;
            self.handleChange();
        });

        this.values = ko.observableArray(model.values);
        var valuesSubscription = this.values.subscribe(function (newValue) {
        	if(newValue.length==0) {
        		//AQ-6416
        		self.model.values = ['']
        	}else{
        		self.model.values = newValue;
        	}     
            self.displayValue(getDisplayValue());
            self.handleChange();
        });

        this.valueAsBoolean = ko.computed({
            owner: self,
            read: function () {
                return this.value() === 'true';
            },
            write: function (newValue) {
                this.value(newValue ? 'true' : 'false');
            }
        });

        /**
         * Handles the refresh of the field.
         */
        this.handleChange = function handleChange() {
            if (self.model.refresh && !self.model.readonly && !self.updating) {
                self.submit(self.model.key);
            }
        };
        self.updating = false;
        this.update = function update(model) {
            self.updating = true; // variable to check whether not submit while updating the model
            self.questionText(model.questionText);
            self.explainText(model.explainText);
            self.domain(model.domain);
            self.hasWarning(hasWarning(model));
            self.hasError(hasError(model));
            self.values(model.values);
            if(model.values.length == 0) {
            	//AQ-6416
            	model.values[0] = '';
            	self.value('');
        	}else{
        		self.value(model.values[0]);
        	}
            self.required(model.required);
            self.readonly(model.readonly);
            self.messages(model.messages);
            self.validations(model.validations);
            self.updating = false;
        };

        this.dispose = function () {
            valueSubscription.dispose();
            valuesSubscription.dispose();
        };
        
        if(!self.multiValued && !self.displayValue()){
        	self.model.values[0] = '';
        }
    };

    /**
     * Represents the Image Model.
     *
     * @constructor
     * @name blueriq.models.ImageModel
     * @param {Object} model The image model.
     * @param {Object} context The data context.
     * @returns An ImageModel instance.
     */
    blueriq.models.ImageModel = function ImageModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'image';

        this.url = this.getApiUri() + 'image/' + model.name + '/key/' + model.key;
        this.height = model.height;
        this.width = model.width;
    };

    /**
     * Represents the Link Model.
     *
     * @constructor
     * @name blueriq.models.LinkModel
     * @param {Object} model The link model.
     * @param {Object} context The data context.
     * @returns A LinkModel instance.
     */
    blueriq.models.LinkModel = function ImageModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'link';

        this.url = this.getApiUri() + 'document/' + model.parameters['document-type'] + '/' + model.parameters['document-name'] + '/' + model.parameters['page-name'];

        this.text = ko.observable(model.text);

        this.update = function LinkModelUpdate(model) {
            self.text(model.text);
        };
    };

    /**
     * Represents an asset.
     *
     * @constructor
     * @name blueriq.models.AssetModel
     * @param {Object} model The asset model.
     * @param {Object} context The data context.
     * @returns An AssetModel instance.
     */
    blueriq.models.AssetModel = function AssetModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'asset';

        this.text = ko.observable(model.text);

        this.update = function AssetModelUpdate(model) {
            self.text(model.text);
        };
    };

    /**
     * Represents a textitem.
     *
     * @constructor
     * @name blueriq.models.TextitemModel
     * @param {Object} model The textitem model.
     * @param {Object} context The data context.
     * @returns A TextItemModel instance.
     */
    blueriq.models.TextitemModel = function TextItemModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);
        this.type = 'textitem';
        this.plainText = ko.observable(model.plainText);
        this.nodes = ko.observableArray(model.nodes);

        this.update = function TextItemModelUpdate(model) {
            self.nodes(model.nodes);
            self.plainText(model.plainText);
        };
    };

    /**
     * Represents a failed element.
     *
     * @constructor
     * @name blueriq.models.FailedElementModel
     * @param {Object} model The failed element model.
     * @param {Object} context The data context.
     * @returns A FailedElementModel instance.
     */
    blueriq.models.FailedElementModel = function FailedElementModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);
        this.type = 'failedelement';

        this.message = model.message;
        this.stackTrace = model.stackTrace;
    };

    blueriq.models.TableModel = function TableModel(model, context) {

        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);
        this.type = 'table';

        if (model.children.length > 0) {
            this.hasHeader = context.session.getModel(model.children[0]).name === 'header';
        } else {
            this.hasHeader = false;
        }

        if (this.hasHeader) {
            this.header = this.children()[0];
            this.rows = ko.computed(function () {
                return self.children().slice(1);
            });
        } else {
            this.header = null;
            this.rows = ko.computed(function () {
                return self.children();
            });
        }
    };

    blueriq.models.TableNavigationModel = function TableNavigationModel(model, context) {

        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);
        this.type = 'tablenavigation';

        this.first = this.children()[0];
        this.previous = this.children()[1];
        this.selector = this.children()[2];
        this.next = this.children()[3];
        this.last = this.children()[4];
    };

    blueriq.models.TableSortedHeaderModel = function TableSortedHeaderModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);
        this.type = 'tablesortedheader';

        function isSortedAscending(model) {
            return jQuery.inArray('ascending', model.styles) > 0;
        }

        function isSortedDescending(model) {
            return jQuery.inArray('descending', model.styles) > 0;
        }

        var contentModel = context.session.getModel(model.children[0]);
        this.content = ko.observable(context.modelFactory.createViewModel(contentModel, context));

        // the sort button may not be created if the header is hidden
        if (model.children[1]) {
            var buttonModel = context.session.getModel(model.children[1]);
            this.sortedAscending = ko.observable(isSortedAscending(buttonModel));
            this.sortedDescending = ko.observable(isSortedDescending(buttonModel));

            this.handleClick = function () {
                if (!buttonModel.disabled) {
                    self.submit(buttonModel.key);
                }
            };

            //buttonChangeSubscription
            context.session.subscribe(function (type, key, model) {
                if (type === 'update' && key === buttonModel.key) {
                    self.sortedAscending(isSortedAscending(model));
                    self.sortedDescending(isSortedDescending(model));
                }
            });
        } else {
            // dummy implementations
            this.sortedAscending = ko.observable(true);
            this.sortedDescending = ko.observable(false);
            this.handleClick = function () {};
        }

        this.dispose = function () {
            if (self.buttonChangeSubscription) {
                self.buttonChangeSubscription.dispose();
            }
        };
    };

    /**
     * Represents a search container for a table, which contains a search field and button to perform the actual search.
     *
     * @constructor
     * @name blueriq.models.TableSearchModel
     * @param {Object} model The container model.
     * @param {Object} context The data context.
     * @returns A table search model instance
     */
    blueriq.models.TableSearchModel = function TableSearchModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);
        this.type = 'tablesearch';

        this.searchField = context.modelFactory.createViewModel(context.session.getModel(model.children[0]), context);

        this.searchButton = model.children[1];
        this.disabled = context.session.getModel(model.children[1], context).disabled;
        this.searchTerm = ko.observableArray(this.searchField.values());

        /**
         * Performs a search action with the current search terms.
         *
         * @function
         * @name blueriq.models.TableSearchModel#search
         */
        this.search = function () {
            if (self.disabled) {
                return;
            }
            var parameters = {};
            parameters[self.searchField.model.name] = self.searchTerm();
            self.submit(self.searchButton, parameters);
        };

        this.keyPressed = function (data, event) {
            if (event.keyCode === 13) {
                self.search();
            }
            return true;
        };

        this.keyUp = function () {//data, event
            return true;
        };
    };

    /**
     * Representation for file download container.
     *
     * @constructor
     * @name blueriq.models.FileDownloadModel
     * @param {Object} model The container model.
     * @param {Object} context The data context.
     * @returns A FileDownloadModel instance.
     */
    blueriq.models.FileDownloadModel = function FileDownloadModel(model, context) {

        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);

        /**
         * Download button wrapped by this download container
         *
         * @member {blueriq.models.Button}
         */
        this.buttonViewModel = context.modelFactory.createViewModel(context.session.getModel(model.children[0]), context);
        var unauthorizedKey = model.children[1];
        this.buttonViewModel.handleClick = function ButtonModelClick() {
        	$.ajax({
        		type: 'GET',
        		url: bqApp.configuration.baseUri + context.session.id + '/filedownload/' + self.properties().configurationid + '/checkauthorization',
        		success: function() {
        			window.location = bqApp.configuration.baseUri + context.session.id + '/filedownload/' + self.properties().configurationid;
        		}, 
        		error: function() { 
        			self.submit(unauthorizedKey);
        		} 
        	});
        };
    };

    /**
     * Representation for file upload container.
     *
     * @constructor
     * @name blueriq.models.FileUploadModel
     * @param {Object} model The container model.
     * @param {Object} context The data context.
     * @returns A FileUploadModel instance.
     */
    blueriq.models.FileUploadModel = function FileUploadModel(model, context) {
        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);

        this.url = ko.observable(bqApp.configuration.baseUri + context.session.id + '/subscription/' + context.subscriptionId + '/fileupload/' + self.properties().configurationid + '/');
        this.csrfToken = context.session.csrfToken; // observable
        
        this.singleFileMode = self.properties().singlefilemode;
        this.allowedExtensions = self.properties().allowedextensions;
        this.maxFileSize = self.properties().maxfilesize;

        this.singleUploadLabel = self.properties().singleuploadlabel;
        this.multiUploadLabel = self.properties().multiuploadlabel;
        this.fileSizeDescription = self.properties().filesizedescription;
        this.extensionDescription = self.properties().extensiondescription;
        this.fileSizeValidationMessage = self.properties().filesizevalidationmessage;
        this.extensionValidationMessage = self.properties().extensionvalidationmessage;
        this.uploadSuccesMessage = self.properties().uploadsuccesmessage;
        this.uploadFailedMessage = self.properties().uploadfailedmessage;

        this.files = ko.observableArray();
        this.invalidFiles = ko.computed(function () {
            return ko.utils.arrayFilter(self.files(), function (file) {
                return !file.isValid;
            });
        });
        
        this.uploadStartHandler = function uploadStartHandler() { //data
            self.files.removeAll();
        };

        this.uploadDoneHandler = function uploadDoneHandler(data) {
            context.eventHandler.handleEvents(true, data);
            self.uploadProgress(undefined);
        };

        this.uploadProgress = ko.observable(undefined);
        this.progressPercentageHandler = function progressPercentageHandler(data) {
            self.uploadProgress(data + '%');
        };

        this.serverErrorMessagesContainer = ko.observable();        
        this.update = function FileUploadModelUpdate(model) {
        	self.properties(model.properties);
        	self.url(context.configuration.baseUri + context.session.id + '/subscription/' + context.subscriptionId + '/fileupload/' + self.properties().configurationid + '/');
            if (model.children.length === 3) {
                self.serverErrorMessagesContainer(context.modelFactory.createViewModel(context.session.getModel(model.children[0]), context));
            }
        };
    };

    /**
     * Viewmodel that represents field for a filetype.
     *
     * @constructor
     * @name blueriq.models.FileTypeModel
     * @param {Object} model The field model.
     * @param {Object} context The data context.
     * @returns A FileTypeModel instance.
     */
    blueriq.models.FileTypeModel = function FileTypeModel(model, context) {

        var self = this;
        blueriq.models.FieldModel.call(self, model, context);
        this.type = 'filetype';

        this.image = ko.computed(function () {
            var fileType = self.value();
            if (fileType) {
                fileType = fileType.toLowerCase();
                if (fileType.indexOf('application/pdf') > -1) {
                    return 'image-pdf';
                } else if (fileType.indexOf('image/') > -1) {
                    return 'icon-picture';
                }
            }
            return 'icon-file';
        });
    };

    /**
     * Viewmodel that represents instancelist.
     *
     * @constructor
     * @name blueriq.models.InstanceListModel
     * @param {Object} model The container model.
     * @param {Object} context The data context.
     * @returns A TabContainerModel instance.
     */
    blueriq.models.InstanceListModel = function InstanceListModel(model, context) {

        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);
        this.type = 'instancelist';

        this.showSearch = ko.computed(function () {
            if (self.children().length > 3) {
                return true;
            }
            if (model.children.length < 2) {
                return false;
            }
            //also show when there is a search query
            var searchContainerModel = context.session.getModel(model.children[1]);
            if (!searchContainerModel) {
                return false;
            }
            var searchFieldModel = context.session.getModel(searchContainerModel.children[0]);
            return searchFieldModel.values && searchFieldModel.values.length > 0;
        });
    };

    /**
     * Viewmodel that represents a tab container.
     *
     * @constructor
     * @name blueriq.models.TabContainerModel
     * @param {Object} model The container model.
     * @param {Object} context The data context.
     * @returns A TabContainerModel instance.
     */
    blueriq.models.TabContainerModel = function TabContainerModel(model, context) {

        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);

        this.tabContainers = ko.computed(function () {
            return ko.utils.arrayFilter(self.children(), function (child) {
                return child.type === 'container';
            });
        });

        this.tabLabels = ko.computed(function () {
            var labels = [];
            var tabContainers = self.tabContainers();
            for (var i = 0; i < tabContainers.length; i++) {
                labels[i] = {
                    'displayName': tabContainers[i].displayName,
                    'containerKey': tabContainers[i].key
                };
            }

            return labels;
        });
    };

    /**
     * Creates a VisualizationModel to display a single chart
     * @constructor
     * @name blueriq.services.VisualizationModel
     * @param {Object} model - The model for the chart
     * @param {Object} context - The data context
     * @returns {@link blueriq.models.VisualizationModel} single chart-model
     */
    blueriq.models.VisualizationModel = function VisualizationModel(model, context) {
        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);
        this.type = 'visualization';
        this.style = model.styles[0];
        this.displayName = model.displayName;

        this.statistics = [];
        var children = this.children();

        for ( var i = 0; i < children.length; i++ )
        {
            var str = null;
            try
            {
                str = children[i].displayName();
            }
            catch (err)
            {
                str = null;
            }
            str = !(!str || 0 === str.length) ? str : children[i].model.name;
            this.statistics.push([str, children[i].model.properties.value]);
        }
    };

    /**
     * Creates a StatisticModel to display a single statistic
     * @constructor
     * @name blueriq.services.StatisticModel
     * @param {Object} model - The model for the statistic
     * @param {Object} context - The data context
     * @returns {@link blueriq.models.StatisticModel} single statistic-model
     */
    blueriq.models.StatisticModel = function StatisticModel(model, context){
        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);
        this.style = model.styles[0];
        this.statistic = model.properties.value;
    };

}(window.blueriq, window.bqApp, window.ko));