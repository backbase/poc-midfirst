(function() {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'input-icon',
            condition: function(el) {
                return [
                    el.dataType === 'text',
                    !el.hasDomain,
                    el.styles.indexOf("nolabel") !== -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();
