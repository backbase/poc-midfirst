(function() {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'dropdown',
            condition: function(el) {
                return [
                    el.hasOptions,
                    !el.multiple
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();
