(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'button-unauthorized',
            condition: function(el) {
                return [
                    el.isButton,
                    el.name === 'Unauthorized'
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();