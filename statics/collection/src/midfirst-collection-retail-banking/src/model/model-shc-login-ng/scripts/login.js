/**
 * Model factory for model-shc-login-ng
 *
 * @inner
 * @type {function}
 * @param {Object} Promise An ES2015 compatible `Promise` object.
 *
 * @return {Object}
 */
export default function loginModel(Promise, data) {
    let _jspringData = {username: null, portalname: null, pagename: null, password: null};
    let _formHeaders = {'Accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'};

    function buildQueryString(obj) {
        return `username=${obj.username}&password=${obj.password}`;
    }

    function buildQueryStringOtp(pCode) {
        return 'otp_code=' + pCode;
    }


    /**
     * @name loginModel#login
     * @type {function}
     *
     * @description
     * Makes a login request
     *
     * @param {string} username
     * @param {string} password
     * @param {string} token XSRF token
     * @param {string} portalName Portal name
     * @param {string} pageName Page name
     * @returns {Promise}
     */
    function login(username, password, token, portalName, pageName) {
        _jspringData.username = username;
        _jspringData.portalname = portalName;
        _jspringData.pagename = pageName;
        return data.postLogin(buildQueryString({
                username,
                password,
            }),
            {
                'X-BBXSRF': token,
                Accept: 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded;',
                'Cache-Control': 'no-cache, no-store, must-revalidate'
            }
        ).then(function (response) {
            if (response.status === 201) {
                if (response.data.session.status === "Verified") {
                    _jspringData.password = response.data.session.id;
                    return jSpringSecurityCheck(response.data)
                }
            } else if (response.status === 200) {
                _jspringData.password = response.data.session.id;
                var deferred = Promise.defer();
                deferred.resolve({statusText: "OTP", note: "OTP Verification Required"});
                return deferred.promise;
            }
        });
    }


    function otpVerification(pCode) {
        return data.postOtpVerification( _jspringData.password,buildQueryStringOtp(pCode),_formHeaders).then(function (response) {
            if (response.status === 200) {
                if (response.data.session.status === "Verified") {
                    return jSpringSecurityCheck(response.data);
                }
            } else {

            }
        });
    }

    function jSpringSecurityCheck(pData) {
        var options = {
            'j_username': pData.session.username,
            'j_password': _jspringData.password,
            'portal_name': _jspringData.portalname,
            'page_name': _jspringData.pagename
        };
        var securityData = Object.keys(options)
            .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(options[k]))
            .join('&');
        return data.securityCheck(securityData, _formHeaders);
    }

    /**
     * @name loginModel#logout
     * @type {function}
     *
     * @description
     * Makes a logout request
     *
     * @param {string} token XSRF token
     * @returns {Promise}
     */
    function logout(token) {
        return data.postLogout({
            'X-BBXSRF': token
        });
    }

    /**
     * @name loginModel
     * @type {object}
     *
     * @description
     * Model for widget-shc-login-ng and widget-shc-user-menu-ng
     */
    return {
        login,
        logout,
        otpVerification,
    };
}
