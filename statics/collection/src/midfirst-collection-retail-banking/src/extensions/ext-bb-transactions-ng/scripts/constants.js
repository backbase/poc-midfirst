/**
 * @description
 * Category icon CSS class prefix
 *
 * @name categoryClassPrefix
 * @type {string}
 */
const categoryClassPrefix = 'bb-transaction-category-';

export default categoryClassPrefix;

/**
 * @description
 * Uncategorized CSS icon class
 *
 * @name uncategorizedIconClass
 * @type {string}
 */
export const uncategorizedIconClass = 'uncategorized';

/**
 * @description
 * Widget custom type preferences
 *
 * @name Types
 * @type {object}
 */
export const Types = {
  TYPE_1: 'type1',
  TYPE_2: 'type2',
  TYPE_3: 'type3',
  TYPE_4: 'type4',
};
