(function() {
    angular.module('forms-ui').directive('shcAccordionToggle',  /*@ngInject*/ function () {
        return {
            require: {
                form: '^^bbForm'
            },
            restrict: 'A',
            link: function ($scope, element, attrs, formsController) {
                var context = (element.context) ? element.context : element[0];
                $scope.$on("force-toggle-emit",function () {
                    console.log("force-toggle-emit picked");
                    $scope.$emit('update',$scope.element);
                });
            }
        };
    });
})();
