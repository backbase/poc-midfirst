(function (blueriq, bqApp) {
    'use strict';

    /**
     * Register default templates for elements
     */

    blueriq.bootcardsTemplateFactory = function (viewModel, context) {

        if (viewModel.type === 'page' && viewModel instanceof blueriq.models.bootcards.PageModel) {
            return blueriq.templates.page.dashboardPage;
        } else if (viewModel.type === 'page' && jQuery.inArray('widget', context.modus) > -1) {
            return blueriq.templates.page.widgetPage;
        } else if (viewModel.type === 'mobilelist') {
            return blueriq.templates.container.list.container;
        }else if(viewModel.type === 'listitem'){
            return blueriq.templates.container.list.item;
        }else if(viewModel.type === 'container'){
            if(viewModel.contentStyle === 'ButtonList'){
                return blueriq.templates.container.buttonList;
            }
        }

        if(viewModel.type === 'page'){
            console.log('normal page');
        }

        //default element type as template
        //return prefix + viewModel.type + '/' + viewModel.type;
    };

    /**
     * Register automatically if bqApp is already created
     */
    if (bqApp) {
        bqApp.templateFactory.registerModelHandler(blueriq.bootcardsTemplateFactory);
    }
}(window.blueriq, window.bqApp));
