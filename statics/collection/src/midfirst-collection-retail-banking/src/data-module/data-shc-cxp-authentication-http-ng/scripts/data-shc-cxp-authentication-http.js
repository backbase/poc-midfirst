/* global window */
export default (conf) => (httpClient) => {
    const portalServerRoot = window.b$.portal.config.serverRoot.replace(/\//g, '');
    const config = {
        endpointOTPVerification: `${conf.baseUri}${portalServerRoot}/services/rest/v1/authentication/session/{id}/verifyOTP`,
        endpointLogin: `${conf.baseUri}${portalServerRoot}/services/rest/v1/authentication/session/initiate`,
        endpointLogout: `${conf.baseUri}${portalServerRoot}/bb-public-api/security`
    };

    /**
     * @name CXPAuthenticationData#postLogin
     * @type {function}
     * @description Perform a POST request to the URI.
     * @param {object} data - configuration object
     * @param {object} headers - custom headers
     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
     *
     * @example
     * cXPAuthenticationData
     *  .postLogin(data, headers)
     *  .then(function(result){
   *    console.log(result)
   *  });
     */
    function postLogin(data, headers) {
        const url = `${config.endpointLogin}`;

        return httpClient({
            method: 'POST',
            url,
            data,
            headers
        });
    }

    function postOtpVerification(pId,data, headers) {
        let url = (`${config.endpointOTPVerification}`).replace('{id}',pId);
        return httpClient({
            method: 'POST',
            url,
            data,
            headers
        });
    }


    function securityCheck(data, headers) {
        return httpClient({
            method: 'POST',
            url: portalServerRoot + '/j_spring_security_check' + '?rd=' + new Date().getTime(),
            data: data,
            headers: headers
        });
    }

    /**
     * @name CXPAuthenticationData#postLogout
     * @type {function}
     * @description Perform a POST request to the URI.
     * @param {object} headers - custom headers
     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
     *
     * @example
     * cXPAuthenticationData
     *  .postLogout(headers)
     *  .then(function(result){
   *    console.log(result)
   *  });
     */
    function postLogout(headers) {
        const url = `${config.endpointLogout}/logout`;

        return httpClient({
            method: 'POST',
            url,
            headers
        });
    }

    return ({
        postLogin,
        postLogout,
        postOtpVerification,
        securityCheck
    });
}
;
