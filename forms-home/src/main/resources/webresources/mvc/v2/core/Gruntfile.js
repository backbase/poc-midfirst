/* jshint node:true */
module.exports = function (grunt) {
    'use strict';

    require('time-grunt')(grunt);
    require('load-grunt-tasks')(grunt);

    var scripts = [
        'core.js',
        'core-handlers.js',
        'core-templates.js',
        'core-viewmodels.js',
        'plugins/**/*.js',
        '!**/*.min.js',
        '!**/*.debug.js',
        '!**/*.intellisense.js'
    ];


    var src = {
        scripts: scripts
    };

    grunt.initConfig(
        {
            // ----- Environment
            pkg: grunt.file.readJSON('package.json'),

            compass: {
                dev: {
                    options: {
                        config: 'config.rb',
                        environment: 'development',
                        sourcemap: true
                    }
                },
                dist: {
                    options: {
                        config: 'config.rb',
                        environment: 'production',
                        force: true // overwrite existing files
                    }
                }
            },

            jshint: {
                options: {
                    jshintrc: '.jshintrc'
                },
                all: scripts
            },

            jscs: {
                options: {
                    config: '.jscs'
                },
                dev: {
                    files: {
                        src: scripts
                    }
                }
            },

            // todo : check if needed, because minify is done by build server
            //uglify: {
            //    dev: {
            //        options: {
            //            beautify: true, // for debugging
            //            banner: '/*! <%= pkg.name %> <%= pkg.version %> <%= grunt.template.today("yyyy-mm-dd HH:MM") %> */',
            //            mangle: false, // for debugging
            //            sourceMap: true,
            //            sourceMapIncludeSources: true
            //        },
            //        files: {
            //            'js/<%= pkg.name.toLowerCase() %>.js': src.scripts
            //        }
            //    },
            //    dist: {
            //        options: {
            //            banner: '/*! <%= pkg.name %> <%= pkg.version %> <%= grunt.template.today("yyyy-mm-dd HH:MM") %> */'
            //        },
            //        files: {
            //            'js/<%= pkg.name.toLowerCase() %>.min.js': src.scripts
            //        }
            //    }
            //},

            watch: {
                //todo : add saas and then use compass
                //compass: {
                //    files: ['_scss/**/*.scss'],
                //    tasks: ['compass:dev']
                //},
                script: {
                    files: scripts,
                    tasks: ['jscs', 'jshint'] //todo add 'uglify:dev' if needed. uglify is done by build server
                }
            }


        }
    );

    grunt.registerTask('default', ['jscs', 'jshint', 'watch']);
    grunt.loadNpmTasks('grunt-notify');

};
