(function () {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'container-redirect',
            condition: function(el) {
                return [
                    el.isContainer,
                    el.styles.indexOf("redirectLanguage")>-1
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();