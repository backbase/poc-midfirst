/* global window */
import Cookies from './cookies';
/**
 * @name LoginController
 * @type {object}
 *
 * @description
 * Login widget
 */
export default function LoginController(model, widgetInstance) {
    const $ctrl = this;

    const portal = window.b$.portal;

    const loginRedirectPage = widgetInstance.getStringPreference('loginRedirectPage');

    const loginRedirectUrl =
        `${portal.config.serverRoot}/${portal.portalName}/${loginRedirectPage}`;

    const cookies = Cookies.getCookies();

    const token = {
        key: 'BBXSRF',
        value: ''
    };

    const username = '';
    const password = '';
    const verificationCode = '';
    const needsToken = false;

    const loginError = false;

    const $onInit = () => {
        token.value = cookies[token.key];
    };


    const redirectToDashboard = function () {
        window.location.assign(loginRedirectUrl);
    };

    const otpVerification = () => {
        model.otpVerification($ctrl.verificationCode).then((pData) => {
            if(pData.statusText) {
                if (pData.statusText === "OK") {
                    redirectToDashboard();
                }
            }
        }).catch(function () {
            $ctrl.verificationCode = "";
            $ctrl.loginError = true;
        });

    };

    const login = () =>
        model.login($ctrl.username, $ctrl.password, token.value, portal.portalName, window.b$.portal.pageName)
            .then((pData) => {
                if(pData.statusText) {
                    if (pData.statusText ==="OK") {
                        redirectToDashboard();
                    }else if (pData.statusText==="OTP") {
                        $ctrl.needsToken = true;
                    }
                }

            }).catch((pData) => {
                $ctrl.loginError = true;
                $ctrl.password = '';
            }
        );

    Object.assign($ctrl, {
        /**
         * @description
         * AngularJS Lifecycle hook used to initialize the controller
         * @type {function}
         *
         * @name LoginController#$onInit
         * @returns {void}
         */
        $onInit,
        /**
         * @description Login function
         * @type {function}
         *
         * @name LoginController#login
         * @returns {Promise}
         */
        login,
        /**
         * @name LoginController#username
         * @type {string}
         */
        username,
        /**
         * @name LoginController#password
         * @type {string}
         */
        password,
        loginError,
        verificationCode,
        needsToken,
        otpVerification
    });
}
