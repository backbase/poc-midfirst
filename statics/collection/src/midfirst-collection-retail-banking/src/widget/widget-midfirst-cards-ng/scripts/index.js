/**
 * @module widget-midfirst-cards-ng
 *
 * @description
 * Cards
 */
import angular from 'vendor-bb-angular';

import widgetModuleKey, { widgetKey } from 'lib-bb-widget-ng';
import eventBusModuleKey, { eventBusKey } from 'lib-bb-event-bus-ng';
import extendHooks from 'lib-bb-widget-extension-ng';

import uiBbFormatAmount from 'ui-bb-format-amount'

import Controller from './controller';

import defaultHooks from './default-hooks';

const moduleKey = 'widget-midfirst-cards-ng';
const hooksKey = `${moduleKey}:hooks`;

export default angular
  .module(moduleKey, [
    widgetModuleKey,
    eventBusModuleKey,
      uiBbFormatAmount,
  ])

  .factory(hooksKey, extendHooks(defaultHooks))

  .controller('CardsController', [
    // dependencies to inject
    eventBusKey,
    hooksKey,
    widgetKey,
    /* into */
    Controller,
  ])

  .name;
