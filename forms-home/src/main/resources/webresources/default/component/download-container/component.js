(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'download-container',
            condition: function(el) {
                return [
                    el.isContainer,
                    el.contentStyle === "filedownload"
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();