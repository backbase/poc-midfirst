(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'container-worklist',
            condition: function(el) {
                return [
                    el.contentStyle === 'worklist',
                    el.isContainer
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();