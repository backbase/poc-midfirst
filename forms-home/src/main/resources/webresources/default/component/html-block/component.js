(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'html-block',
            condition: function(el) {
                return [
                    el.type === 'asset'
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();