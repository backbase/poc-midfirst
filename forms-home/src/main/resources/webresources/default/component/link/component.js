(function() {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'link',
            condition: function(el) {
                return [
                    el.type === 'link'
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();
