//providing a scoped namesspaces to inject in the iife
var jsScopes = {
    //Defining Chart.js namespacing
    chartJs: {
        Chart: window.Chart,
        legend: window.legend
    },

    //defining odometer namespacing
    odometerJs: {
        odometerOptions: window.odometerOptions
    }
};

(function (ko, chartJs, odometerJs) {
    'use strict';

    ko.virtualElements.allowedBindings.bqMode = true;

    /**
	 * This handler adds a render modus to the context.
	 */
    ko.bindingHandlers.bqMode = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var mode = valueAccessor();

            var modus = bindingContext.modus ? bindingContext.modus.slice() : [];
            if (mode || 0 === mode.length) {
                modus = modus.concat(mode);
            }

            //var newProperties = valueAccessor(),
            var childBindingContext = bindingContext.createChildContext(viewModel);

            ko.utils.extend(childBindingContext, {
                modus: modus
            });
            ko.applyBindingsToDescendants(childBindingContext, element);

            return {
                controlsDescendantBindings: true
            };
        }
    };

    /**
	 * This handler creates datepicker.
	 */
    ko.bindingHandlers.bqDatePicker = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            $(element).datepicker({
                format: viewModel.context.session.language.patterns.date
            }).on('changeDate', function (ev) {
                var value = $(ev.currentTarget).find('input').val();
                valueAccessor()(value);
            }).on('show', function () {
                if (viewModel.readonly()) {
                    $(element).datepicker('hide');
                }
            });
        },
        update: function (element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            if (value) {
                $(element).datepicker('setValue', value);
            }
        }
    };

    /**
	 * This handler generates a tooltip.
	 */
    ko.bindingHandlers.bqTooltip = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var placement;
            if (!placement) {
                placement = 'right';
            }
            $(element).tooltip({
                title: viewModel.explainText,
                placement: placement
            });
        }
    };

    /**
	 * This handler generates a multiple select box.
	 */
    ko.bindingHandlers.bqMultiSelect = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            var obj = valueAccessor(), allBindings = allBindingsAccessor(), lookupKey = allBindings.lookupKey;
            $(element).select2(obj);
            if (lookupKey) {
                var value = ko.utils.unwrapObservable(allBindings.value);
                $(element).select2('data', ko.utils.arrayFirst(obj.data.results, function (item) {
                    return item[lookupKey] === value;
                }));
            }

            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $(element).select2('destroy');
            });
        },
        update: function (element) {
            $(element).trigger('change');
        }
    };

    /**
	 * This handler shows a modal based on its value accessor.
	 */
    ko.bindingHandlers.bqModal = {
        init: function () {
        },
        update: function (element, valueAccessor) {
            if (valueAccessor()) {
                $(element).modal('show');
            } else {
                $(element).modal('hide');
            }
        }
    };

    /**
	 * This handler converts the enters in a text to html enters(linebreaks).
	 */
    ko.bindingHandlers.bqHtml = {
        update: function (element, valueAccessor) {
            if(valueAccessor() !== null) {
                ko.utils.setHtml(element, valueAccessor().replace(/\r?\n/g, '<br />'));
            }
        }
    };

    /**
	* This handler handles the file upload container
	*/
    ko.bindingHandlers.bqFileUpload = {
        update: function (element, valueAccessor, allBindings, viewModel) {
            $(element).fileupload({
                url: new blueriq.QueryStringBuilder(allBindings().url()).param('X-CSRF-Token', viewModel.csrfToken()).toUrl()
            });
        },
        init: function (element, valueAccessor, allBindings, viewModel) {
            var attachFileUpload = function attachFileUpload(element, labelElement, inputElement, descriptionElement, configuration) {
                $(document).bind('drop dragover', function (e) {
                    e.preventDefault();
                });

                // Input checking
                if (!element) {
                    throw new Error('bqFileUpload: element is mandatory');
                }
                if (!labelElement) {
                    throw new Error('bqFileUpload: labelElement is mandatory');
                }
                if (!inputElement) {
                    throw new Error('bqFileUpload: inputElement is mandatory');
                }
                if (!configuration || !configuration.url) {
                    throw new Error('bqFileUpload: url is mandatory');
                }

                // Utility functions
                var formatFileSize = function formatFileSize(bytes) {
                    if (typeof bytes !== 'number') { return ''; }
                    if (bytes >= 1000000000) { return (bytes / 1000000000).toFixed(2) + ' GB'; }
                    if (bytes >= 1000000) { return (bytes / 1000000).toFixed(2) + ' MB'; }
                    return (bytes / 1000).toFixed(2) + ' KB';
                };

                var formatBitrate = function formatBitrate(bits) {
                    if (typeof bits !== 'number') { return ''; }
                    if (bits >= 1000000000) { return (bits / 1000000000).toFixed(2) + ' Gbit/s'; }
                    if (bits >= 1000000) { return (bits / 1000000).toFixed(2) + ' Mbit/s'; }
                    if (bits >= 1000) { return (bits / 1000).toFixed(2) + ' kbit/s'; }
                    return bits.toFixed(2) + ' bit/s';
                };

                var formatTime = function formatTime(seconds) {
                    var date = new Date(seconds * 1000), days = Math.floor(seconds / 86400);
                    days = days ? days + 'd ' : '';
                    return days + ('0' + date.getUTCHours()).slice(-2) + ':' + ('0' + date.getUTCMinutes()).slice(-2) + ':' + ('0' + date.getUTCSeconds()).slice(-2);
                };

                // var formatPercentage = function formatPercentage(floatValue) {
                //     return (floatValue * 100).toFixed(2);
                // };

                // Set text on UI elements
                if (configuration.singleFileMode) {
                    labelElement.text(configuration.singleUploadLabel);
                } else {
                    labelElement.text(configuration.multiUploadLabel);
                    inputElement.attr('multiple', 'multiple');
                }

                if (descriptionElement) {
                    var description = '';
                    if (configuration.fileSizeDescription && configuration.maxFileSize) {
                        description += configuration.fileSizeDescription.replace('{0}', formatFileSize(configuration.maxFileSize));
                    }
                    if (configuration.extensionDescription && configuration.allowedExtensions) {
                        if (configuration.fileSizeDescription && configuration.maxFileSize) {
                            description += ' / ';
                        }
                        description += configuration.extensionDescription.replace('{0}', configuration.allowedExtensions.split('|').join(', '));
                    }
                    descriptionElement.text(description);
                }

                // Set messages and settings for upload component
                var messages = {};
                if (configuration.extensionValidationMessage) {
                    messages.acceptFileTypes = configuration.extensionValidationMessage;
                }
                if (configuration.fileSizeValidationMessage) {
                    messages.maxFileSize = configuration.fileSizeValidationMessage;
                }

                var settings = {
                    url: new blueriq.QueryStringBuilder(configuration.url).param('X-CSRF-Token', viewModel.csrfToken()).toUrl(), 
                    singleFileUploads: false,
                    dropZone: element,
                    messages: messages,
                    formData: function() {//form
                        var pageEvent = viewModel.context.session.createPageEvent(viewModel.key, {});
                        return [{name: 'pageEvent', value: JSON.stringify(pageEvent)}];
                    }
                };

                if (configuration.allowedExtensions) {
                    settings.acceptFileTypes = new RegExp('(' + configuration.allowedExtensions + ')$');
                }
                if (configuration.maxFileSize) {
                    settings.maxFileSize = configuration.maxFileSize;
                }

                // Create upload component
                var fileUpload = element.fileupload(settings);

                // Set callback handlers
                fileUpload.on('fileuploadprocessalways', function (e, data) {
                    if (configuration.fileAddedHandler) {
                        var selectedFile = data.files[data.index];
                        var file = {
                            name: selectedFile.name,
                            size: formatFileSize(selectedFile.size),
                            isValid: !selectedFile.error,
                            errorMessage: selectedFile.error
                        };
                        configuration.fileAddedHandler(file);
                    }
                });

                fileUpload.on('fileuploadadd', function (e, data) {
                    // Callback on start of (multiple) file upload
                    if (configuration.uploadStartHandler) {
                        configuration.uploadStartHandler(data.response().result);
                    }
                });

                fileUpload.on('fileuploadsend', function (e, data) {
                    var doUpload = true;
                    for (var i = 0; i < data.files.length; i++) {
                        var file = data.files[i];
                        if (file.error) {
                            //upload = false; What is upload?
                            break;
                        }
                    }
                    return doUpload;
                });

                fileUpload.on('fileuploadprogressall', function (e, data) {
                    if (configuration.progressPercentageHandler) {
                        configuration.progressPercentageHandler(parseInt(data.loaded / data.total * 100, 10));
                    }
                    if (configuration.progressUploadSpeedHandler) {
                        configuration.progressUploadSpeedHandler(formatBitrate(data.bitrate));
                    }
                    if (configuration.progressRemainingTimeHandler) {
                        configuration.progressRemainingTimeHandler(formatTime((data.total - data.loaded) * 8 / data.bitrate));
                    }
                    if (configuration.progressRemainingFileSizeHandler) {
                        configuration.progressRemainingFileSizeHandler(formatFileSize(data.loaded));
                    }
                    if (configuration.progressUploadedFileSizeHandler) {
                        configuration.progressUploadedFileSizeHandler(formatFileSize(data.total));
                    }
                });

                fileUpload.on('fileuploaddone', function (e, data) {
                    if (configuration.uploadDoneHandler) {
                        // Check response type for IE support
                        var responseContentType = data.response().jqXHR.getResponseHeader('Content-type');

                        if (!responseContentType || responseContentType.indexOf('application/json') === -1) {
                            try {
                                // Browser doesn't support XHR request for file upload
                                var response = data.response();
                                var jsonText;

                                if (response.result instanceof Object) {
                                    // Get JSON text from inner node
                                    jsonText = response.result[0].childNodes[1].innerText;
                                }
                                else {
                                    jsonText = response.result;
                                }
                                // Pass data as true JSON
                                configuration.uploadDoneHandler(jQuery.parseJSON(jsonText));
                            }
                            catch (err) {
                                console.error('Error during file upload response handling: ' + err);
                            }
                        }
                        else {
                            configuration.uploadDoneHandler(data.response().result);
                        }
                    }
                });
            };

            // Initialization
            var bindings = allBindings();

            var fileUploadElement = $(element);
            var labelElement = fileUploadElement.find('.fileUploadLabel');
            var inputElement = fileUploadElement.find('.fileUploadInput');
            var descriptionElement = fileUploadElement.find('.fileUploadDescription');

            attachFileUpload(fileUploadElement, labelElement, inputElement, descriptionElement, {
                'url': bindings.url(),
                'allowedExtensions': bindings.allowedExtensions,
                'maxFileSize': bindings.maxFileSize,
                'singleFileMode': bindings.singleFileMode,
                'singleUploadLabel': bindings.singleUploadLabel,
                'multiUploadLabel': bindings.multiUploadLabel,
                'fileSizeDescription': bindings.fileSizeDescription,
                'extensionDescription': bindings.extensionDescription,
                'fileSizeValidationMessage': bindings.fileSizeValidationMessage,
                'extensionValidationMessage': bindings.extensionValidationMessage,
                'fileAddedHandler': function (file) {
                    valueAccessor().push(file);
                },
                'uploadStartHandler': function (data) {
                    if (bindings.uploadStartHandler) {
                        bindings.uploadStartHandler(data);
                    }
                },
                'uploadDoneHandler': function (data) {
                    if (bindings.uploadDoneHandler) {
                        bindings.uploadDoneHandler(data);
                    }
                },
                'progressPercentageHandler': function (data) {
                    if (bindings.progressPercentageHandler) {
                        bindings.progressPercentageHandler(data);
                    }
                }
            });
        }
    };

    ko.bindingHandlers.bqSearchField = {
        init: function (element, valueAccessor, allBindings, viewModel) {
            $(element).select2({
                width: 'resolve',
                tags: [],
                formatNoMatches: '',
                dropdownCssClass: 'select2-hidden',
                initSelection: function (element, callback) {
                    var data = [];
                    $(element.val().split(',')).each(function () {
                        if (this.trim().length > 0) { // Ignore empty strings
                            data.push({ id: this.valueOf(), text: this.valueOf() });
                        }
                    });
                    callback(data);
                }
            }).on('change', function (val) {
                // Handle add
                if (val.added) {
                    var newValue = valueAccessor()().concat();
                    if (val.added.text.trim().length > 0) {
                        newValue[valueAccessor()().length] = val.added.text.trim();
                        valueAccessor()(newValue);
                    }
                    else {
                        // Empty string detected
                        val.val.splice(val.val.length - 1); // Remove last inserted value
                    }
                }
                // Handle remove
                if (val.removed) {
                    var index = valueAccessor()().indexOf(val.removed.text);
                    if (index > -1) {
                        valueAccessor()().splice(index, 1);
                    }
                }
                viewModel.search();
            }).select2('val', valueAccessor()()); // Trigger initSelection callback

            $('.select2-search-field > input.select2-input').on('keyup', function (e) {
                if (e.target.value.trim().length === 0) { // TODO fix
                    element.value = '';
                    e.target.value = '';
                    // TODO also set value from source element to empty
                }
                return true;
            });
        }
    };

    /**
	 * This handler generates a switch box (http://www.bootstrap-switch.org/) for a domain with two values.
	 */
    ko.bindingHandlers.bqToggle = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            $(element).bootstrapSwitch()
                .on('switch-change', function (event, data) {
                if (data.value) {
                    valueAccessor()(viewModel.domain()[0].value);
                } else {
                    valueAccessor()(viewModel.domain()[1].value);
                }
            })
            .bootstrapSwitch('setOnLabel', viewModel.domain()[0].displayValue)
            .bootstrapSwitch('setOffLabel', viewModel.domain()[1].displayValue);

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            $(element).bootstrapSwitch('setState', viewModel.domain()[0].value === viewModel.value());
        }
    };

    /**
	 * This handler creates toggle functionality for stacktraces
	 */
    ko.bindingHandlers.bqStackTrace = {
        init: function (element, valueAccessor) {
            $(element).click(function () {
                $('#' + valueAccessor()).toggle();
                if ($('#' + valueAccessor()).is(':hidden')) {
                    $(element).removeClass('icon-chevron-down');
                    $(element).addClass('icon-chevron-right');
                } else {
                    $(element).removeClass('icon-chevron-right');
                    $(element).addClass('icon-chevron-down');
                }
            });
        }
    };

    /**
	 * This handler creates a tooltip for an iconButton with the caption
	 */
    ko.bindingHandlers.bqIconTooltip = {
        init: function (element, valueAccessor) {
            if (!valueAccessor()()) {
                return;
            }
            var placement;
            if (!placement) {
                placement = 'bottom';
            }
            var text = ko.unwrap(valueAccessor());
            $(element).tooltip({
                title: text,
                placement: placement
            });
        }
    };

    /**
	 * This handler creates a datetimepicker.
	 */
    ko.bindingHandlers.bqDateTimePicker = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var datetimeformat = viewModel.context.session.language.patterns.date.toUpperCase() + ' HH:mm:ss';
            $(element).datetimepicker({
                format: datetimeformat,
                pick12HourFormat: false,
                sideBySide: true,
                useSeconds: true
            }).on('change', function (ev) {
                var value = $(ev.currentTarget).find('input').val();
                valueAccessor()(value);
            }).on('show', function () {
                if (viewModel.readonly()) {
                    $(element).datetimepicker('hide');
                }
            });
        },
        update: function (element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            if (value) {
                $(element).data('DateTimePicker').setDate(value);
            }
        }
    };

    ko.bindingHandlers.visualizationColors =
    [
        '#3da0ea', // Blueriq-Blue
        '#f377ab', // Blueriq-Pink
        '#707070', // Blueriq-Dark-Gray
        '#A8A8A8', // Blueriq-Light-Gray
        '#e7912a', // Orange
        '#4ec9ce', // Teal
        '#ec7337', // Orange-Red
        '#bacf0b' // Green
    ];

    /**
	 * This handler creates a visualization.
	 */
    ko.bindingHandlers.bqVisualization ={
        init: function(element, valueAccessor, allBindingsAccessor){
            var incomingData = allBindingsAccessor().data;
            var statistics = element.getContext('2d');
            var chartType = allBindingsAccessor().chartType;
            var title = allBindingsAccessor().title;
            var animationEase = 'easeInOutQuad';

            // HTML-elem:   canvas, td,           tr,           td,        div
            var legendDiv = null;//element.parentElement.parentElement.children[1].children[0];

            // HTML-elem:  canvas, td,           tr,           table,        tr,         td,         div
            var titleDiv = null; //element.parentElement.parentElement.parentElement.children[0].children[0].children[0];

            // Retrieving elements
            var parentElement = element.parentElement;
            if ( parentElement !== undefined && parentElement !== null )
            {
                var grandParentElement = parentElement.parentElement;
                if ( grandParentElement !== undefined && grandParentElement !== null )
                {
                    if ( grandParentElement.children.length > 1 )
                    {
                        var uncleElement = grandParentElement.children[1];
                        if ( uncleElement !== undefined && uncleElement !== null ) {
                            if (uncleElement.children.length > 0) {
                                legendDiv = uncleElement.children[0];
                            }
                        }
                    }

                    var greatGrandParentElement = grandParentElement.parentElement;
                    if ( greatGrandParentElement !== undefined && greatGrandParentElement !== null ){
                        if ( greatGrandParentElement.children.length > 0 ){
                            var grandUncleElement = greatGrandParentElement.children[0];
                            if ( grandUncleElement !== undefined && grandUncleElement !== null ){
                                if ( grandUncleElement.children.length > 0 ) {
                                    var secondUncle = grandUncleElement.children[0];
                                    if (secondUncle !== undefined && secondUncle !== null) {
                                        if (secondUncle.children.length > 0) {
                                            titleDiv = secondUncle.children[0];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (titleDiv !== null) {
                if (title !== null) {
                    titleDiv.innerHTML = title;
                } else {
                    titleDiv.innerHTML = 'Chart';
                }
            }

            // Creating data sets
            var data = null;
            switch ( chartType ){
                case 'bar':
                case 'line':
                case 'radar':
                    var incomingLabels = [];
                    var incomingPoints = [];
                    for ( var j = 0; j < incomingData.length ; j += 1){
                        incomingLabels.push(incomingData[j][0]);
                        incomingPoints.push(incomingData[j][1]);
                    }
                    data = {
                        labels : incomingLabels,
                        datasets : [
                        {
                            fillColor : ko.bindingHandlers.visualizationColors[0],
                            strokeColor : ko.bindingHandlers.visualizationColors[2],
                            pointColor : ko.bindingHandlers.visualizationColors[5],
                            pointStrokeColor : ko.bindingHandlers.visualizationColors[3],
                            data : incomingPoints
                        }]
                    };
                    break;
                case 'pie':
                case 'polar':
                case 'doughnut':
                    data = [];
                    for ( var i = 0 ; i < incomingData.length ; i += 1){
                        data.push(
                        {
                            color : ko.bindingHandlers.visualizationColors[i % 6],
                            label : incomingData[i][0],
                            value : incomingData[i][1]
                        });
                    }
                    break;
            }

            // Creating charts
            var myChart = null;
            switch ( chartType ){
                case 'line':
                    myChart = new chartJs.Chart(statistics).Line(data);
                    break;
                case 'pie':
                    myChart = new chartJs.Chart(statistics).Pie(data, {
                        animateScale: true,
                        responsive: true,
                        animationEasing: animationEase
                    });
                    if (legendDiv !== null) {
                        chartJs.legend(legendDiv, data, myChart);
                    }
                    break;
                case 'bar':
                    myChart = new chartJs.Chart(statistics).Bar(data);
                    break;
                case 'polar':
                    myChart = new chartJs.Chart(statistics).PolarArea(data, {
                        animationEasing: animationEase
                    });
                    if (legendDiv !== null) {
                        chartJs.legend(legendDiv, data, myChart);
                    }
                    break;
                case 'radar':
                    myChart = new chartJs.Chart(statistics).Radar(data);
                    break;
                case 'doughnut':
                    myChart = new chartJs.Chart(statistics).Doughnut(data, {
                        animateScale: true,
                        animationEasing: animationEase });
                    if (legendDiv !== null) {
                        chartJs.legend(legendDiv, data, myChart);
                        var container = greatGrandParentElement.parentElement.parentElement;
                        if (container.offsetWidth < greatGrandParentElement.offsetWidth) {
                        	//move legend bellow the chart
                        	$(greatGrandParentElement.insertRow(2)).append(legendDiv.parentElement);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    };

    /**
     * This handler creates a odometer.
     */
    ko.bindingHandlers.bqStatistic =
    {
        init: function(element, valueAccessor, allBindingsAccessor){
            odometerJs.odometerOptions =
            {
                auto: false
            };
            var odometerElement = element;
            var statistic = allBindingsAccessor().data;

            new odometerJs.Odometer(
                {
                    el: odometerElement,
                    value: statistic,
                    format: 'd',
                    theme: 'plaza',
                    duration: 3000,
                    animation: 'count'
                });
        }
    };

    /**
	 * Drag/drop target for fileupload
	 *
	 * source: https://github.com/blueimp/jQuery-File-Upload/wiki/Drop-zone-effects
	 */
    $(document).bind('dragover', function (e) {
        var dropZone = $('.dropzone'), foundDropzone, timeout = window.dropZoneTimeout;
        if (!timeout) {
            dropZone.addClass('in');
        } else {
            clearTimeout(timeout);
        }
        var found = false, node = e.target;

        do {
            if ($(node).hasClass('dropzone')) {
                found = true;
                foundDropzone = $(node);
                break;
            }
            node = node.parentNode;
        } while (node !== null);

        dropZone.removeClass('in hover');

        if (found) {
            foundDropzone.addClass('hover');
        }

        window.dropZoneTimeout = setTimeout(function () {
            window.dropZoneTimeout = null;
            dropZone.removeClass('in hover');
        }, 100);
    });

})(window.ko, jsScopes.chartJs, jsScopes.odometerJs);

