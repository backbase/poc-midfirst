/**
 * @module data-shc-party-data-http-ng
 *
 * @description A data module for accessing the Party Data REST API
 *
 * @returns {String} `data-shc-party-data-http-ng`
 * @example
 * import shcPartyDataModuleKey, {
 *   shcPartyDataKey,
 * } from 'data-shc-party-data-http-ng';
 */

import ng from 'vendor-bb-angular';

import shcPartyData from './data-shc-party-data-http';

const shcPartyDataModuleKey = 'data-shc-party-data-http-ng';
/**
 * @name shcPartyDataKey
 * @type {string}
 * @description Angular dependency injection key for the Party Data data service
 */
export const shcPartyDataKey = `${shcPartyDataModuleKey}:shcPartyData`;
/**
 * @name default
 * @type {string}
 * @description Angular dependency injection module key
 */
export default ng
    .module(shcPartyDataModuleKey, [])

/**
   * @name shcPartyData
   * @type {object}
   * @constructor
   *
   * @description Public api for service data-shc-party-data-http
   *
   */
    .provider(shcPartyDataKey, [() => {
        const config = {
            baseUri: '/',
        };

        /**
     * @name shcPartyDataProvider
     * @type {object}
     * @description
     * Data service that can be configured with custom base URI.
     *
     * @example
     * angular.module(...)
     *   .config(['data-shc-party-data-http-ng:shcPartyDataProvider',
     *     (dataProvider) => {
     *       dataProvider.setBaseUri('http://my-service.com/');
     *       });
     */
        return {
            /**
       * @name shcPartyDataProvider#setBaseUri
       * @type {function}
       * @param {string} baseUri Base URI which will be the prefix for all HTTP requests
       */
            setBaseUri: (baseUri) => {
                config.baseUri = baseUri;
            },

            /**
       * @name shcPartyDataProvider#$get
       * @type {function}
       * @return {object} An instance of the service
       */
            $get: [
                '$http',
                /* into */
                shcPartyData(config),
            ],
        };
    }])

    .name;
