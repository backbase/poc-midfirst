(function () {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'widget-intake',
            condition: function(el) {
                return [el.name === "WidgetIntake"];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();