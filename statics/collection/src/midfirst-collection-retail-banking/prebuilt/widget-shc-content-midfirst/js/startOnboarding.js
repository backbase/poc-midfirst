
$(document).ready(function() {

    $('.btn-stop-onboarding').click(function() {
        stopOnboarding();

    })

    $('.start-onboarding').click(function() {
        startOnboarding();
    })


    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            stopOnboarding();
        }
    });

    function stopOnboarding() {
        $('.widget-shc-content--customclass-jumbotron').removeClass('onboarding-focussed');
        $('.template-container-banking-navigation').removeClass('onboarding-focussed');
        $('.template-shc-navigation').removeClass('onboarding-focussed');
        $('.onboarding-wrapper').removeClass('onboarding-focussed');
        $('.shc-retail-master').removeClass('onboarding-focussed');
    }

    function startOnboarding() {
        $('.widget-shc-content--customclass-jumbotron').addClass('onboarding-focussed');
        $('.template-container-banking-navigation').addClass('onboarding-focussed');
        $('.template-shc-navigation').addClass('onboarding-focussed');
        $('.onboarding-wrapper').addClass('onboarding-focussed');
        $('.shc-retail-master').addClass('onboarding-focussed');
    }
})

