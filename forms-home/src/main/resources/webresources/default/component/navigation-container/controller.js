(function () {
    angular.module('forms-ui').directive('navigationContainer', function (resizeService, scrollFactory, $rootScope) {
        return {
            require: {
                form: '^^bbForm'
            },
            restrict: 'A',

            link: function ($scope, element) {
                var context = (element.context) ? element.context : element[0];
                var childArray = [];
                var readyChildren = 0;
                var activeChild;
                var activeChildId = "";
                var sizeObject;
                //--
                var scrollFactoryInstance = scrollFactory.getInstance();
                //--
                var _itemActiveWatch;
                var _itemReadyWatch;
                var _resizeWatch;
                var _updateWatch;
                var _finishPage;
                var _watchShift;
                var _inputFocusWatch;
                var _inputBlurWatch;
                var _inputFocusModel;
                var _scrollToQuestionWatch;
                //--
                var focusTarget;

                _resizeWatch = $scope.$on("window-resize", resizeChilds);
                _updateWatch = $scope.$on("update", formUpdateCatch);
                _finishPage = $scope.$on("finish-page", finishPage);
                _watchShift = $scope.$on('navigation::release-shifting', releaseShift);
                _inputFocusWatch = $scope.$on('current-input-focus', handleInputFocus);
                _inputBlurWatch = $scope.$on('current-input-blur', handleInputBlur);
                //--
                $scope.goUp = goUp;
                $scope.goDown = goDown;
                //--
                scrollFactoryInstance.attach(context, context.querySelector('.questions-wrap'));
                //-

                $scope.$on('$destroy', function () {
                    console.log("navigation-container destroyed");
                    _resizeWatch();
                    _updateWatch();
                    _itemActiveWatch();
                    _itemReadyWatch();
                    _finishPage();
                    _watchShift();
                    _inputFocusWatch();
                    _inputBlurWatch();
                    _scrollToQuestionWatch();
                    childArray.length = 0;
                    childArray = null;
                    delete(childArray);
                    scrollFactoryInstance.dettach();
                    scrollFactoryInstance = null;
                });

                _scrollToQuestionWatch = $scope.$on("scroll-to-question", function (pEvent, pParam) {
                    if (!pEvent.defaultPrevented) {
                        pEvent.defaultPrevented = true;
                        var classSearch = pParam.replace(new RegExp("_", 'g'), "-");
                        var found;
                        childArray.forEach(function (pItem) {
                            if (!found) {
                                found = pItem.classList.contains(classSearch) ? pItem : null;
                                activeChild = found;
                            }
                        });
                        if (found) {
                            activeChild = found;
                            gotoActiveChild();
                        }
                    }
                    pEvent.preventDefault();
                });

                function init() {
                    setTimeout(function () {
                        setTimeout(function () {
                            //---
                            var cleanClass = $scope.element.styles.toString().replace(new RegExp(",", 'g'), ' ');
                            var activeQuestionSearch = cleanClass.match(/\bactive_question_(?:\d*)\b/g);
                            var activeQuestion = (activeQuestionSearch) ? ~~(activeQuestionSearch[0].replace("active_question_", "")) : null;
                            console.log("activeQuestionSearch", activeQuestionSearch);
                            //---
                            console.log("navigation-container init:: activeQuestion?", activeQuestion);
                            activeChild = (activeQuestion) ? childArray[activeQuestion - 1] : childArray[childArray.length - 1];
                            activeChild = (!activeChild) ? childArray[childArray.length - 1] : activeChild;
                            console.log("activeChild:: ", activeChild.className);
                            /* Fallback when error TODO: Remove when model doesn't provide wrong info */
                            /* if (!activeChild) activeChild = childArray[childArray.length - 1];
                             activeQuestion = null;
                             if (activeQuestionSearch) {
                             var activePick = activeQuestionSearch[0];
                             activePick = activePick.replace(new RegExp('_', 'g'), '-');
                             context.classList.remove(activePick);
                             activeQuestionSearch = null;
                             }*/
                            resizeService.triggerResize();
                            //resizeChilds();

                            if (!gotoActiveChild()) {
                                $scope.$broadcast("activate-question", activeChild);
                            }

                            TweenLite.killDelayedCallsTo(gotoActiveChild);
                            TweenLite.delayedCall(.1, gotoActiveChild);
                        }, 0);
                    }, 0);
                }

                _itemReadyWatch = $scope.$on("question-item-ready", function (pEvent, pItem) {
                    //childMap[readyChildren] = pItem;
                    childArray.push(pItem);
                    readyChildren++;
                    checkReadyItems();
                });

                _itemReadyWatch = $scope.$on("question-item-destroyed", function (pEvent, pItem) {
                    var search = childArray.indexOf(pItem);
                    childArray.splice(search, 1);
                    readyChildren--;
                });


                _itemActiveWatch = $scope.$on("question-active", function (pEvent, pId) {
                    if (activeChildId === pId)return;
                    childArray.forEach(function (pItem) {
                        if (pItem.getAttribute("question-id") === pId) {
                            activeChild = pItem;
                            activeChildId = pId;
                            $rootScope.$broadcast("navigation::active-question", activeChildId);
                            //var textTarget = activeChild.querySelector('input[type="text"]');
                            var textTarget = activeChild.querySelector('input');
                            if (textTarget) {
                                TweenLite.killDelayedCallsTo(focusInput);
                                TweenLite.delayedCall(.5, focusInput, [textTarget]);
                            }
                        }
                    });

                });
                /// --------------------------------------------------------------------------------------------------------------
                /// --------------------------------------------------------------------------------------------------------------
                /// --------------------------------------------------------------------------------------------------------------
                function focusInput(pTarget) {
                    $(pTarget).focus();
                }

                function handleInputFocus(pEvent, pParam) {
                    focusTarget = pParam;
                    document.body.addEventListener("keyup", keyboardCatch);
                    //document.body.addEventListener("focus",handleInputBlur);
                }

                function handleInputBlur(pEvent, pParam) {
                    //console.log("handleInputBlur", pEvent, pParam);
                    focusTarget = null;
                    document.body.removeEventListener("keyup", keyboardCatch);
                }

                function keyboardCatch(pEvent) {
                    if (pEvent.keyCode && pEvent.keyCode === 13) {
                        var found;
                        childArray.forEach(function (pItem) {
                            if (!found && pItem.contains(focusTarget.target)){
                                found = pItem;
                            }
                        });
                        $scope.$broadcast('force-emit',found);
                    }
                }

                function releaseShift(pEvent) {
                    if (context.classList.contains("shift-right")) context.classList.remove("shift-right");
                }

                function gotoActiveChild() {
                    ///console.log("goToActiveChild",activeChild);
                    return scrollFactoryInstance.scrollTo(activeChild, 0);
                }

                function finishPage(pEvent, pTargetEvent) {
                    console.log("finishPage");
                    pTargetEvent.stopPropagation();
                    TweenLite.to(context, .25, {
                        opacity: 0, onComplete: function () {
                            console.log("pageFaded");
                            pTargetEvent.targetScope.$emit("update");
                        }
                    });
                    TweenLite.to(context, .25, {ease: Circ.easeIn, y: -100});
                }

                function formUpdateCatch(pEvent, pTarget) {
                    //if (pTarget) console.log("formUpdateCatch", pTarget.styles);
                    if (pTarget && pTarget.styles.indexOf("scroll_event__next") !== -1) {
                        setTimeout(function () {
                            var error = false;
                            childArray.forEach(function (pItem) {
                                if (!error) {
                                    if (pItem.querySelector(".ng-invalid")) {
                                        error = pItem;
                                    }
                                }
                            });
                            if (!error) {
                                console.log("didn't find error in form going to activeChild");
                                goDown();
                            } else {
                                console.log("found error child", activeChild);
                                activeChild = error;
                                gotoActiveChild();
                            }
                        }, 50);
                    }

                    if (pTarget && pTarget.styles.indexOf("video_event__fadevideo") !== -1) {
                        $rootScope.$broadcast('fade-video');
                    }
                    if (pTarget && pTarget.styles.indexOf("scroll_event__finish") !== -1) {
                        $rootScope.$broadcast('accordion::collapse');
                        $rootScope.$broadcast('navigation::release-shifting');
                        pEvent.stopPropagation();
                        TweenLite.to(context, .25, {
                            opacity: 0, onComplete: function () {
                                pEvent.targetScope.$emit("update");
                            }
                        });
                        TweenLite.to(context, .25, {ease: Circ.easeIn, y: -100});
                    }

                }

                function resizeChilds() {
                    //var rect = context.getBoundingClientRect();
                    sizeObject = resizeService.getSize();
                    //console.log("NC: context",context,"RC:: WinH:: ", sizeObject.h, "RT",rect.top,"offTop",context.offsetTop);
                    $scope.$broadcast("questions-align", {
                        w: sizeObject.w,
                        questionH: sizeObject.h - context.offsetTop,
                        winH: sizeObject.h - context.offsetTop
                    });
                }

                function checkReadyItems() {
                    var isReady = (($scope.element.children.length) === readyChildren);
                    if (isReady) init();
                }

                function goUp() {
                    resizeService.triggerResize();
                    if (activeChild) {
                        var index = childArray.indexOf(activeChild);
                        if (index > 0) {
                            scrollFactoryInstance.scrollTo(childArray[index - 1], 0, 0);
                        }
                    }
                }

                function goDown() {
                    //resizeService.triggerResize();
                    console.log("goDown currentActive", activeChild.className);
                    if (activeChild) {
                        var index = childArray.indexOf(activeChild);
                        if (index < childArray.length - 1) {
                            scrollFactoryInstance.scrollTo(childArray[index + 1], 0, 0);
                        }
                    }
                }
            }
        };
    });
})();