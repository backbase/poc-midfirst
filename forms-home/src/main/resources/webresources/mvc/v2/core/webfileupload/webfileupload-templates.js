(function (blueriq, bqApp) {
    'use strict';

    blueriq.webFileUploadTemplateFactory = function (viewModel, bindingContext) {

        var prefix = viewModel.context.configuration.templatePath;

        if (viewModel && viewModel.type === 'webFileUpload') {
            return blueriq.templates.webfileupload.webfileupload;
        }
    };

    if (bqApp) {
        bqApp.templateFactory.registerModelHandler(blueriq.webFileUploadTemplateFactory);
    }

})(window.blueriq, window.bqApp);
