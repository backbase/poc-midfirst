/**
 * @name uiBBMBeneficiarySelect
 * @type {object}
 *
 * @property {array<AccountsView>} accounts List of accounts to filter and select with user input
 * @property {boolean} allowCreate Is creating of a new beneficiary allowed
 * @property {object} messages Localized messages
 * @property {object} ngModel Beneficiary object
 * @property {function} onButtonClick Handler for button clicks
 */
const component = {
  bindings: {
    allowCreate: '<',
    accounts: '<',
    messages: '<',
    ngModel: '<',
    enableIban: '<',
    onButtonClick: '&',
  },
  controller: 'controller',
  template: `
    <div 
      class="ui-bbm-beneficiary-select"
      ng-class="{
        'ui-bbm-beneficiary-select--has-focus': $ctrl.state.activeField,
        'ui-bbm-beneficiary-select--has-focus-name': $ctrl.state.activeField === 'name', 
        'ui-bbm-beneficiary-select--has-focus-identifier': $ctrl.state.activeField === 'identifier'
      }" 
      data-role="beneficiary-select">
      <div class="ui-bbm-beneficiary-select-inner">
        <div class="ui-bbm-beneficiary-select-button">
          <button
            class="bbm-button"
            type="button"
            data-role="beneficiary-select-button"
            ng-click="$ctrl.onButtonClick()">
            <span class="bbm-icon bbm-icon--addressbook fa fa-book fa-2x" />
          </button>
        </div>
        <div class="ui-bbm-beneficiary-select-name table-view-cell shcm-floatlabel">
          <input
            class="bbm-input"
            type="text"
            name="name"
            ng-model="$ctrl.state.beneficiary.name"
            ng-change="$ctrl.onNameInputChanged()"
            autocomplete="off"
            autocorrect="off"
            autocapitalize="off"
            ng-focus="$ctrl.onNameFocus($event)"
            ng-blur="$ctrl.onBlur()"
            data-role="beneficiary-select-name"
            required />
          <label>{{ $ctrl.messages.namePlaceholder }}</label>
        </div>
        <div class="input-group">
          <div class="ui-bbm-beneficiary-select-identifier input-group__item shcm-floatlabel">
          <div ng-include="$ctrl.inputTemplate"></div>            
          </div>
        </div>
        <!-- SEARCH RESULTS -->
        
        
        <ul  class="ui-bbm-beneficiary-select-list table-view"   ng-class="{'no-results':(!$ctrl.state.suggestions.length || $ctrl.state.suggestions.length===0)}" data-role="beneficiary-select-list" ng-if="$ctrl.state.isListOpened">        
         <li
            class="list-group-item" data-role="contact" ng-repeat="account in $ctrl.state.suggestions track by $index"
            ng-click="$ctrl.onAccountClick(account)">
            <div class="bbm-contact" ng-class="{'bbm-contact--image':(account.photoUrl)}">                 
              <div class="bbm-contact-avatar" ng-class="{'bbm-contact-avatar--image-avatar':(account.photoUrl)}">
                <ui-bb-avatar                   
                  class="bbm-avatar bbm-avatar-sm bbm-avatar-circle"
                  name="account.name"
                  image="account.photoUrl">
                  </ui-bb-avatar>
              </div>
              <div class="bbm-contact-information">
                <h4
                  class="bbm-contact-name"
                  data-role="contact-name"
                  ng-bind="account.name"></h4>
                <div class="bbm-contact-identifier">
                  <span
                    class="prevent-ios-click"
                    data-role="contact-iban"
                    ng-bind="account.identifier | formatIBAN">
                  </span>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  `,
};

export default component;
