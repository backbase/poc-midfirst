/**
 * Removes whitespaces from a given string
 * @param {string} str
 * @returns {string}
 * @inner
 */
const removeWhitespaces = (str) => (
  str.replace(/\s/g, '')
);

/**
 * @param {object} accountA
 * @param {object} accountB
 * @returns {boolean}
 */
const isSameAccount = (accountA, accountB) => Boolean(
  (accountA.name || '').trim() === (accountB.name || '').trim() &&
  (accountA.identifier || '').trim() === (accountB.identifier || '').trim()
);

export const formatIBAN = (iban) => iban && iban.replace(/(....)/g, '$1 ');

/**
 * Creates a search index for the given list of accounts
 * @param {Array<object>} accounts
 * @returns {Array<object>}
 * @inner
 */
export const createSearchIndex = (accounts) => (
  (accounts || []).map(account => {
    const iban = (account.identifier || '').toLowerCase().trim();
    const name = (account.name || '').toLowerCase();
    return [
      name,
      iban,
      removeWhitespaces(iban),
    ];
  })
);

/**
 * @param {Array.<Object>} accounts
 * @param {Object} account
 * @returns {boolean}
 */
export const isNewBeneficiary = (accounts, account) => (
  !accounts.some(item => isSameAccount(item, account))
);

/**
 * Returns true, if the name of the given account matches
 * to the given query string. False otherwise.
 * @param {string} query
 * @param {Array<string>} accountSearchStrings
 * @returns {boolean}
 */
export const matches = (query, accountSearchStrings) => (
  query && accountSearchStrings.some(str => str.includes(query))
);
