/**
 * @module widget-midfirst-bill-pay-ng
 * @name BillPayController
 *
 * @description
 * Bill pay
 */

export default function BillPayController(bus, hooks, widget) {
  const $ctrl = this;

  /**
   * AngularJS Lifecycle hook used to initialize the controller
   *
   * @name BillPayController#$onInit
   * @type {function}
   * @returns {void}
   */
  const $onInit = () => {
    $ctrl.items = hooks.itemsFromModel([]);

    bus.publish('cxp.item.loaded', {
      id: widget.getId(),
    });

    bus.publish('bb.item.loaded', {
      id: widget.getId(),
    });
  };

  Object.assign($ctrl, {
    $onInit,

  });
}
