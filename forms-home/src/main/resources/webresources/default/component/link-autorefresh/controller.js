
(function () {
    angular.module('forms-ui').directive('linkAutoRefresh', function ($location) {
        return {
            require: {
                form: '^^bbForm'
            },
            restrict: 'A',
            link: function ($scope, element, attrs, formsController) {
                formsController.form.emitBehaviour($scope.element._e.plainText);
            }
        };
    });
})();