(function(blueriq, bqApp, ko) {
	'use strict';

	// create namespace
	blueriq.models.bootcards = {};

	blueriq.defaultBootcardsModelFactory = function(model, context) {

		function isBootcardsPage(model, context) {
			if (model.type === 'page') {
				for (var i = 0; i < model.children.length; i++) {
					var element = context.session.getModel(model.children[i]);
					if (element.contentStyle
						&& (element.contentStyle === 'dashboard_menu' ||
							element.contentStyle === 'dashboard_body' ||
							element.contentStyle === 'dashboard_menuback')) {
						return true;
					}
				}
			}
			return false;
		}

		if (model.type === 'page') {
			if (isBootcardsPage(model, context)) {
				return new blueriq.models.bootcards.PageModel(model, context);
			}
		} else if (model.contentStyle && model.contentStyle.indexOf('list') > -1 && model.styles.indexOf('mobilelist') > -1) {
			return new blueriq.models.bootcards.ListContainerModel(model,context);
		} else if (model.contentStyle === 'tablerow') {
			return new blueriq.models.bootcards.ListItemModel(model, context);
		} else if (model.contentStyle === 'formfooter') {
			return new blueriq.models.bootcards.FormFooterModel(model, context);
		}
	};

	/**
	 * Register automatically if bqApp is already created
	 */
	if (bqApp) {
		bqApp.modelFactory.register(blueriq.defaultBootcardsModelFactory);
	}

	/**
	 * Extends table so it can be displayed as a lis
	 */
	blueriq.models.bootcards.ListContainerModel = function ListContainerModel(model, context) {

		var self = this;
		blueriq.models.ContainerModel.call(self, model, context);
		this.type = 'mobilelist';

		this.table = ko.observable();

		for(var i=0; i<this.children().length; i++){
			if(this.children()[i].type === 'table'){
				this.table(this.children()[i]);
			}
		}
	};

	blueriq.models.bootcards.ListItemModel = function ListItemModel(model,context) {
		var self = this;

		blueriq.models.ContainerModel.call(self, model, context);
		this.type = 'listitem';
		this.icon = blueriq.iconFactory.getIconByCode('icon_file_o');
		this.captions = new Array();
		this.offlineavailability = false;

		if(context.configuration.isMobile && model.properties.offlineavailability) {
			this.offlineavailability = true;

			this.taskDetails ={
					taskName: model.properties.taskname,
					taskDisplayName: model.properties.taskdisplayname,
					taskId: model.properties.taskid,
					caseId: model.properties.caseid,
					persistencyId: model.properties.persistencyid
			}
			if(model.properties.taskcustomdisplayproperty){
				this.taskDetails.taskCustomDisplayProperty = model.properties.taskcustomdisplayproperty;
			}

			var key = context.sessionService.storage.composeKey('mobile.offline', this.taskDetails.caseId, this.taskDetails.persistencyId);
			this.buttonClass = ko.observable('fa-3x pull-right');
			this.actionType = ko.observable('');
			context.session.getButtonAction(key, function(newActionType){
				self.actionType(newActionType);
				context.session.getButtonClass(newActionType, function(newClassName){
					self.buttonClass('fa-3x pull-right ' + newClassName)
				});
			});

			var updateButtonClass = function updateButtonClass(className) {
				context.session.getButtonAction(key, function(newActionType){
					self.actionType(newActionType);
					self.buttonClass('fa-3x pull-right ' + className);
				});
			}
			this.taskOfflineClick = function taskOfflineClick() {
				context.session.executeOfflineTaskAction(self.actionType(), context.session.id, this.taskDetails, updateButtonClass);
			}

		}

		// get action for item
		// TODO the header check should not be needed?
		if (model.name !== 'header') {
			for (var j = 0; j < this.children().length; j++) {
				//check if table cell has content
				if(this.children()[j].children().length > 0) {
					//if content is a button
					if (this.children()[j].children()[0].type === 'button') {
						//can set the action only one time, other buttons will be ignored
						if (typeof this.action === 'undefined'){
							this.action= this.children()[j].children()[0];
						}
						// TODO: support multiple buttons
					} else {
						if(this.children()[j].children()[0].text){
							this.captions.push(this.children()[j].children()[0].text);
						}else {
							if (this.children()[j].children()[0].displayValue) {
								this.captions.push(this.children()[j].children()[0].displayValue());
							}
						}
					}
				}
			}
		}

	};

	blueriq.models.bootcards.PageModel = function PageModel(model, context) {
		var self = this;
		if(model.properties.offlineenabled){
			context.configuration.offlineEnabled = true;
		}
		blueriq.models.dashboard.PageModel.call(self, model, context);
		var offlineAvailabilityHandler = function(){
			if (context.configuration.offlineEnabled) {
				var offlineLanguage={
						offlinebutton : model.properties.offlinebutton,
						offlinenotification : model.properties.offlinenotification,
						onlinebuttonn : model.properties.onlinebutton,
						onlinenotification : model.properties.onlinenotification,
						offlinesavebutton : model.properties.offlinesavebutton,
						offlinecancelbutton : model.properties.offlinecancelbutton,
						offlineworklistheader : model.properties.offlineworklistheader,
						offlinepreparationdate : model.properties.offlinepreparationdate,
						offlinenotasks : model.properties.offlinenotasks
				}

				context.sessionService.storage.insert("offlineLanguage",offlineLanguage);
				context.session.networkAvailabilitySubscription = context.messageBus.subscribe('networkAvailability',function (id, networkAvailable) {
					self.isOffline(!networkAvailable);
	            });
			}
		}

		var pageModelOffline = null;
		var taskFinished = null;

		if(context.configuration.isMobile){
			this.isMobile = true;
			this.isOffline = ko.observable(false);
			if (context.configuration.offlineEnabled) {
				this.offlinebutton=model.properties.offlinebutton;
				this.offlinenotification=model.properties.offlinenotification;
			}
			this.goOffline = function() {
				context.configuration.exitAndRestartApp();
			}
			offlineAvailabilityHandler();
		}else{
			this.isMobile = false;
		}

		// function for getting the model from children based on contentstyle
		function getmodel(contentStyle) {
			if (self.menu() === null || typeof self.menu === 'undefined') {
				return null;
			}

			for (var i = 0; i < self.menu().children().length; i++) {
				if (self.menu().children()[i].contentStyle === contentStyle) {
					return self.menu().children()[i];
				} else if (self.menu().children()[i].contentStyle === contentStyle) {
					return self.menu().children()[i];
				}
			}
			return null;
		}

		/**
		 * submenus of dashboard_menu
		 */
		this.userMenu = ko.computed(function() {
			return getmodel('usermenu');
		});

		this.menubar = ko.computed(function() {
			return getmodel('menubar');
		});

		this.backButton = ko.computed(function() {
			var result = null;
			for (var i = 0; i < this.children().length; i++) {
				if (this.children()[i].contentStyle === 'dashboard_menuback') {
					result = this.children()[i].children()[0];
				}
			}

			return result;
		}, this);

		this.mobilefooter = ko.observable(null);

		this.isFooterActive = ko.computed(function() {
			if (self.mobilefooter() === null) {
				return false;
			} else if (typeof self.mobilefooter() === 'undefined') {
				return false;
			} else {
				return self.mobilefooter().visible;
			}
		}, self);

		// listeners
		var subLoad = context.messageBus.subscribe('onFormFooterLoaded',
			function(e, viewmodel) {
				console.log('set footer');
				self.mobilefooter(viewmodel);
                subLoad.dispose();

			});
		var subUnload = context.messageBus.subscribe('onFormFooterDisposed',
			function(e, viewmodel) {
				console.log('remove footer '+viewmodel.isLoaded);
                self.mobilefooter(null);
                subUnload.dispose();
			});
	};

	/**
	 * formfooter for mobile with a notify so page model
	 *
	 * @param model
	 * @param context
	 * @constructor
	 */
	blueriq.models.bootcards.FormFooterModel = function FormFooterModel(model,
																		context) {
		var self = this;
        this.isLoaded = false;
		blueriq.models.ContainerModel.call(self, model, context);

		console.log('blueriq.models.bootcards.FormFooterModel');

		this.visible = ko.observable(true);

		this.loaded = function() {
            this.isLoaded = true;
			context.messageBus.notify('onFormFooterLoaded', self);
		};

		var subHide = context.messageBus.subscribe('native.showkeyboard',
			function(e) {
				self.visible(false);
			});

		var subShow = context.messageBus.subscribe('native.hidekeyboard',
			function(e) {
				self.visible(true);
			});

		this.dispose = function() {
            subHide.dispose();
            subShow.dispose();
            self.context.messageBus.notify('onFormFooterDisposed', self);
		};

	};

	/**
	 * Viewmodel for the offline worklist
	 */
	blueriq.models.bootcards.OfflineWorkListModel = function OfflineWorkListModel(model, context) {
		var self = this;
		var storage = context.sessionService.storage;
		this.displayTask = ko.observable(false);
		this.displayTaskList = ko.observable(false);
		this.taskModel = ko.observable("");
		this.preparedOn = ko.observable("");
		var preparationDateText = "";
		var offlineAvailabilityHandler = function(){
			context.session.networkAvailabilitySubscription = context.messageBus.subscribe('networkAvailability',function (id, networkAvailable) {
                if(networkAvailable){
                	self.isOnline(true);
                }else{
                	self.isOnline(false);
                }
            });
		}
		context.session = new blueriq.SessionController(model.sessionId, model.sessionId, context.sessionService, context.utilityService, context.messageBus, context.eventHandler);
		this.templateName = 'themes/bootcards/template/offline/listoffline';

		var visitOfflineElements = function(model, callback){
			var queue = [model];
			while(queue.length > 0){
				var viewModel = queue.shift();
				if(viewModel.children && (typeof viewModel.children == 'function')){
					$(viewModel.children()).each(function(index, elem){
						queue.push(elem);
					});
				}
				callback(viewModel);
			}
		}

        /**
         * Starts the offline execution of a task. Buttons and refresh fields are disabled.
         *
         * @function
         * @name blueriq.models.bootcards#executeOfflineTask
         * @param {Object} item Offline task data
         */
		this.executeOfflineTask = function (offlineTaskModel) {
		    var newPageModel = {};
		    context.session.models = newPageModel;
		    var pageElement = "";
		    for (var i = 0; i < offlineTaskModel.elements.length; i++) {
		        var element = offlineTaskModel.elements[i];
		        var key = element.key;
		        newPageModel[key] = element;
		        if (element.type == 'page') {
		            pageElement = element;
		        }else if(element.type === 'field'){
		        	element.refresh = false;
		        	if (offlineTaskModel.values && offlineTaskModel.values[element.key]) {
		        		element.values = offlineTaskModel.values[element.key];
		        	}
		        }else if(element.type === 'button'){
		        	element.disabled = true;
		        }
		    }
		    self.taskKey = offlineTaskModel._key;
		    var taskViewModel = new blueriq.models.bootcards.PageModel(pageElement, context);
		    self.taskModel(taskViewModel);
		    self.displayTask(true);
		    self.displayTaskList(false);
		    if (offlineTaskModel.createdOn) {
		    	var creationDate = offlineTaskModel.createdOn.slice(0,offlineTaskModel.createdOn.indexOf('.')).replace('T', ' ');
		    	self.preparedOn(preparationDateText + ' ' + creationDate);
		    }
		}
		this.handleSave = function(){
			var fieldValues ={};
			visitOfflineElements(self.taskModel(), function(viewModel){
				if(viewModel.type ==='field'){
					fieldValues[viewModel.key] = viewModel.values();
				}
			});
			self.displayTask(false);
			storage.get(self.taskKey, function(storedTask){
				storedTask.values = fieldValues;
				storage.insert(self.taskKey,storedTask);
				self.displayTaskList(true);
			});
		}
		this.handleCancel = function(){
			this.displayTask(false);
			self.displayTaskList(true);
		}

		if(context.configuration.isMobile){
			self.isMobile = true;
			self.isOnline = ko.observable(false);
			self.titleName = ko.observable('Offline Tasks');
			self.cancelText = ko.observable('Cancel');
			self.saveText = ko.observable('Save');
			self.onlinebutton = ko.observable('Go Online');
			self.onlinenotification = ko.observable('Online Available');
			self.noTasksText = ko.observable('No tasks');
			storage.get('offlineLanguage', function(offlineLanguage){
                self.titleName(offlineLanguage.offlineworklistheader);
				self.cancelText(offlineLanguage.offlinecancelbutton);
				self.saveText(offlineLanguage.offlinesavebutton);
				self.onlinebutton(offlineLanguage.onlinebuttonn);
				self.onlinenotification(offlineLanguage.onlinenotification);
				self.noTasksText(offlineLanguage.offlinenotasks);
				preparationDateText = offlineLanguage.offlinepreparationdate;
			});

			self.goOnline = function() {
				context.configuration.exitAndRestartApp();
			}
			context.session.networkAvailabilitySubscription = context.messageBus.subscribe('networkAvailability',function (id, networkAvailable) {
                if(networkAvailable){
                	self.isOnline(true);
                }else{
                	self.isOnline(false);
                }
            });
		}else{
			self.isMobile = false;
		}

		this.items = ko.observableArray();
		storage.getAll('mobile.offline', function(taskItems){
			self.availableOfflineTasks = ((taskItems.length > 0) ? true : false);
			var customPropertyPresent = false;
			for (var key=0; key < taskItems.length; key ++ ) {
				var taskItem = taskItems[key];
				if(!taskItem.values){
					if(taskItem.taskCustomDisplayProperty){
						customPropertyPresent = true;
						break;
					}
				}
			}

			for (var key=0; key < taskItems.length; key ++ ) {
				var taskItem = taskItems[key];
				if(!taskItem.values){
					if(taskItem.taskCustomDisplayProperty){
						taskItem.taskDescription = taskItem.taskName;
						taskItem.taskName = taskItem.taskCustomDisplayProperty;
					}else {
						taskItem.taskDescription = ' ';
					}
				}
				taskItem.remove = function(){
					self.items.pop(taskItem);
					storage.remove(taskItem._key);
				}
				self.items.push(taskItem);
			}
			self.items.sort(function(a, b){
				if(a.taskName < b.taskName) return -1;
			    if(a.taskName > b.taskName) return 1;
			    return 0;
			})
			self.displayTaskList(true);
		});
	};


	/**
	 * fieldmodel for mobile with a prop for showing explanation text
	 *
	 * @param model
	 * @param context
	 * @constructor
	 */
	var BaseFieldModel = blueriq.models.FieldModel;
	blueriq.models.FieldModel = function FieldModel(model,context) {
		var self = this;
		BaseFieldModel.call(self, model, context);
		this.explanationTextVisible = ko.observable(false);

	};

}(window.blueriq, window.bqApp, window.ko));