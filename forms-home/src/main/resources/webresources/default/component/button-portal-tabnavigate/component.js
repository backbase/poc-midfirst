(function() {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'button-portal-tabnavigate',
            condition: function(el) {
                return [
                    el.isButton,
                    el.styles.indexOf('button_portal_tabnavigate') > -1
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();
