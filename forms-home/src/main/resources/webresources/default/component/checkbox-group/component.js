(function() {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'checkbox-group',
            controller: 'CheckboxGroupController as cbg',
            condition: function(el) {
                return [
                    el.hasOptions,
                    el.multiple
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();
