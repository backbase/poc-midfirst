(function() {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'input-datetime',
            controller: 'BootstrapDateTimePickerController as controller',
            condition: function(el) {
                return [
                    el.dataType === 'datetime'
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();
