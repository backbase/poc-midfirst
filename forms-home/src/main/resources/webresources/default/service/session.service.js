(function() {
    'use strict';
    /* jshint validthis: true */

    var HTML_SESSIONID_MATCHER = /appUri:.*?server\/vaadin\/sessionTools-(.+)'/;
    var DEFAULT_THEME = 'forms';
    var DEFAULT_UI = 'mvc';

    /**
     * Create and populate form sessions
     * @service SessionService
     * @ngInject
     */

    function SessionService($http, $sessionStorage, $q, StringUtils, Session,$cookies) {

        /**
         * Create a form session
         * @param {Object} options      Session options
         * @return {Promise<Session>}
         */
        function createSession(options) {
            console.log("FORMS_UI SessionService:: createSession",options);
            var session = new Session(options);

            var initialize = initializeSession.bind(null, session);
            var subscribe = subscribeToSession.bind(null, session);

            return $q.when(session)
                .then(initialize)
                .then(subscribe)
                .then(returnSession);

            function returnSession() {
                console.log("FormsUI SessionService LastStep ReturnSession",session);
                return session;
            }

        }

        function destroySession(session) {
            console.log("DestroySession");
            var storageKey = session.getSessionKey();
            $sessionStorage.setItem(storageKey, '');
            $sessionStorage.setItem(storageKey+"username", 'nousername');
            session.sessionId = '';
            session.username =  'nousername';
        }

        /**
         * Subscribe to model updates on a session
         * @param {Session} session
         * @return {Promise}
         * @private
         */
        function subscribeToSession(session) {
            console.log("FORMS_UI:: subscribeToSession",session);

            //var runtimePath = (session.options.runtimePath)?session.options.runtimePath: '../../../..';
            var runtimePath = session.options.runtimePath;
            var subscribeUrl = runtimePath+ '/server/session/' + session.sessionId + '/api/subscribe';

            console.log("FORMS_UI:: SessionService subscribeToSession RPATH",session.options.runtimePath);
            console.log("subscribeToSession:: doing postCallX",subscribeUrl);
            return $http.post(subscribeUrl);
        }

        /**
         * Initialize a session object
         * Grab a session id from local storage or create a new session otherwise
         * @private
         */
        function initializeSession(session) {
            console.log("FORMS_UI:: initializeSession",session);
            var options = session.options;

            // default values for theme/ui, used on forms runtime
            options.ui = options.ui || DEFAULT_UI;
            options.theme = options.theme || DEFAULT_THEME;

            var storageKey = session.getSessionKey();
            var sessionId = options.sessionId || $sessionStorage.getItem(storageKey);
            var usernameCookie =  $sessionStorage.getItem(storageKey+"username");

            console.log("checking username... picked from cookie :: ",usernameCookie);
            console.log("checking username... picked from session :: ",options.username );

            var sameUsername  = (usernameCookie  === session.options.username);


            if ((sessionId && sameUsername) || !window.b$) {
                console.log("theres a previous sessionId and the username is same so no createSession");
                session.sessionId = sessionId;
                return $q.when(session);
            }
            //--
            console.log("there's no previous session or usernames are different:: creating session");
            //--
            return createSessionId(session).then(function() {
                $sessionStorage.setItem(storageKey, session.sessionId);
                $sessionStorage.setItem(storageKey+"username", session.username);
                console.log("setting username in cookie::",session.username);
            });
        }

        /**
         * Get a session id from forms runtime
         * @private
         */
        function createSessionId(session) {
            console.log("FORMS_UI:: SessionService createSessionId");
            var urlOptions = StringUtils.toQueryString(session.options);
            console.log("FORMS_UI:: SessionService createSessionId RPATH",session.options.runtimePath);
            //var runtimePath = (session.options.runtimePath)?session.options.runtimePath: '../../../..';
            var runtimePath = session.options.runtimePath;
            var sessionUrl = runtimePath + '/server/start?' + urlOptions;
            sessionUrl+="&jsessionid="+$cookies.get('JSESSIONID');
            var sessionUsername = session.options.username;

            console.log("FORMS_UI:: SessionService createSessionId sessionUrl",sessionUrl);
            // TODO check invalid session ID and recover from it
            //return $http.get(sessionUrl).then(function(response) {
            return $http.get(sessionUrl).then(function(response) {
                var sessionData = response.data;
                var sessionId;

                if (typeof sessionData === 'object') {
                    sessionId = sessionData.sessionId;
                } else {
                    sessionId = parseSessionFromHtml(sessionData);
                }

                session.sessionId = sessionId;
                session.username =  sessionUsername;


                return session;
            });
        }

        /**
         * Grabs a session ID from an HTML document returned on forms runtime call
         * @private
         */
        function parseSessionFromHtml(html) {
            var sessionId = html.match(HTML_SESSIONID_MATCHER);
            return sessionId && sessionId[1] || '';
        }

        return {
            createSession: createSession,
            destroySession: destroySession,

            DEFAULT_THEME: DEFAULT_THEME,
            DEFAULT_UI: DEFAULT_UI
        };
    }

    angular.module('forms-ui').service('SessionService', SessionService);
})();
