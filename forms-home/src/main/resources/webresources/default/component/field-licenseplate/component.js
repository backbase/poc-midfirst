(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'field-licenseplate',
            condition: function(el) {
                return [el.isField,el.styles.indexOf("licenseplate") > -1,el.styles.indexOf("nolabel") > -1];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();