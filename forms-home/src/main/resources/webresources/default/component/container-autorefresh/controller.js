(function() {
    angular.module('forms-ui').directive('containerAutorefresh',  /*@ngInject*/ function () {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs,$filter) {
                var context = (element.context)?element.context:element[0];
                setTimeout(function () {
                    $scope.$broadcast("force-emit");
                },20);
            }
        };
    });
})();
