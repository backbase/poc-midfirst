(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'field-nolabel',
            condition: function(el) {
                return [el.isField,
                        el.styles.indexOf("nolabel") > -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();