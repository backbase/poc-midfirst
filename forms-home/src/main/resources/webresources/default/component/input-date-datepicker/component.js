(function() {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'input-date-datepicker',
            condition: function(el) {
                return [
                    el.dataType === 'date',
                    el.styles.indexOf("datepicker") > -1
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();
