(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'input-currency',
            condition: function(el) {
                return [
                    el.dataType === 'currency'
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();