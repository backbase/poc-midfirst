/**
 * @module widget-midfirst-actions-ng
 * @name ActionsController
 *
 * @description
 * Actions
 */

export default function ActionsController(bus, hooks, widget) {
    const $ctrl = this;


    let actionsList = [
        {
            id: 0,
            title: "Push notification when Checking Account is less than $500",
            description: "Notifications",
            icon: "push",
            enabled: false,
            expanded: false,
        },
        {
            id: 1,
            title: "Replenish checking account when lower than $200",
            description: "Internal transfers",
            icon: "replenish",
            enabled: true,
            expanded: false,
        },
        {
            id: 2,
            title: "Push notification on transactions bigger than $500",
            description: "Notifications",
            icon: "push",
            enabled: true,
            expanded: false,

        },
        {
            id: 3,
            title: "Settle debits at the end of the month",
            description: "External transfers",
            icon: "settle",
            enabled: true,
            expanded: false,
        },
        {
            id: 4,
            title: "Push notification about rejected payments",
            description: "Notifications",
            icon: "push",
            enabled: true,
            expanded: false,
        },
        {
            id: 5,
            title: "Push notification about suspicious logins",
            description: "Notifications",
            icon: "push",
            enabled: true,
            expanded: false,
        },
    ]

    const toggleExpanded = (actionId) => {

        console.log('toggled' + actionId);

        this.actionsList.forEach(action => {
            if (action.id === actionId) {
                action.expanded = !action.expanded;
            }
        })
    }


    /**
     * AngularJS Lifecycle hook used to initialize the controller
     *
     * @name ActionsController#$onInit
     * @type {function}
     * @returns {void}
     */
    const $onInit = () => {
        $ctrl.items = hooks.itemsFromModel([]);

        bus.publish('cxp.item.loaded', {
            id: widget.getId(),
        });

        bus.publish('bb.item.loaded', {
            id: widget.getId(),
        });
    };

    Object.assign($ctrl, {
        $onInit,
        actionsList,
        toggleExpanded,
    });
}
