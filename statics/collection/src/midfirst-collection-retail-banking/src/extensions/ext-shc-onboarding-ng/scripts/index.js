/**
 * @module ext-shc-onboarding-ng
 *
 * @description
 * Basic extension for widget-shc-onboarding-ng
 *
 * @requires vendor-bb-angular-ng-aria
 */
import ngAriaModuleKey from 'vendor-bb-angular-ng-aria';

// uncomment below to include CSS in your extension
// import '../styles/index.css';

import "./typer";

export const dependencyKeys = [
  ngAriaModuleKey,
];

export const hooks = {};

export const helpers = {};

export const events = {};
