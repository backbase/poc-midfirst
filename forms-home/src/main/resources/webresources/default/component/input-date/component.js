(function() {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'input-date',
            condition: function(el) {
                return [
                    el.dataType === 'date'
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();
