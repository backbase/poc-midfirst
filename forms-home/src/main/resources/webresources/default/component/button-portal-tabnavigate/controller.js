(function () {
    angular.module('forms-ui').directive('buttonPortalTabNavigate', function ($location) {
        return {
            require: {
                form: '^^bbForm'
            },
            restrict: 'A',
            link: function ($scope, element, attrs, formsController) {
                var context = (element.context) ? element.context : element[0];
                var captionSplit = $scope.element.get('caption').split('||');
                var targetPage = captionSplit[1];
                var targetContent = captionSplit[2];
                $scope.captionText = captionSplit[0];
                console.log("ButtonPortaTablNavigate targetPage:: ",targetPage,'$scope.captionText',$scope.captionText);
                // ----------------------------
                $scope.clickEvent = function () {
                    if (gadgets) {
                        var tabPanelsParent = $(context).closest(".tab-content");
                        var claimTab = tabPanelsParent.find(".dashboard-tab--"+targetPage);
                        var tabFound = $(claimTab).closest(".tab-pane");
                        var containerId = tabFound[0].getAttribute("id");
                        if(targetContent)gadgets.pubsub.publish('tab-switch', {tabId: containerId,tabClass:'dashboard-tab--active-'+targetContent});
                        else gadgets.pubsub.publish('tab-switch', {tabId: containerId});
                    }
//
                    if (!$scope.element.disabled) {
                        $scope.$emit('update', $scope.element);
                    }
                }
            }
        };
    });
})();