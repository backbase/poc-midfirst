(function () {
    /**
     * @ngInject
     */
    function registerElement(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'radio-record',
            condition: function (el) {
                return [
                    el.isField,
                    el.hasOptions,
                    !el.multiple,
                    el.styles.indexOf('radio_record') > -1
                ];
            }

        });
    }

    angular.module('forms-ui').run(registerElement);
})();
