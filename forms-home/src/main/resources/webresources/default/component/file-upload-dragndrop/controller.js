(function () {

    function mobileDetectorController ($scope) {
            $scope.isMobile =  (('ontouchstart' in window) || (navigator.MaxTouchPoints > 0)|| (navigator.msMaxTouchPoints > 0));
        /*var standalone = window.navigator.standalone,
            userAgent = window.navigator.userAgent.toLowerCase(),
            safari = /safari/.test( userAgent ),
            ios = /iphone|ipod|ipad/.test( userAgent );
        if( ios ) {
            if ( !standalone && safari ) {
                //browser
                $scope.isMobile =false;
            } else if ( standalone && !safari ) {
                $scope.isMobile =true;
                //standalone
            } else if ( !standalone && !safari ) {
                //uiwebview
                $scope.isMobile =true;
            }
        } else {
            $scope.isMobile =false;
        }*/
        console.log("mobileDetectorController isMobile",$scope.isMobile);
    }

    /*function FileUploadMobileController($scope,Upload) {
        console.log("FileUploadMobileController INIT");
        var csrf = $scope.$parent.$parent.csrfToken;
        var url = $scope.$parent.$parent.urlUpload;

        $scope.$watch("file", function (pNuValue, pOldValue) {
            var targetValue = (pNuValue && !pOldValue) ? pNuValue : pOldValue;
            console.log("FileUploadDragDropMobileController FILE CHANGED",targetValue);
        });
        $scope.upload = function (file) {
            console.log("Mobile UploadSelection:: ", file, "Token", form.model.csrfToken);
            /*
             if (!file || !file.name.length)
             return;

             Upload.upload({
             url: url,
             headers: {
             "X-CSRF-Token": form.model.csrfToken
             },
             file: file
             }).progress(function (evt) {
             $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
             console.log("uploadProgress:: ",$scope.progress);
             $scope.progressStyle.width = $scope.progress + '%';
             }).success(function (data) {
             console.log("uploadSuccess");
             formsController.form.handleUpdates({data: data});
             }).error(function (data) {
             console.log("upload error", data);
             }).finally(function () {
             $scope.progress = 0;
             $scope.progressStyle.width = 0;
             });
             };
        };
    } */

    function FileUploadDragDropController($scope,Upload) {

        var csrf = $scope.$parent.$parent.csrfToken;
        var url = $scope.$parent.$parent.urlUpload;
        $scope.$watch("files",function (pNuValue,pOldValue) {
            var targetValue = (pNuValue && !pOldValue)?pNuValue:pOldValue;
            if (targetValue && targetValue.length){
                upload(targetValue[0]);
            }

            function upload (file) {
                if (!file || !file.name.length)
                    return;

                Upload.upload({
                    url: url,
                    headers: {
                        "X-CSRF-Token": csrf
                    },
                    file: file
                }).progress(function (evt) {
                    $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                    console.log("uploadProgress:: ", $scope.progress);
                    $scope.progressStyle.width = $scope.progress + '%';
                }).success(function (data) {
                    console.log("uploadSuccess",data);
                    $scope.$emit("force-update",data);
                }).error(function (data) {
                    console.log("upload error", data);
                }).finally(function () {
                    $scope.progress = 0;
                    $scope.progressStyle.width = 0;
                });
            }
        });
    }

    angular.module('forms-ui').controller('mobileDetectorController', mobileDetectorController );
    angular.module('forms-ui').controller('fileUploadDragDropController', ['$scope','Upload', FileUploadDragDropController]);
    //angular.module('forms-ui').controller('FileUploadMobileController', ['$scope','Upload', FileUploadMobileController]);
    angular.module('forms-ui').directive('fileUploadDragDrop', function (Upload) {
        return {
            require: {
                form: '^^bbForm'
            },
            restrict: 'A',
            link: function ($scope, element, attrs, formsController) {
                var context = (element.context) ? element.context : element[0];

                function makeUrl(runtimeUrl, sessionId, subscription, fileuploadID) {
                    return runtimeUrl + '/server/session/' + sessionId + '/subscription/' + subscription + '/fileupload/' + fileuploadID + '/';
                }

                function createTypeFilePattern(str) {
                    if (!str || str.length === 0) {
                        return "";
                    }

                    var pattern, arr, i, l;
                    arr = str.split('|') || [];
                    for (i = 0, l = arr.length; i < l; i++) {
                        arr[i] = '.' + arr[i];
                    }

                    pattern = arr.join(',');
                    return pattern;
                }

                var url, allowedExtensions, maxFileSize;


                var uploadProperties = $scope.element.properties;

                console.log("received:: uploadProperties", uploadProperties);

                allowedExtensions = createTypeFilePattern(uploadProperties.allowedextensions || '');

                maxFileSize = uploadProperties.maxfilesize || "";
                maxFileSize = maxFileSize.toString();

                var form = formsController.form;
                var sessionConfig = form.session;


                $scope.$on('fileDropped', function (pParam) {
                        $scope.upload(pParam);
                });

                $scope.$on('force-update',function (pEvent,pData) {
                    formsController.form.handleUpdates({data: pData});
                });


                url = makeUrl(sessionConfig.options.runtimePath, sessionConfig.sessionId, sessionConfig.sessionId, uploadProperties.configurationid);

                $scope.urlUpload = url;
                $scope.maxFileSize = maxFileSize;
                $scope.allowedExtensions = allowedExtensions;
                $scope.csrfToken = form.model.csrfToken;

                $scope.progress = 0;

                $scope.progressStyle = {
                    width: 0
                };

            }
        };
    });
})();
