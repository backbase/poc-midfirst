(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'amendment-list',
            condition: function(el) {
                return [
                    el.isContainer,
                    el.styles.indexOf("amendment_list") > -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();