(function () {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'container',
            condition: function(el) {
                return [el.isContainer];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();