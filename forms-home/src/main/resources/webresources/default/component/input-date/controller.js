(function () {
    angular.module('forms-ui').directive('inputDate', function () {
        return {

            restrict: 'A',
            link: function ($scope, element, attrs, formsController) {
                var context = (element.context) ? element.context : element[0];
                var cleanClass = $scope.element.styles.toString().replace(new RegExp(",", 'g'), ' ');
                var dateSearch = cleanClass.match(/\bdate_(?:\w*)\b/g);
                var dateFormat = !dateSearch?null:dateSearch[0];
                // --
                $scope.dateValue = $scope.element.value[0];
                // --
                if ($scope.dateValue) {
                    if (dateFormat) {
                        var splitDash = $scope.dateValue.split("-");
                        var correctDashes = splitDash.length === 3;
                        if (splitDash && correctDashes) {
                            if (dateFormat === "date_NL") {
                                $scope.dateValue = splitDash[2]+"-"+splitDash[1]+"-"+splitDash[0];
                            }
                            if (dateFormat === "date_UK") {
                                $scope.dateValue = splitDash[2]+"/"+splitDash[1]+"/"+splitDash[0];
                            }
                            $scope.$evalAsync();
                        }
                    }
                }

                $scope.$watch('dateValue',function (pNuValue,pOldValue) {
                    if (pNuValue && pOldValue) {
                        if (dateFormat) {
                            var splitDash = pNuValue.split("-");
                            var splitSlash = pNuValue.split("/");
                            var correctDashes = splitDash.length ===3;
                            var correctSlash = splitSlash.length === 3;
                            var response = pNuValue;
                            var correctSpacing = correctDashes || correctSlash;
                            var splitResponse;
                            if (correctSpacing) {
                                splitResponse = (correctDashes) ? splitDash:splitSlash;
                            }
                            // --
                            if (dateFormat === "date_NL" && correctSpacing) {
                                response = splitResponse[2]+"-"+splitResponse[1]+"-"+splitResponse[0];
                            }

                            if (dateFormat === "date_UK" && correctSpacing) {
                                response = splitResponse[2]+"-"+splitResponse[1]+"-"+splitResponse[0];
                            }

                            if (dateFormat === "date_US" && correctSpacing) {
                                response = splitResponse[2]+"-"+splitResponse[0]+"-"+splitResponse[1];
                            }

                            if (!correctSpacing) {
                                response = null;
                            }
                            $scope.element.value[0] = response;
                        }else {
                            $scope.element.value[0] = pNuValue;
                        }

                    } else {
                        $scope.element.value[0] = null;
                    }

                    console.log("$scope.element.value[0]",$scope.element.value[0]);
                });
            }
        };
    });
})();
