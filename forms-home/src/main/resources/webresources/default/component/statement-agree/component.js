(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'statement-agree',
            condition: function(el) {
                return [
                    el.isField,
                    el.styles.indexOf("statement_agree") > -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();