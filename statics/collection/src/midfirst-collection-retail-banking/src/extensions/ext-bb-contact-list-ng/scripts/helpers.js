const resetFormState = form => {
  if (form) {
    form.$setUntouched();
    form.$setPristine();
  }
};

const isFormPristine = form => !form || form.$pristine;

const cancelEditForm = (ext, $ctrl) => {
  if (ext.contactForm) {
    resetFormState(ext.contactForm);
    Object.assign(ext, { contactForm: null });
  }

  if (ext.contactToSelect) {
    $ctrl.selectContact(ext.contactToSelect);
    Object.assign(ext, { contactToSelect: null });
  } else {
    $ctrl.cancelContactForm();
  }
};

const tryToCancelEditForm = (ext, $ctrl) => {
  if (isFormPristine(ext.contactForm)) {
    cancelEditForm(ext, $ctrl);
  } else {
    Object.assign(ext, { cancelFormConfirmOpened: true });
  }
};

const saveContact = ($ctrl, form) => {
  const contact = $ctrl.state.contact.data;
  return $ctrl.saveContact(contact).then(() => resetFormState(form));
};

const helpers = context => {
  const i18n = context.$filter('i18n');

  return {
    cancelEditForm,
    tryToCancelEditForm,
    saveContact,

    notificationMessage: (statusObject) => {
      let message = statusObject.text || '';
      if (statusObject.i18n) {
        message = i18n(statusObject.i18n);
      }
      return message;
    },
  };
};

export default helpers;
