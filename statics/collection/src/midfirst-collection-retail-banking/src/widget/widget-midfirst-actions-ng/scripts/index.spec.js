import widget from './index';

describe('Actions', () => {
  it('should export the module name', () => {
    expect(widget).toEqual('widget-midfirst-actions-ng');
  });
});
