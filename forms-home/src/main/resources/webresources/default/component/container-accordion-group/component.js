(function () {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'container-accordion-group',
            condition: function(el) {
                return [
                    el.isContainer,
                    el.contentStyle === "AccordionGroup"
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();