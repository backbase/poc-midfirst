(function () {
    'use strict';

    /**
     * The forms UI module
     */
    var app = angular.module('forms-ui', ['ngSanitize', 'ngFileUpload', 'ngCookies']);
    app.value('$sessionStorage', window.sessionStorage);

    /**
     * Useful constants
     */

    app.factory('textService', function () {
        return {
            parseBold: function (pText) {
                var boldPrefix = /(\[)[^\[]+(\])/g;
                var response = pText.match(boldPrefix);
                if (!response) {
                    return pText;
                } else {
                    response = response[0];
                    response = response.split("[").join("");
                    response = response.split("]").join("");
                    return response;
                }

            },
            parseNormal: function (pText) {
                var boldPrefix = /(\[)[^\[]+(\])/g;
                var response = pText.match(boldPrefix);
                if (!response) {
                    return pText;
                } else {
                    response = response[0];
                    return pText.replace(response, "");
                }
            }
        }
    });

    app.factory('resizeService', ['$rootScope', function ($rootScope) {
        window.addEventListener("resize", handleResize);
        //window.addEventListener('focus', handleWindowFocus);

        var winW = 0;
        var winH = 0;

        var bodyW = 0;
        var bodyH = 0;
        var target = 0;

        updateValues();

        function updateValues() {
            //console.log("updateValues::",target.offsetHeight,target);
            winW = window.innerWidth;
            winH = window.innerHeight;
            if (target && target.offsetHeight) winH = target.offsetHeight;
            //bodyW = (document.body.clientWidth) ? document.body.clientWidth : window.innerWidth;
            //bodyH = (document.body.clientHeight) ? document.body.clientHeight : window.innerHeight;
            if (winH % 2 === 0) {
                winH += 1;
            }
        }

        function streamResize() {
            updateValues();
            $rootScope.$broadcast("window-resize");
        }

        function handleWindowFocus() {
            streamResize();
            TweenLite.killDelayedCallsTo(streamResize);
            TweenLite.delayedCall(.1, streamResize);
        }

        function handleResize() {
            TweenLite.killDelayedCallsTo(streamResize);
            TweenLite.delayedCall(.1, streamResize);
        }

        return {
            getSize: function () {
                return {w: winW, h: winH};
            },

            triggerResize: function () {
                TweenLite.killDelayedCallsTo(streamResize);
                TweenLite.delayedCall(.1, streamResize);
            },
            attachParent: function (pTarget) {
                target = pTarget;
                streamResize();
            }
        };
    }]);

    app.factory('scrollFactory', ['$rootScope', 'resizeService', function ($rootScope, resizeService) {
        var _factoryItem = {
            getInstance: function () {
                var attached;
                var target;
                var contentTarget;
                var accum = 0;
                var accumDummy = {val: 0};
                var scrollWindowH = 0;
                var targetHeight;
                var scrollYPerc = 0;
                var startingTop = 0;
                var _resizeWatch;
                var _lastScrollUpdate = 0;

                this.dettach = function () {
                    attached = false;
                    target.removeEventListener("scroll", handleScroll);
                    contentTarget.removeEventListener("wheel", handleMouseWheel);
                    contentTarget.removeEventListener("mousewheel", handleMouseWheel);
                    contentTarget.removeEventListener("MozMousePixelScroll", handleMouseWheel);
                    _resizeWatch();
                };

                this.attach = function (pTarget, pContentTarget) {
                    if (attached) return;
                    attached = true;
                    target = pTarget;
                    contentTarget = pContentTarget;
                    startingTop = target.offsetTop;
                    setMouseWheelEvent(pContentTarget, handleMouseWheel);
                    target.addEventListener("scroll", handleScroll);

                    _resizeWatch = $rootScope.$on("window-resize", resize);
                    resize();
                };

                this.scrollUp = function (pMultiplier) {
                    var addition = pMultiplier * (scrollWindowH * .8);
                    accum -= addition;
                    //TweenLite.to(accumDummy, 1, {overwrite: 0, ease: Circ.easeOut, val: accum, onUpdate: setScroll});
                };

                this.scrollDown = function (pMultiplier) {
                    var addition = pMultiplier * (scrollWindowH * .8);
                    accum += addition;
                    //TweenLite.to(accumDummy, 1, {overwrite: 0, ease: Power3.easeOut, val: accum, onUpdate: setScroll});
                };

                this.scrollTo = function (pTarget, pOffset, pDelay) {
                    pOffset = (!pOffset) ? 0 : pOffset;
                    pDelay = (!pDelay) ? 0 : pDelay;
                    accum = pTarget.offsetTop + pOffset;
                    if ((accum || accum === 0 ) && accumDummy.val !== accum) {
                        TweenLite.to(accumDummy, 1, {
                            overwrite: 1,
                            ease: Power3.easeInOut,
                            delay: pDelay,
                            val: accum,
                            onUpdate: setScroll
                        });
                        return true;
                    }
                    else {
                        $rootScope.$broadcast("scroll-update", accumDummy.val);
                        return false;
                    }
                };

                //-- Local Functions
                function resize() {
                    scrollWindowH = resizeService.getSize().h - startingTop;
                    TweenLite.killDelayedCallsTo(updateTargetHeight);
                    TweenLite.delayedCall(.1, updateTargetHeight);
                }

                function updateTargetHeight() {
                    if (target) {
                        var rect = contentTarget.getBoundingClientRect();
                        target.style.height = scrollWindowH + "px";
                        targetHeight = rect.height;
                        if (scrollYPerc) {
                            //console.log("tooo",scrollYPerc *  (targetHeight - scrollWindowH),"(targetHeight - scrollWindowH)",(targetHeight - scrollWindowH));
                            accumDummy.val = scrollYPerc * (targetHeight - scrollWindowH);
                            target.scrollTop = accumDummy.val;
                        }
                        //console.log("ModuleJS::scrollWindowH",scrollWindowH);
                    }
                }

                function setScroll() {
                    //TweenLite.set(target, {scrollTo: accumDummy.val});
                    target.scrollTop = accumDummy.val;
                    var nuVal = ~~(accumDummy.val);
                    var yDiff = (targetHeight - scrollWindowH);
                    if (yDiff === 0) {
                        scrollYPerc = 1;
                    } else {
                        scrollYPerc = accumDummy.val / (targetHeight - scrollWindowH);
                    }



                    if (nuVal !== _lastScrollUpdate) {
                        _lastScrollUpdate = nuVal;
                        //console.log("scroll-update 1:", _lastScrollUpdate);
                        $rootScope.$broadcast("scroll-update", _lastScrollUpdate);

                    }
                }


                function handleScroll(pEvent) {
                    accumDummy.val = target.scrollTop;
                    if (_lastScrollUpdate !== accumDummy.val) {
                        //console.log("scroll-update 2:", _lastScrollUpdate, target.scrollTop);
                        _lastScrollUpdate = accumDummy.val;
                        accum = accumDummy.val;
                        $rootScope.$broadcast("scroll-update", accumDummy.val);
                    }
                    pEvent.preventDefault();
                }

                function handleMouseWheel(pEvent) {
                    pEvent.preventDefault();
                    var delta = pEvent.deltaY || pEvent.detail || pEvent.wheelDelta;
                    accum += delta;
                    accum = accum <= 0 ? 0 : accum;
                    //--
                    var diff = accum - (targetHeight - scrollWindowH);
                    //--
                    if (scrollYPerc >= 1 && delta > 0) {
                        accum = (targetHeight - scrollWindowH);
                        accumDummy.val = accum;
                    }

                    /// TweenLite.killTweensOf(accumDummy);
                    TweenLite.to(accumDummy, .2, {overwrite: 1, ease: Power2.easeOut, val: accum, onUpdate: setScroll});
                }

                function setMouseWheelEvent(pTarget, pFunction) {
                    var elem = pTarget;
                    if (elem.addEventListener) {
                        if ('onwheel' in document) {
                            elem.addEventListener("wheel", pFunction);
                        } else if ('onmousewheel' in document) {

                            elem.addEventListener("mousewheel", pFunction);
                        } else {
                            // Firefox < 17

                            elem.addEventListener("MozMousePixelScroll", pFunction);
                        }
                    } else { // IE8-

                        elem.attachEvent("onmousewheel", pFunction);
                    }
                }

                //-- Local Functions

                return this;
            }
        };
        //--
        return _factoryItem;
    }]);


    app.constant('formConsts', {

        //keepalive ping interval
        PING_INTERVAL: 1000 * 60,

        //session expired related errors
        AQUIMA_SESSION_EXCEPTION: 'com.aquima.web.api.exception.UnknownSubscriptionException',
        SESSION_EXPIRED_ERR: 'SESSION_EXPIRED',
        UNKNOWN_ERR: 'UNKNOWN_ERR',
        UNABLE_TO_CREATE_SESSION_ERR: 'UNABLE_TO_CREATE_SESSION',

        //unknown/missing app related errors
        AQUIMA_UNKNOWN_APP_EXCEPTION: 'com.aquima.interactions.portal.exception.UnknownApplicationException',
        UNKNOWN_APP_ERR: 'UNKNOWN_APP',

        //unavailable language related errors
        AQUIMA_UNKNOWN_LANG_EXCEPTION: 'com.aquima.interactions.metamodel.exception.UnknownLanguageException',
        UNKNOWN_LANG_ERR: 'UNKNOWN_LANG'

    });

    /**
     * Sets up:
     * - Debug mode
     * - The template base
     */
    /*@ngInject*/
    app.config(function ($provide, $logProvider, $httpProvider) {

        var debug = true;
        $logProvider.debugEnabled(debug);
        $provide.value('debugEnabled', debug);

        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.withCredentials = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    });

    /**
     * Custom exception handling
     */
    /*@ngInject*/
    /*app.factory('$exceptionHandler', function($log, debugEnabled) {
     return function() {
     if(debugEnabled) {
     var args = [ 'Exception occurred: '].concat(Array.prototype.slice.call(arguments));
     $log.debug.apply($log, args);
     }
     };
     });*/
})();