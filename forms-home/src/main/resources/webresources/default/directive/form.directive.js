    (function () {
    'use strict';
    /* jshint validthis: true */

    /**
     * @directive bbForm
     */
    function bbFormDirective(resizeService) {
        return {
            restrict: 'E',
            controller: 'BBFormController as bbForm',
            templateUrl: 'directive/form.html',
            bindToController: {
                config: '<'
            },

            link:function ($scope, element){
                var context = (element.context)?element.context:element[0];
                var target = $(context).closest(".ext-bb-forms-ng")[0];
                resizeService.attachParent(target);
                $scope.$emit("form-rendered");
            }
        };
    }

    /**
     * @controller BBFormController
     * @ngInject
     */
    function BBFormController($scope, $log, SessionService, FormService, messages,textService) {
        var form = this;

        $scope.$on('update', applyChanges);
        this.startSession = startSession;
        this.lookupMessage = lookupMessage;
        this.emitBehaviour = emitBehaviour;

        $scope.parseNormal = textService.parseNormal;
        $scope.parseBold = textService.parseBold;


        $scope.$on('configUpdated', function (pEvent, pParamConfig) {
            console.log("CONFIG UPDATE CHECK",pParamConfig);
            form.config = pParamConfig.config;
            setTimeout(function () {
                console.log("FORMSUI:: configUpdated::", form);
                createSession();
            },0);

            //console.log("FORMSUI:: configUpdated::", form.config);
            //console.log("FORMSUI:: configUpdated::", form.config, "user::", form.config.userId);
            //;
        });

        function createSession() {
            console.log("FORMSUI:: CreateSession config",form);
            //console.log("FORMSUI:: CreateSession config", form.config, "session", form.session, "user", form.config.userId);
            SessionService.createSession(form.config, form.user).then(function (session) {
                form.session = session;
                form.model = FormService.createForm(form.session);
                loadModel();
            });

        }

        function startSession() {
            console.log("FORMS UI:: StartSession::", form);
            form.session = null;
            form.model = null;

            //setup messages
            form.messages = FormService.getLocalizedMessages($scope.languageCode, messages);


            if (form.config) {
                var requiredPreferenceKeys = ['project', 'flow', 'version', 'lang'];
                var errors = [];
                requiredPreferenceKeys.forEach(function (value) {
                    if (!form.config[value]) errors.push(value);
                });
                if (errors.length === 0) createSession();
                else {
                    console.log("Creating Session with no params missing elems", errors);
                    createSession();
                }
            }


        }

        function lookupMessage(messageKey) {
            return FormService.lookupMessage(form, messageKey);
        }

        function emitBehaviour(pParam) {
            $scope.$emit("emitBehaviour", pParam);
        }

        function beforeRefresh() {
            //console.log("FormsUI:: FormsDirective:: beforeRefresh");
            form.isRefreshing = true;
        }

        function afterRefresh() {
            //console.log("FormsUI:: FormsDirective:: AfterRefresh");
            var model = form.model;

            if (model) {
                form.root = model.elementList.find(function (e) {
                    return e.isPage;
                });
                $scope.$applyAsync(function () {
                    form.isRefreshing = false;
                });
            }

        }

        function onFormError(error) {
            console.log("onFormError:: caught error",error);
            SessionService.destroySession(form.session);
            //---
            if(error) console.log("onFormError:: endOfFlow?",error.endOfFlow,"restartSession?",error.restartSession);
            //---
            if (!error) {
                console.log("onFormError:: starting session again",error);
                startSession();
            } else {

                if (error.endOfFlow) {
                    console.log("onFormError:: EndOfFlow");
                }

                if (error.restartSession) {
                    console.log("onFormError:: restartSession set starting session again",error);
                    startSession();
                }
            }
            //---
            $log.error(error);
        }

        function loadModel() {
            console.log("FormsUI:: FormsDirective:: loadModel");
            beforeRefresh();
            FormService.updateForm(form.model).catch(onFormError).finally(afterRefresh);
        }

        function applyChanges($event) {
            var element = $event.targetScope.element;

            if (element.isButton || element.canBeUpdated) {
                beforeRefresh();
                form.isLoading = element.isButton;

                FormService.applyChanges(form.model, element)
                    .catch(onFormError)
                    .finally(afterRefresh);
            }
        }

        this.startSessionWithConfig = function (pConfig) {
            //form.config = null;
            form.config = pConfig;
            //SessionService.destroySession(form.session);
            console.log("FORMSUI:: startSessionWithConfig::", form.config);
            createSession();

        };

        this.handleUpdates = function (pData) {
            FormService.handleUpdates(pData, this.model);
        };
    }

    function getElementByKey(key) {
        return this.model && this.model.getElementByKey(key);
    }

    BBFormController.prototype = {
        constructor: BBFormController,
        getElementByKey: getElementByKey,
        $onInit: function () {
            this.startSession();
        }
    };

    var app = angular.module('forms-ui');

    app.controller('BBFormController', BBFormController);
    app.directive('bbForm', bbFormDirective);
})();
