/**
 * @module widget-midfirst-cards-ng
 * @name CardsController
 *
 * @description
 * Cards
 */

export default function CardsController(bus, hooks, widget) {
  const $ctrl = this;

  /**
   * AngularJS Lifecycle hook used to initialize the controller
   *
   * @name CardsController#$onInit
   * @type {function}
   * @returns {void}
   */
  const $onInit = () => {
    $ctrl.items = hooks.itemsFromModel([]);

    bus.publish('cxp.item.loaded', {
      id: widget.getId(),
    });

    bus.publish('bb.item.loaded', {
      id: widget.getId(),
    });
  };

  Object.assign($ctrl, {
    $onInit,

  });
}
