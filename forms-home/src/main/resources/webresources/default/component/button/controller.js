(function () {
    angular.module('forms-ui').directive('shcButtonSimple', /*@ngInject*/ function () {
        return {
            require: {
                form: '^^bbForm'
            },
            restrict: 'A',
            link: function ($scope, element, attrs, formsController) {
                var context = (element.context) ? element.context : element[0];
                $scope.$on("force-emit", function (pEvent, pParam) {
                    if (!pParam) {
                        if (!pEvent.defaultPrevented &&  (!$scope.element.disabled) ) {
                            pEvent.defaultPrevented = true;
                            $scope.$emit('update', $scope.element);
                        }
                        pEvent.preventDefault();
                    }
                });

                $scope.clickEvent = function () {
                    if (!$scope.element.disabled) {
                        $scope.$emit('update', $scope.element);
                    }
                }
            }
        };
    });
})();
