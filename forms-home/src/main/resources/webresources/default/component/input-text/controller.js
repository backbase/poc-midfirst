(function() {
    angular.module('forms-ui').directive('inputText',  /*@ngInject*/ function () {
        return {
            restrict: 'A',
            require: {
                form: '^^bbForm'
            },
            link: function ($scope, element, attrs,formsController) {
                var context = (element.context)?element.context:element[0];
                //console.log("input-text trigger rendered",context);
                context.addEventListener("focus",onFocus);
                context.addEventListener("blur",onBlur);
                var refreshOnChange = false;

                setTimeout(init,0);
                $scope.valueChanged = valueChanged;
                function onBlur(pEvent) {
                    //console.log("inputBlur:",pEvent);
                    $scope.$emit("current-input-blur",{targetModel:$scope.element,target:context});
                    //context.removeEventListener("focus",onFocus);
                }

                function onFocus(pEvent) {
                    //console.log("onFocus:",pEvent);
                    $scope.$emit("current-input-focus", {targetModel: $scope.element, target: context});
                }

                function valueChanged () {
                    if (refreshOnChange) {
                        $scope.$emit('update');
                    }
                }

                function init (){
                    refreshOnChange = context.classList.contains("field-refreshonchange");
                }
            }

        };
    });
})();
