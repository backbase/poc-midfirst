(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'input-integer',
            condition: function(el) {
                return [
                    el.dataType === 'integer' || el.dataType === 'number'
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();