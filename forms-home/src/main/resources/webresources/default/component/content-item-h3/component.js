(function () {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'content-item-h3',
            condition: function (el) {
                return [
                    el.type === 'contentitem',
                    el.contentStyle === 'Heading3'
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();