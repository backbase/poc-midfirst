'use strict';
/* jshint node: true */

var gulp = require('gulp');
var releaseType = process.env.RELEASE_TYPE;

var PACKAGE_MESSAGE =
	'/**\n' +
	' * %s - Copyright © %s Backbase B.V - All Rights Reserved\n' +
	' * Version %s\n' +
	' * %s\n' +
	' * @license\n' +
	' */\n';

function getVersionNumber() {
	return require('./package.json').version;
}

gulp.task('clean', function() {
	var rimraf = require('rimraf');
	return rimraf.sync('./webresources/dist');
});

//jshint
gulp.task('lint', function() {
	var jshint = require('gulp-jshint');

	return gulp.src('./webresources/default/**/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(jshint.reporter('fail'));
});

// Creates regular and minified distributions as UMD modules
//gulp.task('package',  function() {
gulp.task('package', ['package-templates'], function() {
	var sourcemaps = require('gulp-sourcemaps'),
		rename = require('gulp-rename'),
        ngAnnotate = require('gulp-ng-annotate'),
		wrap = require('gulp-wrap'),
		concat = require('gulp-concat'),
		uglify = require('gulp-uglify'),
		insert = require('gulp-insert'),
		util = require('util');

	var meta = require('./package.json');
	var year = new Date().getFullYear();
	var packageMessage = util.format(PACKAGE_MESSAGE, meta.description, year, meta.version, meta.homepage);

	var files = [
		'./webresources/default/module.js',
		'./webresources/default/**/!(*spec).js',
        './webresources/dist/forms-angular-ui.tpl.js'
	];

	var wrapperOptions = {
		src: './umd-wrapper.tmpl'
	};

	var uglifyOptions = {
		preserveComments: 'some'
	};

	return gulp.src(files)
		.pipe(sourcemaps.init())
		.pipe(ngAnnotate())
		.pipe(concat('forms-angular-ui.js'))
		.pipe(wrap(wrapperOptions))
		.pipe(insert.prepend(packageMessage))
		.pipe(gulp.dest('./webresources/dist'))
		.pipe(rename('forms-angular-ui.min.js'))
		.pipe(uglify(uglifyOptions))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('./webresources/dist'));
});


gulp.task('package-legacy', ['package-templates'], function() {
    var sourcemaps = require('gulp-sourcemaps'),
        rename = require('gulp-rename'),
        ngAnnotate = require('gulp-ng-annotate'),
        wrap = require('gulp-wrap'),
        concat = require('gulp-concat'),
        uglify = require('gulp-uglify'),
        insert = require('gulp-insert'),
        util = require('util');

    var meta = require('./package.json');
    var year = new Date().getFullYear();
    var packageMessage = util.format(PACKAGE_MESSAGE, meta.description, year, meta.version, meta.homepage);

    var files = [
        './webresources/default/module.js',
        './webresources/default/**/!(*spec).js',
        './webresources/dist/forms-angular-ui.tpl.js'
    ];

    var wrapperOptions = {
        src: './umd-wrapper-legacy.tmpl'
    };

    var uglifyOptions = {
        preserveComments: 'some'
    };

    return gulp.src(files)
        .pipe(sourcemaps.init())
        .pipe(ngAnnotate())
        .pipe(concat('forms-angular-ui-legacy.js'))
        .pipe(wrap(wrapperOptions))
        .pipe(insert.prepend(packageMessage))
        .pipe(gulp.dest('./webresources/dist'))
        .pipe(rename('forms-angular-ui-legacy.min.js'))
        .pipe(uglify(uglifyOptions))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./webresources/dist'));
});



//currently, simply copies the templates to the dist directory
gulp.task('package-templates', [], function() {
	var templateCache = require('gulp-templatecache');

	var tplCacheOptions = {
		output: 'forms-angular-ui.tpl.js',
		strip: 'webresources/default/',
		prepend: '',
		moduleName: 'forms-ui',
		minify: {}
	};

	return gulp.src('./webresources/default/**/*.html')
		.pipe(templateCache(tplCacheOptions))
		.pipe(gulp.dest('./webresources/dist'));

});

// Run tests after packaging, so we can test the minified build
gulp.task('test', ['package-templates'], function(done) {
	var karma = require('karma');

	karma.server.start({
		configFile: __dirname + '/karma.conf.js',
		singleRun: true
	}, done);
});

//watches and runs package when src changes. Useful for running unit tests against the build
gulp.task('watch', ['clean','package','package-legacy'], function() {
	console.log('Watching for changes...');
	gulp.watch('./webresources/default/**/*.js', ['package','package-legacy']);
    gulp.watch('./webresources/default/**/*.html', ['package','package-legacy']);
	//gulp.watch('./webresources/default/**/*.html', ['package-templates']);
});

gulp.task('default', ['test']);

///////////////////////////////////////////////////////////////////////////////
// Release tasks:
// 1. Tag and push
// 2. Checkout/pull master
// 3. bump version
// 4. add and commit
// 5. push to origin
///////////////////////////////////////////////////////////////////////////////

//add commit messages to the Changelog
gulp.task('tag-release', function(done) {
	var git = require('gulp-git');

	if (!releaseType) {
		throw new Error('No release type defined. You must run this task from Jenkins');
	}

	var v = getVersionNumber();
	git.tag(v, 'Tagging release: ' + v, function(err) {
		if (err) {
			console.log(err);
		} else {
			var v = getVersionNumber();
			git.push('origin', v, function(err) {
				if (err) {
					console.log(err);
				} else {
					done();
				}
			});
		}
	});
});

gulp.task('checkout-master', ['tag-release'], function(done) {
	var git = require('gulp-git');

	git.checkout('master', function(err) {
		if (err) {
			console.log(err);
		} else {
			git.pull('origin', 'master', function(err) {
				if (err) {
					console.log(err);
				} else {
					done();
				}
			});
		}
	});
});

gulp.task('bump-next-version', ['checkout-master'], function() {
	var bump = require('gulp-bump');
	var git = require('gulp-git');

	return gulp.src('./package.json')
		.pipe(bump({
			type: releaseType
		}))
		.pipe(gulp.dest('./'))
		.pipe(git.add())
		.pipe(git.commit('Updating development version'));
});

gulp.task('push-next-version', ['bump-next-version'], function(done) {
	var git = require('gulp-git');

	git.push('origin', 'master', function(err) {
		if (err) {
			console.log(err);
		} else {
			done();
		}
	});
});

gulp.task('release', ['push-next-version'], function() {
	console.log('PERFORMING RELEASE');
});
