(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'field-floatlabel',
            condition: function(el) {
                return [
                    el.isField,
                    el.styles.indexOf('field_float_label') !== -1];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();