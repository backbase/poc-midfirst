export default {
  /**
  * Triggered when product is selected.
  * @event bb.event.product.selected
  * @type {Product}
  */
  PRODUCT_SELECTED: 'bb.event.product.selected',
  REFLOW_TRIGGERED: 'bb.event.product.accounts.reflow',
};
