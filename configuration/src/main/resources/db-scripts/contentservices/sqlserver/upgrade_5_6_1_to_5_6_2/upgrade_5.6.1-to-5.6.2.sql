drop index ODPATH_IDX on OBJECT_DATA;
go

create index ODPATH_IDX on OBJECT_DATA(uniquePathCheck) INCLUDE (path);
go

alter table RELATIONSHIPS alter column targetRepositoryId nvarchar(36) not null;
go

alter table RELATIONSHIPS alter column targetObjectId nvarchar(36) not null;
go