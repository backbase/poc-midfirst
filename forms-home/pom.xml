<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation=" http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.backbase.showcase</groupId>
        <artifactId>forms-parent</artifactId>
        <version>2.2.0-SNAPSHOT</version>
        <relativePath>../forms-parent/pom.xml</relativePath>
    </parent>

    <artifactId>forms-home</artifactId>
    <version>2.2.0-SNAPSHOT</version>

    <packaging>pom</packaging>

    <name>Backbase Forms :: Forms Home</name>
    <description>A module that contains Runtime configuration, static resources, and extra classpath.</description>

    <properties>
        <!-- Standard Maven properties -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <maven.resources.escapeString>\</maven.resources.escapeString>
        <maven.resources.includeEmptyDirs>true</maven.resources.includeEmptyDirs>

        <work.dir>${project.build.directory}/${project.build.finalName}</work.dir>
        <local.filter>local</local.filter>
        <data.dir>${project.basedir}/../data</data.dir>
    </properties>

    <build>
        <finalName>forms-home</finalName>

        <resources>
            <resource>
                <directory>src/main/templates</directory>
                <filtering>true</filtering>
                <includes>
                    <include>*.*</include> <!--Filter top-level files only-->
                </includes>
                <targetPath>${project.basedir}/src/main/resources</targetPath>
            </resource>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>false</filtering>
                <includes>
                    <include>*/**</include> <!--Do not filter subdirectories-->
                </includes>
                <excludes>
                    <exclude>webresources/**/*.less</exclude>
                    <exclude>webresources/**/less</exclude>
                    <exclude>webresources/bower_components</exclude>
                </excludes>
            </resource>
        </resources>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <executions>
                    <execution>
                        <id>dist</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>resources</goal>
                        </goals>
                        <configuration>
                            <encoding>UTF-8</encoding>
                            <outputDirectory>${work.dir}/dist</outputDirectory>
                            <filters>
                                <filter>${project.basedir}/src/main/filters/dist.properties</filter>
                            </filters>
                        </configuration>
                    </execution>
                    <!-- DEV Execution need to be done last as it copies it into resources folder -->
                    <execution>
                        <id>dev</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>resources</goal>
                        </goals>
                        <configuration>
                            <encoding>UTF-8</encoding>
                            <outputDirectory>${work.dir}/dev</outputDirectory>
                            <filters>
                                <filter>${project.basedir}/src/main/filters/dev.properties</filter>
                            </filters>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <configuration>
                    <descriptors>
                        <descriptor>assembly/dev.xml</descriptor>
                        <descriptor>assembly/dist.xml</descriptor>
                    </descriptors>
                </configuration>
                <executions>
                    <execution>
                        <id>make-assemblies</id>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
 
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-clean-plugin</artifactId>
                <version>3.0.0</version>
                <configuration>
                    <filesets>
                        <fileset>
                            <directory>${project.basedir}/src/main/resources/webresources/bower_components</directory>
                            <includes>
                                <include>**</include>
                            </includes>
                            <followSymlinks>false</followSymlinks>
                        </fileset>
                        <fileset>
                            <directory>${project.basedir}/src/main/resources/webresources/dist</directory>
                            <includes>
                                <include>**</include>
                            </includes>
                            <followSymlinks>false</followSymlinks>
                        </fileset>
                    </filesets>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>exec-maven-plugin</artifactId>
                <version>1.3.2</version>
                <executions>
                    <execution>
                        <id>forms-npm-install</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>exec</goal>
                        </goals>
                        <configuration>
                            <executable>npm</executable>
                            <arguments>
                                <argument>install</argument>
                            </arguments>
                            <workingDirectory>${project.basedir}/src/main/resources</workingDirectory>
                        </configuration>
                    </execution>
                    <execution>
                        <id>forms-bower-install</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>exec</goal>
                        </goals>
                        <configuration>
                            <executable>npm</executable>
                            <arguments>
                                <argument>run</argument>
                                <argument>bower</argument>
                                <argument>install</argument>
                            </arguments>
                            <workingDirectory>${project.basedir}/src/main/resources</workingDirectory>
                        </configuration>
                    </execution>
                    <execution>
                        <id>forms-gulp</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>exec</goal>
                        </goals>
                        <configuration>
                            <executable>./node_modules/gulp/bin/gulp.js</executable>
                            <arguments>
                                <argument>package</argument>
                            </arguments>
                            <workingDirectory>${project.basedir}/src/main/resources</workingDirectory>
                        </configuration>
                    </execution>
                    <execution>
                        <id>forms-gulp-legacy</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>exec</goal>
                        </goals>
                        <configuration>
                            <executable>./node_modules/gulp/bin/gulp.js</executable>
                            <arguments>
                                <argument>package-legacy</argument>
                            </arguments>
                            <workingDirectory>${project.basedir}/src/main/resources</workingDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
