/**
 * @module ext-shc-my-products-menu-toggle-ng
 *
 * @description
 * Default extension for widget-shc-my-products-menu-toggle-ng
 *
 * @requires vendor-bb-angular-ng-aria
 */
import ngAriaModuleKey from 'vendor-bb-angular-ng-aria';

export const dependencyKeys = [ngAriaModuleKey];

export const hooks = {};

export const helpers = {
    toggle: () => {
        const sidebar = document.querySelector('.sidebar');
        sidebar.classList.toggle('sidebar--mobile-menu');

        const menuToggle = document.querySelector('.my-products-menu-toggle');
        menuToggle.classList.toggle('my-products-menu-toggle--open');

        document.body.classList.toggle('no-scroll');
    },
};

export const events = {};
