/**
 * @module ext-shc-sidebar-right-ng
 *
 * @description
 * Extention template for shc sidebar
 *
 * @requires vendor-bb-angular-ng-aria
 */
import ngAriaModuleKey from 'vendor-bb-angular-ng-aria';
import i18nKey from 'ui-bb-i18n-ng';

import extHelpers from './helpers';




// uncomment below to include CSS in your extension
// import '../styles/index.css';

export const dependencyKeys = [
    ngAriaModuleKey,
    i18nKey
];

export const hooks = {};

export const helpers = extHelpers;

export const events = {};
