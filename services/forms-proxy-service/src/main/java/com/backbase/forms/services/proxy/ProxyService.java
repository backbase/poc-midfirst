package com.backbase.forms.services.proxy;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;

/**
 * Service responsible for executing calls to a proxied server.
 * 
 * @author Walter Werner
 *
 */
public interface ProxyService {

	/**
	 * Executes the giving request.
	 * 
	 * @param originalRequest
	 *            the original request
	 * @param httpRequest
	 *            the request to be executed
	 * @return the server response
	 * @throws IOException
	 *             if something goes wrong
	 */
	HttpResponse execute(HttpServletRequest originalRequest,
			HttpUriRequest httpRequest) throws IOException;

}