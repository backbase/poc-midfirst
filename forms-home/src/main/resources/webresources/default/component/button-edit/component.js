(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'button-edit',
            condition: function(el) {
                return [
                    el.isButton,
                    el.classList.indexOf("edit-control") > -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();