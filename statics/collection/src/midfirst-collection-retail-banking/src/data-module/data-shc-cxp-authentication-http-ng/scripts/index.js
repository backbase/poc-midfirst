/**
 * @module data-shc-cxp-authentication-http-ng
 *
 * @description A data module for accessing the CXP Authentication REST API
 *
 * @returns {String} `data-shc-cxp-authentication-http-ng`
 * @example
 * import cXPAuthenticationDataModuleKey, {
 *   cXPAuthenticationDataKey,
 * } from 'data-shc-cxp-authentication-http-ng';
 */

import ng from 'vendor-bb-angular';

import cXPAuthenticationData from './data-shc-cxp-authentication-http';

const cXPAuthenticationDataModuleKey = 'data-shc-cxp-authentication-http-ng';
/**
 * @name cXPAuthenticationDataKey
 * @type {string}
 * @description Angular dependency injection key for the CXP Authentication data service
 */
export const cXPAuthenticationDataKey =
  'data-shc-cxp-authentication-http-ng:cXPAuthenticationData';
/**
 * @name default
 * @type {string}
 * @description Angular dependency injection module key
 */
export default ng
    .module(cXPAuthenticationDataModuleKey, [])

/**
   * @name CXPAuthenticationData
   * @type {object}
   * @constructor
   *
   * @description Public api for service data-shc-cxp-authentication-http
   *
   */
    .provider(cXPAuthenticationDataKey, [() => {
        const config = {
            baseUri: '/',
        };

        /**
     * @name CXPAuthenticationDataProvider
     * @type {object}
     * @description
     * Data service that can be configured with custom base URI.
     *
     * @example
     * angular.module(...)
     *   .config(['data-shc-cxp-authentication-http-ng:cXPAuthenticationDataProvider',
     *     (dataProvider) => {
     *       dataProvider.setBaseUri('http://my-service.com/');
     *       });
     */
        return {
            /**
       * @name CXPAuthenticationDataProvider#setBaseUri
       * @type {function}
       * @param {string} baseUri Base URI which will be the prefix for all HTTP requests
       */
            setBaseUri: (baseUri) => {
                config.baseUri = baseUri;
            },

            /**
       * @name CXPAuthenticationDataProvider#$get
       * @type {function}
       * @return {object} An instance of the service
       */
            $get: [
                '$http',
                /* into */
                cXPAuthenticationData(config),
            ],
        };
    }])

    .name;
