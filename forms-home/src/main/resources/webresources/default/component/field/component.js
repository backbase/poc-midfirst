(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'field',
            condition: function(el) {
                return [el.isField];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();