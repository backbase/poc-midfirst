(function() {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'input-float-label-password',
            condition: function(el) {
                return [
                    el.dataType === 'text',
                    !el.hasDomain,
                    el.styles.indexOf("field_float_label") !== -1,
                    el.styles.indexOf("password") !== -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();
