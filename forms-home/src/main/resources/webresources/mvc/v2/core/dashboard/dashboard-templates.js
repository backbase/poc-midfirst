(function (blueriq, bqApp) {
    'use strict';
    blueriq.defaultDashboardModelHandler = function (viewModel, bindingContext) {
        var prefix = viewModel.context.configuration.templatePath;
        var containerPrefix = prefix + 'container/';

        if (viewModel.type === 'container' && viewModel.contentStyle === 'dashboard_header') {
            return blueriq.templates.container.dashboard.dashboardHeader;
        } else if (viewModel.type === 'container' && viewModel.contentStyle === 'dashboard_menu') {
            return blueriq.templates.container.dashboard.dashboardMenu;
        } else if (viewModel.type === 'container' && viewModel.contentStyle === 'dashboard_body') {
            return blueriq.templates.container.dashboard.dashboardBody;
        } else if (viewModel.type === 'container' && viewModel.contentStyle === 'dashboard_footer') {
            return blueriq.templates.container.dashboard.dashboardFooter;

        } else if (viewModel.type === 'container' && viewModel.contentStyle === 'dashboard_row') {
            return blueriq.templates.container.row;
        } else if (viewModel.type === 'container' && viewModel.contentStyle && viewModel.contentStyle.indexOf('dashboard_column') === 0) {
            return blueriq.templates.container.column;
        } else if (viewModel.type === 'container' && viewModel.contentStyle === 'dashboard_widget') {
            return blueriq.templates.container.widget;
        } else if (viewModel.type === 'flowwidget') {
            return blueriq.templates.flowWidget.flowWidget;
        } else if (viewModel.type === 'container' && viewModel.contentStyle === 'chartEasyPie') {
            return blueriq.templates.container.easyPieChart;
        } else if (viewModel.type === 'storecomment') {
            return blueriq.templates.comment.storeComment;
        } else if (viewModel.type === 'commentlist') {
            return blueriq.templates.comment.commentList;
        } else if (viewModel.contentStyle === 'timeline') {
            return blueriq.templates.container.timeline;
        } else if (viewModel.type === 'container' && viewModel.contentStyle === 'menubar') {
            if (bindingContext.modus && jQuery.inArray('widget', bindingContext.modus) > -1) {
                return;
            }
            return blueriq.templates.container.navbar;
        }
    };

    /**
     * Register automatically if bqApp is already created
     */
    if (bqApp) {
        bqApp.templateFactory.registerModelHandler(blueriq.defaultDashboardModelHandler);
    }

}(window.blueriq, window.bqApp));