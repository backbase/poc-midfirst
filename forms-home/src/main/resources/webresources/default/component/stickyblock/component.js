(function() {
    /**
     * @ngInject
     */
    function registerControl(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'stickyblock',
            condition: function (el) {
                return [
                    el.isContainer,
                    el.styles.indexOf("stickyblock") > -1
                ];
            }

        });
    }
    angular.module('forms-ui').run(registerControl);
})();
