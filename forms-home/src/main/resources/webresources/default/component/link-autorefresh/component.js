(function() {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'link-autorefresh',
            condition: function(el) {
                return [
                    el.type === 'textitem',
                    el.styles.indexOf('link_autorefresh') > -1
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();
