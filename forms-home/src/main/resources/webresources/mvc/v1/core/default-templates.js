(function (blueriq, bqApp) {
    'use strict';

    /**
     * Register default templates for elements
     */
    blueriq.defaultModelHandler = function (viewModel, context) {

        var prefix = (viewModel.context.configuration.baseId ? viewModel.context.configuration.baseId : 'core') + '/template/';
        var statisticPrefix = prefix + 'statistic/';
        var containerPrefix = prefix + 'container/';

        if (viewModel.type === 'table') {
            return containerPrefix + 'table/table';
        } else if (viewModel.model.contentStyle === 'tablecell') {
            if (viewModel.displayName()) {
                return containerPrefix + 'table/tabletextcell';
            } else {
                return containerPrefix + 'table/tablecell';
            }
        } else if (viewModel.type === 'tablesearch') {
            return containerPrefix + 'table/tablesearch';
        } else if (viewModel.type === 'tablesortedheader') {
            return containerPrefix + 'table/tablesortedheader';
        } else if (viewModel.type === 'tablenavigation') {
            return containerPrefix + 'table/tablenavigation';
        } else if (viewModel.type === 'visualization') {
            return statisticPrefix + 'visualization';
        }
        else if (viewModel.type === 'container') {
            if (viewModel.contentStyle.lastIndexOf('table', 0) === 0) {
                return containerPrefix + 'table/' + viewModel.contentStyle;
            } else if (viewModel.contentStyle === 'externalcontent') {
                return containerPrefix + 'externalcontent';
            } else if (viewModel.contentStyle === 'breadcrumbcontainer') {
                return containerPrefix + 'breadcrumb';
            } else if (viewModel.contentStyle === 'timeline') {
                return containerPrefix + 'timeline';
            }
            else if (viewModel.contentStyle === 'statistic') {
                return statisticPrefix + 'statistic';
            }
            else if (viewModel.contentStyle === 'menubar') {
                return containerPrefix + 'menubar';
            } else if (viewModel.contentStyle === 'instance_linker') {
                return containerPrefix + 'instancelinker';
            } else if (viewModel.contentStyle === 'fileupload') {
                return containerPrefix + 'fileupload';
            } else if (viewModel.contentStyle === 'filedownload') {
                return containerPrefix + 'filedownload';
            } else if (viewModel.contentStyle === 'tabs') {
                return containerPrefix + 'tabs';
            } else if (viewModel.contentStyle === 'formfooter') {
                return containerPrefix + 'footer';
            }

        } else if (viewModel.type === 'textitem') {
            if (jQuery.inArray('authenticated_user', viewModel.model.styles) > -1) {
                return prefix + 'textitem/authenticateduser';
            } else if (jQuery.inArray('logout_link', viewModel.model.styles) > -1) {
                return prefix + 'textitem/logout';
            } else if (jQuery.inArray('styled', viewModel.model.styles) > -1) {
                return prefix + 'textitem/textitemstyled';
            }
        }

        if (viewModel.type === 'instancelist') {
            return containerPrefix + 'instancelist';
        }

        if (viewModel.type === 'filetype') {
            return prefix + 'field/filetypefield';
        }

        if (viewModel.type === 'field') {
            if (jQuery.inArray('fieldValueOnly', context.modus) > -1) {
                if (viewModel.readonly()) {
                    return prefix + 'field/readonlyvalue';
                } else {
                    return prefix + 'field/value';
                }
            } else if (viewModel.readonly()) {
                return prefix + 'field/readonly';
            } else {
                return prefix + 'field/field';
            }
        }

        if (viewModel.type === 'button') {
            /* jscs:disable requireCamelCaseOrUpperCaseIdentifiers */
            if (viewModel.presentationStyles().isIcon) {
                if (viewModel.presentationStyles().only_icon) {
                    return prefix + 'button/onlyiconbutton';
                } else {
                    return prefix + 'button/iconbutton';
                }
            } else {
                if (viewModel.presentationStyles().button_link) {
                    return prefix + 'button/buttonlink';
                } else if (viewModel.presentationStyles().button_primary) {
                    return prefix + 'button/buttonprimary';
                }
            }

            /* jscs:enable requireCamelCaseOrUpperCaseIdentifiers */
        }

        if (viewModel.type === 'container') {
            if (!viewModel.displayName()) {
                return prefix + 'container/plaincontainer';
            }
        }

        //default element type as template
        return prefix + viewModel.type + '/' + viewModel.type;
    };

    /**
     * Register default templates for field values
     */
    blueriq.defaultFieldValueHandler = function (viewModel) { //,context
        /* jscs:disable requireCamelCaseOrUpperCaseIdentifiers */

        var prefix = (viewModel.context.configuration.baseId ? viewModel.context.configuration.baseId : 'core') + '/template/field/';

        if (viewModel.multiValued) {
            if (viewModel.presentationStyles().options_horizontal) {
                return prefix + 'checkboxhorizontal';
            } else if (viewModel.presentationStyles().options_vertical) {
                return prefix + 'checkboxvertical';
            }
        } else {
            if (viewModel.presentationStyles().toggle && viewModel.domain() && viewModel.domain().length === 2) {
                return prefix + 'toggle';
            } else if (viewModel.presentationStyles().options_vertical) {
                return prefix + 'radiovertical';
            } else if (viewModel.presentationStyles().options_horizontal) {
                return prefix + 'radiohorizontal';
            }
        }

        if (viewModel.hasDomain) {
            if (viewModel.multiValued) {
                return prefix + 'domainmulti';
            }
            return prefix + 'domainsingle';
        }

        if (viewModel.dataType === 'text') {
            if (viewModel.presentationStyles().memo) {
                return prefix + 'memo';
            } else if (viewModel.presentationStyles().password) {
                return prefix + 'password';
            }
        }

        return prefix + viewModel.dataType;


        /* jscs:enable requireCamelCaseOrUpperCaseIdentifiers */
    };

    /**
     * Register automatically if bqApp is already created
     */
    if (bqApp) {
        bqApp.templateFactory.registerModelHandler(blueriq.defaultModelHandler);
        bqApp.templateFactory.registerFieldValueHandler(blueriq.defaultFieldValueHandler);
    }
}(window.blueriq, window.bqApp));