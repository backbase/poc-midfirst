(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'button-accordion-toggle',
            condition: function(el) {
                return [
                    el.isButton,
                    el.styles.indexOf('Accordion_Toggle') !== -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();