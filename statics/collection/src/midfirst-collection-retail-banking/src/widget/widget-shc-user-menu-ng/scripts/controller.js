/* global window */
import Cookies from './cookies';
/**
 * @name UserMenuController
 * @type {object}
 *
 * @description
 * User Menu widget
 */
export default function UserMenuController(model, widgetInstance) {
  const $ctrl = this;

  const portal = window.b$.portal;

  const logoutRedirectPage = widgetInstance.getStringPreference('logoutRedirectPage');
  const userDataUrl = widgetInstance.getStringPreference('userDataUrl');

  const logoutRedirectUrl =
    `${portal.config.serverRoot}/${portal.portalName}/${logoutRedirectPage}`;

  const cookies = Cookies.getCookies();

  let notificationsExpanded = false;


  const toggleNotifications = () => {
        this.notificationsExpanded = !this.notificationsExpanded;
    }

  const token = {
    key: 'BBXSRF',
    value: ''
  };

  const $onInit = () => {
    token.value = cookies[token.key];
    gadgets.pubsub.subscribe("usermenu-logout",function (e) {
      logout();
    });
  };

  const logout = () => {     
    model.logout(token.value)
      .then(() => {
        window.location.assign(logoutRedirectUrl);
      });
  };

  const loggedUser = () => {         
    // for anonymous user portal returns null as a string
    if (!portal.loggedInUserId || portal.loggedInUserId === 'null') {
      return '';
    }

    return portal.loggedInUserId;
  };

  Object.assign($ctrl, {
    /**
     * @description
     * AngularJS Lifecycle hook used to initialize the controller
     * @type {function}
     *
     * @name UserMenuController#$onInit
     * @returns {void}
     */
    $onInit,
    /**
     * @description Logout function
     * @type {function}
     *
     * @name UserMenuController#logout
     * @returns {Promise}
     */
    logout,
    loggedUser,
    /**
     * @name UserMenuController#userDataUrl
     * @type {string}
     */
    userDataUrl,

      toggleNotifications,
      notificationsExpanded,
  });
}
