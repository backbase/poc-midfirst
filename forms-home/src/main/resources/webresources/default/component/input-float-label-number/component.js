(function() {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'input-float-label-number',
            condition: function(el) {
                return [
                    el.dataType === 'integer' || el.dataType === 'number',
                    !el.hasDomain,
                    el.styles.indexOf("field_float_label") !== -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();
