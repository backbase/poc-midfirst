/* global window */
export default (conf) => (httpClient) => {
    const portalServerRoot = window.b$.portal.config.serverRoot.replace(/\//g, '');
    const config = {
        endpointUserInfo: `${conf.baseUri}${portalServerRoot}/services/rest/v1/party-data-management/party`,

    };

    /**
   * @name CXPAuthenticationData#postLogin
   * @type {function}
   * @description Perform a POST request to the URI.
   * @param {object} data - configuration object
   * @param {object} headers - custom headers
   * @returns {Promise.<object>} A promise resolving to object with headers and data keys
   *
   * @example
   * cXPAuthenticationData
   *  .postLogin(data, headers)
   *  .then(function(result){
   *    console.log(result)
   *  });
   */
    function getUserInfo(data) {

        const url = `${config.endpointUserInfo}`;       
        return httpClient({
            method: 'GET',
            url,
            data
        });
    }



    return ({
        getUserInfo
    });
};
