import angular from 'vendor-bb-angular';

import Controller from './controller';

describe('ui-bbm-beneficiary-select-ng::controller', () => {
  let $element;

  const accountA = { name: 'Account A', identifier: '1234' };
  const accountB = { name: 'Account B', identifier: '1234 56' };
  const accountC = { name: 'Account C', identifier: '1234 56 78' };

  const $timeout = (fn) => fn();

  Object.assign($timeout, {
    cancel: (timerId) => {},
  });

  const document = [{
    body: {}
  }];

  const ngModelCtrl = {
    $setViewValue: () => {},
    $setValidity: () => {},
  };

  let nameModelCtrl;
  let identifierModelCtrl;

  /**
   * --------------------------------------------------------------------------------------
   * Helpers
   * --------------------------------------------------------------------------------------
   */

  const createAccounts = () => [
    accountA,
    accountB,
    accountC,
  ];

  const createElement = () => {
    const element = angular.element();

    const nameElement = angular.element();
    const identifierElement = angular.element();

    nameElement.controller = (key) => {
      if (key === 'ngModel') {
        return nameModelCtrl;
      }
    };

    identifierElement.controller = (key) => {
      if (key === 'ngModel') {
        return identifierModelCtrl;
      }
    };

    element.controller = (key) => {
      if (key === 'ngModel') {
        return ngModelCtrl;
      }
    };

    element.find = (selector) => {
      if (selector === 'input') {
        return {
          eq: (idx) => {
            if (idx === 0) {
              return nameElement;
            } else if (idx === 1) {
              return identifierElement;
            }
          }
        };
      }
    };

    return element;
  };

  const changeIdentifier = (ctrl, value) => {
    setFocus(ctrl, 'identifier');

    ctrl.state.beneficiary.identifier = value;

    identifierModelCtrl.$viewValue = value;
    identifierModelCtrl.$viewChangeListeners.forEach(fn => fn());
  };

  const changeName = (ctrl, value) => {
    setFocus(ctrl, 'name');

    ctrl.state.beneficiary.name = value;

    nameModelCtrl.$viewValue = value;
    nameModelCtrl.$viewChangeListeners.forEach(fn => fn());
  };

  const createController = (props) => {
    $element = createElement();

    const ctrl = new Controller($element, $timeout, document);

    Object.assign(ctrl, props);

    return ctrl;
  };

  const setFocus = (ctrl, field) => {
    if (field === 'name') {
      ctrl.onNameFocus();
    } else if (field === 'identifier') {
      ctrl.onIdentifierFocus();
    }
  };

  beforeEach(() => {
    nameModelCtrl = {
      $viewChangeListeners: [],
      $viewValue: '',
    };

    identifierModelCtrl = {
      $viewChangeListeners: [],
      $viewValue: '',
    };
  });

  /**
   * --------------------------------------------------------------------------------------
   * Specs
   * --------------------------------------------------------------------------------------
   */

  describe('Events', () => {
    describe('$onChanges', () => {
      it('should update the beneficiary on model changes', () => {
        const ngModel = {
          name: 'Account A',
          identifier: 'account_a',
        };

        const ctrl = createController({
          ngModel,
        });

        ctrl.$onChanges({ ngModel });

        expect(ctrl.state.beneficiary).toEqual(ngModel);
      });

      it('should update search index on accounts change', () => {
      });
    });

    describe('On name change', () => {
      it('should update suggestions according to the name value', () => {
        const ctrl = createController({
          accounts: createAccounts(),
        });

        ctrl.$onChanges({
          accounts: {
            currentValue: ctrl.accounts,
          },
        });

        ctrl.$postLink();

        changeName(ctrl, 'a');

        expect(ctrl.state.suggestions).toEqual([
          accountA,
          accountB,
          accountC,
        ]);

        changeName(ctrl, '78');

        expect(ctrl.state.suggestions).toEqual([
          accountC,
        ]);
      });

      it('should open suggestions the list if the name is not empty and close it otherwise', () => {
        const ctrl = createController({
          accounts: createAccounts(),
        });

        ctrl.$onChanges({
          accounts: {
            currentValue: ctrl.accounts,
          },
        });

        ctrl.$postLink();

        expect(ctrl.state.isListOpened).toBe(false);

        changeName(ctrl, 'a');
        expect(ctrl.state.isListOpened).toBe(true);

        changeName(ctrl, '');
        expect(ctrl.state.isListOpened).toBe(false);
      });
    });

    describe('On identifier change', () => {
      it('should update suggestions according to the identifier value', () => {
        const ctrl = createController({
          accounts: createAccounts(),
        });

        ctrl.$onChanges({
          accounts: {
            currentValue: ctrl.accounts,
          },
        });

        ctrl.$postLink();

        changeIdentifier(ctrl, '1');

        expect(ctrl.state.suggestions).toEqual([
          accountA,
          accountB,
          accountC,
        ]);

        changeIdentifier(ctrl, '78');

        expect(ctrl.state.suggestions).toEqual([
          accountC,
        ]);
      });

      it('should open suggestions list if identifier is not empty and close it otherwise', () => {
        const accounts = createAccounts();

        const ctrl = createController({
          accounts,
        });

        ctrl.$onChanges({
          accounts: {
            currentValue: accounts,
          }
        });

        ctrl.$postLink();

        expect(ctrl.state.isListOpened).toBe(false);

        changeIdentifier(ctrl, 'a');
        expect(ctrl.state.isListOpened).toBe(true);

        changeIdentifier(ctrl, '');
        expect(ctrl.state.isListOpened).toBe(false);
      });
    });

    describe('On name input focus', () => {
      it('should update `state.activeField` property', () => {
        const accounts = createAccounts();
        const ctrl = createController({ accounts });

        ctrl.$onChanges({
          accounts: {
            currentValue: accounts,
          }
        });

        ctrl.$postLink();
        expect(ctrl.state.activeField).toBe('');

        ctrl.onNameFocus();
        expect(ctrl.state.activeField).toBe('name');
      });
    });

    describe('On identifier input focus', () => {
      it('should update `state.activeField` property', () => {
        const accounts = createAccounts();
        const ctrl = createController({ accounts });

        ctrl.$onChanges({
          accounts: {
            currentValue: accounts,
          }
        });

        ctrl.$postLink();
        expect(ctrl.state.activeField).toBe('');

        ctrl.onIdentifierFocus();
        expect(ctrl.state.activeField).toBe('identifier');
      });
    });

    describe('On suggestion click', () => {
      it('should set clicked account as a beneficiary', () => {
        const accounts = createAccounts();

        const ctrl = createController({
          accounts,
        });

        ctrl.$onChanges({
          accounts: {
            currentValue: accounts,
          }
        });

        ctrl.$postLink();

        changeName(ctrl, 'a');

        spyOn(ngModelCtrl, '$setViewValue');

        ctrl.onAccountClick(accounts[1]);

        expect(ctrl.state.beneficiary).toEqual(accountB);
        expect(ngModelCtrl.$setViewValue).toHaveBeenCalledWith(accountB);
      });
    });
  });

  describe('Validation', () => {
    it('should set the model as invalid if a new beneficiary was entered and creating is not allowed', () => {
      const ctrl = createController({
        accounts: createAccounts(),
        allowCreate: false,
      });

      spyOn(ngModelCtrl, '$setValidity');

      ctrl.$onChanges({
        accounts: {
          currentValue: ctrl.accounts,
        },
      });

      ctrl.$postLink();

      changeName(ctrl, 'a');
      changeIdentifier(ctrl, '123');

      ctrl.onBlur();

      expect(ngModelCtrl.$setValidity).toHaveBeenCalledWith('createBeneficiary', false);
    });

    it('should set the model as valid if creating is allowed', () => {
      const ctrl = createController({
        accounts: createAccounts(),
        allowCreate: true,
      });

      spyOn(ngModelCtrl, '$setValidity');

      ctrl.$onChanges({
        accounts: {
          currentValue: ctrl.accounts,
        },
      });

      ctrl.$postLink();

      changeName(ctrl, 'a');
      ctrl.onBlur();

      expect(ngModelCtrl.$setValidity).toHaveBeenCalledWith('createBeneficiary', true);
    });

    it('should update validity when allowCreate parameter is changed', () => {
      const ctrl = createController({
        accounts: createAccounts(),
        allowCreate: false,
      });

      spyOn(ngModelCtrl, '$setValidity');

      ctrl.$onChanges({
        accounts: {
          currentValue: ctrl.accounts,
        },
      });

      ctrl.$postLink();

      changeName(ctrl, 'a');
      changeIdentifier(ctrl, 'a');

      ctrl.onBlur();

      expect(ngModelCtrl.$setValidity).toHaveBeenCalledWith('createBeneficiary', false);

      ctrl.allowCreate = true;

      ctrl.$onChanges({
        allowCreate: {
          currentValue: ctrl.allowCreate,
        },
      });

      changeName(ctrl, 'a');
      changeIdentifier(ctrl, 'a');

      ctrl.onBlur();

      expect(ngModelCtrl.$setValidity).toHaveBeenCalledWith('createBeneficiary', true);
    });

    it('should update validity when beneficiary was changed', () => {
      const accounts = createAccounts();

      const ctrl = createController({
        accounts,
        allowCreate: false,
      });

      spyOn(ngModelCtrl, '$setValidity');

      ctrl.$onChanges({
        accounts: {
          currentValue: ctrl.accounts,
        },
      });

      ctrl.$postLink();

      changeName(ctrl, 'a');
      changeIdentifier(ctrl, 'a');

      ctrl.onBlur();

      expect(ngModelCtrl.$setValidity).toHaveBeenCalledWith('createBeneficiary', false);

      ctrl.ngModel = accounts[0];

      ctrl.$onChanges({
        ngModel: {
          currentValue: accounts[0],
        },
      });

      expect(ngModelCtrl.$setValidity).toHaveBeenCalledWith('createBeneficiary', true);
    });
  });
});
