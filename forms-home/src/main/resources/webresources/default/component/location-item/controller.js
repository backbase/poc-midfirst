(function () {
    angular.module('forms-ui').directive('googlemapLocation', /*@ngInject*/ function () {
        return {
            restrict: 'A',
            require: {
                form: '^^bbForm'
            },
            link: function ($scope, element, attrs) {
                var context = (element.context) ? element.context : element[0];
                var latLong;
                var map;
                var marker;
                var mapTarget = context.querySelector('.google_map');
                var generatedStyles = [
                    {
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#302f2b"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#746855"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#242f3e"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.locality",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#e1e4e9"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#a4a4a4"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.business",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#514f49"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "labels.text",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#e1e4e9"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#46443f"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#c2bcb2"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#746855"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#1f2835"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#f3d19c"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#2f3948"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.station",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#e1e4e9"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#4a6f9d"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#515c6d"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#17263c"
                            }
                        ]
                    }
                ];
                $scope.$watch("element.plainText", function (pNuValue, pOldValue) {
                    if (pNuValue) {
                        latLong = pNuValue.split(",");
                        checkForGoogle();
                    }
                });
                // --------------------------------
                function mapReady() {
                    var coords = {lat: 0, lng: 0};
                    if (latLong.length === 2) {
                        coords.lat = Number(latLong[0]);
                        coords.lng = Number(latLong[1]);
                    }
                    if (map) {
                        //--

                        mapTarget.classList.remove("animateIn");
                        mapTarget.classList.add("animateOut");

                        TweenLite.delayedCall(.2,function () {
                            mapTarget.classList.remove("animateOut");
                            mapTarget.classList.add("animateIn");
                            map.setCenter(coords);
                            marker.setPosition(coords);
                        })

                        //--
                    } else {
                        map = new google.maps.Map(mapTarget, {
                            zoom: 17,
                            center: coords,
                            styles: generatedStyles
                        });

                        marker = new google.maps.Marker({
                            position: coords,
                            map: map
                        });
                        mapTarget.classList.add("animateIn");
                    }

                }

                function checkForGoogle() {
                    if (map) {
                        mapReady();
                    } else {
                        try {
                            mapReady();
                        } catch (err) {
                            TweenLite.delayedCall(.05, checkForGoogle);
                        }
                    }
                }
            }
        };
    });
})();
