package com.backbase.forms.services.proxy;

import java.io.InputStream;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of {@link HttpRequestFactory}. It creates the correct
 * HTTP Client Request based on the method.
 * 
 * @author Walter Werner
 *
 */
public class DefaultHttpRequestFactory implements HttpRequestFactory {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DefaultHttpRequestFactory.class);

	private String serverBaseUrl;

	@Override
	public HttpUriRequest createRequest(String path, String method,
			String contentType, int contentLength, InputStream body) {
		HttpUriRequest result = createRequest(path, method);
		if (result instanceof HttpEntityEnclosingRequestBase) {
			HttpEntityEnclosingRequestBase entityRequest = (HttpEntityEnclosingRequestBase) result;
			entityRequest.setEntity(new InputStreamEntity(body, contentLength,
					ContentType.create(contentType)));
		}
		return result;
	}

	private HttpUriRequest createRequest(String path, String method) {
		String url = serverBaseUrl + (path.startsWith("/") ? path : "/" + path);
		LOGGER.debug("Creating request {}:{}", method, url);
		if ("GET".equals(method)) {
			return new HttpGet(url);
		} else if ("POST".equals(method)) {
			return new HttpPost(url);
		} else if ("PUT".equals(method)) {
			return new HttpPut(url);
		} else if ("DELETE".equals(method)) {
			return new HttpDelete(url);
		} else if ("HEAD".equals(method)) {
			return new HttpHead(url);
		} else if ("OPTIONS".equals(method)) {
			return new HttpOptions(url);
		} else if ("PATCH".equals(method)) {
			return new HttpPatch(url);
		} else if ("TRACE".equals(method)) {
			return new HttpTrace(url);
		}
		throw new IllegalArgumentException("Invalid http method " + method);
	}

	/**
	 * The server base URL.
	 * 
	 * @return the server url
	 */
	public String getServerBaseUrl() {
		return serverBaseUrl;
	}

	/**
	 * Sets the server base URL. This URL should be set with the base URL of the
	 * server being proxied.
	 * 
	 * @param serverBaseUrl
	 */
	public void setServerBaseUrl(String serverBaseUrl) {
		LOGGER.debug("Using forms base URL {}", serverBaseUrl);
		if (serverBaseUrl.endsWith("/")) {
			serverBaseUrl = serverBaseUrl.substring(0,
					serverBaseUrl.length() - 2);
		}
		this.serverBaseUrl = serverBaseUrl;
	}

}
