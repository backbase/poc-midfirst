package com.backbase.expert.forms.extensions.services;

import com.aquima.interactions.framework.service.ServiceResult;
import com.aquima.interactions.portal.IService;
import com.aquima.interactions.portal.IServiceContext;
import com.aquima.interactions.portal.IServiceResult;
import com.aquima.interactions.portal.ServiceException;
import com.aquima.web.config.annotation.AquimaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@AquimaService("BB_ClearServiceError")
public class ClearServiceError implements IService {

	private static final Logger LOG = LoggerFactory.getLogger(ClearServiceError.class);
	
	private static final String FAIL = "fail";
	private static final String OK = "ok";

	/**
	 * Method to handle the plugin.
	 * @param context -> ServiceContext
	 * @throws ServiceException -> ServiceException
	 * @throws Exception -> Exception
	 * @return ServiceResult
	 */
	public IServiceResult handle(IServiceContext context) throws ServiceException {

        try {
            LOG.debug("ClearServiceError: called");
            context.clearErrors();
            context.setStopOnErrors(false);
            LOG.debug("ClearServiceError: returned OK");
            return new ServiceResult(OK);
        }
        catch(Exception e) {
            LOG.debug("ClearServiceError: returned FAIL");
            return new ServiceResult(FAIL);
        }
	}
	
}