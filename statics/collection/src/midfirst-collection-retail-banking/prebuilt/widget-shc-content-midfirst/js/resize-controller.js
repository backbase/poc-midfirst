"use strict";
    function ResizeController() {
        ResizeController.prototype.updateValues = function () {
            this.winW = window.innerWidth;
            this.winH = window.innerHeight;

            this.bodyW = (document.body.clientWidth) ? document.body.clientWidth : window.innerWidth;
            this.bodyH = (document.body.clientHeight) ? document.body.clientHeight : window.innerHeight;


        };

        ResizeController.prototype.bindFunctions = function () {
            this.handleResize = this.handleResize.bind(this);
            this.handleOrientationChange = this.handleOrientationChange.bind(this);
            this.resizeCall = this.resizeCall.bind(this);
        };

        ResizeController.prototype.cacheElements = function () {

        };

        ResizeController.prototype.resizeCall = function (e) {
            gadgets.pubsub.publish('window-resize', {w: this.winW, h: this.winH});

        };

        ResizeController.prototype.handleResize = function (e) {
            this.updateValues();
            this.resizeCall();
        };

        ResizeController.prototype.handleOrientationChange = function (e) {
            this.handleResize();

        };
        //--
        this.cacheElements();
        this.bindFunctions();
        window.addEventListener("resize", this.handleResize);
        //--
        var _this = this;
        setTimeout(function () {
            _this.updateValues();
        },50);
        //--
    }

if (window.b$ && !window.b$.resizeController) window.b$.resizeController = new ResizeController();





