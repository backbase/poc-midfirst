


angular.module('forms-ui').directive('linkControl', function() {
    return {
        restrict: 'A',
        require: {
            form: '^^bbForm'
        },
        link: function ($scope, element, attrs,formsController) {
            var form = formsController.form;
            var sessionConfig = form.config;
            var session = form.session;
            $scope.downloadURL = makeUrl(sessionConfig.runtimePath, session.sessionId,$scope.element.get('parameters')['document-type'], $scope.element.get('parameters')['document-name']);
            function makeUrl(runtimeUrl, sessionId, pDocumentType, pDocumentName) {
                return runtimeUrl + '/server/session/' + sessionId + '/api/document/'+pDocumentType+'/'+pDocumentName+'/'+pDocumentName+'.'+pDocumentType;
            }

        }
    };
});
