(function (blueriq, bqApp, ko) {
    'use strict';

    blueriq.webFileUploadModelFactory = function (model, context) {
        if (model && model.type === 'container' && model.contentStyle === 'webfile_upload') {
            return new blueriq.models.WebFileUploadModel(model, context);
        }
    };

    if (bqApp) {
        bqApp.modelFactory.register(blueriq.webFileUploadModelFactory);
    }

    blueriq.models.WebFileUploadModel = function (model, context) {
        var self = this;

        this.browserSupported = window.FormData !== undefined;

        blueriq.models.ContainerModel.call(self, model, context);

        this.type = 'webFileUpload';

        this.buttonText = context.session.getModel(model.children[0]).caption;

        this.file = ko.observable();
        this.fileObjectURL = ko.observable();
        this.attributeName = context.session.getModel(model.children[0]).name;

        this.fileUploadTextItem = ko.observable();

        this.update = function WebFileUploadModelUpdate(model) {
            if (model.children[1] !== undefined) {
                self.fileUploadTextItem(context.modelFactory.createViewModel(context.session.getModel(model.children[1]), context));
            }
        };

        this.upload = function () {
            var fd = new FormData();
            fd.append('file', self.file());
            fd.append('elementName', self.attributeName);
            fd.append('pageEvent', JSON.stringify(context.session.createPageEvent()));

            $.ajax({
                type: 'POST',
                url: new blueriq.QueryStringBuilder(bqApp.configuration.baseUri + context.session.id + '/api/subscription/' + context.subscriptionId + '/webfileupload/handle')
            				.param('X-CSRF-Token', context.session.csrfToken()).toUrl(),
                contentType: false,
                cache: false,
                processData: false,
                data: fd,
                success: function (data) {
                    context.eventHandler.handleEvents(true, data);
                },
                error: function () {
                    context.eventHandler.handleEvents(false);
                }
            });
        };
    };
})(window.blueriq, window.bqApp, window.ko);
