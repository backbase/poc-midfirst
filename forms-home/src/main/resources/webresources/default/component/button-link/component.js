(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'button-link',
            condition: function(el) {
                return [
                    el.isButton,
                    el.styles.indexOf("button_external_link") >-1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();