(function (blueriq, bqApp) {
    'use strict';

    blueriq.cmisTemplateFactory = function (viewModel, bindingContext) {

        var prefix = viewModel.context.configuration.templatePath;

        if (viewModel && viewModel.model.name === 'fileDownload') {
            return blueriq.templates.fileDownload.cmis.fileDownloadLink;
        }
    };
    if (bqApp) {
        bqApp.templateFactory.registerModelHandler(blueriq.cmisTemplateFactory);
    }

})(window.blueriq, window.bqApp);