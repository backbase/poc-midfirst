(function () {
    'use strict';

    var app = angular.module('forms-ui');

    /**
     * Format Content Item Filter
     * Formats content item objects for display
     */
    app.filter('formatContentItem', function ($filter) {

        function findNode (pNode, pStyle, pNodeType) {
            function checkForChildNodes() {
                var res = null;
                if (pNode.nodes) {
                    pNode.nodes.forEach(function (pItem) {
                        if (!res) {
                            res = findNode(pItem, pStyle, pNodeType);
                        }
                    });
                }

                return res;
            }
            //console.log("checking item",pNode.presentationStyle);

            if (pNode.nodeType && pNode.presentationStyle && pNodeType && pStyle) {

                if (pNode.nodeType === pNodeType && pNode.presentationStyle === pStyle) {
                    return pNode;
                } else {
                    return checkForChildNodes();
                }
            } else {
                if (!pStyle && pNode.nodeType && pNodeType) {
                    if (pNode.nodeType === pNodeType) {
                        return pNode;
                    } else {
                        return checkForChildNodes();
                    }
                }
            }
        }

        function postOrder(nodes) {
            var returnVal = '';

            angular.forEach(nodes, function (value) {
                if (value.nodeType === 'text') {
                    returnVal += value.text || '';
                    if (value.nodes) {
                        returnVal += postOrder(value.nodes);
                    }
                } else if (value.nodeType === 'value') {
                    returnVal += value.values.join('') || '';
                    if (value.nodes) {
                        returnVal += postOrder(value.nodes);
                    }
                } else if (value.nodeType === 'style') {

                    switch (value.presentationStyle) {
                        case 'NewLine': {
                            returnVal += '<br />';
                            break;
                        }
                        case 'Bold': {
                            returnVal += '<strong class="' + $filter('beautify')(value.presentationStyle.split(' ')) + '">';
                            returnVal += value.text || '';
                            if (value.nodes) {
                                returnVal += postOrder(value.nodes);
                            }
                            returnVal += '</strong>';
                            break;
                        }

                        case 'Link': {
                            //console.log("picked contentItemLink", value);
                            var linTextNode = findNode(value,"LinkText","style");
                            var linUrlNode = findNode(value,"href","style");
                            var linkText;
                            var linkURL;
                            if (linTextNode) {
                                linkText = findNode(linTextNode,null,"text").text;
                            }
                            if (linUrlNode) {
                                linkURL = findNode(linUrlNode,null,"text").text;
                            }
                            returnVal += ' <a target=_blank href='+linkURL+'>'+linkText+'</a>';
                            break;
                        }

                        default: {
                            returnVal += '<span class="' + $filter('beautify')(value.presentationStyle.split(' ')) + '">';
                            returnVal += value.text || '';
                            if (value.nodes) {
                                returnVal += postOrder(value.nodes);
                            }
                            returnVal += '</span>';
                        }
                    }
                }
            });

            return returnVal;

        }

        return function (contentItemNodes) {
            return postOrder(contentItemNodes);
        };
    });
})();