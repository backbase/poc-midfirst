(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'content-item',
            condition: function(el) {
                return [
                    el.type === 'contentitem'
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();