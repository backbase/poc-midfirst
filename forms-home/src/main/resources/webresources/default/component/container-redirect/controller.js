(function() {
    angular.module('forms-ui').directive('containerRedirect',  /*@ngInject*/ function () {
        return {
            restrict: 'A',
            require: {
                form: '^^bbForm'
            },
            link: function ($scope, element, attrs,formsController) {
                var context = (element.context)?element.context:element[0];
                var hidValue = $scope.$parent.element.children[0].value[0];
                var split = hidValue.split("||");
                var version = split[0];
                var flow = split[1];
                var lang = split[2];
                var project = split[3];

                var config = {flow:flow,lang:lang,project:project,theme:'theme-retail',runtimePath:'../../../..'};

                formsController.form.startSessionWithConfig(config);

                setTimeout(function () {
                    $scope.$emit('update');
                },1000);

            }
        };
    });
})();
