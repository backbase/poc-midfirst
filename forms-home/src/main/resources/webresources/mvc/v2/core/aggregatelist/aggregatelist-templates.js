(function (blueriq, bqApp) {
    'use strict';

    blueriq.aggregateTemplateFactory = function (viewModel) { //, bindingContext

        var prefix = viewModel.context.configuration.templatePath +'aggregatelist/';

        if (viewModel && viewModel.contentStyle) {
            if (['al_search_number', 'al_search_currency', 'al_search_percentage'].indexOf(viewModel.contentStyle) !== -1) {
                return blueriq.templates.aggregateList.alSearchNumeric;
            } else if (viewModel.contentStyle === 'al_footer') {
                return blueriq.templates.aggregateList.alFooter;
            }else if (viewModel.contentStyle === 'al_header') {
                return blueriq.templates.aggregateList.alHeader;
            }else if (viewModel.contentStyle === 'al_headercell') {
                return blueriq.templates.aggregateList.alheaderCell;
            }else if (viewModel.contentStyle === 'al_search_boolean') {
                return blueriq.templates.aggregateList.alSearchBoolean;
            }else if (viewModel.contentStyle === 'al_search_date') {
                return blueriq.templates.aggregateList.alSearchDate;
            }else if (viewModel.contentStyle === 'al_search_datetime') {
                return blueriq.templates.aggregateList.alSearchDateTime;
            }else if (viewModel.contentStyle === 'al_search_domain') {
                return blueriq.templates.aggregateList.alSearchDomain;
            }else if (viewModel.contentStyle === 'al_search_integer') {
                return blueriq.templates.aggregateList.alSearchInteger;
            }else if (viewModel.contentStyle === 'al_search_text') {
                return blueriq.templates.aggregateList.alSearchText;
            }
        }
    };

    if (bqApp) {
        bqApp.templateFactory.registerModelHandler(blueriq.aggregateTemplateFactory);
    }

})(window.blueriq, window.bqApp);

