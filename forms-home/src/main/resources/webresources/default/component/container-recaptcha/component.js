(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'container-recaptcha',
            condition: function(el) {
                return [
                    el.contentStyle === 'BB_Recaptcha',
                    el.isContainer
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();