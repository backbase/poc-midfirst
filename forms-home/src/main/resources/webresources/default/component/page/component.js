(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'page',
            condition: function(el) {
                return [
                    el.isPage
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();