(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'step-button',
            condition: function(el) {
                return [
                    el.isButton,
                    el.styles.indexOf('step_button') > -1
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();