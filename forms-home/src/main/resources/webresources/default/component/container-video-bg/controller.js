(function() {
    angular.module('forms-ui').directive('videoBg',  /*@ngInject*/ function () {
        return {
            restrict: 'A',
            require: {
                form: '^^bbForm'
            },
            link: function ($scope, element, attrs,formsController) {
                var context = (element.context)?element.context:element[0];
                var cleanClass = $scope.element.styles.toString().replace(new RegExp(",",'g'),' ');
                var video ;
                var shown = false;
                $scope.videoID = cleanClass.match(/\bvideo_(?:\w*)\b/g)[0];

                $scope.$on("fade-video",function (pEvent) {
                    //TweenLite.to(video,.3,{opacity:0});
                    video.classList.remove("playing");
                    shown = false;
                    if (pEvent&& pEvent.stopPropagation)pEvent.stopPropagation();
                });

                function init () {
                    video  = context.querySelector('video');
                    video.addEventListener("playing",function () {
                        if (!shown) video.classList.add("playing");
                        shown = true;
                    });
                }

                setTimeout(function () {
                    init();
                },0);
            }
        };
    });
})();
