(function () {
    angular.module('forms-ui').directive('shcUiAccordionGroup', /*@ngInject*/ function (resizeService,$rootScope) {
        return {
            require: {
                form: '^^bbForm'
            },
            restrict: 'A',
            link: function ($scope, element, attrs, formsController) {
                var context = (element.context) ? element.context : element[0];
                var _resizeWatch = $scope.$on("accordion-resize", resizeGroup);
                var _handleScrollToItem = $scope.$on("navigation::scroll-to-item",scrollToItem);
                var _handleStylesUpdate = $scope.$watch("element.classList",handleStylesUpdate);
                // --
                var accordionItems = context.querySelector(".accordion-items");
                var isActive = $scope.element.styles.indexOf("active") !== -1;
                // --
                var _lastScrolledItem;
                var isCollapsed = false;
                var isInit = false;

                $scope.$on('$destroy', function () {
                    console.log("accordion group  destroyed");
                    _resizeWatch();
                    _handleScrollToItem();
                    _handleStylesUpdate();
                });

                $scope.callCollapse = function () {
                    $scope.$broadcast("force-toggle-emit");
                };

                $scope.$on("update", function (pEvent, pTargetScope) {
                    if (pTargetScope && pTargetScope.styles.indexOf("Accordion_Toggle") !== -1) {
                        return;
                    }
                    if (isActive) {
                        pEvent.stopPropagation();
                        pEvent.preventDefault();
                        var searchTarget = pEvent.targetScope;
                        var found;
                        while (!found && searchTarget) {
                            if (searchTarget.element && searchTarget.element.styles) {
                                if (searchTarget.element.styles.toString().indexOf('question_identifier_') !== -1) {
                                    found = searchTarget.element.styles[0];
                                }
                            }
                            if (!found) {
                                if (searchTarget.$parent) {
                                    searchTarget = searchTarget.$parent;
                                } else {
                                    searchTarget = null;
                                }
                            }
                        }
                        if (found) {
                            $scope.$emit("accordion-item-selected", found);
                        }
                    } else {
                        if (pTargetScope) {
                            // console.log("picked pageSwitch",pEvent,pParam2);
                            pEvent.stopPropagation();
                            $rootScope.$broadcast('fade-video');
                            $rootScope.$broadcast('finish-page',pEvent);
                            $rootScope.$broadcast('accordion::collapse');
                            $rootScope.$broadcast('navigation::release-shifting');
                        }
                        /* var targets = context.getElementsByClassName('');
                         var tline = new TimelineLite({tweens:tArray,onComplete:function(){
                         pEvent.targetScope.$emit("update");
                         }}); */
                    }
                });


                function init () {
                    isInit = true;
                    if (isActive) {
                        $scope.$broadcast("activate-accordion-item");
                    }

                }



                function resizeGroup(pEvent) {
                    if (_lastScrolledItem) {
                        TweenLite.to(accordionItems,.25,{scrollTop: _lastScrolledItem.offsetTop - accordionItems.offsetTop});
                    }
                }

                function scrollToItem (pEvent,pParam) {
                    if (isActive) {
                        _lastScrolledItem = pParam;
                        TweenLite.to(accordionItems,.25,{scrollTop: pParam.offsetTop-  accordionItems.offsetTop});
                    }
                }

                function handleStylesUpdate (pNu,pOld) {
                    console.log("AccordionGroup:: picking new styles",pNu);
                    isCollapsed = (pNu.indexOf("collapsed") !== -1);
                    TweenLite.killDelayedCallsTo(checkCollapsedState);
                    if (!isInit) {
                        function checkInit () {
                            if(isInit) {
                                checkCollapsedState();
                            }else {
                                TweenLite.delayedCall(.05,checkInit);
                            }
                        }
                        checkInit();
                    }else {
                        TweenLite.delayedCall(.05,checkCollapsedState);
                    }
                }
                function checkCollapsedState () {
                    if (isCollapsed) {
                        $scope.$emit("accordion::collapse-accordion-group",context);
                    }else {
                        $scope.$emit("accordion::expand-accordion-group",context);
                    }
                }

                setTimeout(function () {
                   init();
                },0);
            }
        };
    });

})();
