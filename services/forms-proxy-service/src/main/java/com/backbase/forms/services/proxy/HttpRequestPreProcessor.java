package com.backbase.forms.services.proxy;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpRequest;
import org.apache.http.client.CookieStore;

/**
 * Preprocessors are executed before the request is executed by the proxy
 * service. It can be used to change the request before it is executed, for
 * example add standard headers.
 * 
 * @author Walter Werner
 *
 */
public interface HttpRequestPreProcessor {

	/**
	 * Called before the request is executed.
	 * 
	 * @param originalRequest the original request
	 * @param cookieStore the cookie store taht will be used for the request
	 * @param request the request that will be executed
	 */
	void process(HttpServletRequest originalRequest, CookieStore cookieStore,
			HttpRequest request);
}
