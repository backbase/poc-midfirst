import FORMAT_AMOUNT_TEMPLATE_URL from './constants';

import controller from './selector.controller';

/**
 * @name uiBbAccountSelector
 * @type {Object}
 *
 * @property {Object} ngModel Account selector model
 * @property {Array} accounts List of accounts
 * @property {Function} ngChange Callback function triggered when account is selected
 * @property {Boolean} selectAll True if select all option is enabled
 * @property {Labels} labels Object with labels
 * @property {?String} selectAllTemplateId Enables select all option and contains template id
 * @property {String} customTemplateId Id of the template that will be used for rendering
 *   instead of default ui-bb-account-selector/option-template.html template
 * @property {Function} onAccountsLoad Function to retrieve filtered accounts. It should
 *   update accounts assigns to `accounts` property and number of accounts in `totalItems` property
 * @property {Function} onClear Function to be called once the search input is cleared
 * @property {Function} totalItems Determines total number of accounts that available in database
 * @property {SearchBox} searchBox Object contains minLength of search
 *   string and config object of searchBox
 * @property {?String} formatAmountTemplateUrl Custom template url for popup element.
 */
const uiBbAccountSelector = {
  bindings: {
    model: '<ngModel',
    ngChange: '&',
    accounts: '<',
    selectAll: '<',
    labels: '<',
    customTemplateId: '@',
    selectAllTemplateId: '@',
    onAccountsLoad: '&',
    onClear: '&',
    totalItems: '=',
    searchBox: '<',
    formatAmountTemplateUrl: '@?',
  },
  template: ['$attrs', (attrs) => `
    <div class="bb-account-selector">
      <ui-bb-dropdown-select class="bb-account-selector-toggle drop-shadow"
        data-ng-model="$ctrl.state.accounts.selected"
        data-ng-change="$ctrl.onChange({ $item: $item.isSelectAll ? null : $item })"
        data-template-url="ui-bb-account-selector/selected-template.html"
        data-is-open="$ctrl.state.isOpen"
        data-placeholder="${attrs.placeholder}">
        <li class="dropdown-option p-3"
          data-ng-if="::$ctrl.searchBox">
          <ui-bb-search-box-ng class="d-block"
            data-ng-model="$ctrl.state.search.value"
            data-config="$ctrl.state.search.config"
            data-ng-click="$ctrl.onSearchBoxClick($event)"
            data-on-change="$ctrl.onSearch(search)"
            data-on-submit="$ctrl.onSearch(search)"
            data-is-loading="$ctrl.state.accounts.isSearching">
          </ui-bb-search-box-ng>
        </li>
        <li class="dropdown-option p-3"
          data-ng-show="!$ctrl.state.accounts.data.length"
          data-ng-bind="$ctrl.labels.noAccounts">
        </li>
        <ui-bb-list data-on-scroll-to-end="$ctrl.onLoadMore()"
          class="d-block bb-account-selector-scrollable-area">
          <ui-bb-dropdown-option data-ng-if="$ctrl.selectAll"
            data-option="$ctrl.state.allAccountsOption"
            data-template-url="ui-bb-account-selector/option-template.html">
          </ui-bb-dropdown-option>
          <ui-bb-dropdown-option data-ng-repeat="account in $ctrl.state.accounts.data"
            data-option="account"
            data-template-url="ui-bb-account-selector/option-template.html">
          </ui-bb-dropdown-option>
          <li class="dropdown-option p-3"
            data-ng-show="$ctrl.state.accounts.isLoadingMore">
            <span class="bb-account-selector-spinner center-block"></span>
          </li>
        </ui-bb-list>
      </ui-bb-dropdown-select>
    </div>
    <script type="text/ng-template" id="ui-bb-account-selector/selected-template.html">
      <a href
        tabindex="0"
        class="d-block pl-3 py-4 pr-10 bb-account-selector-option"
        data-ng-include="'ui-bb-account-selector/option-content-template.html'">
      </a>
    </script>
    <script type="text/ng-template" id="ui-bb-account-selector/option-template.html">
      <a href
        tabindex="0"
        class="d-block pl-3 py-3 pr-7 bb-account-selector-option"
        data-ng-include="'ui-bb-account-selector/option-content-template.html'">
      </a>
    </script>
    <script type="text/ng-template" id="ui-bb-account-selector/option-content-template.html">
      <div data-ng-if="$option.isSelectAll"
        class="d-flex flex-column flex-sm-row align-items-sm-center justify-content-sm-between
          bb-account-selector-option-content">
        <div class="d-inline-block" data-ng-bind="$option.label"></div>
        <div class="d-inline-block" data-ng-bind="$option.quantity"></div>
      </div>
      <div data-ng-if="!$option.isSelectAll"
        class="d-flex flex-column flex-sm-row align-items-sm-center justify-content-sm-between
          bb-account-selector-option-content">
        <div class="d-inline-block">
          <div><b data-ng-bind="$option.name"></b></div>
          <div data-ng-bind="$option.identifier"></div>
        </div>
        <div class="d-inline-block" data-ng-hide="$option.hideAmounts">
          <div data-ng-include="'${FORMAT_AMOUNT_TEMPLATE_URL}'"></div>
        </div>
      </div>
    </script>
    
    <script type="text/ng-template" id="${FORMAT_AMOUNT_TEMPLATE_URL}">
      <ui-bb-format-amount 
        data-amount="$option.amount"
        data-currency="'$'"
        data-wrap>
      </ui-bb-format-amount>
    </script>
  `],
  controller: ['$templateCache', '$element', '$attrs', '$scope', controller],
};

export default uiBbAccountSelector;

/**
 * Labels type definition
 * @typedef {Object} Labels
 * @property {String}  allAccounts Label for all accounts
 * @property {String}  accounts Label for plural accounts
 * @property {String}  account Label for single accounts
 */

/**
 * Labels type definition
 * @typedef {Object} SearchBox
 * @property {String}  minLength Minimum length of search string to apply search
 * @property {Object}  config SearchBox config object
 */
