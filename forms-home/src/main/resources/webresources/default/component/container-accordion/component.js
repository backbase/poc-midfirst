(function () {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'container-accordion',
            condition: function(el) {
                return [
                    el.isContainer,
                    el.contentStyle === "Accordion"
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();