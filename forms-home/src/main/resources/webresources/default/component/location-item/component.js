(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'location-item',
            condition: function(el) {
                return [
                    el.type === 'textitem',
                    el.name === 'Location'
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();