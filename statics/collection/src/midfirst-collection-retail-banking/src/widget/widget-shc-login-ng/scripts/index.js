/**
 * @module widget-shc-login-ng
 *
 * @description
 * Login widget
 */
import angular from 'vendor-bb-angular';

import widgetModuleKey, {
  widgetKey,
} from 'lib-bb-widget-ng';

import modelLoginModuleKey, { modelLoginKey } from 'model-shc-login-ng';
import Controller from './controller';

export default angular
  .module('widget-shc-login-ng', [
    modelLoginModuleKey,
    widgetModuleKey,
  ])

  .controller('LoginController', [
    // dependencies to inject
    modelLoginKey,
    widgetKey,
    /* into */
    Controller,
  ])

  .name;
