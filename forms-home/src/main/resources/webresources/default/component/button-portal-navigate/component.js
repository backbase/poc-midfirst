(function() {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'button-portal-navigate',
            condition: function(el) {
                return [
                    el.isButton,
                    el.styles.indexOf('button_portal_navigate') > -1
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();
