(function () {
    /**
     * @ngInject
     */
    function registerElement(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'button-icon',
            condition: function (el) {
                return [
                    el.isButton,
                    el.styles.indexOf('button_icon') > -1
                ];
            }

        });
    }

    angular.module('forms-ui').run(registerElement);
})();
