(function () {
    angular.module('forms-ui').directive('containerLightbox', /*@ngInject*/ function ($rootScope) {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs, $filter) {
                var context = (element.context) ? element.context : element[0];
                //--
                console.log("lightbox rendered:: ", context);
                //--
                var _updateWatch = $scope.$on("update", formUpdateCatch);
                var _submitSuccessWatch = $scope.$on("lightbox-transition",transitionAllOut);
                var panel ;

                $scope.$on('$destroy', function () {
                    _updateWatch();
                    _submitSuccessWatch();
                });




                function transitionAllOut (pEvent,pElement) {
                    console.log("transitionAllOut:: ", pEvent, pElement);


                    $rootScope.$broadcast('finish-page', pEvent);
                    $rootScope.$broadcast('accordion::collapse');
                    $rootScope.$broadcast('navigation::release-shifting');

                    TweenLite.to(panel, .3, {opacity: 0});
                    TweenLite.to(panel, .3, {ease: Circ.easeIn, scale: .8});
                    TweenLite.to(context, .2, {delay: .1, opacity: 0});

                }

                function init() {
                    panel = context.querySelector(".LightboxPanel");
                    TweenLite.set(panel,{opacity:0,scale:.8});
                    TweenLite.to(context, .2, {opacity: 1});
                    TweenLite.to(panel, .2, {opacity: 1});
                    TweenLite.to(panel, .3, {ease:Circ.easeOut,scale: 1});
                }

                function formUpdateCatch(pEvent, pTarget) {
                    if (pTarget && (pTarget.styles.indexOf("LightBox_Close") !== -1 || pTarget.styles.indexOf("LightBox_Submit") !== -1)) {
                        TweenLite.to(panel, .3, {opacity: 0});
                        TweenLite.to(panel, .3, {ease:Circ.easeIn,scale: .8});
                        TweenLite.to(context, .2, {delay:.1,
                            opacity: 0, onComplete: function () {
                                pEvent.targetScope.$emit("update");
                            }});
                        pEvent.stopPropagation();
                        pEvent.preventDefault();
                    }

                }


                setTimeout(init, 0);
            }
        };
    });

})();
