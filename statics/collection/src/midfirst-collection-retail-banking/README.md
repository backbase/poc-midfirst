# Showcase Retail Collection

Contains the Showcase Retail Collection 2.0. This collection has a Maven dependency on Retail Collection 2.x which gets packaged into this collection as well. Objective is to put extensions, theme and potential custom widgets in this collection.

## How to use

### Build

After cloning the collection run the following command in the root:

`mvn clean package`

This command will produce the following 2 zip archives in the ***target*** folder. 

* collection-shc-retail-cxp-dist-<VERSION>.zip: This archive contains a zip of zips and can be imported into CXP Portal

* collection-shc-retail-ext-dist-<VERSION>.zip: This archive contains the expanded dist structure which can be unzipped and package with the mobile app.

### Serve

Install dependencies with:

`npm i`

Using bb-serve we are able to serve the collection standalone. You can run the command below from the root to serve the collection. 

`bb-serve project --template ./target/bb-serve-template-blank/package/index.hbs src -b sass --prebuilt prebuilt`

For ease of use for developers, bb-serve is now a dev dependency. The workflow for running bb-serve is simplified to:

`npm start`

## Deployment of Collection (only run by Jenkins)

The collection is deployed to artifacts.backbase.com so the Showcase Project will be able to retrieve them. By running the command below the 2 zips will be deployed to artifacts.backbase.com. 

`mvn deploy`