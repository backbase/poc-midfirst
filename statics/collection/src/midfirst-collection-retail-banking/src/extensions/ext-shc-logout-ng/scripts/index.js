/**
 * @module ext-shc-logout-ng
 *
 * @description
 * Default extension for User Menu widget.
 *
 * @requires ui-bb-avatar-ng
 * @requires ui-bb-i18n-ng
 * @requires vendor-bb-angular-ng-aria
 *
 * @example
 * <!-- User Menu widget model.xml -->
 * <property name="extension" viewHint="text-input,admin">
 *  <value type="string">ext-shc-logout-ng</value>
 * </property>
 */
import uiBbAvatarKey from 'ui-bb-avatar-ng';
import i18nKey from 'ui-bb-i18n-ng';
import ngAriaModuleKey from 'vendor-bb-angular-ng-aria';

export const dependencyKeys = [
    uiBbAvatarKey,
    i18nKey,
    ngAriaModuleKey,
];

export default dependencyKeys;
