(function () {

    /**
     * Controller for stepped-slider components
     * Adds a mapper between options and values to local scope
     * @ngInject
     */

    angular.module('forms-ui').directive('bootstrapDatepicker', function () {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs, $filter) {
                var context = (element.context) ? element.context : element[0];
                console.log("datePickerSimpleXX link", context);
                var target = $(context);
                var input = target[0].querySelector("input");
                $scope.showDatepicker = showDatepicker;
                $scope.dateValue = $scope.element.value[0];


                var datePicker = null;

                var cleanClass = $scope.element.styles.toString().replace(new RegExp(",", 'g'), ' ');
                var dateFormat = cleanClass.match(/\bdate_(?:\w*)\b/g)[0];
                console.log("dateFormat", dateFormat);
                console.log("dateValue", $scope.dateValue);

                if ($scope.dateValue) {
                    var splitDash = $scope.dateValue.split("-");
                    var correctDashes = splitDash.length === 3;
                    if (splitDash && correctDashes) {
                        if (dateFormat === "date_NL") {
                            //YYYY MONTH DAY
                            $scope.dateValue = splitDash[2] + "-" + splitDash[1] + "-" + splitDash[0];
                        }

                        if (dateFormat === "date_UK") {
                            $scope.dateValue = splitDash[2] + "/" + splitDash[1] + "/" + splitDash[0];
                        }

                        $scope.$evalAsync();
                    }
                }
                target.datepicker.dates['en'] = {
                    days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                    daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                    daysMin: ["S", "M", "T", "W", "T", "F", "S"],
                    months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                    monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                    today: "Today",
                    clear: "Clear",
                    format: "mm/dd/yyyy",
                    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
                    weekStart: 1
                };

                target.datepicker({
                    format: $scope.element.FormAttributeSuggestion,
                    clearBtn: true,
                    container: target[0],
                    todayHighlight: true,
                    maxViewMode: 2,
                    endDate: '0d'
                });

                target.on('changeDate', function (e) {
                    var targetDate = new Date(e.date);
                    var nuDate = new Date(targetDate.getTime());
                    nuDate = nuDate.getFullYear() + '-' + (nuDate.getMonth() + 1) + '-' + nuDate.getDate();
                    $scope.element.value = [nuDate];
                    $scope.$emit('update');
                    $scope.$evalAsync();
                    hideDatepicker();
                });

                function hideDatepicker() {
                    var target = getDatePicker();                     
                    if (target.classList.contains('open')) target.classList.remove("open");
                }

                function showDatepicker() {
                    var target = getDatePicker();
                    if (!target.classList.contains('open')) target.classList.add("open");

                }

                function getDatePicker() {
                    if (!datePicker) {
                        datePicker = context.querySelector(".datepicker-inline");
                    }
                    return datePicker;
                }

                function init() {
                    hideDatepicker();
                }

                setTimeout(init, 0);
            }
        };
    });
})();
