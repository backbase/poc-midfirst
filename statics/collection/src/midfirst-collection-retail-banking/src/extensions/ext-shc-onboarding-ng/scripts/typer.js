var globalTypers = {};

var Typer = function(element) {
    this.element = element;
    var delim = element.dataset.delim || ","; // default to comma
    var words = element.dataset.words || "override these,sample typing";
    this.words = words.split(delim).filter(function(v){return v;}); // non empty words
    this.delay = element.dataset.delay || 200;
    this.loop = false;
    this.deleteDelay = element.dataset.deletedelay || element.dataset.deleteDelay || 800;

    this.progress = { word:0, char:0, building:true, atWordEnd:false, looped: 0 };
    this.typing = false;

    var colors = element.dataset.colors || "black";
    this.colors = colors.split(",");
    this.element.style.color = this.colors[0];
    this.colorIndex = 0;

    this.doTyping();
};

Typer.prototype.start = function() {
    if (!this.typing) {
        this.typing = true;
        this.doTyping();
    }
};
Typer.prototype.stop = function() {
    this.typing = false;
};
Typer.prototype.doTyping = function() {
    var e = this.element;
    var p = this.progress;
    var w = p.word;
    var c = p.char;
    var currentChar = this.words[w][c];
    p.atWordEnd = false;

    if (p.building) {
        e.innerHTML += currentChar;
        p.char += 1;
        if (p.char == this.words[w].length) {
            p.building = false;
            p.atWordEnd = true;
        }
    }

    if(p.atWordEnd) p.looped += 1;

    if(!p.building && (this.loop == "false" || this.loop <= p.looped) ){
        this.typing = false;
    }

    var myself = this;
    setTimeout(function() {
        if (myself.typing) { myself.doTyping(); };
    }, p.atWordEnd ? this.deleteDelay : this.delay);
};

function TyperSetup() {

    var elements = document.getElementsByClassName("typer");
    for (var i = 0, e; e = elements[i++];) {
        globalTypers[e.id] = new Typer(e);
    }
    var elements = document.getElementsByClassName("typer-stop");
    for (var i = 0, e; e = elements[i++];) {
        var owner = globalTypers[e.dataset.owner];
        e.onclick = function(){owner.stop();};
    }
    var elements = document.getElementsByClassName("typer-start");
    for (var i = 0, e; e = elements[i++];) {
        var owner = globalTypers[e.dataset.owner];
        e.onclick = function(){owner.start();};
    }
}


console.log('ja');
$(document).ready(function() {
    TyperSetup(); //only when ready
})

