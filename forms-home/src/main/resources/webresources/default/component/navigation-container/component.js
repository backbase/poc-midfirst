(function () {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'navigation-container',
            condition: function(el) {
                return [el.isContainer, el.styles.indexOf('navigation_container') > -1];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();