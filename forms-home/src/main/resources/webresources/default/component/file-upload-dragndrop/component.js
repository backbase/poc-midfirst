(function() {

    /**
     * @ngInject
     */
    function registerElement(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'file-upload-dragndrop',
            condition: function(el) {
                return  [
                    el.contentStyle === 'fileupload',
                    // el.styles.indexOf("drag_drop") > -1,/*/**/*/
                    el.isContainer
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerElement);

})();
