/* global $ */

$.get('/portalserver/services/rest/v1/party-data-management/party', function (data) {

    if (data) {
        var titleElem = document.getElementsByClassName('profile-info__append-name')[0];
        if (titleElem) {
            var currentText = titleElem.innerHTML;
            var lastChar = currentText.substr(currentText.length - 1);
            var postFix = " ";

            if (lastChar === ' ') {
                postFix = "";
            }

            titleElem.innerHTML = currentText + postFix + data.firstName + '!';
        }
    }

});
