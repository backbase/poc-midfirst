import widget from './index';

describe('Onboarding', () => {
  it('should export the module name', () => {
    expect(widget).toEqual('widget-shc-onboarding-ng');
  });
});
