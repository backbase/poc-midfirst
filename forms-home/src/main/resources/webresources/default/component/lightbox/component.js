(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'lightbox',
            condition: function(el) {
                return [
                    el.isContainer,
                    el.styles.indexOf("lightbox") > -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();