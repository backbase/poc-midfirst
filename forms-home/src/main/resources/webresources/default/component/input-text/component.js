(function() {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'input-text',
            condition: function(el) {
                return [
                    el.dataType === 'text',
                    !el.hasDomain
                ];


            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();
