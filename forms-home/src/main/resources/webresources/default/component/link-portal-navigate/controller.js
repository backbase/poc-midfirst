(function () {
    angular.module('forms-ui').directive('linkPortalNavigate', function ($location) {
        return {
            require: {
                form: '^^bbForm'
            },
            restrict: 'A',
            link: function ($scope, element, attrs, formsController) {
                console.log("linkPortalNavigate");
                var targetPort = (window.location.port === 8686) ? 7777 : window.location.port;
                var captionSplit = $scope.element.get('caption').split('||');
                var targetPage = captionSplit[1];
                $scope.captionText = captionSplit[0];
                var url = (b$) ? window.location.protocol + "//" + window.location.hostname + ":" + targetPort + b$.portal.portalServer.serverURL + b$.portal.portalName + "/" + targetPage : '';

                $scope.clickEvent = function () {
                    window.open(url, "_parent");
                }

            }
        };
    });
})();