/* jshint node:true */
module.exports = function (grunt) {
    'use strict';

    require('time-grunt')(grunt);
    require('load-grunt-tasks')(grunt);
    grunt.loadNpmTasks('grunt-browser-sync');



    var scripts = [
        'js/**/*.js'
    ];

    var sass = [
        'sass/**/*.scss'
    ];


    var src = {
        scripts: scripts
    };

    grunt.initConfig(
        {
            // ----- Environment
            pkg: grunt.file.readJSON('package.json'),


            sass: {
                options: {
                    sourceMap: true
                },
                dist: {
                    files: {
                        'css/style.css': 'sass/style.scss'
                    }
                }
            },

            jshint: {
                options: {
                    jshintrc: '.jshintrc'
                },
                all: scripts
            },

            jscs: {
                options: {
                    config: '.jscs'
                },
                dev: {
                    files: {
                        src: scripts
                    }
                }
            },

            watch: {
                script: {
                    files: scripts,
                    tasks: ['jscs', 'jshint'] 
                },
                css: {
                    files: ['sass/**/*.scss'],
                    tasks: ['sass']
                },
                html: {
                    files: ['template/**/*.html'],
                    tasks: []
                }

            },

            'http-server': {
                'dev': {
                    root: '../../',
                    runInBackground: true
                }
            },
            browserSync: {
                dev: {
                    bsFiles: {
                        src : 'css/style.css'
                    },
                    options: {
                        watchTask:true,
                        proxy: "http://0.0.0.0:8086/forms-runtime/server/start?project=export-CreditCard&flow=Dashboard&version=0.0-Trunk&languageCode=en-GB&ui=mvc&theme=bootstrap3"
                    }
                }
            }

        }
    );

    grunt.registerTask('default-bootstrap3', ['jscs', 'jshint', 'http-server', 'watch']);
    grunt.registerTask('start', ['browserSync', 'watch']);

    grunt.loadNpmTasks('grunt-notify');

};
