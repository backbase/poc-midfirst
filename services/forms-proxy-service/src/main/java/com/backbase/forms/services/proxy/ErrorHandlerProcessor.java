package com.backbase.forms.services.proxy;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.processor.DefaultExchangeFormatter;
import org.apache.camel.spi.ExchangeFormatter;
import org.apache.camel.util.MessageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Error handler implementation.
 * 
 * @author Walter Werner
 *
 */
// TODO: Make it generic and use in other routes
public class ErrorHandlerProcessor implements Processor {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ErrorHandlerProcessor.class);
	private ExchangeFormatter formatter;

	public ErrorHandlerProcessor() {
		DefaultExchangeFormatter formatter = new DefaultExchangeFormatter();
		formatter.setShowExchangeId(true);
		formatter.setMultiline(true);
		formatter.setShowHeaders(true);
		formatter.setStyle(DefaultExchangeFormatter.OutputStyle.Fixed);
		this.formatter = formatter;
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		Throwable cause = (exchange.getException() != null) ? exchange
				.getException() : (Throwable) exchange.getProperty(
				Exchange.EXCEPTION_CAUGHT, Throwable.class);
		LOGGER.error(MessageHelper.dumpMessageHistoryStacktrace(exchange,
				formatter, true), cause);
		exchange.getIn().setHeader(Exchange.HTTP_RESPONSE_CODE, 500);
		exchange.getIn().setBody(
				"{\"error\":\"Error executing forms request\"}");
	}

}
