(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'table',
            condition: function(el) {
                return [
                    el.type === 'table'
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();