/**
 * @module widget-shc-user-menu-ng
 *
 * @description
 * User Menu widget
 */
import angular from 'vendor-bb-angular';

import widgetModuleKey, {
  widgetKey,
} from 'lib-bb-widget-ng';

import modelLoginModuleKey, { modelLoginKey } from 'model-shc-login-ng';
import Controller from './controller';

export default angular
  .module('widget-shc-user-menu-ng', [
    modelLoginModuleKey,
    widgetModuleKey,
  ])

  .controller('UserMenuController', [
    // dependencies to inject
    modelLoginKey,
    widgetKey,
    /* into */
    Controller,
  ])

  .name;
