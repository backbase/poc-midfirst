/**
 * @module ext-midfirst-cards-ng
 *
 * @description
 * Basic extension for widget-midfirst-cards-ng
 *
 * @requires vendor-bb-angular-ng-aria
 */
import ngAriaModuleKey from 'vendor-bb-angular-ng-aria';
import uiBbAccountCard from 'ui-bb-account-card';

// uncomment below to include CSS in your extension
// import '../styles/index.css';

export const dependencyKeys = [
  ngAriaModuleKey,
    uiBbAccountCard,
];

export const hooks = {};

export const helpers = {};

export const events = {};
