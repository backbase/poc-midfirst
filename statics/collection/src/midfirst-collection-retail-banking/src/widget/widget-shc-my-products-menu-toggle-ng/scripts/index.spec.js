import widget from './index';

describe('My Products Menu Button', () => {
  it('should export the module name', () => {
    expect(widget).toEqual('widget-shc-my-products-menu-toggle-ng');
  });
});
