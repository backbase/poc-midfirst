(function () {
    /**
     * @ngInject
     */
    function registerControl(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'radio-field',
            condition: function (el) {
                return [
                    el.hasOptions,
                    !el.multiple,
                    el.styles.indexOf('radio') > -1 || el.styles.indexOf('Radio') > -1
                ];
            }

        });
    }

    angular.module('forms-ui').run(registerControl);
})();
