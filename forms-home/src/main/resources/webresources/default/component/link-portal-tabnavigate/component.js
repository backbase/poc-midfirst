(function() {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'link-portal-tabnavigate',
            condition: function(el) {
                return [
                    el.isButton,
                    el.styles.indexOf('link_portal_tabnavigate') > -1
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();
