(function() {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'input-creditcard-date-datepicker',
            condition: function(el) {
                return [
                    el.dataType === 'date',
                    el.name === "Creditcard.EffectiveDate"
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();
