(function () {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'container-accordion-group-item',
            condition: function(el) {
                return [
                    el.isContainer,
                    el.contentStyle === "AccordionItem"
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();