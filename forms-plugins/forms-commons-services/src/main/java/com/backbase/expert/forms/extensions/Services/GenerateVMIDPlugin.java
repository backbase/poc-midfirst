package com.backbase.expert.forms.extensions.services;

import com.aquima.interactions.foundation.IValue;
import com.aquima.interactions.framework.service.ServiceResult;
import com.aquima.interactions.portal.IService;
import com.aquima.interactions.portal.IServiceContext;
import com.aquima.interactions.portal.IServiceResult;
import com.aquima.interactions.portal.ServiceException;
import com.aquima.web.config.annotation.AquimaService;
import com.backbase.expert.forms.extensions.utils.FormsUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@AquimaService("BB_GenerateVMID")
public class GenerateVMIDPlugin implements IService {

    private final static Logger LOG = LoggerFactory.getLogger(GenerateVMIDPlugin.class);
    private static final String PARAM_OUTPUT_ID = "ID";


    @Override
    public IServiceResult handle(IServiceContext serviceContext) throws ServiceException {

        String id = new java.rmi.dgc.VMID().toString();
        String idField = serviceContext.getParameter(PARAM_OUTPUT_ID);

        LOG.debug("Persisted ID: {}", id);

        FormsUtils.setAttr(serviceContext, idField, id, false, true);
        return new ServiceResult();
    }
}
