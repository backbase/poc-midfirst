(function() {
    angular.module('forms-ui').directive('shcUiAccordionGroupItem',  /*@ngInject*/ function (textService) {
        return {
            require: {
                form: '^^bbForm'
            },
            restrict: 'A',
            link: function ($scope, element, attrs, formsController) {
                var context = (element.context) ? element.context : element[0];
                $scope.parseBold =textService.parseBold;
                $scope.parseNormal =textService.parseNormal;

                var _watchActiveQuestion ;
                var _watchActivateQuestion = $scope.$on("activate-accordion-item",handleActivateQuestion);

                var cleanClass = $scope.element.styles.toString().replace(new RegExp(",",'g'),' ');
                var questionId = cleanClass.match(/\bquestion_identifier_(?:\d*)\b/g)[0];


                $scope.clicked = function () {
                    $scope.$broadcast("force-emit");
                };

                $scope.$destroy = function () {
                    if (_watchActiveQuestion) _watchActiveQuestion();
                    _watchActivateQuestion();
                };

                function handleActiveQuestion (pEvent,pParam) {
                    $scope.isActive = questionId === pParam;
                    if($scope.isActive) $scope.$emit("navigation::scroll-to-item",context);
                }

                function handleActivateQuestion () {
                    _watchActivateQuestion = $scope.$on("navigation::active-question",handleActiveQuestion);
                }
            }
        };
    });
})();
