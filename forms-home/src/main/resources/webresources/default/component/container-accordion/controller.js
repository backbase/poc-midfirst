(function () {
    angular.module('forms-ui').directive('shcUiAccordion', /*@ngInject*/ function ($rootScope, resizeService) {
        return {
            require: {
                form: '^^bbForm'
            },
            restrict: 'A',
            link: function ($scope, element, attrs, formsController) {
                //--
                var _resizeWatch = $scope.$on("window-resize", resizeContainer);
                var _collapseWatch = $scope.$on('accordion::collapse', collapseAccordion);
                var _collapseGroupWatch = $scope.$on("accordion::collapse-accordion-group", collapseGroupWatch);
                var _expandGroupWatch = $scope.$on("accordion::expand-accordion-group", expandGroupWatch);
                //--
                var context = (element.context) ? element.context : element[0];
                var headArray;
                var accordionWraps;
                var accordionHeight = resizeService.getSize().h;
                //--
                $scope.$on("accordion-item-selected", function (pEvent, pParam) {
                    pEvent.preventDefault();
                    $rootScope.$broadcast("scroll-to-question", pParam);
                });

                $scope.$on("$destroy", function () {
                    _resizeWatch();
                    _collapseWatch();
                });

                function init () {
                    headArray = Array.prototype.slice.call(context.getElementsByClassName("accordion-row--head"));
                    accordionWraps = Array.prototype.slice.call(context.getElementsByClassName("accordion-items"));

                }


                function collapseAccordion() {
                    console.log("collapse-accordion", context.className);
                    if (!context.classList.contains("collapsed")) context.classList.add("collapsed");
                }

                function collapseGroupWatch(pEvent, pParam) {
                    pParam = pParam.querySelector(".accordion-items");
                    pParam.collapsed = true;
                    checkAccordionSize();
                }

                function expandGroupWatch(pEvent, pParam) {
                    pParam = pParam.querySelector(".accordion-items");
                    pParam.collapsed = false;
                    console.log("expand Group",pParam);
                    checkAccordionSize();
                }


                function checkAccordionSize () {
                    if (accordionWraps) {
                        var openItems = accordionWraps.filter(function (pItem) {
                            return !pItem.collapsed;
                        });
                        var sharedHeight = accordionHeight / openItems.length;
                        accordionWraps.forEach(function (pItem,pKey) {
                            var key = pKey + 1;
                            if (!pItem.collapsed) {
                                var children = Array.prototype.slice.call(pItem.getElementsByClassName("accordion-row"));
                                var childHeight = 0;
                                children.forEach(function (pItem) {
                                    childHeight += pItem.clientHeight;
                                });

                                var targetH = childHeight < sharedHeight?childHeight:sharedHeight;
                                if (key === accordionWraps.length) targetH = sharedHeight;
                                TweenLite.to(pItem, .4, {ease:Circ.easeInOut,height: targetH});
                                //console.log("targetH",targetH);
                            }
                            else TweenLite.to(pItem, .4, {ease:Circ.easeInOut,height:0});
                        });
                    }
                }


                function resizeContainer(pEvent) {
                    //var rect = context.getBoundingClientRect();
                    accordionHeight = resizeService.getSize().h ;
                    var headsSize = 0;
                    //context.style.height = accordionHeight+"px";
                    TweenLite.to(context, 0.3, {height: accordionHeight});
                    //--
                    var heads = headArray.length;
                    //--
                    headArray.forEach(function (pItem) {
                        var localRect = pItem.getBoundingClientRect();
                        headsSize += localRect.height;
                    });

                    accordionHeight = accordionHeight - headsSize;
                    checkAccordionSize();

                    $scope.$broadcast("accordion-resize");
                }

                setTimeout(init,0);
            }
        };
    });

})();
