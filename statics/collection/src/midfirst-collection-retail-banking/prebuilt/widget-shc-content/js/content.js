/**
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : content.js
 *  Description:
 *
 *  Source code for CXP Universal Content Widget
 *  ----------------------------------------------------------------
 */

b$ = b$ || {};
b$.universal = b$.universal || {};
b$.universal.widgets = b$.universal.widgets || {};
b$.universal.widgets.contentWidget = (function ($, bd) {
	'use strict';

	return {
		/**
		 * Initialize content widget
		 * @param  {Object} oWidget Backbase widget object
		 */
		start: function (oWidget) {
			var that = this;
			var currentCustomClasses = '';

			var delayTime = (be.ice && bd && bd.designMode === 'true') ? 0 : 200;
			var renderICEWidget = function () {
				var jqWidget = $(oWidget.body);

				if (!be.ice) {
					$('img[src=""], img:not([src])').addClass('be-ice-hide-image');
					jqWidget.addClass("be-ice-hide-image");
					jqWidget.addClass('be-ice-widget-rendered be-ice-widget-rendered--presentation');
					setHeight();
					checkForBgContent();
					setBlockAlignment();
					setContrastType();
					addCustomClasses();
					//initialAccordionExpandEvent();
					return;
				}
				oWidget.iceConfig = be.ice.config;

				var isMasterpage = be.utils.module('top.bd.PageMgmtTree.selectedLink').isMasterPage,
					isManageable = isMasterpage || (
						// .model.manageable === true means widget belongs to master page
						// and is made manageable. In this case content still shouldn't be editable
						oWidget.model.manageable === '' ||
						oWidget.model.manageable === undefined
					);

				if (isManageable && be.ice.controller && /(?:ADMIN|CREATOR)/.test(oWidget.model.securityProfile)) {
					var templateUrl = String(oWidget.getPreference('templateUrl')),
						enableEditing = function () {

							// it is possible to swap template for editorial
							// here is an example for image template
							if (templateUrl.match(/\/image\.html$/)) {
								templateUrl = templateUrl.replace(/\/image\.html$/, '/image-editorial.html');
							}
							setHeight();
							checkForBgContent();
							setBlockAlignment();
							setContrastType();
							addCustomClasses();
							//initialAccordionExpandEvent();

							return be.ice.controller.edit(oWidget, templateUrl)
								.then(function (dom) {
									jqWidget.find('.bp-g-include').html(dom);

									return dom;
								}).then(function () {
									checkForBgContent();
								});
						};

					// Get template list from options.json
					that.options = that.options || (function(){
						var optionsPref = oWidget.getPreference('template-options');
						var optionsURL = optionsPref.replace('$(contextRoot)/', b$.portal.portalServer.serverURL);
						var itemRoot = optionsPref.substring(0, optionsPref.indexOf('/widget-shc-content/') + 19);

						return $.getJSON(optionsURL).then(function(data){
							return data.options.templateUrl.map(function(option) {
								return {
									label: option.label,
									value: option.value.replace('$(itemRoot)', itemRoot)
								};
							});
						});
					})();

					that.options.then(function(data){
						var aPrefs = b$.portal.portalModel.filterPreferences(oWidget.model.preferences.array);
						for (var i = aPrefs.length - 1; i >= 0; i--) {
							if (aPrefs[i].name === 'templateUrl') {
								aPrefs[i].inputType.options = data;
							}
						}
					});

					// This event fired before preference form being open
					// oWidget.addEventListener('preferences-form', function (event) {});

					// this widget has property rendering example (template: simple.html)
					// so we need to refresh widget after property 'title' modified
					oWidget.model.addEventListener('PrefModified', function (oEvent) {
						if (oEvent.attrName === 'title') {
							enableEditing();
						}

						setHeight();
						setBlockAlignment();
						setContrastType();
						checkForBgContent();
						addCustomClasses();
						//initialAccordionExpandEvent();

					}, false, oWidget);

					return enableEditing();
				} else {
					jqWidget.addClass('be-ice-widget-rendered');
					checkForBgContent();
				}

				function checkForBgContent() {
					var templateUrl = String(oWidget.getPreference('templateUrl'));
					if (templateUrl.indexOf('shc-modal-column-content') > -1) {
						b$.universal.widgets.contents.bgContent.init(oWidget, oWidget.body.querySelector('.shc-modal-column-img'));
					}
					b$.universal.widgets.contents.bgContent.init(oWidget);
				}

				function setBlockAlignment() {
					var alignment = oWidget.getPreference('blockAlign');
					oWidget.body.classList.remove("block-left", "block-right");
					oWidget.body.classList.add(alignment);
					var search = oWidget.body.getElementsByClassName("text-block");
					search = Array.prototype.slice.call(search);
					search.forEach(function (pItem) {
						pItem.classList.remove("text-left");
						pItem.classList.remove("text-right");
					});

					switch (alignment) {
						case 'block-left':
							search.forEach(function (pItem) {
								pItem.classList.add("text-left");
							});
							break;
						case 'block-right':
							search.forEach(function (pItem) {
								pItem.classList.add("text-right");
							});
							break;
					}
				}

				function setHeight() {
					//--
					var height = oWidget.getPreference('widgetHeight');
					var heightMobile = oWidget.getPreference('widgetHeightMobile');
					//--
					oWidget.body.style.overflow = "hidden";
					//--
					gadgets.pubsub.unsubscribe("window-resize", resizingPerc);
					gadgets.pubsub.unsubscribe("window-resize", resizingPercMobile);
					if (height.length) {
						height = height.split(" ").join('');
						oWidget.body.style.setProperty('--widget-height', height);
						var lastChar = height.substr(height.length - 1, 1);
						var perc = ~~(height.substr(0, height.length - 1));
						if (lastChar && lastChar === '%') {
							gadgets.pubsub.subscribe("window-resize", resizingPerc);
							resizingPerc();
						}
					}

					if (heightMobile.length) {
						heightMobile = heightMobile.split(" ").join('');
						oWidget.body.style.setProperty('--widget-height--mobile', heightMobile);
						//--
						var lastCharM = heightMobile.substr(heightMobile.length - 1, 1);
						var percHMobile = ~~(heightMobile.substr(0, heightMobile.length - 1));
						//-
						if (lastCharM && lastCharM === '%') {
							gadgets.pubsub.subscribe("window-resize", resizingPercMobile);
							resizingPercMobile();
						}
					}

					function resizingPerc() {
						var res = window.b$.resizeController.winH * (perc / 100);
						oWidget.body.style.setProperty('--widget-height', res + "px");

					}

					function resizingPercMobile() {
						var res = window.b$.resizeController.winH * (percHMobile / 100);
						oWidget.body.style.setProperty('--widget-height--mobile', res + "px");
					}
				}

				function setContrastType() {
					var theme = oWidget.getPreference('colorTheme');
					oWidget.body.classList.remove("theme--base", "theme--contrast");
					oWidget.body.classList.add(theme);
				}

				function initialAccordionExpandEvent() {
					var accordion = oWidget.body.querySelector('.accordion');
					if (accordion !== null) {
						accordion.onclick = function () {
							this.classList.toggle("active");
							var panel = this.nextElementSibling;
							if (panel.style.maxHeight) {
								panel.style.maxHeight = null;
							} else {
								panel.style.maxHeight = panel.scrollHeight + "px";
							}
						};
					}
				}

				function addCustomClasses() {
					var widgetCustomClasses = oWidget.getPreference('widgetCustomClasses');
					var classList = oWidget.body.classList;

					if(widgetCustomClasses !== currentCustomClasses && currentCustomClasses) {
						// Remove previous classes
						currentCustomClasses.split(/\s/).forEach(function(className){
							classList.remove(className);
						});
					}
					if(widgetCustomClasses) {
						// Add custom classes
						widgetCustomClasses.split(/\s/).forEach(function(className){
							classList.add(className);
						});
					}
					currentCustomClasses = widgetCustomClasses;
				}
			};

			// Extend widget in design mode
			setTimeout(renderICEWidget, delayTime);

		}
	};
}(window.jQuery, window.bd));
