(function () {
    /**
     * @ngInject
     */
    function registerElement(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'radio-button-group',
            controller: 'RadioGroupController as controller',
            condition: function (el) {
                return [
                    el.hasOptions,
                    !el.multiple,
                    el.styles.indexOf('radio_button_group') !== -1
                ];
            }

        });
    }

    angular.module('forms-ui').run(registerElement);
})();
