/**
 * @module widget-midfirst-bill-pay-ng
 *
 * @description
 * Bill pay
 */
import angular from 'vendor-bb-angular';

import widgetModuleKey, { widgetKey } from 'lib-bb-widget-ng';
import eventBusModuleKey, { eventBusKey } from 'lib-bb-event-bus-ng';
import extendHooks from 'lib-bb-widget-extension-ng';

import Controller from './controller';

import defaultHooks from './default-hooks';

const moduleKey = 'widget-midfirst-bill-pay-ng';
const hooksKey = `${moduleKey}:hooks`;

export default angular
  .module(moduleKey, [
    widgetModuleKey,
    eventBusModuleKey,
  ])

  .factory(hooksKey, extendHooks(defaultHooks))

  .controller('BillPayController', [
    // dependencies to inject
    eventBusKey,
    hooksKey,
    widgetKey,
    /* into */
    Controller,
  ])

  .name;
