/**
 * @description
 * Helpers for ext-shc-insurance-panel-policy-ng
 *
 * @name Helpers
 * @type {Object}
 */

export default () => {
	return {	
        getUserImage: pInfo => (pInfo && pInfo.photoUrl) ?  `url(${pInfo.photoUrl})` : '',        
        getUsersFullname: pInfo => (pInfo && pInfo.firstName)  ? `${pInfo.firstName} ${pInfo.lastName}` : ''
    };
}
