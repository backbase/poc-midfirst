(function (blueriq) {
    'use strict';

    if (!blueriq) {
        blueriq = {};
    }

    window.blueriq = blueriq;

    if (!blueriq.models) {
        blueriq.models = {};
    }

    if (!blueriq.services) {
        blueriq.services = {};
    }

    blueriq.models.dashboard = {};
    blueriq.services.dashboard = {};

    /**
     * Service for handling widget related actions on the server.
     * @constructor
     * @name blueriq.services.dashboard.WidgetService
     * @param {string} baseUri - Base URL of the server API.
     * @returns {@link blueriq.services.dashboard.WidgetService} widgetservice to handle widget-actions
     */
    blueriq.services.dashboard.WidgetService = function WidgetService(baseUri) {
        if (!baseUri) {
            throw new Error('baseUri is mandatory');
        }

        /**
         * Creates a subsession for the specified session id, that gets the specified token as identifier.
         * @function
         * @name blueriq.services.dashboard.WidgetService#createSession
         * @param {string} sessionId - The id of the session to create a subsession on.
         * @param {string} csrfToken - The CSRF token for the main session.
         * @param {string} token - The identifier for the subsession to be created.
         * @param {function} callback - Callback method which will be called after successfully creating the session an returning the session
         */
        this.createSession = function (sessionId, csrfToken, token, callback) {
            if (!sessionId) {
                throw new Error('sessionId is mandatory');
            }
            if (!csrfToken) {
            	throw new Error('csrfToken is mandatory');
            }
            if (!token) {
                throw new Error('token is mandatory');
            }
            $.ajax({
                type: 'POST',
                url: baseUri + sessionId + '/api/widget/' + token,
                headers: {
                	'X-CSRF-Token': csrfToken
                },
                success: function (data) {
                    if (callback) {
                        callback(data.sessionId);
                    }
                },
                contentType: 'application/json'
            });
        };
    };

}(window.blueriq));if (typeof window.blueriq === 'undefined') {
    window.blueriq = {};
}
(function (ko, blueriq) {
    'use strict';

    /**
     * Creates a widgetBox to place widgets in
     * @constructor
     * @return {@link blueriq.models.dashboard.widgetBox} Widgetbox
     */
    ko.bindingHandlers.widgetBox = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var box = $(element).next();
            // If next element is a box and hasn't .non-collapsible class
            if (box.hasClass('box') && !box.hasClass('non-collapsible')) {
                $(element).append('<a href="#" class="box-collapse pull-right" onclick="var self = $(this).hide(100, \'linear\'); self.parent(\'.box-header\').next(\'.box\').slideUp(400, function() { $(\'.box-expand\', self.parent(\'.box-header\')).show(100, \'linear\'); });">hide&nbsp;&nbsp;<i class="icon-caret-up"></i></a>')
                    .append('<a href="#" class="box-expand pull-right" style="display: none" onclick="var self = $(this).hide(100, \'linear\'); self.parent(\'.box-header\').next(\'.box\').slideDown(400, function() { $(\'.box-collapse\', self.parent(\'.box-header\')).show(100, \'linear\'); });">show&nbsp;&nbsp;<i class="icon-caret-down"></i></a>');
            }
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var innerBox = $(element).find('.box');

        }
    };
    /**
     * Creates a bleuriq menubar
     * @constructor
     * @return {@link ko.bindingHandlers.bqMenubar} blueriq menubar
     */
    ko.bindingHandlers.bqMenubar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

            // Add click handler for closing wrappers
            $('html').mousedown(function () {
                $('.menubar-submenu-wrapper.open').removeClass('open');
                $('.menubar-submenu-toggle.open').removeClass('open');
            }).find('.menubar-submenu-toggle').click(function (e) {
                return false;
            });

            $('.menubar-submenu-wrapper').mousedown(function () {
                return false;
            });

            // Add click handler for opening submenu's
            $('.menubar-submenu-toggle').click(
                function (event) {
                    $('.menubar-submenu-wrapper.open').removeClass('open');
                    $('.menubar-submenu-toggle.open').removeClass('open');

                    var id = $(this).parent().attr('id');
                    $(this).addClass('open');

                    var wrapper = $('.menubar-submenu-wrapper[data-dropdown-owner=' + id + ']');
                    wrapper.addClass('open');
                    wrapper.width(wrapper.find('li').length * 82);

                    // Position submenu wrapper
                    if (!wrapper.parent().is('.dashboard-menu')) {
                        wrapper.css({top: $(this).parent().offset().top, left: $('.dashboard-menu').width() + 10});
                        wrapper.appendTo('.dashboard-menu');
                    }
                }
            );
        }
    };

    ko.virtualElements.allowedBindings.bqMenubar = true;


    ko.bindingHandlers.bqDashboardMenu = {
        init: function (element) {
            var container = $(element).parent();
            $('.menubar-horizontal-active').unbind().click(function (event) {
                if (container.hasClass('open')) {
                    container.css({
                        height: 37
                    });
                } else {
                    container.css({
                        height: $('.menubar').innerHeight() + 37
                    });
                }
                container.toggleClass('open');
                return false;
            }).text(
                $(($('.active', container).length === 1) ? '.active' : '.active .active', container).text()
            );
        }
    };

    ko.virtualElements.allowedBindings.bqDashboardMenu = true;


})(window.ko, window.blueriq);(function (blueriq, bqApp) {
    'use strict';
    blueriq.defaultDashboardModelHandler = function (viewModel, bindingContext) {
        var prefix = viewModel.context.configuration.templatePath;
        var containerPrefix = prefix + 'container/';

        if (viewModel.type === 'container' && viewModel.contentStyle === 'dashboard_header') {
            return blueriq.templates.container.dashboard.dashboardHeader;
        } else if (viewModel.type === 'container' && viewModel.contentStyle === 'dashboard_menu') {
            return blueriq.templates.container.dashboard.dashboardMenu;
        } else if (viewModel.type === 'container' && viewModel.contentStyle === 'dashboard_body') {
            return blueriq.templates.container.dashboard.dashboardBody;
        } else if (viewModel.type === 'container' && viewModel.contentStyle === 'dashboard_footer') {
            return blueriq.templates.container.dashboard.dashboardFooter;

        } else if (viewModel.type === 'container' && viewModel.contentStyle === 'dashboard_row') {
            return blueriq.templates.container.row;
        } else if (viewModel.type === 'container' && viewModel.contentStyle && viewModel.contentStyle.indexOf('dashboard_column') === 0) {
            return blueriq.templates.container.column;
        } else if (viewModel.type === 'container' && viewModel.contentStyle === 'dashboard_widget') {
            return blueriq.templates.container.widget;
        } else if (viewModel.type === 'flowwidget') {
            return blueriq.templates.flowWidget.flowWidget;
        } else if (viewModel.type === 'container' && viewModel.contentStyle === 'chartEasyPie') {
            return blueriq.templates.container.easyPieChart;
        } else if (viewModel.type === 'storecomment') {
            return blueriq.templates.comment.storeComment;
        } else if (viewModel.type === 'commentlist') {
            return blueriq.templates.comment.commentList;
        } else if (viewModel.contentStyle === 'timeline') {
            return blueriq.templates.container.timeline;
        } else if (viewModel.type === 'container' && viewModel.contentStyle === 'menubar') {
            if (bindingContext.modus && jQuery.inArray('widget', bindingContext.modus) > -1) {
                return;
            }
            return blueriq.templates.container.navbar;
        }
    };

    /**
     * Register automatically if bqApp is already created
     */
    if (bqApp) {
        bqApp.templateFactory.registerModelHandler(blueriq.defaultDashboardModelHandler);
    }

}(window.blueriq, window.bqApp));(function (blueriq, bqApp, ko) {
    'use strict';

    blueriq.defaultDashboardModelFactory = function (model, context) {
        var widgetService = new blueriq.services.dashboard.WidgetService(context.configuration.baseUri);

        /**
         * Checks whether a given page is a dashboard page.
         * @function
         * @private
         * @name blueriq.models.dashboard.ComponentManager~isDashBoardPage
         * @param {Object} model - The model data.
         * @param {Object} context - The context data .
         * @returns {boolean} boolean result telling whether the given page is a dashboardPage
         */
        function isDashboardPage(model, context) {
            if (model.type === 'page') {
                for (var i = 0; i < model.children.length; i++) {
                    var element = context.session.getModel(model.children[i]);
                    if (element.contentStyle && (
                        element.contentStyle === 'dashboard_header' ||
                        element.contentStyle === 'dashboard_menu' ||
                        element.contentStyle === 'dashboard_body' ||
                        element.contentStyle === 'dashboard_footer')) {
                        return true;
                    }
                }
            }
            return false;
        }

        if (model.type === 'page' && isDashboardPage(model, context)) {
            return new blueriq.models.dashboard.PageModel(model, context);
        } else if (model.type === 'container') {
            if (model.contentStyle === 'dashboard_flowwidget') {
                return new blueriq.models.dashboard.FlowWidgetModel(model, context, widgetService);
            } else if (model.contentStyle && model.contentStyle.indexOf('dashboard_column') === 0) {
                return new blueriq.models.dashboard.ColumnModel(model, context);
            } else if (model.contentStyle === 'storecomment') {
                return new blueriq.models.dashboard.StoreCommentModel(model, context);
            } else if (model.contentStyle === 'commentlist') {
                return new blueriq.models.dashboard.CommentListModel(model, context);
            }
        }
    };

    /**
     * Register automatically if bqApp is already created
     */
    if (bqApp) {
        bqApp.modelFactory.register(blueriq.defaultDashboardModelFactory);
    }

    /**
     * Represents a dashboard page.
     * @constructor
     * @name blueriq.models.dashboard.PageModel
     * @param {Object} model - The model data.
     * @param {Object} context - The context data .
     * @returns {@link blueriq.models.dashboard.PageModel} A Page Instance.
     */
    blueriq.models.dashboard.PageModel = function DashboardPageModel(model, context) {
        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);

        this.type = 'page';

        this.messages = ko.observableArray(model.messages);

        this.header = ko.observable(null);
        this.menu = ko.observable(null);
        this.body = ko.observable(null);
        this.footer = ko.observable(null);

        /**
         * Set metadata for PageModel
         * @function
         * @private
         * @name blueriq.models.dashboard.PageModel~setMetaData
         */
        function setMetadata() {
            for (var i = 0; i < self.children().length; i++) {
                var element = self.children()[i];
                if (element.contentStyle && element.contentStyle === 'dashboard_header') {
                    self.header(element);
                } else if (element.contentStyle && element.contentStyle === 'dashboard_menu') {
                    self.menu(element);
                } else if (element.contentStyle && element.contentStyle === 'dashboard_body') {
                    self.body(element);
                } else if (element.contentStyle && element.contentStyle === 'dashboard_footer') {
                    self.footer(element);
                }
            }
        }

        setMetadata();

        var baseUpdate = self.update;
        /**
         * Update method
         * @function
         * @name blueriq.models.dashboard.PageModel#update
         * @param {object} model context
         */
        this.update = function PageUpdate(model) {
            baseUpdate(model);

            setMetadata();
            self.messages(model.messages);
        };
    };

    /**
     * Represents a column container.
     * @constructor
     * @name blueriq.models.dashboard.ColumnModel
     * @param {Object} model The Container model.
     * @param {Object} context The data context.
     * @returns {@link blueriq.models.dashboard.ColumnModel} A columncontainer
     */
    blueriq.models.dashboard.ColumnModel = function ColumnModel(model, context) {
        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);

        self.span = ko.observable(model.contentStyle.substring(16));
    };

    /**
     * Represents a widget that has a flow.
     * @constructor
     * @name blueriq.models.dashboard.FlowWidgetModel
     * @param {Object} model - The widget model.
     * @param {Object} context - The data context.
     * @returns {@link blueriq.models.dashboard.FlowWidgetModel} An FlowWidgetModel instance.
     */
    blueriq.models.dashboard.FlowWidgetModel = function FlowWidgetModel(model, context, widgetService) {
        var self = this;

        blueriq.models.ContainerModel.call(self, model, context);

        this.type = 'flowwidget';

        self.session = ko.observable(null);

        widgetService.createSession(context.session.id, context.session.csrfToken(), model.properties.info, function (sessionId) {
            self.session(new blueriq.models.SessionModel(sessionId, context));
        });

        /**
         * Function to dispose the widget
         * @function
         * @name blueriq.services.dashboard.CommentListModel#dispose
         */
        this.dispose = function FlowWidgetModelDispose() {
            if (self.session()) {
                self.session().dispose();
            }
        };
    };

    /**
     * Creates a Model to create comments
     * @constructor
     * @name blueriq.services.dashboard.StoreCommentModel
     * @param {Object} model - The model for the comment
     * @param {Object} context - The data context
     * @returns {@link blueriq.models.dashboard.StoreCommentModel} instance of a container to create comments
     */
    blueriq.models.dashboard.StoreCommentModel = function StoreCommentModel(model, context) {
        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);
        this.type = 'storecomment';

        this.commentField = context.session.getModel(model.children[0]);
        this.commentButton = model.children[1];
        this.buttonText = context.session.getModel(model.children[1]).caption;
        this.commentValue = ko.observable(this.commentField.values[0]);
        this.submitted = ko.observable(false);

        /**
         * Handles click event to add a comment
         * @function
         * @name blueriq.services.dashboard.StoreCommentModel#addComment
         * @param {Object} model - The model for the comment
         * @param {Object} context - The data context
         */
        this.addComment = function addComment() {
            var parameters = {};

            if (self.commentValue() !== null && self.commentValue() !== '') {
                parameters[self.commentField.name] = [self.commentValue()];
                self.submitted(true);
                self.commentValue(null);
            }
            self.submit(self.commentButton, parameters);
        };

        /**
         * Handles keypresses in commentbox to avoid textchanges after text has been submitted
         * @function
         * @override
         * @name blueriq.services.dashboard.StoreCommentModel#keyPressed
         * @param {Object} data The data context
         * @param {Object} event The keypressed-event
         * @returns {boolean} returns true so the keypressed event goes up in the stack and might be handled elsewhere
         */
        this.keyPressed = function keyPressed() {//data, event
            if (self.submitted()) {
                self.submitted(false);
            }
            return true;
        };
    };

    /**
     * Creates a CommentListModel to display a single comment
     * @constructor
     * @name blueriq.services.dashboard.CommentListModel
     * @param {Object} model - The model for the comment
     * @param {Object} context - The data context
     * @returns {@link blueriq.models.dashboard.CommentListModel} single comment-model
     */
    blueriq.models.dashboard.CommentListModel = function CommentListModel(model, context) {
        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);
        this.type = 'commentlist';
        this.maxCharacters = 100;

        /**
         * Function to check whether a comment should resize.
         * @function
         * @name blueriq.services.dashboard.CommentListModel#shouldResize
         * @param {string} comment - The comment
         * @returns {boolean} Tells whether the comment should resize
         */
        this.shouldResize = function shouldResize(comment) {
            return (comment.match(/\n/g) !== null || comment.length > this.maxCharacters);
        };

        /**
         * Function to get the short version a comment (1st line and max the first 100 characters in this 1st line)
         * @function
         * @name blueriq.services.dashboard.CommentListModel#getShortComment
         * @param {String} comment - The comment
         * @returns {String} the short version of a comment
         */
        this.getShortComment = function getShortComment(comment) {
            comment = comment.split(/\n/g)[0];
            comment = comment.substr(0, this.maxCharacters);
            comment = comment + '...';
            return comment;
        };

        /**
         * Function to toggle the html comments
         * @function
         * @name blueriq.services.dashboard.CommentListModel#toggleText
         * @param {Object} element - The element from the DOM
         * @param {Object} event - The onclick event
         */
        this.toggleText = function toggleText(element, event) {
            $(event.currentTarget).parent().find('.shortComment').toggle();
            $(event.currentTarget).parent().find('.totalComment').toggle();
        };
    };
}(window.blueriq, window.bqApp, window.ko));