(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'button',
            condition: function(el) {
                return [
                    el.isButton
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();