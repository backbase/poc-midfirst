/**
 * @module widget-shc-avatar-ng
 *
 * @description
 * Widget shc sidebar right
 */
import angular from 'vendor-bb-angular';

import shcPartyModelModuleKey, {shcPartyModelKey} from 'model-shc-party-data';
import widgetModuleKey, {widgetKey} from 'lib-bb-widget-ng';
import modelLoginModuleKey, { modelLoginKey } from 'model-shc-login-ng';

import Controller from './controller';

export default angular
    .module('widget-shc-avatar-ng', [widgetModuleKey,shcPartyModelModuleKey,modelLoginModuleKey])

    .controller('ShcAvatarController', [
        // dependencies to inject
        widgetKey,
        shcPartyModelKey,
		modelLoginKey,
        /* into */
        Controller
    ])

    .name;
