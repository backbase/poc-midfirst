(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'step-container',
            condition: function(el) {
                return [
                    el.isContainer,
                    el.contentStyle === "stepcontainer"
                ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();