/* global b$ */
(function () {
    'use strict';

    var Container = b$.bdom.getNamespace('http://backbase.com/2013/portalView').getClass('container');

    Container.extend(function () {
        Container.apply(this, arguments);
        this.isPossibleDragTarget = true;
    }, {
        localName: 'templateShcSimpleNavigation',
        namespaceURI: 'templates_templateShcSimpleNavigation'
    }, {
        template: function (json) {
            var data = {item: json.model.originalItem};
            return window[this.namespaceURI][this.localName](data);
        },
        handlers: {
            DOMReady: function (event) {
                const htmlNode = event.currentTarget.htmlNode;
                const pid = htmlNode.getAttribute('data-pid');
                const isFooterNav = pid.indexOf('footer-navigation') > -1;
                if (isFooterNav) initializeFooterNavigation(htmlNode);
            },
            preferencesSaved: function (event) {
                if (event.target === this) {
                    this.refreshHTML(function () {
                        //add code, HTML refreshed
                    });
                }
            }
        }
    });

    function initializeFooterNavigation(node) {
        const footerNavigation = node.querySelector('.footer-navigation');
        const groups = footerNavigation.querySelectorAll('.footer-navigation__group');
        groups.forEach(function(group) {
            const title = group.querySelector('.footer-navigation__title');
            const children = group.querySelector('.footer-navigation__children');
            title.addEventListener('click', function (event) {
                event.preventDefault();
                const titleExpanded = node.querySelector('.footer-navigation__title-expanded');
                const childrenExpanded = node.querySelector('.footer-navigation__children-expanded');
                const differentGroupSelected = childrenExpanded !== children;
                if (titleExpanded && differentGroupSelected) titleExpanded.classList.remove('footer-navigation__title-expanded');
                title.classList.toggle('footer-navigation__title-expanded');
                if (childrenExpanded && differentGroupSelected) childrenExpanded.classList.remove('footer-navigation__children-expanded');
                children.classList.toggle('footer-navigation__children-expanded');
            });
        });
    }
})();
