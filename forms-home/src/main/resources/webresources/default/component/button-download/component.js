(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'button-download',
            condition: function(el) {
                return [
                    el.isButton,
                    el.name === 'downloadButton'
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();