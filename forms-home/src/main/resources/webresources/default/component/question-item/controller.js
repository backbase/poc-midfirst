(function () {
    angular.module('forms-ui').directive('questionItem', function (textService) {
        return {
            require: {
                form: '^^bbForm'
            },
            restrict: 'A',
            link: function ($scope, element) {
                var context = (element.context) ? element.context : element[0];
                //var questionId = ~~(Math.random()*2000)+"";
                var cleanClass = $scope.element.styles.toString().replace(new RegExp(",", 'g'), ' ');
                var questionId = cleanClass.match(/\bquestion_identifier_(?:\d*)\b/g)[0];
                context.setAttribute("question-id", questionId);
                $scope.$emit("question-item-ready", context);
                //console.log("questionReady",context);
                var _topPos;
                var _elemContent = context.querySelector('.question-item__content');
                var _winH;
                var _elemHeight = 0;
                var _marginLimit = 50;
                var _alignWatch;
                var _scrollWatch;
                var _forceEmitWatch;
                var _activeQuestionWatch;
                //--
                var _localActive = false;
                //--
                TweenLite.set(_elemContent, {opacity: 0});
                TweenLite.set(_elemContent, {marginTop: _marginLimit});
                //--
                $scope.boldPrefix = "/(\[)[^\[]+(\])/g";
                //--

                $scope.$on('$destroy', function () {
                    console.log("question-item destroyed");
                    _scrollWatch();
                    _alignWatch();
                    _forceEmitWatch();
                    _activeQuestionWatch();
                    $scope.$emit("question-item-destroyed", context);
                });

                _forceEmitWatch = $scope.$on("force-emit", function (pEvent, pParam) {
                    if (!pEvent.defaultPrevented) {
                        if (pParam && pParam === context) {
                            pEvent.defaultPrevented = true;
                            $scope.$broadcast("force-emit");
                            pEvent.preventDefault();
                        }
                    }
                });

                _activeQuestionWatch = $scope.$on("activate-question", function (pEvent, pParam) {
                    if (pParam === context) {
                        TweenLite.set(_elemContent, {opacity: 0});
                        TweenLite.set(_elemContent, {marginTop: _marginLimit});
                        TweenLite.to(_elemContent, .2, {delay: .2, ease: Sine.easeOut, overwrite: 1, opacity: 1});
                        TweenLite.to(_elemContent, .25, {delay: .2, overwrite: 1, ease: Circ.easeOut, marginTop: 0});
                        $scope.$emit("question-active", questionId);
                    }
                });

                _alignWatch = $scope.$on("questions-align", function (pEvent, pParam) {
                    //_topPos = _cleanElem.getBoundingClientRect().top;
                    _topPos = context.offsetTop;
                    _winH = pParam.winH;
                    context.style.height = (pParam.questionH) + "px";
                    _elemHeight = pParam.questionH;
                    //console.log("QI:: _elemHeight",_elemHeight);
                });


                _scrollWatch = $scope.$on('scroll-update', function (pEvent, pParam) {
                    //console.log("scroll-update::",pParam);
                    var howMuchIn = _winH - _topPos + pParam;
                    var nextVal = howMuchIn / _elemHeight;
                    if (nextVal !== $scope.percIn) {
                        $scope.percIn = nextVal;

                        if ($scope.percIn > .6 && $scope.percIn <= 1.25) {
                            //-
                            var cPerc = $scope.percIn - .6;
                            var percDiff = (cPerc / .4);
                            var mTopTarget = (1 - percDiff) * _marginLimit;
                            //-
                            if (mTopTarget < 0) mTopTarget = 0;
                            if (percDiff > 1) percDiff = 1;
                            //--
                            TweenLite.to(_elemContent, .1, {
                                overwrite: 0,
                                ease: Linear.easeNone,
                                marginTop: mTopTarget
                            });
                            TweenLite.to(_elemContent, .1, {ease: Sine.easeOut, overwrite: 0, opacity: percDiff});

                            if (!_localActive) {
                                _localActive = true;
                                $scope.$emit("question-active", questionId);
                                $scope.$evalAsync();
                            }

                        } else {
                            _localActive = false;
                        }

                        if ($scope.percIn > 1) {
                            $scope.percOut = $scope.percIn - 1;
                            if ($scope.percOut > 1) $scope.percOut = 1;

                            if ($scope.percOut && ($scope.percOut > .5 && $scope.percOut <= .8)) {
                                //-
                                var cPerc = $scope.percOut - .5;
                                var percDiff = (cPerc / .3);
                                var mTopTarget = (percDiff) * -_marginLimit;
                                //-
                                TweenLite.to(_elemContent, .1, {
                                    overwrite: 0,
                                    ease: Linear.easeNone,
                                    marginTop: mTopTarget
                                });
                                TweenLite.to(_elemContent, .1, {
                                    ease: Sine.easeOut,
                                    overwrite: 0,
                                    opacity: 1 - percDiff
                                });
                            }
                        }
                    }
                });

                ///----
                //-----
                $scope.parseBold = textService.parseBold;
                $scope.parseNormal = textService.parseNormal;
            }
        };
    });
})();