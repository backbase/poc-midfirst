# Step 1. Package the project

Make sure the project is packaged, and all webapps and configuration is compiled. 
eg. 

"mvn clean install -Pclean-database"

This is done with a  in the root of the project.

# Step 2. Package the Docker image

To package the docker image, use

"mvn clean package". 

This produces a Docker image that can be run.

# Step 3. Run the Docker image

Assuming you have docker installed, run the following.

"docker run -P backbase/showcase-default"

The -P will bridge the TCP connections from the virtual machine, to those of the local host. 
To find which port the virtual machine is bound to, use

"docker ps -a"

This shows the ports that forward to the virtual machine ( see PORTS ).
eg. 0.0.0.0:32779->8080/tcp, 0.0.0.0:32778->8778/tcp, 0.0.0.0:32777->9779/tcp