package com.backbase.forms.services.proxy;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of {@link ProxyService}. It stores the Cookie Store in
 * the user session, and uses it for all calls to the server. A different
 * instance of this service should be created for each proxied server.
 * 
 * @author Walter Werner
 *
 */
public class DefaultProxyService implements ProxyService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DefaultProxyService.class);

	private String sessionAttributeName;

	private List<HttpRequestPreProcessor> preProcessors;

	@Override
	public HttpResponse execute(HttpServletRequest originalRequest,
			HttpUriRequest httpRequest) throws IOException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Excuting {}:{}", httpRequest.getMethod(),
					httpRequest.getURI());
		}
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpSession session = originalRequest.getSession();
		CookieStore cookieStore = (CookieStore) session
				.getAttribute(sessionAttributeName);
		if (cookieStore != null) {
			httpClient.setCookieStore(cookieStore);
		}
		HttpResponse httpResponse = httpClient.execute(httpRequest);
		session.setAttribute(sessionAttributeName, httpClient.getCookieStore());
		return httpResponse;
	}

	/**
	 * The name of the session attribute that is used to store the cookie store.
	 * 
	 * @return the attribute name
	 */
	public String getSessionAttributeName() {
		return sessionAttributeName;
	}

	/**
	 * Sets the name of the session attribute to be used to store the cookie
	 * store.
	 * 
	 * @param sessionAttributeName
	 *            the attribute name
	 */
	public void setSessionAttributeName(String sessionAttributeName) {
		this.sessionAttributeName = sessionAttributeName;
	}

}
