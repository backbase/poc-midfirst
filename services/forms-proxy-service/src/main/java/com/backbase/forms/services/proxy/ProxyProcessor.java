package com.backbase.forms.services.proxy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.HttpClientUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This processor is responsible for executing a proxied request.
 *
 * @author Walter Werner
 */
// TODO: This is a but messy. Needs some refactoring, together with
// DefaultProxyService.
public class ProxyProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ProxyProcessor.class);

    @Autowired
    private ProxyService proxyService;

    private String proxyBasePath;

    @Autowired
    private HttpRequestFactory requestFactory;

    private List<String> copyResponseHeaders = Collections.emptyList();

    private List<String> copyRequestHeaders = Collections.emptyList();

    @Override
    public void process(Exchange exchange) throws Exception {
        Message message = exchange.getIn();
        HttpServletRequest request = (HttpServletRequest) message
                .getHeader(Exchange.HTTP_SERVLET_REQUEST);
        HttpResponse response = proxyService.execute(request,
                // Cannot use the input stream of the request as it is empty. The one
                // from the body contains the actual content.
                createRequest(request, (InputStream) message.getBody()));
        try {
            setResponseHeaders(message, response);
            setResponseBody(message, response);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
    }

    private HttpUriRequest createRequest(HttpServletRequest request,
                                         InputStream in) throws IOException {
        String path = getProxiedPathAndQueryString(request);
        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("Proxing {} to {}", request.getRequestURI(), path);
        }
        HttpUriRequest httpRequest = requestFactory.createRequest(path,
                request.getMethod(), request.getContentType(),
                request.getContentLength(), in);
        copyRequestHeaders(request, httpRequest);
        return httpRequest;
    }

    private void copyRequestHeaders(HttpServletRequest request,
                                    HttpUriRequest httpRequest) {
        Enumeration<?> headers = request.getHeaderNames();
        while (headers.hasMoreElements()) {
            String headerName = (String) headers.nextElement();
            if(copyRequestHeaders.contains(headerName)) {
                setHeader(httpRequest, headerName,
                        request.getHeader(headerName));
            } else if(LOGGER.isDebugEnabled()) {
                LOGGER.debug("Response header {} with value {} not copied.",
                        headerName, request.getHeader(headerName));
            }
        }
    }

    public void setHeader(HttpUriRequest httpRequest, String name, String value) {
        LOGGER.debug("Coping request header {} with value {}.", name, value);
        httpRequest.setHeader(name, value);
    }

    private String getProxiedPathAndQueryString(HttpServletRequest request) {
        return request.getPathInfo().replaceFirst(proxyBasePath, "")
                + getQueryString(request);
    }

    private String getQueryString(HttpServletRequest request) {
        String queryString = request.getQueryString();
        return StringUtils.isEmpty(queryString) ? StringUtils.EMPTY : "?"
                + queryString;
    }

    private void setResponseBody(Message message, HttpResponse response)
            throws IllegalStateException, IOException {
        // Need to copy it as we are going to close the connection.
        // TODO: We should copy through a file for big content
        ByteArrayOutputStream copy = new ByteArrayOutputStream();
        IOUtils.copy(response.getEntity().getContent(), copy);
        message.setBody(new ByteArrayInputStream(copy.toByteArray()));
    }

    private void setResponseHeaders(Message message, HttpResponse response) {
        // Copying status code..
        message.setHeader(Exchange.HTTP_RESPONSE_CODE, response.getStatusLine()
                .getStatusCode());
        HttpEntity entity = response.getEntity();
        setResponseHeader(message, Exchange.CONTENT_TYPE,
                entity.getContentType());
        setResponseHeader(message, Exchange.CONTENT_LENGTH,
                entity.getContentLength());
        setResponseHeader(message, Exchange.CONTENT_ENCODING,
                entity.getContentEncoding());
        Header[] headers = response.getAllHeaders();
        for (Header header : headers) {
            if(shouldCopyHeader(header)) {
                setResponseHeader(message, header.getName(), header.getValue());
            } else if(LOGGER.isDebugEnabled()) {
                LOGGER.debug("Response header {} with value {} not copied.",
                        header.getName(), header.getValue());
            }
        }
    }

    private void setResponseHeader(Message message, String name, Object value) {
        LOGGER.debug("Setting response header {} with value {}.", name, value);
        message.setHeader(name, value);
    }

    private boolean shouldCopyHeader(Header header) {
        for (String headerName : copyResponseHeaders) {
            if(header.getName().equalsIgnoreCase(headerName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * The base path of the proxy url. Usually, this is the same value as the
     * value set as URL for the Servlet route.
     *
     * @return the proxy base path
     */
    public String getProxyBasePath() {
        return proxyBasePath;
    }

    /**
     * Sets the base path of the proxy url. Usually, this is the same value as
     * the value set as URL for the Servlet route.
     *
     * @param proxyBasePath the proxy base path
     */
    public void setProxyBasePath(String proxyBasePath) {
        this.proxyBasePath = proxyBasePath;
    }

    /**
     * List containing the name of the headers to be copied to from the original
     * request to the request to the actual server.
     *
     * @return list containing the header names
     */
    public List<String> getCopyRequestHeaders() {
        return Collections.unmodifiableList(copyRequestHeaders);
    }

    /**
     * Sets the list with the name of the headers to be copied to from the
     * original request to the request to the actual server.
     *
     * @param copyRequestHeaders list with headers names
     */
    public void setCopyRequestHeaders(List<String> copyRequestHeaders) {
        this.copyRequestHeaders = new ArrayList<String>(copyRequestHeaders);
    }

    /**
     * List containing the name of the headers to be copied to from the server
     * response to the response to the client.
     *
     * @return list containing the headers names
     */
    public List<String> getCopyResponseHeaders() {
        return Collections.unmodifiableList(copyResponseHeaders);
    }

    /**
     * Sets the list with the name of the headers to be copied to from the
     * server response to the response to the client.
     *
     * @param copyResponseHeaders list with headers names
     */
    public void setCopyResponseHeaders(List<String> copyResponseHeaders) {
        this.copyResponseHeaders = new ArrayList<String>(copyResponseHeaders);
    }

}
