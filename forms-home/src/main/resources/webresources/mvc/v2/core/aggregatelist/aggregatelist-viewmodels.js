/**
 * Contains all aggregate dao viewmodel definitions.
 *
 * @namespace blueriq.aggregatedao
 */
(function (blueriq, bqApp, ko, moment) {
    'use strict';

    if (!blueriq) {
        throw new Error('blueriq is mandetory');
    }

    blueriq.aggregatedao = {};

    /**
     * Registers the factory method for aggregate list ViewModels.
     */
    blueriq.aggregateModelFactory = function (model, context) {
        if (model && model.type === 'container') {
            if (model.contentStyle === 'al_header') {
                return new blueriq.aggregatedao.AggregateListHeaderModel(model, context);
            } else if (model.contentStyle === 'al_headercell') {
                return new blueriq.aggregatedao.AggregateListTableHeaderModel(model, context);
            } else if (model.contentStyle === 'al_footer') {
                return new blueriq.aggregatedao.AggregateListFooterModel(model, context);
            } else if (model.contentStyle === 'al_search_text') {
                return new blueriq.aggregatedao.StringSearchModel(model, context);
            } else if (model.contentStyle === 'al_search_boolean') {
                return new blueriq.aggregatedao.BooleanSearchModel(model, context);
            } else if (model.contentStyle === 'al_search_integer') {
                return new blueriq.aggregatedao.IntegerSearchModel(model, context);
            } else if ($.inArray(model.contentStyle, ['al_search_number', 'al_search_currency', 'al_search_percentage']) !== -1) {
                return new blueriq.aggregatedao.NumericSearchModel(model, context);
            } else if (model.contentStyle === 'al_search_date') {
                return new blueriq.aggregatedao.DateSearchModel(model, context);
            } else if (model.contentStyle === 'al_search_datetime') {
                return new blueriq.aggregatedao.DateTimeSearchModel(model, context);
            } else if (model.contentStyle === 'al_search_domain') {
                return new blueriq.aggregatedao.DomainSearchModel(model, context);
            }
        }
    };

    if (bqApp) {
        bqApp.modelFactory.register(blueriq.aggregateModelFactory);
    }

    ko.extenders.timeNumberFormat = function (target, unit) {
        var regExp;
        target.subscribe(function (newValue) {
            if (unit === 'H') {
                regExp = new RegExp('^([ 01][0-9]|2[0-3])?$');
            } else {
                regExp = new RegExp('^([0-5][0-9])?$');
            }
            if (regExp.test(newValue)) {
                target(newValue);
            } else {
                target('00');
            }
        });
        return target;
    };

    /**
     * The ViewModel for the aggregate list header.
     *
     * The header contains the 'clear all search filters' button. The corresponding KO template
     * is aggregatelist/header.html. A custom KO template is needed in order to hide the search field.
     *
     * @param {Object} model the JSON model sent from the server
     * @param {Object} context the application context
     */
    blueriq.aggregatedao.AggregateListHeaderModel = function AggregateListHeaderModel(model, context) {
        var self = this;
        blueriq.models.BaseModel.call(self, model, context);
        this.contentStyle = model.contentStyle;

        // messageBus communication channels
        var incoming = model.properties.containername + '_searchChanged';
        var outgoing = model.properties.containername + '_searchCleared';
        var requestState = model.properties.containername + '_requestState';

        // The search object, aggregates search filters from all columns
        this.searchFields = {};

        // the hidden search field
        var searchFieldModel = context.session.getModel(model.children[0]);
        this.searchFieldViewModel = context.modelFactory.createViewModel(searchFieldModel, context);

        var updateSearchFields = function (jsonString) {
            if (jsonString) {
                var parsed = JSON.parse(jsonString);
                $(parsed).each(function (index, element) {
                    self.searchFields[element.index] = element.value;
                });
            } else {
                context.messageBus.notify(outgoing);
                self.searchFields = {};
            }
        };

        updateSearchFields(this.searchFieldViewModel.value());
        var updateSearchFieldsSubscription = this.searchFieldViewModel.value.subscribe(updateSearchFields);

        // message when results have a hard limit applied
        var showLimitMessage = function (model) {
            if (model.children.length === 2) {
                var limitContainerModel = context.session.getModel(model.children[1]);
                var limitMessageModel = context.session.getModel(limitContainerModel.children[0]);
                var showAllModel = context.session.getModel(limitContainerModel.children[1]);

                self.showAllKey = showAllModel.key;
                self.limitMessageKey = limitMessageModel.key;

                self.hasLimit(true);
                self.limitMessage(limitMessageModel.text);
                self.showAll(showAllModel.caption);
                return showAllModel.key;
            } else {
                self.hasLimit(false);
                self.limitMessage(null);
                self.showAll(null);
            }

            return null;
        };

        this.hasLimit = ko.observable(false);
        this.limitMessage = ko.observable();
        this.showAll = ko.observable();

        showLimitMessage(model);
        var limitChangeSubscription = context.session.subscribe(function (type, key, changeModel) {
            if (type === 'update' && key === model.key) {
                showLimitMessage(changeModel);
            } else if (type === 'update' && key === self.limitMessageKey) {
                self.limitMessage(changeModel.text);
            } else if (type === 'update' && key === self.showAllKey) {
                self.showAll(changeModel.caption);
            }
        });

        this.handleShowAll = function () {
            if (self.showAllKey) {
                self.submit(self.showAllKey);
            }
        };

        // aggregate search filters
        var searchSubscription = context.messageBus.subscribe(incoming, function (event, data) {
            if (data.isDefault) {
                delete self.searchFields[data.columnIndex];
            } else {
                self.searchFields[data.columnIndex] = data.value;
            }

            // conversion to format expected by the server
            var msg = [];
            for (var index in self.searchFields) {
                msg.push({
                    index: index,
                    value: self.searchFields[index]
                });
            }

            var json = JSON.stringify(msg);
            self.searchFieldViewModel.value(json);
        });

        // handle requests from columns for their search filter state
        var requestStateSubscription = context.messageBus.subscribe(requestState, function (event, data) {
            if (self.searchFields[data.columnIndex]) {
                data.callback(self.searchFields[data.columnIndex]);
            }
        });

        this.dispose = function () {
            if (updateSearchFieldsSubscription) {
                updateSearchFieldsSubscription.dispose();
            }
            if (limitChangeSubscription) {
                limitChangeSubscription.dispose();
            }
            if (searchSubscription) {
                searchSubscription.dispose();
            }
            if (requestStateSubscription) {
                requestStateSubscription.dispose();
            }
        };
    };

    /**
     * The ViewModel for a header cell in the aggregate table.
     *
     * @param {Object} model the JSON model sent from the server
     * @param {Object} context the application context
     */
    blueriq.aggregatedao.AggregateListTableHeaderModel = function AggregateListTableHeaderModel(model, context) {
        var self = this;
        blueriq.models.BaseModel.call(self, model, context);
        this.contentStyle = model.contentStyle;

        /**
         * Checks if column is sorted ascending.
         * @private
         */
        function isSortedAscending(model) {
            return model && jQuery.inArray('ascending', model.styles) > 0;
        }

        /**
         * Checks if the column is sorted descending.
         * @private
         */
        function isSortedDescending(model) {
            return model && jQuery.inArray('descending', model.styles) > 0;
        }

        var contentViewModel = null;
        var sortModel = null;
        this.popoverVisible = false;
        this.searchViewModel = null;
        var sortChangeSubscription = null;
        var searchVisibleSubscription = null;

        function initChild(childModel) {
            if (childModel.type === 'textitem') {
                contentViewModel = context.modelFactory.createViewModel(childModel, context);
            } else if (childModel.type === 'button') {
                sortModel = childModel;
                sortChangeSubscription = context.session.subscribe(function (type, key, changedModel) {
                    if (type === 'update' && key === sortModel.key) {
                        self.sortedAscending(isSortedAscending(changedModel));
                        self.sortedDescending(isSortedDescending(changedModel));
                    }
                });
            } else if (childModel.type === 'container') {
                self.searchViewModel = context.modelFactory.createViewModel(childModel, context);
                searchVisibleSubscription = self.searchViewModel.visible.subscribe(function (value) {
                    self.popoverVisible = value;
                });
            } else {
                throw new Error('invalid model, unexpected child of type ' + childModel.type);
            }
        }

        if (model.children.length > 3) {
            throw new Error('invalid mode, expected at most 3 children');
        }

        for (var i = 0; i < model.children.length; i++) {
            initChild(context.session.getModel(model.children[i]));
        }

        this.content = ko.observable(contentViewModel);
        this.sortedAscending = ko.observable(isSortedAscending(sortModel));
        this.sortedDescending = ko.observable(isSortedDescending(sortModel));

        this.handleClick = function () {
            if (sortModel) {
                self.submit(sortModel.key);
            }

            // this function must return true, otherwise checkbox KO bindings in
            // search popovers won't work
            return true;
        };

        this.dispose = function () {
            if (sortChangeSubscription) {
                sortChangeSubscription.dispose();
            }

            if (searchVisibleSubscription) {
                searchVisibleSubscription.dispose();
            }
        };
    };

    /**
     * The ViewModel for the aggregate list footer.
     *
     * The footer contains navigation buttons and feedback text regarding the currently displayed records.
     *
     * @param {Object} model the JSON model sent from the server
     * @param {Object} context the application context
     */
    blueriq.aggregatedao.AggregateListFooterModel = function AggregateListFooterModel(model, context) {
        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);

        this.contentStyle = model.contentStyle;

        // parse children
        var children = this.children();
        if (children.length < 6) {
            throw new Error('invalid model, expected at least 6 children');
        } else if (children.length > 7) {
            context.log.log('too many children, expected at most 7 children but received ' + children.length);
        }

        this.first = children[0];
        if (!(this.first instanceof blueriq.models.ButtonModel)) {
            throw new Error('invalid model for "first page" button');
        }

        this.previous = children[1];
        if (!(this.previous instanceof blueriq.models.ButtonModel)) {
            throw new Error('invalid model for "previous page" button');
        }

        this.selectorContainer = children[2];
        if (!(this.selectorContainer instanceof blueriq.models.ContainerModel)) {
            throw new Error('invalid model for paging selector container');
        }

        // the container is expected to have exactly 3 children:
        // - the text before the field
        // - the field
        // - the text after the field
        var selectorChildren = this.selectorContainer.children();
        if (selectorChildren.length < 3) {
            throw new Error('invalid paging container model, expected 3 children but received ' + selectorChildren.length);
        } else if (selectorChildren.length > 3) {
            context.log.log('invalid paging container model, expected 3 children but received ' + selectorChildren.length);
        }

        this.selectorPrefix = selectorChildren[0];
        if (!(this.selectorPrefix instanceof blueriq.models.AssetModel)) {
            throw new Error('invalid model for selector prefix');
        }

        this.selectorField = selectorChildren[1];
        if (!(this.selectorField instanceof blueriq.models.FieldModel)) {
            throw new Error('invalid model for selector field');
        }

        this.selectorSuffix = selectorChildren[2];
        if (!(this.selectorSuffix instanceof blueriq.models.AssetModel)) {
            throw new Error('invalid model for selector suffix');
        }

        this.next = children[3];
        if (!(this.next instanceof blueriq.models.ButtonModel)) {
            throw new Error('invalid model for "next page" button');
        }

        this.last = children[4];
        if (!(this.last instanceof blueriq.models.ButtonModel)) {
            throw new Error('invalid model for "last page" button');
        }

        this.clear = children[5];
        if (!(this.clear instanceof blueriq.models.ButtonModel)) {
            context.log.error('invalid model for "clear" button: ', this.clear);
            throw new Error('invalid model for "clear" button');
        }

        if (children.length > 6) {
            this.feedback = children[6];
            if (!(this.feedback instanceof blueriq.models.AssetModel)) {
                throw new Error('invalid model for paging feedback');
            }
        } else {
            this.feedback = false;
        }
    };

    blueriq.aggregatedao.BaseSearchModel = function BaseSearchModel(model, context) {
        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        var textElement = model.properties.title;
        if (textElement) {
            this.title = textElement;
        } else {
            this.title = 'Search';
        }

        this.simple = ko.observable(false);
        var simpleFlag = model.properties.simple;
        if (simpleFlag && simpleFlag === true) {
            this.simple(true);
        }

        this.id = ko.observable(model.key);
        this.visible = ko.observable(false);
        this.showAllText = model.properties.showall;
        this.showUnknownText = model.properties.showunknown;
        this.iconActive = ko.observable(false);

        var notify = 0;
        var subscriptions = [];
        var incoming = model.properties.containername + '_searchCleared';
        var outgoing = model.properties.containername + '_searchChanged';
        var request = model.properties.containername + '_requestState';

        this.requestState = function (callback) {
            context.messageBus.notify(request, {
                columnIndex: model.properties.index,
                callback: callback
            });

            // this points to the actual subclass
            self.updateIcon();
        };

        this.subscribe = function (observable) {
            subscriptions.push(observable.subscribe(function (/*value*/) {
                if (notify === 0) {
                    self.sendNotification();
                }
            }));
        };

        this.preventNotification = function (func) {
            notify++;
            try {
                func();
            }
            finally {
                notify--;
            }
        };

        this.withTransaction = function (func) {
            self.preventNotification(func);
            self.trySendNotification();
        };

        this.sendNotification = function () {
            var msg = self.createMessage();
            context.messageBus.notify(outgoing, msg);
        };

        this.trySendNotification = function () {
            if (notify === 0) {
                self.sendNotification();
            }
        };

        var searchClearedSubscription = context.messageBus.subscribe(incoming, function () {
            self.preventNotification(self.handleClear);
            self.handleMouseOut();
        });

        this.onBeforeShow = function () {
            self.visible(true);
        };

        this.onShow = function () {
            // method overridden in subclasses to set initial focus
        };

        this.onInit = function (element) {
            var popover = element.parent().find('.popover');

            // initial bring to front, to make sure a newly opened popover is always on top
            bringToFront(popover);

            // stop event propagation to table header on click and manage z-index
            element.parent().on('click', function (evt) {
                evt.stopPropagation();
                bringToFront(popover);
            });

            // use event capture to dismiss the popover with a click outside it - not supported in IE8
            if (document.addEventListener && document.removeEventListener) {
                var dismissListener = function (evt) {
                    var elems = $(evt.target).parents().add(evt.target);
                    // .bq-table-header-sortable includes .popover
                    if (!elems.hasClass('search-filter-trigger') && !elems.hasClass('bq-table-header-sortable')) {
                        element.popover('hide');
                        self.onHide();
                        document.removeEventListener('click', dismissListener, true);
                    }
                };

                document.addEventListener('click', dismissListener, true);
            }
        };

        this.onHide = function (evt) {
            if (evt) {
                evt.stopPropagation();
            }
            self.visible(false);
            self.handleMouseOut();
        };

        this.handleMouseOver = function () {
            self.iconActive(true);
        };

        this.handleMouseOut = function () {
            self.iconActive(self.visible() || self.isActive());
        };

        this.updateIcon = function () {
            self.iconActive(self.visible() || self.isActive());
        };

        this.dispose = function () {
            if (searchClearedSubscription) {
                searchClearedSubscription.dispose();
            }
            $(subscriptions).each(function () {
                this.dispose();
            });
        };
    };

    blueriq.aggregatedao.StringSearchModel = function StringSearchModel(model, context) {
        var self = this;
        blueriq.aggregatedao.BaseSearchModel.call(self, model, context);

        this.contentStyle = model.contentStyle;
        this.showAll = ko.observable(true);
        this.text = ko.observable('');
        this.textFocused = ko.observable(false);
        this.showUnknown = ko.observable(true);

        // this flag is used in order to be able to handle the click on the clear button before the blur event on text field
        this.mouseOverClearButton = false;

        this.createMessage = function () {

            var value;
            if (!!self.simple()) {
                value = {
                    type: 'string',
                    text: self.text()
                };
            } else {
                value = {
                    type: 'string',
                    showAll: self.showAll(),
                    text: self.text(),
                    showUnknown: self.showUnknown()
                };
            }

            return {
                columnIndex: model.properties.index,
                isDefault: !self.isActive(),
                value: value
            };
        };

        var clearOnTextFilled = function () {
            self.withTransaction(function () {
                self.showAll(!self.text());
            });
        };

        var showAllSubscription = this.showAll.subscribe(function (checked) {
            self.withTransaction(function () {
                if (checked) {
                    self.text('');
                }
            });
        });

        var textFocusedSubscription = this.textFocused.subscribe(function () {
            if (!self.textFocused() && !self.mouseOverClearButton) { // if focus is lost and clear button was not clicked - workaraound for IE
                clearOnTextFilled();
            }
        });

        this.subscribe(this.showUnknown);

        this.handleKeyUp = function (data, evt) {
            if (evt.keyCode === 13) { // if ENTER key
                clearOnTextFilled();
            }
        };

        this.onShow = function () {
            self.textFocused(true);
        };

        this.handleClear = function () {
            self.showAll(true);
            self.text('');
            self.showUnknown(true);
        };

        this.handleClearButton = function () {
            self.withTransaction(function () {
                self.text('');
                self.showAll(true);
            });
        };

        this.isActive = function () {
            return !!self.text() || !self.showUnknown() || !self.showAll();
        };

        // workaround for IE
        this.clearButtonHovered = function () {
            self.mouseOverClearButton = true;
        };
        this.clearButtonNotHovered = function () {
            self.mouseOverClearButton = false;
        };

        //var superDispose = this.dispose;
        this.dispose = function () {
            if (showAllSubscription) {
                showAllSubscription.dispose();
            }
            if (textFocusedSubscription) {
                textFocusedSubscription.dispose();
            }
        };

        // this must be the last call
        this.requestState(function (state) {
            if (state) {
                self.preventNotification(function () {
                    self.showAll(state.showAll);
                    self.text(state.text);
                    self.showUnknown(state.showUnknown);
                });
            }
        });
    };

    blueriq.aggregatedao.BooleanSearchModel = function BooleanSearchModel(model, context) {
        var self = this;
        blueriq.aggregatedao.BaseSearchModel.call(self, model, context);

        this.contentStyle = model.contentStyle;

        this.showTrue = ko.observable(true);
        this.showFalse = ko.observable(true);
        this.showUnknown = ko.observable(true);
        this.showTrueText = model.properties.showtrue;
        this.showFalseText = model.properties.showfalse;

        this.createMessage = function () {

            var value;
            if (!!self.simple()) {
                value = {
                    type: 'boolean',
                    showTrue: self.showTrue(),
                    showFalse: self.showFalse()
                };
            } else {
                value = {
                    type: 'boolean',
                    showTrue: self.showTrue(),
                    showFalse: self.showFalse(),
                    showUnknown: self.showUnknown()
                };
            }

            return {
                columnIndex: model.properties.index,
                isDefault: !self.isActive(),
                value: value
            };
        };

        this.subscribe(this.showTrue);
        this.subscribe(this.showFalse);
        this.subscribe(this.showUnknown);

        this.handleClear = function () {
            self.withTransaction(function () {
                self.showTrue(true);
                self.showFalse(true);
                self.showUnknown(true);
            });
        };

        this.isActive = function () {
            return !self.showTrue() || !self.showFalse() || !self.showUnknown();
        };

        this.requestState(function (state) {
            if (state) {
                self.preventNotification(function () {
                    self.showTrue(state.showTrue);
                    self.showFalse(state.showFalse);
                    self.showUnknown(state.showUnknown);
                });
            }
        });

    };

    blueriq.aggregatedao.NumericSearchModel = function NumericSearchModel(model, context) {
        var self = this;
        blueriq.aggregatedao.BaseSearchModel.call(self, model, context);
        this.contentStyle = model.contentStyle;

        this.showAll = ko.observable(true);
        this.eq = ko.observable('');
        this.eqFocused = ko.observable(false);
        this.lte = ko.observable('');
        this.lteFocused = ko.observable(false);
        this.gte = ko.observable('');
        this.gteFocused = ko.observable(false);
        this.showUnknown = ko.observable(true);

        this.subscribe(this.showUnknown);

        this.createMessage = function () {

            var value;
            if (!!self.simple()) {
                value = {
                    type: 'numeric',
                    eq: self.eq(),
                    lte: self.lte(),
                    gte: self.gte()
                };
            } else {
                value = {
                    type: 'numeric',
                    showAll: self.showAll(),
                    eq: self.eq(),
                    lte: self.lte(),
                    gte: self.gte(),
                    showUnknown: self.showUnknown()
                };
            }

            return {
                columnIndex: model.properties.index,
                isDefault: !self.isActive(),
                value: value
            };
        };

        var clearEquals = function () {
            self.withTransaction(function () {
                if (self.gte() !== '' || self.lte() !== '') {
                    self.eq('');
                }
                self.updateShowAll();
            });
        };

        var clearBetween = function () {
            self.withTransaction(function () {
                if (self.eq() !== '') {
                    self.lte('');
                    self.gte('');
                }
                self.updateShowAll();
            });
        };

        this.handleGteKeyUp = function (data, evt) {
            if (evt.keyCode === 13) { // if ENTER key
                clearEquals();
            }
        };

        this.handleLteKeyUp = function (data, evt) {
            if (evt.keyCode === 13) { // if ENTER key
                clearEquals();
            }
        };

        this.handleEqKeyUp = function (data, evt) {
            if (evt.keyCode === 13) { // if ENTER key
                clearBetween();
            }
        };

        var showAllSubscription = this.showAll.subscribe(function (checked) {
            self.withTransaction(function () {
                if (checked) {
                    self.eq('');
                    self.lte('');
                    self.gte('');
                }
            });
        });

        var gteFocusedSubscribe = this.gteFocused.subscribe(function () {
            if (!self.gteFocused()) {
                clearEquals();
            }
        });

        var lteFocusedSubscribe = this.lteFocused.subscribe(function () {
            if (!self.lteFocused()) {
                clearEquals();
            }
        });

        var eqFocusedSubscribe = this.eqFocused.subscribe(function () {
            if (!self.eqFocused()) {
                clearBetween();
            }
        });

        this.updateShowAll = function () {
            if (self.lte() !== '' || self.gte() !== '' || self.eq() !== '') {
                self.showAll(false);
            } else {
                self.showAll(true);
            }
        };

        this.handleClear = function () {
            self.showAll(true);
            self.eq('');
            self.lte('');
            self.gte('');
            self.showUnknown(true);
        };

        this.onShow = function () {
            // set initial focus
            if (self.gte()) {
                self.gteFocused(true);
            } else if (self.lte()) {
                self.lteFocused(true);
            } else if (self.eq()) {
                self.eqFocused(true);
            } else {
                self.gteFocused(true);
            }
        };

        this.isActive = function () {
            return self.lte() !== '' || self.gte() !== '' || self.eq() !== '' || !self.showAll() || !self.showUnknown();
        };

        var superDispose = this.dispose;
        this.dispose = function () {
            superDispose.call(self);

            if (eqFocusedSubscribe) {
                eqFocusedSubscribe.dispose();
            }

            if (lteFocusedSubscribe) {
                lteFocusedSubscribe.dispose();
            }

            if (gteFocusedSubscribe) {
                gteFocusedSubscribe.dispose();
            }

            if (showAllSubscription) {
                showAllSubscription.dispose();
            }
        };

        this.requestState(function (state) {
            if (state) {
                self.preventNotification(function () {
                    self.showAll(state.showAll);
                    self.lte(state.lte);
                    self.gte(state.gte);
                    self.eq(state.eq);
                    self.showUnknown(state.showUnknown);
                });
            }
        });
    };

    blueriq.aggregatedao.IntegerSearchModel = function IntegerSearchModel(model, context) {
        var self = this;
        blueriq.aggregatedao.NumericSearchModel.call(self, model, context);
        this.contentStyle = model.contentStyle;

        this.createMessage = function () {

            var value;
            if (!!self.simple()) {
                value = {
                    type: 'integer',
                    eq: self.eq(),
                    lte: self.lte(),
                    gte: self.gte()
                };
            } else {
                value = {
                    type: 'integer',
                    showAll: self.showAll(),
                    eq: self.eq(),
                    lte: self.lte(),
                    gte: self.gte(),
                    showUnknown: self.showUnknown()
                };
            }

            return {
                columnIndex: model.properties.index,
                isDefault: !self.isActive(),
                value: value
            };
        };
    };

    blueriq.aggregatedao.DateSearchModel = function DateSearchModel(model, context) {
        var self = this;
        blueriq.aggregatedao.BaseSearchModel.call(self, model, context);

        this.contentStyle = model.contentStyle;

        this.createMessage = function () {

            var value;
            if (!!self.simple()) {
                value = {
                    type: 'date',
                    to: self.toValue(),
                    from: self.fromValue(),
                    on: self.onValue()
                };
            } else {
                value = {
                    type: 'date',
                    showAll: self.showAll(),
                    to: self.toValue(),
                    from: self.fromValue(),
                    on: self.onValue(),
                    showUnknown: self.showUnknown()
                };
            }

            return {
                columnIndex: model.properties.index,
                isDefault: !self.isActive(),
                value: value
            };
        };

        this.todayText = model.properties.today;

        this.toChecked = ko.observable(false);
        this.toVisible = ko.observable(false);
        this.toValue = ko.observable(null);
        var toValueSubscription = this.toValue.subscribe(function (value) {
            self.withTransaction(function () {
                self.toChecked(!!value);
                self.updateShowAll();
            });
        });
        var toCheckedSubscription = this.toChecked.subscribe(function (checked) {
            self.withTransaction(function () {
                if (!checked) {
                    self.toValue(null);
                } else if (self.toValue() === null) {
                    self.toChecked(false);
                } else {
                    self.onChecked(false);
                    self.showAll(false);
                }
            });
            return true;
        });

        this.fromChecked = ko.observable(false);
        this.fromVisible = ko.observable(true);
        this.fromValue = ko.observable(null);
        var fromValueSubscription = this.fromValue.subscribe(function (value) {
            self.withTransaction(function () {
                self.fromChecked(!!value);
                self.updateShowAll();
            });
        });
        var fromCheckedSubscription = this.fromChecked.subscribe(function (checked) {
            self.withTransaction(function () {
                if (!checked) {
                    self.fromValue(null);
                } else if (self.fromValue() === null) {
                    self.fromChecked(false);
                } else {
                    self.onChecked(false);
                    self.showAll(false);
                }
            });
            return true;
        });

        this.onChecked = ko.observable(false);
        this.onVisible = ko.observable(false);
        this.onValue = ko.observable(null);
        var onValueSubscription = this.onValue.subscribe(function (value) {
            self.withTransaction(function () {
                self.onChecked(!!value);
                self.updateShowAll();
            });
        });
        var onCheckedSubscription = this.onChecked.subscribe(function (checked) {
            self.withTransaction(function () {
                if (!checked) {
                    self.onValue(null);
                } else if (self.onValue() === null) {
                    self.onChecked(false);
                } else {
                    self.toChecked(false);
                    self.fromChecked(false);
                    self.showAll(false);
                }
            });
            return true;
        });

        this.showUnknown = ko.observable(true);
        this.subscribe(this.showUnknown);

        this.showAll = ko.observable(true);
        var showAllSubscription = this.showAll.subscribe(function (checked) {
            self.withTransaction(function () {
                if (checked) {
                    self.toChecked(false);
                    self.fromChecked(false);
                    self.onChecked(false);
                    self.onValue(null);
                    self.toValue(null);
                    self.fromValue(null);
                }
            });
        });

        this.updateShowAll = function () {
            if (self.fromValue() !== null || self.toValue() !== null || self.onValue() !== null) {
                self.showAll(false);
            } else {
                self.showAll(true);
            }
        };

        this.handleToClick = function () {
            if (self.toVisible()) {
                self.toChecked(!self.toChecked());
            }

            self.toVisible(true);
            self.fromVisible(false);
            self.onVisible(false);

            return true;
        };

        this.handleFromClick = function () {
            if (self.fromVisible()) {
                self.fromChecked(!self.fromChecked());
            }

            self.toVisible(false);
            self.fromVisible(true);
            self.onVisible(false);

            return true;
        };

        this.handleOnClick = function () {
            if (self.onVisible()) {
                self.onChecked(!self.onChecked());
            }

            self.toVisible(false);
            self.fromVisible(false);
            self.onVisible(true);

            return true;
        };

        this.handleClear = function () {
            self.showAll(true);
            self.toVisible(false);
            self.fromVisible(true);
            self.onVisible(false);

            self.toValue(null);
            self.fromValue(null);
            self.onValue(null);
            self.showUnknown(true);
        };

        this.handleToday = function () {
            var now = new Date();
            var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            if (self.toVisible()) {
                if (!self.toValue() || self.toValue().getTime() !== today.getTime()) {
                    self.toValue(today);
                }
            } else if (self.fromVisible()) {
                if (!self.fromValue() || self.fromValue().getTime() !== today.getTime()) {
                    self.fromValue(today);
                }
            } else if (self.onVisible()) {
                if (!self.onValue() || self.onValue().getTime() !== today.getTime()) {
                    self.onValue(today);
                }
            }
        };

        this.isActive = function () {
            return self.toValue() || self.fromValue() || self.onValue() || !self.showAll() || !self.showUnknown();
        };

        var superDispose = this.dispose;
        this.dispose = function () {
            superDispose.call(self);

            if (toValueSubscription) {
                toValueSubscription.dispose();
            }
            if (toCheckedSubscription) {
                toCheckedSubscription.dispose();
            }
            if (fromValueSubscription) {
                fromValueSubscription.dispose();
            }
            if (fromCheckedSubscription) {
                fromValueSubscription.dispose();
            }
            if (onValueSubscription) {
                onValueSubscription.dispose();
            }
            if (onCheckedSubscription) {
                onValueSubscription.dispose();
            }
            if (showAllSubscription) {
                showAllSubscription.dispose();
            }
        };

        this.requestState(function (state) {
            if (state) {
                self.preventNotification(function () {
                    self.showAll(state.showAll);
                    if (state.to) {
                        self.toValue(new Date(state.to));
                    }
                    if (state.from) {
                        self.fromValue(new Date(state.from));
                    }
                    if (state.on) {
                        self.onValue(new Date(state.on));
                    }
                    self.showUnknown(state.showUnknown);
                });
            }
        });
    };

    /**
     * The viewmodel used for data time search.
     *
     * @param {Object} model the JSON model sent from the server
     * @param {Object} context the application context
     */
    blueriq.aggregatedao.DateTimeSearchModel = function DateTimeSearchModel(model, context) {

        this.onBeforeShowDateTime = function () {
            self.visible(true);
        };

        var self = this;
        blueriq.aggregatedao.BaseSearchModel.call(self, model, context);
        this.contentStyle = model.contentStyle;

        this.createMessage = function () {

            var value;
            if (!!self.simple()) {
                value = {
                    type: 'datetime',
                    to: self.toHiddenValue,
                    from: self.fromHiddenValue,
                    on: self.onValue()
                };
            } else {
                value = {
                    type: 'datetime',
                    showAll: self.showAll(),
                    to: self.toHiddenValue,
                    from: self.fromHiddenValue,
                    on: self.onValue(),
                    showUnknown: self.showUnknown()
                };
            }

            return {
                columnIndex: model.properties.index,
                isDefault: !self.isActive(),
                value: value
            };
        };

        // Functions for hours and minutes display
        this.formatNumber = function (number) {
            if (number < 10) {
                return '0' + number;
            } else {
                return number;
            }
        };

        this.readNumber = function (number) {
            return parseInt(number);
        };

        this.nowText = model.properties.now;
        this.todayText = model.properties.today;

        // Options
        this.showAll = ko.observable(true);

        this.showUnknown = ko.observable(true);
        this.subscribe(this.showUnknown);

        // on
        this.onChecked = ko.observable(false);
        this.onVisible = ko.observable(false);
        this.onValue = ko.observable(null);

        var onValueSubscription = this.onValue.subscribe(function (value) {
            self.withTransaction(function () {
                self.onChecked(!!value);
                self.updateShowAll();
            });
        });
        var onCheckedSubscription = this.onChecked.subscribe(function (checked) {
            self.withTransaction(function () {
                if (!checked) {
                    self.onValue(null);
                } else if (self.onValue() === null) {
                    self.onChecked(false);
                } else {
                    self.toChecked(false);
                    self.fromChecked(false);
                    self.showAll(false);
                }
            });
            return true;
        });

        // to
        this.toChecked = ko.observable(false);
        this.toVisible = ko.observable(false);
        this.toValue = ko.observable(null);
        this.toHours = ko.observable(this.formatNumber(new Date().getHours())).extend({
            timeNumberFormat: 'H'
        });
        this.toMinutes = ko.observable(this.formatNumber(new Date().getMinutes())).extend({
            timeNumberFormat: 'M'
        });
        this.toHiddenValue = null;
        var updateToValue = function (value) {
            self.withTransaction(function () {
                if (!!value) {
                    self.toHiddenValue = new Date(value.getTime());
                    self.toHiddenValue.setHours(self.toHours());
                    self.toHiddenValue.setMinutes(self.toMinutes());
                } else {
                    self.toHiddenValue = null;
                }
                self.toChecked(!!value);
                self.updateShowAll();
            });
        };
        var updateToTime = function () {
            if (self.toValue()) {
                updateToValue(self.toValue());
            }
        };
        var toValueSubscription = this.toValue.subscribe(updateToValue);
        var toHoursSubscription = this.toHours.subscribe(updateToTime);
        var toMinutesSubscription = this.toMinutes.subscribe(updateToTime);
        var toCheckedSubscription = this.toChecked.subscribe(function (checked) {
            self.withTransaction(function () {
                if (!checked) {
                    self.toValue(null);
                } else if (self.toValue() === null) {
                    self.toChecked(false);
                } else {
                    self.onChecked(false);
                    self.showAll(false);
                }
            });
        });

        // from
        this.fromChecked = ko.observable(false);
        this.fromVisible = ko.observable(true);
        this.fromValue = ko.observable(null);
        this.fromHours = ko.observable(this.formatNumber(new Date().getHours())).extend({
            timeNumberFormat: 'H'
        });
        this.fromMinutes = ko.observable(this.formatNumber(new Date().getMinutes())).extend({
            timeNumberFormat: 'M'
        });
        this.fromHiddenValue = null;
        var updateFromValue = function (value) {
            self.withTransaction(function () {
                if (!!value) {
                    self.fromHiddenValue = new Date(value.getTime());
                    self.fromHiddenValue.setHours(self.fromHours());
                    self.fromHiddenValue.setMinutes(self.fromMinutes());
                } else {
                    self.fromHiddenValue = null;
                }
                self.fromChecked(!!value);
                self.updateShowAll();
            });
        };
        var updateFromTime = function () {
            if (self.fromValue()) {
                updateFromValue(self.fromValue());
            }
        };
        var fromValueSubscription = this.fromValue.subscribe(updateFromValue);
        var fromHoursSubscription = this.fromHours.subscribe(updateFromTime);
        var fromMinutesSubscription = this.fromMinutes.subscribe(updateFromTime);
        var fromCheckedSubscription = this.fromChecked.subscribe(function (checked) {
            self.withTransaction(function () {
                if (!checked) {
                    self.fromValue(null);
                } else if (self.fromValue() === null) {
                    self.fromChecked(false);
                } else {
                    self.onChecked(false);
                    self.showAll(false);
                }
            });
        });

        this.updateShowAll = function () {
            if (self.fromValue() !== null || self.toValue() !== null || self.onValue() !== null) {
                self.showAll(false);
            } else {
                self.showAll(true);
            }
        };

        this.handleToClick = function () {
            self.toVisible(true);
            self.fromVisible(false);
            self.onVisible(false);
            return true;
        };

        this.handleFromClick = function () {
            self.toVisible(false);
            self.fromVisible(true);
            self.onVisible(false);
            return true;
        };

        this.handleOnClick = function () {
            if (self.onVisible()) {
                self.onChecked(!self.onChecked());
            }

            self.toVisible(false);
            self.fromVisible(false);
            self.onVisible(true);
        };

        var showAllSubscription = this.showAll.subscribe(function (checked) {
            self.withTransaction(function () {
                if (checked) {
                    self.toChecked(false);
                    self.fromChecked(false);
                    self.onChecked(false);
                    self.toValue(null);
                    self.fromValue(null);
                    self.onValue(null);
                }
            });
        });

        this.handleClear = function () {
            self.showAll(true);
            self.toVisible(false);
            self.fromVisible(true);
            self.onVisible(false);
            self.toValue(null);
            self.fromValue(null);
            self.onValue(null);
            self.showUnknown(true);
        };

        this.handleNow = function () {
            var now = new Date();
            var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            if (self.toVisible()) {
                if (!self.toValue() || self.toValue().getTime() !== today.getTime()) {
                    self.toValue(today);
                }
                self.toHours(self.formatNumber(now.getHours()));
                self.toMinutes(self.formatNumber(now.getMinutes()));
            } else if (self.fromVisible()) {
                if (!self.fromValue() || self.fromValue().getTime() !== today.getTime()) {
                    self.fromValue(today);
                }
                self.fromHours(self.formatNumber(now.getHours()));
                self.fromMinutes(self.formatNumber(now.getMinutes()));
            }
        };

        this.handleToday = function () {
            var now = new Date();
            var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

            if (self.onVisible()) {
                if (!self.onValue() || self.onValue.getTime !== today.getTime()) {
                    self.onValue(today);
                }
            }
        };

        this.isActive = function () {
            return !!self.toValue() || !!self.fromValue() || !!self.onValue() || !self.showAll() || !self.showUnknown();
        };

        var superDispose = this.dispose;
        this.dispose = function () {
            superDispose.call(self);

            if (toValueSubscription) {
                toValueSubscription.dispose();
            }
            if (toHoursSubscription) {
                toHoursSubscription.dispose();
            }
            if (toMinutesSubscription) {
                toMinutesSubscription.dispose();
            }
            if (toCheckedSubscription) {
                toCheckedSubscription.dispose();
            }
            if (fromValueSubscription) {
                fromValueSubscription.dispose();
            }
            if (fromHoursSubscription) {
                fromHoursSubscription.dispose();
            }
            if (fromMinutesSubscription) {
                fromMinutesSubscription.dispose();
            }
            if (fromCheckedSubscription) {
                fromValueSubscription.dispose();
            }
            if (onValueSubscription) {
                onValueSubscription.dispose();
            }
            if (onCheckedSubscription) {
                onCheckedSubscription.dispose();
            }
            if (showAllSubscription) {
                showAllSubscription.dispose();
            }
        };

        this.requestState(function (state) {
            if (state) {
                self.preventNotification(function () {
                    self.showAll(state.showAll);
                    var date;
                    if (state.to) {
                        date = new Date(state.to);
                        self.toValue(new Date(date.getFullYear(), date.getMonth(), date.getDate()));
                        self.toHours(self.formatNumber(date.getHours()));
                        self.toMinutes(self.formatNumber(date.getMinutes()));
                    }
                    if (state.from) {
                        date = new Date(state.from);
                        self.fromValue(new Date(date.getFullYear(), date.getMonth(), date.getDate()));
                        self.fromHours(self.formatNumber(date.getHours()));
                        self.fromMinutes(self.formatNumber(date.getMinutes()));
                    }
                    if (state.on) {
                        self.onValue(new Date(state.on));
                    }
                    self.showUnknown(state.showUnknown);
                });
            }
        });
    };

    blueriq.aggregatedao.DomainSearchModel = function DomainSearchModel(model, context) {
        var self = this;
        blueriq.aggregatedao.BaseSearchModel.call(self, model, context);
        this.contentStyle = model.contentStyle;

        this.showAll = ko.observable(true);
        this.showUnknown = ko.observable(true);
        this.valueList = ko.observableArray();
        this.selectAllText = model.properties.selectall;

        var propagate = true; // flag indicating whether showAll change event propagates to other checkboxes
        var fieldModel = context.session.getModel(model.children[0]);
        var domain = fieldModel.domain;

        this.size = domain.length;
        this.showAllVisible = ko.observable(fieldModel.dataType !== 'boolean');

        var domainValues = new Array(this.size);
        var subscriptions = [];
        var sum = this.size;

        var handler = function (checked) {
            self.withTransaction(function () {
                if (checked === false) {
                    sum--;
                    propagate = false;
                    self.showAll(false);
                    propagate = true;
                } else {
                    sum++;
                    if (sum === self.size) {
                        self.showAll(true);
                    }
                }
            });
        };

        for (var i = 0; i < this.size; i++) {
            domainValues[i] = {};
            domainValues[i].value = domain[i].value;
            domainValues[i].checked = ko.observable(true);
            subscriptions.push(domainValues[i].checked.subscribe(handler));
            if (domain[i].displayValue) {
                domainValues[i].displayValue = domain[i].displayValue;
            } else {
                domainValues[i].displayValue = domainValues[i].value;
            }
            this.valueList().push(domainValues[i]);
        }

        this.createMessage = function () {

            var value;
            if (!!self.simple()) {
                value = {
                    type: 'domain',
                    values: self.getStringWithValues()
                };
            } else {
                value = {
                    type: 'domain',
                    showAll: self.showAll(),
                    values: self.getStringWithValues(),
                    showUnknown: self.showUnknown()
                };
            }

            return {
                columnIndex: model.properties.index,
                isDefault: !self.isActive(),
                value: value
            };
        };

        this.getStringWithValues = function () {
            var toReturn = [];
            var i = 0;
            for (var index in self.valueList()) {
                if (!!self.valueList()[index].checked()) {
                    toReturn[i++] = self.valueList()[index].value;
                }
            }
            return toReturn;
        };

        this.subscribe(this.showUnknown);
        var showAllSubscription = this.showAll.subscribe(function (checked) {
            self.withTransaction(function () {
                if (propagate) {
                    for (var index in self.valueList()) {
                        self.valueList()[index].checked(checked);
                    }
                }
                propagate = true;
            });
        });

        this.handleClear = function () {
            self.withTransaction(function () {
                self.showAll(true);
                self.showUnknown(true);
            });
        };

        this.isActive = function () {
            var active = false;
            for (var index in self.valueList()) {
                if (self.valueList()[index].checked() === false) {
                    active = true;
                    break;
                }
            }
            return active || !self.showUnknown() || (self.showAllVisible() && !self.showAll());
        };

        var superDispose = this.dispose;
        this.dispose = function () {
            superDispose.call(self);

            if (showAllSubscription) {
                showAllSubscription.dispose();
            }

            $(subscriptions).each(function () {
                this.dispose();
            });
        };

        this.requestState(function (state) {
            if (!state) {
                return;
            }

            self.preventNotification(function () {
                self.showAll(state.showAll);
                $(self.valueList()).each(function (index, element) {
                    element.checked($.inArray(element.value, state.values) !== -1);
                });
                self.showUnknown(state.showUnknown);
            });
        });
    };

    //
    // private module variables and functions
    //

    var bringToFront = (function () {
        var topZIndex = 10000;

        return function ($element) {
            var elementZIndex = $element.css('z-index');
            if (elementZIndex < topZIndex) {
                topZIndex++;
                $element.css('z-index', topZIndex);
            }
        };
    })();

})(window.blueriq, window.bqApp, window.ko, window.moment);
