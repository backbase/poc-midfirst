package com.backbase.expert.forms.extensions.services;

import com.aquima.interactions.foundation.IValue;
import com.aquima.interactions.framework.service.ServiceResult;
import com.aquima.interactions.portal.IService;
import com.aquima.interactions.portal.IServiceContext;
import com.aquima.interactions.portal.IServiceResult;
import com.aquima.interactions.portal.ServiceException;
import com.aquima.web.config.annotation.AquimaService;
import com.backbase.expert.forms.extensions.utils.FormsUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@AquimaService("BB_Log")
public class LogService implements IService {

    private final static Logger LOG = LoggerFactory.getLogger(LogService.class);

    @Override
    public IServiceResult handle(IServiceContext serviceContext) throws ServiceException {
        final String logLevel = serviceContext.getParameter("Level");
        IValue message = FormsUtils.getExpressionAttrByParameter(serviceContext, "Message");
        if (StringUtils.isNotEmpty(logLevel)) {
            if (logLevel.equalsIgnoreCase("DEBUG")) {
                LOG.debug(message.stringValue());
            } else if (logLevel.equalsIgnoreCase("INFO")) {
                LOG.info(message.stringValue());
            } else if (logLevel.equalsIgnoreCase("WARN")) {
                LOG.warn(message.stringValue());
            } else if (logLevel.equalsIgnoreCase("ERROR")) {
                LOG.error(message.stringValue());
            }
        }

        return new ServiceResult();
    }
}
