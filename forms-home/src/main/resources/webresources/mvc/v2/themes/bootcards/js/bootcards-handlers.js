/**
 * Created by f.van.rijswijk on 7-9-2015.
 */

(function (ko) {


    //handlers


    ko.bindingHandlers.initBootcards = {
        init: function (element, valueAccessor, allBindingsAccessor, viewmodel) {
            bootcards.init({
                offCanvasBackdrop: true,
                offCanvasHideOnMainClick: true,
                enableTabletPortraitMode: true,
                disableRubberBanding: true,
                disableBreakoutSelector: 'a.no-break-out'
            });
            //enable FastClick
            //window.addEventListener('load', function () {
            //    FastClick.attach(document.body);
            //}, false);

            //activate the sub-menu options in the offcanvas menu
            $('.collapse').collapse();
        }
    };

    ko.virtualElements.allowedBindings.initBootcards = true;

    ko.bindingHandlers.bqListClick = {
        init: function (element, valueAccessor, allBindingsAccessor, viewmodel) {
            $(element).on({
                click: function(){
                    if(typeof viewmodel.action !== 'undefined'){
                        viewmodel.action.handleClick();
                    }
                }
            });
        }
    };
    
    ko.bindingHandlers.bqTaskOfflineClick = {
            init: function (element, valueAccessor, allBindingsAccessor, viewmodel) {
                $(element).on({
                   click: function(){
                       viewmodel.taskOfflineClick();
                   }
                });
            }
        };
    ko.bindingHandlers.bqRemoveOfflineTaskClick = {
            init: function (element, valueAccessor, allBindingsAccessor, viewmodel) {
                $(element).on({
                   click: function(){
                       viewmodel.remove();                	   
                   }
                });
            }
        };

    ko.bindingHandlers.bqTooltip = {
    };
    ko.bindingHandlers.bqIconTooltip = {
    };

    ko.bindingHandlers.bootcardsTooltip = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            $(element).tooltip({
                title: viewModel.explainText,
                placement: 'right auto',
                trigger: 'click',
                container:'body'
            });



        }
    };

    ko.bindingHandlers.bqMobileFooter = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            viewModel.loaded();
            $(element).on({
                remove: function () {
                    viewModel.dispose();
                }
            });
        }
    };


})(window.ko);