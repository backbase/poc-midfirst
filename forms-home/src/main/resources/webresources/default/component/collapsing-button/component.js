(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'collapsing-button',
            condition: function(el) {
                return [
                    el.isButton,
                    el.classList.indexOf("submit-button") > -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();