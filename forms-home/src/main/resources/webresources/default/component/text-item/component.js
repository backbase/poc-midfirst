(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'text-item',
            condition: function(el) {
                return [
                    el.type === 'textitem'
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();