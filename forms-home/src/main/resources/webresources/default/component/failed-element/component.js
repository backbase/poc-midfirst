(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'failed-element',
            condition: function(el) {
                return [ el.type === 'failedelement' ];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();