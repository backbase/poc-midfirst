/**
 * @module model-shc-login-ng
 *
 * @description
 * Model for widget-shc-login-ng and widget-shc-user-menu-ng
 *
 * @example
 * import modelLoginModuleKey, { modelLoginKey } from 'model-shc-login-ng';
 *
 * angular
 *   .module('ExampleModule', [
 *     modelLoginModuleKey,
 *   ])
 *   .factory('someFactory', [
 *     modelLoginKey,
 *     // into
 *     function someFactory(loginModel) {
 *       // ...
 *     },
 *   ]);
 */
import angular from 'vendor-bb-angular';

import cXPAuthenticationDataModuleKey, {
  cXPAuthenticationDataKey,
} from 'data-shc-cxp-authentication-http-ng';

import Model from './login';

export const moduleKey = 'model-shc-login-ng';
export const modelLoginKey = `${moduleKey}:model`;

export default angular
  .module(moduleKey, [
    cXPAuthenticationDataModuleKey,
  ])

  .factory(modelLoginKey, [
    '$q',
    cXPAuthenticationDataKey,
    /* into */
    Model,
  ])

  .name;
