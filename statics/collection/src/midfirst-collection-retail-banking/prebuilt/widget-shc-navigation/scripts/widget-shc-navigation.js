/* global b$, $, bd, gadgets */
(function () {
    'use strict';

    var Container = b$.bdom.getNamespace('http://backbase.com/2013/portalView').getClass('container');

    Container.extend(function () {
        Container.apply(this, arguments);
        this.isPossibleDragTarget = true;
    }, {
        localName: 'templateShcNavigation',
        namespaceURI: 'templates_templateShcNavigation'
    }, {
        template: function (json) {
            var data = {item: json.model.originalItem};
            return window[this.namespaceURI][this.localName](data);
        },
        handlers: {
            DOMReady: function (pTarget) {
                var _context = pTarget.currentTarget.htmlNode;
                var _navigationMain = _context.querySelector('.shc-horizontal-navigation');
                var _navigationMidState = _context.querySelector('.shc-horizontal-navigation--midstate');
                //---
                var dropDowns = [];
                //--
                var _inDesignMode = !!bd.designMode;
                var _model = pTarget.target.model;
                var _loggedIn = b$.portal.loggedInUserId && b$.portal.loggedInUserId.length > 0;
                var _userName = b$.portal.loggedInUserId;
                var _bgURLbase = _model.getPreference('logoURL_base');
                var _bgURLContrast = _model.getPreference('logoURL_contrast');
				var _bgURLbaseMobile = _model.getPreference('logoURL_base_mobile');
				var _bgURLContrastMobile = _model.getPreference('logoURL_contrast_mobile');
                //--
                var _boxSize = _navigationMain.getBoundingClientRect();
                var _boxHeight = _boxSize.bottom || 60;
                //--
                if (_bgURLbase) _bgURLbase = _bgURLbase.replace('$(contextRoot)', '' + b$.portal.portalServer.serverURL + '');
                if (_bgURLContrast) _bgURLContrast = _bgURLContrast.replace('$(contextRoot)', '' + b$.portal.portalServer.serverURL + '');
				if (_bgURLbaseMobile) _bgURLbaseMobile = _bgURLbaseMobile.replace('$(contextRoot)', '' + b$.portal.portalServer.serverURL + '');
				if (_bgURLContrastMobile) _bgURLContrastMobile = _bgURLContrastMobile.replace('$(contextRoot)', '' + b$.portal.portalServer.serverURL + '');

                if(_bgURLContrast)_bgURLContrast = _bgURLContrast.replace(new RegExp('//', 'g'), '/');
				if(_bgURLbase)_bgURLbase = _bgURLbase.replace(new RegExp('//', 'g'), '/');

				if(_bgURLbaseMobile)_bgURLbaseMobile = _bgURLbaseMobile.replace(new RegExp('//', 'g'), '/');
				if(_bgURLContrastMobile)_bgURLContrastMobile = _bgURLContrastMobile.replace(new RegExp('//', 'g'), '/');


                setupProfileAvatar();
                setupCompanyLogo();
                setupDropDowns();
                setupNavigationToggle();
                if (!_inDesignMode) setupScrollLogic();
				else {
					_navigationMain.classList.remove('shc-horizontal-navigation--contrast');
					_navigationMidState.classList.remove('shc-horizontal-navigation--contrast');
                }


                // ------------------------------------------------------------------------------------
                // ------------------------------------------------------------------------------------

                function setupNavigationToggle() {
                    var menuBtn = _context.querySelector('.my-products-menu-toggle__open-button');
                    var closeBtn = _context.querySelector('.shc-horizontal-navigation__menu-navigation-area__close');
                    var targetArea = _context.querySelector('.shc-horizontal-navigation__menu-navigation-area');
                    //--
                    menuBtn.addEventListener('click', function () {
                        targetArea.classList.add('expanded');
                    });
                    closeBtn.addEventListener('click', function () {
                        targetArea.classList.remove('expanded');
                    });

                }

                function setupScrollLogic() {
					var doc = $(document);
                    document.addEventListener('scroll', bodyScroll);
                    window.addEventListener('resize', handleResize);
                    bodyScroll();

                    function bodyScroll() {
                        var scrollTop = doc.scrollTop();
                        if (scrollTop < _boxHeight) {
                            _navigationMain.classList.remove('shc-horizontal-navigation--scrolled');
                            _navigationMain.classList.add('shc-horizontal-navigation--contrast');
                            _navigationMidState.classList.add('shc-horizontal-navigation--contrast');
                        } else {
                            _navigationMain.classList.remove('shc-horizontal-navigation--contrast');
                            _navigationMain.classList.add('shc-horizontal-navigation--scrolled');
                            _navigationMidState.classList.remove('shc-horizontal-navigation--contrast');
                        }
                    }

                    function handleResize() {
                        bodyScroll();
                    }
                }

                function hideDropDowns() {
                    dropDowns.forEach(function (pItem) {
                        pItem.target.classList.add('hidden');
                        pItem.trigger.classList.remove('active');
                        _navigationMain.classList.remove('shc-horizontal-navigation--menuopen');
                    });

                    document.body.removeEventListener('click', hideDropDowns);
                }

                function activateDropdown() {
                    var dropdownAlreadyActive = false;
                    if (this.trigger.classList.contains('active')) dropdownAlreadyActive = true;

                    hideDropDowns();

                    if (!dropdownAlreadyActive) {
                        this.target.classList.remove('hidden');
                        this.trigger.classList.add('active');
                        _navigationMain.classList.add('shc-horizontal-navigation--menuopen');

                        setTimeout(function () {
                            document.body.addEventListener('click', hideDropDowns);
                        }, 100);
                    }
                }

                function setupDropDowns() {
                    Array.prototype.slice.call(document.querySelectorAll('.shc-horizontal-navigation__dropdown-pair')).forEach(function (pItem) {
                        var trigger = pItem.querySelector('.dropdown-toggle');
                        var dObject = {
                            trigger: trigger,
                            target: pItem.querySelector('.shc-horizontal-navigation__link--dropdown__children_area')
                        };
                        dropDowns.push(dObject);
                        trigger.addEventListener('click', activateDropdown.bind(dObject));
                    });
                }

                function setupCompanyLogo() {
                    var logoAreas = _context.querySelectorAll('.shc-horizontal-navigation__logo-area');
                    Array.prototype.slice.call(logoAreas).forEach(function (pItem) {
                        pItem.style.setProperty('--company-logo__base_background-image-url', 'url(' + _bgURLbase + ')');
                        pItem.style.setProperty('--company-logo__contrast_background-image-url', 'url(' + _bgURLContrast + ')');
						pItem.style.setProperty('--company-logo__base_mobile_background-image-url', 'url(' + _bgURLbaseMobile + ')');
						pItem.style.setProperty('--company-logo__contrast_mobile_background-image-url', 'url(' + _bgURLContrastMobile + ')');
                    });
                }

                //--
                function setupProfileAvatar() {
                    if (_loggedIn && _userName) {
                        var dataInfo = {};
                        $.get('/portalserver/services/rest/v1/party-data-management/party', function (data) {
                            dataInfo = data;
                            var profiles = Array.prototype.slice.call(_context.querySelectorAll('.shc-horizontal-navigation__link--profile'));
                            profiles.forEach(function (pItem) {
                                setupChildren(pItem);
                            });
                        });

                        function setupChildren(pArea) {
                            var area = pArea;
                            var toggle = area.querySelector('.dropdown-toggle');
                            var img = area.querySelector('.shc-avatar--image');
                            var logOutBtn = area.querySelector('.btn--logout');
                            var namePlaceHolder = area.querySelector('.shc-horizontal-navigation__link--profile__name');

                            if (img) img.style.backgroundImage = 'url(' + dataInfo.photoUrl + ')';
                            if (namePlaceHolder) namePlaceHolder.innerText = dataInfo.firstName + ' ' + dataInfo.lastName;

                            if (logOutBtn) {
                                logOutBtn.addEventListener('click', function () {
                                    gadgets.pubsub.publish('usermenu-logout');
                                });
                            }
                            if (toggle) {

                                toggle.addEventListener('click', function () {
                                    hideDropDowns();
                                });
                            }
                        }
                    }
                }
            },
            preferencesSaved: function (event) {
                if (event.target === this) {
                    this.refreshHTML(function () {
                        //add code, HTML refreshed
                    });
                }
            }
        }
    });
})();
