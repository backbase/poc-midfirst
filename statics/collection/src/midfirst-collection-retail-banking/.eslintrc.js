module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "sourceType": "module"
    },
    "rules": {
        "indent": [ "error", 4 ],
        "linebreak-style": [ "warn", "unix" ],
        "quotes": [ "warn", "single" ],
        "no-undef": [ "error" ],
        "no-irregular-whitespace": [ "off" ],
        "no-unused-vars": [ "error" ],
        "semi": [ "error", "always" ],
        "no-inner-declarations": [ "off" ],
        "no-useless-escape": [ "off" ]
    }
};
