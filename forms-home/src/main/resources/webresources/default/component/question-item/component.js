(function () {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'question-item',
            condition: function(el) {
                return [el.isContainer, el.styles.indexOf('question_item') > -1];
            }
        });
    }
    angular.module('forms-ui').run(registerComponent);
})();