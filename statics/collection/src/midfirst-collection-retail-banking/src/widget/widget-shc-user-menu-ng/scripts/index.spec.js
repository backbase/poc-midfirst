import widget from './index';

describe('User Menu widget', () => {
  it('should export the module name', () => {
    expect(widget).toEqual('widget-shc-user-menu-ng');
  });
});
