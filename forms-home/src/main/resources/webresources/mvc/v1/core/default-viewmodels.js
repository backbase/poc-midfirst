(function (blueriq, bqApp, ko) {
    'use strict';

    /**

Default modelFactory for the default Blueriq view models.
     */

    blueriq.defaultModelFactory = function (model, context) {

        /* 'special' element models */
        if (model.type === 'field' && jQuery.inArray('filetype', model.styles) > -1) {
            return new blueriq.models.FileTypeModel(model, context);
        }

        /* generic container models */
        if (model.contentStyle === 'table') {
            return new blueriq.models.TableModel(model, context);
        } else if (model.contentStyle === 'tablesearch') {
            return new blueriq.models.TableSearchModel(model, context);
        } else if (model.contentStyle === 'tablesortedheader') {
            return new blueriq.models.TableSortedHeaderModel(model, context);
        } else if (model.contentStyle === 'tablenavigation') {
            return new blueriq.models.TableNavigationModel(model, context);
        } else if (model.contentStyle === 'fileupload') {
            return new blueriq.models.FileUploadModel(model, context);
        } else if (model.contentStyle === 'filedownload') {
            return new blueriq.models.FileDownloadModel(model, context);
        } else if (model.contentStyle === 'instancelist') {
            return new blueriq.models.InstanceListModel(model, context);
        } else if (model.contentStyle === 'tabs') {
            return new blueriq.models.TabContainerModel(model, context);
        } else if(model.contentStyle === 'visualization') {
            return new blueriq.models.VisualizationModel(model, context);
        } else if(model.contentStyle === 'statistic' ) {
            return new blueriq.models.StatisticModel(model, context);
        } else if (model.type === 'container') {
            return new blueriq.models.ContainerModel(model, context);
        }

        /* generic element models*/
        if (model.type === 'page') {
            return new blueriq.models.PageModel(model, context);
        } else if (model.type === 'button') {
            return new blueriq.models.ButtonModel(model, context);
        } else if (model.type === 'contentitem') {
            return new blueriq.models.ContentItemModel(model, context);
        } else if (model.type === 'field') {
            return new blueriq.models.FieldModel(model, context);
        } else if (model.type === 'image') {
            return new blueriq.models.ImageModel(model, context);
        } else if (model.type === 'asset') {
            return new blueriq.models.AssetModel(model, context);
        } else if (model.type === 'textitem') {
            return new blueriq.models.TextitemModel(model, context);
        } else if (model.type === 'breadcrumb') {
            return new blueriq.models.BreadcrumbModel(model, context);
        } else if (model.type === 'link') {
            return new blueriq.models.LinkModel(model, context);
        } else if (model.type === 'failedelement') {
            return new blueriq.models.FailedElementModel(model, context);
        } else {
            return new blueriq.models.UnknownModel(model, context);
        }
    };

    /**
     * Register automatically if bqApp is already created
     */
    if (bqApp) {
        bqApp.modelFactory.register(blueriq.defaultModelFactory);
    }

    /**
     * Represents the base model for all blueriq model based viewmodels.
     *
     * @constructor
     * @name blueriq.models.BaseModel
     * @param {Object} model
     * @param {Object} context
     * @returns A BaseModel object.
     */
    blueriq.models.BaseModel = function BaseModel(model, context) {

        if (!model) {
            throw new Error('model is mandatory');
        }
        if (!context) {
            throw new Error('context is mandatory');
        }

        var self = this;

        function getIcon(styles) {
            for (var i = 0; i < styles.length; ++i) {
                if (styles[i].indexOf('icon_') === 0) {
                    return styles[i].replace(new RegExp('_', 'g'), '-');
                }
            }
        }

        function getStyles(styles) {
            var result = {};
            if (styles) {
                for (var i = 0; i < styles.length; ++i) {
                    result[styles[i]] = true;
                }
                result.icon = getIcon(styles);
                result.isIcon = result.icon ? true : false;
                result.joined = styles.join(' ');
            }
            return result;
        }

        this.key = model.key;
        this.model = model;
        this.context = context;

        this.presentationStyles = ko.observable(getStyles(model.styles));
        var subscription;
        if (self.key) {
            subscription = this.context.session.subscribeModel(self.key, function onChange(type, model) {
                if (type === 'update') {
                    if (self.update) {
                        self.update(model);
                    }
                    self.model = model;
                    self.presentationStyles(getStyles(model.styles));
                } else if (type === 'delete') {
                    if (self.dispose) {
                        self.dispose();
                    }
                    subscription.dispose();
                }
            });
        } else {
            subscription = this.context.session.subscribe(function onChange(type, key, model) {
                if (type === 'update' && self.key === key) {
                    if (self.update) {
                        self.update(model);
                    }
                    self.model = model;
                    self.presentationStyles(getStyles(model.styles));
                } else if (type === 'delete' && self.key === key) {
                    if (self.dispose) {
                        self.dispose();
                    }
                    subscription.dispose();
                }
            });
        }

        /**
         * Gets the API URI for the configuration present on this basemodel.
         *
         * @function
         * @name blueriq.models.BaseModel#getApiUri
         * @returns {String} API URI
         */
        this.getApiUri = function getApiUri() {
            return context.configuration.baseUri + context.session.id + '/api/';
        };

        /**
         * Submits the current page.
         *
         * @function
         * @name blueriq.models.BaseModel#submit
         * @param {String} trigger Key of triggering element.
         * @param {Object} parameters The optional set of parameters.
         */
        this.submit = function submit(trigger, parameters) {
            context.messageBus.notify('beforeSubmit', context.session.id);
            context.session.submit(trigger, parameters, function () {
                context.messageBus.notify('afterSubmit', context.session.id);
            });
        };

        /**
         * Recompose the current page.
         *
         * @function
         * @name blueriq.models.BaseModel#recompose
         */
        this.recompose = function recompose() {
            context.messageBus.notify('beforeRecompose', context.session.id);
            context.session.recompose(function () {
                context.messageBus.notify('afterRecompose', context.session.id);
            });
        };

        /**
         * Short method for the blueriq.TemplateFactory#getTemplate method that
         * returns the id/url of the template given the view model and context.
         *
         * @function
         * @name blueriq.model.BaseModel#template
         * @param {blueriq.models.BaseModel} data viewModel The model to retrieve a template for, must be specified.
         * @param {Object} context bindingContext binding context is an object that holds data that you can reference from your bindings
         * @returns {String} The url/d of the template.
         */
        this.template = function getTemplate(data, context) {
            return self.context.templateFactory.getTemplate(data, context);
        };

        /**
         * Short method for the blueriq.TemplateFactory#getFieldValueTemplate method that
         * returns the id/url of the template given the view model and context.
         *
         * @function
         * @name blueriq.model.BaseModel#fieldValueTemplate
         * @param {blueriq.models.BaseModel} data viewModel The field model to retrieve a value template for, must be specified.
         * @param {Object} context bindingContext binding context is an object that holds data that you can reference from your bindings
         * @returns {String} The url/d of the template.
         */
        this.fieldValueTemplate = function getFieldValueTemplate(data, context) {
            return self.context.templateFactory.getFieldValueTemplate(data, context);
        };

        /**
         * Checks whether or not the specified presentation style is present on this model.
         *
         * Use: presentationStyles().NAME instead for better performance
         *
         * @function
         * @name blueriq.models.BaseModel#hasPresentationStyle
         * @param {String} presentationStyle The presentation style to check for.
         */
        this.hasPresentationStyle = function hasPresentationStyle(presentationStyle) {
            if (presentationStyle) {
                for (var i = 0; i < this.models.styles.length; i++) {
                    if (presentationStyle === this.models.styles[i]) {
                        return true;
                    }
                }
            }
            return false;
        };
    };

    /**
     * Represents an unknown model.
     *
     * @constructor
     * @name blueriq.models.UnknownModel
     * @param {Object} model The unknown model.
     * @param {Object} context The data context.
     * @returns And UnknownModel instance.
     */
    blueriq.models.UnknownModel = function UnknownModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'unknown';
    };

    /**
     * Represents a container.
     *
     * @constructor
     * @name blueriq.models.ContainerModel
     * @param {Object} model The container model.
     * @param {Object} context The data context.
     * @returns An container viewmodel instance.
     */
    blueriq.models.ContainerModel = function ContainerModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(this, model, context);

        this.type = 'container';
        var childModels = {};

        this.contentStyle = model.contentStyle;
        this.displayName = ko.observable(model.displayName);
        this.children = ko.observableArray(null);
        this.properties = ko.observable(model.properties);
        { // initialize children
            var result = [];
            for (var i = 0; i < model.children.length; ++i) {
                var key = model.children[i];
                var dataModel = context.session.getModel(key);
                if (!dataModel) {
                    throw new Error('Illegal state: expected to have a model with key ' + key);
                }
                var viewModel = context.modelFactory.createViewModel(dataModel, context);
                result.push(viewModel);
                childModels[key] = viewModel;
            }
            this.children(result);
        }

        /**
         * Updates this container viewmodel properties.
         *
         * @function
         * @name blueriq.models.ContainerModel#update
         */
        this.update = function update(model) {
            self.displayName(model.displayName);
            self.properties(model.properties);

            var currentChildren = self.model.children;
            var newChildren = model.children;

            // Set/Order children
            var children = [];
            var key, viewModel, dataModel, i;
            for (i = 0; i < newChildren.length; ++i) {
                key = newChildren[i];
                viewModel = childModels[key];
                if (!viewModel) {
                    dataModel = context.session.getModel(key);
                    viewModel = context.modelFactory.createViewModel(dataModel, context);
                    childModels[key] = viewModel;
                }
                children.push(viewModel);
            }

            // Remove Obsolete children
            for (i = 0; i < currentChildren.length; ++i) {
                key = currentChildren[i];
                if (newChildren.indexOf(key) < 0) {
                    delete childModels[key];
                }
            }
            self.children(children); // single set (otherwise it will have multiple notifications);
        };
    };

    /**
     * Represents a page.
     *
     * @constructor
     * @name blueriq.models.PageModel
     * @param {Object} model The Page model.
     * @param {Object} context The data context.
     * @returns A Page Instance instance.
     */
    blueriq.models.PageModel = function PageModel(model, context) {

        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);

        this.type = 'page';

        this.messages = ko.observableArray(model.messages);
        var baseUpdate = self.update;
        this.update = function PageUpdate(model) {
            baseUpdate(model);
            self.messages(model.messages);
        };
    };

    /**
     * Represents a breadcrumb.
     *
     * @constructor
     * @name blueriq.models.BreadcrumbModel
     * @param {Object} model The breadcrumb model.
     * @param {Object} context The data context.
     * @returns A Breadcrumb instance.
     */
    blueriq.models.BreadcrumbModel = function BreadcrumbModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'breadcrumb';

        this.properties = ko.observable(model.properties);

        this.handleClick = function () {
            if (!self.properties().isPassed) {
                return false;
            }
            // Start the flow
            var flowName = self.properties().flowName;
            context.session.startFlow(flowName);
        };

        this.update = function BreadCrumbModelUpdate(model) {
            self.properties(model.properties);
        };
    };

    /**
     * Represents a button.
     *
     * @constructor
     * @name blueriq.models.ButtonModel
     * @param {Object} model The button model.
     * @param {Object} context The data context.
     * @returns A ButtonModel instance.
     */
    blueriq.models.ButtonModel = function ButtonModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'button';
        this.icon = ko.observable();
        this.caption = ko.observable(model.caption);

        this.disabled = ko.observable(model.disabled);

        this.handleClick = function ButtonModelClick() {
            if (!self.disabled()) {
                self.submit(model.key);
            }
        };
        this.update = function ButtonModelUpdate(model) {
            self.caption(model.caption);
            self.disabled(model.disabled);
        };
    };

    /**
     * Represents a content item.
     *
     * @constructor
     * @name blueriq.models.ContentItemModel
     * @param {Object} model The contentitem model.
     * @param {Object} context The data context.
     * @returns A ContentItem instance.
     */
    blueriq.models.ContentItemModel = function ContentItemModel(model, context) {

        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);
        this.type = 'contentitem';

        //strip empty textitems which pollute the model
        for(var i = this.children().length-1 ; i >= 0 ; i--){
            console.log(this.children()[i].type);
            if(this.children()[i].type === 'textitem' && this.children()[i].plainText() === null){
                this.children().splice(i,1);
            }
        }
    };
    /**
     * Represents a field.
     *
     * @constructor
     * @name blueriq.models.FieldModel
     * @param {Object} model The viewmodel.
     * @param {Object} context The data context.
     * @returns A FieldModel instance.
     */
    blueriq.models.FieldModel = function FieldModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'field';

        function hasError(model) {
            for (var i = 0; i < model.messages.length; ++i) {
                if (model.messages[i].type === 'ERROR') {
                    return true;
                }
            }
            return false;
        }

        function hasWarning(model) {
            for (var i = 0; i < model.messages.length; ++i) {
                if (model.messages[i].type === 'WARNING') {
                    return true;
                }
            }
            return false;
        }

        this.dataType = model.dataType;
        this.hasDomain = model.hasDomain;
        this.multiValued = model.multiValued;
        this.maxLength = model.displayLength > 1 ? model.displayLength : null;

        this.domain = ko.observableArray(model.domain);
        this.questionText = ko.observable(model.questionText);
        this.explainText = ko.observable(model.explainText);
        this.hasWarning = ko.observable(hasWarning(model));
        this.hasError = ko.observable(hasError(model));
        this.required = ko.observable(model.required);
        this.readonly = ko.observable(model.readonly);
        this.messages = ko.observableArray(model.messages);
        this.validations = ko.observableArray(model.validations);

        this.value = ko.observable(model.values[0]);

        function formatDisplayValue(value) {
            if (!value) {
                return '';
            }
            if (self.model.dataType === 'currency') {
                return '€ ' + value;
            } else if (self.model.dataType === 'percentage') {
                return value + ' %';
            } else {
                return value;
            }
        }
        function getDisplayValue() {
            var result;
            if (!self.multiValued) {
                if (self.hasDomain) {
                    return getDisplayValueFromDomain(self.model.values[0]);
                } else {
                    return formatDisplayValue(self.model.values[0]);
                }
            } else {
                result = '';
                for (var i = 0; i < self.model.values.length; i++) {
                    if (self.hasDomain) {
                        if(result !== ''){
                            result += ', ';
                        }
                        result += getDisplayValueFromDomain(self.model.values[i]);

                    } else {
                        if (result !== '') {
                            result += ', ';
                        }
                        result += formatDisplayValue(self.model.values[i]);
                    }
                }
            }
            return result;
        }
        function getDisplayValueFromDomain(value) {
            for (var j = 0; j < self.domain().length; j++) {
                if (self.domain()[j].value === value) {
                    var displayValue = self.domain()[j].displayValue;
                    if (displayValue) {
                        return self.domain()[j].displayValue;
                    } else {
                        return formatDisplayValue(self.domain()[j].value);
                    }
                }
            }
        }
        this.displayValue = ko.observable(getDisplayValue());

        var valueSubscription = this.value.subscribe(function (newValue) {
            self.model.values[0] = newValue;
            self.handleChange();
        });

        this.values = ko.observableArray(model.values);
        var valuesSubscription = this.values.subscribe(function (newValue) {
        	if(newValue.length==0) {
        		//AQ-6416
        		self.model.values = ['']
        	}else{
        		self.model.values = newValue;
        	}     
            self.displayValue(getDisplayValue());
            self.handleChange();
        });

        this.valueAsBoolean = ko.computed({
            owner: self,
            read: function () {
                return this.value() === 'true';
            },
            write: function (newValue) {
                this.value(newValue ? 'true' : 'false');
            }
        });

        /**
         * Handles the refresh of the field.
         */
        this.handleChange = function handleChange() {
            if (self.model.refresh && !self.model.readonly && !self.updating) {
                self.submit(self.model.key);
            }
        };
        self.updating = false;
        this.update = function update(model) {
            self.updating = true; // variable to check whether not submit while updating the model
            self.questionText(model.questionText);
            self.explainText(model.explainText);
            self.domain(model.domain);
            self.hasWarning(hasWarning(model));
            self.hasError(hasError(model));
            self.values(model.values);
            if(model.values.length == 0) {
            	//AQ-6416
            	model.values[0] = '';
            	self.value('');
        	}else{
        		self.value(model.values[0]);
        	}
            self.required(model.required);
            self.readonly(model.readonly);
            self.messages(model.messages);
            self.validations(model.validations);
            self.updating = false;
        };

        this.dispose = function () {
            valueSubscription.dispose();
            valuesSubscription.dispose();
        };
        
        if(!self.multiValued && !self.displayValue()){
        	self.model.values[0] = '';
        }
    };

    /**
     * Represents the Image Model.
     *
     * @constructor
     * @name blueriq.models.ImageModel
     * @param {Object} model The image model.
     * @param {Object} context The data context.
     * @returns An ImageModel instance.
     */
    blueriq.models.ImageModel = function ImageModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'image';

        this.url = this.getApiUri() + 'image/' + model.name + '/key/' + model.key;
        this.height = model.height;
        this.width = model.width;
    };

    /**
     * Represents the Link Model.
     *
     * @constructor
     * @name blueriq.models.LinkModel
     * @param {Object} model The link model.
     * @param {Object} context The data context.
     * @returns A LinkModel instance.
     */
    blueriq.models.LinkModel = function ImageModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'link';

        this.url = this.getApiUri() + 'document/' + model.parameters['document-type'] + '/' + model.parameters['document-name'] + '/' + model.parameters['page-name'];

        this.text = ko.observable(model.text);

        this.update = function LinkModelUpdate(model) {
            self.text(model.text);
        };
    };

    /**
     * Represents an asset.
     *
     * @constructor
     * @name blueriq.models.AssetModel
     * @param {Object} model The asset model.
     * @param {Object} context The data context.
     * @returns An AssetModel instance.
     */
    blueriq.models.AssetModel = function AssetModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'asset';

        this.text = ko.observable(model.text);

        this.update = function AssetModelUpdate(model) {
            self.text(model.text);
        };
    };

    /**
     * Represents a textitem.
     *
     * @constructor
     * @name blueriq.models.TextitemModel
     * @param {Object} model The textitem model.
     * @param {Object} context The data context.
     * @returns A TextItemModel instance.
     */
    blueriq.models.TextitemModel = function TextItemModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);
        this.type = 'textitem';
        this.plainText = ko.observable(model.plainText);
        this.nodes = ko.observableArray(model.nodes);

        this.update = function TextItemModelUpdate(model) {
            self.nodes(model.nodes);
            self.plainText(model.plainText);
        };
    };

    /**
     * Represents a failed element.
     *
     * @constructor
     * @name blueriq.models.FailedElementModel
     * @param {Object} model The failed element model.
     * @param {Object} context The data context.
     * @returns A FailedElementModel instance.
     */
    blueriq.models.FailedElementModel = function FailedElementModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);
        this.type = 'failedelement';

        this.message = model.message;
        this.stackTrace = model.stackTrace;
    };

    blueriq.models.TableModel = function TableModel(model, context) {

        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);
        this.type = 'table';

        if (model.children.length > 0) {
            this.hasHeader = context.session.getModel(model.children[0]).name === 'header';
        } else {
            this.hasHeader = false;
        }

        if (this.hasHeader) {
            this.header = this.children()[0];
            this.rows = ko.computed(function () {
                return self.children().slice(1);
            });
        } else {
            this.header = null;
            this.rows = ko.computed(function () {
                return self.children();
            });
        }
    };

    blueriq.models.TableNavigationModel = function TableNavigationModel(model, context) {

        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);
        this.type = 'tablenavigation';

        this.first = this.children()[0];
        this.previous = this.children()[1];
        this.selector = this.children()[2];
        this.next = this.children()[3];
        this.last = this.children()[4];
    };

    blueriq.models.TableSortedHeaderModel = function TableSortedHeaderModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);
        this.type = 'tablesortedheader';

        function isSortedAscending(model) {
            return jQuery.inArray('ascending', model.styles) > 0;
        }

        function isSortedDescending(model) {
            return jQuery.inArray('descending', model.styles) > 0;
        }

        var contentModel = context.session.getModel(model.children[0]);
        this.content = ko.observable(context.modelFactory.createViewModel(contentModel, context));

        // the sort button may not be created if the header is hidden
        if (model.children[1]) {
            var buttonModel = context.session.getModel(model.children[1]);
            this.sortedAscending = ko.observable(isSortedAscending(buttonModel));
            this.sortedDescending = ko.observable(isSortedDescending(buttonModel));

            this.handleClick = function () {
                if (!buttonModel.disabled) {
                    self.submit(buttonModel.key);
                }
            };

            //buttonChangeSubscription
            context.session.subscribe(function (type, key, model) {
                if (type === 'update' && key === buttonModel.key) {
                    self.sortedAscending(isSortedAscending(model));
                    self.sortedDescending(isSortedDescending(model));
                }
            });
        } else {
            // dummy implementations
            this.sortedAscending = ko.observable(true);
            this.sortedDescending = ko.observable(false);
            this.handleClick = function () {};
        }

        this.dispose = function () {
            if (self.buttonChangeSubscription) {
                self.buttonChangeSubscription.dispose();
            }
        };
    };

    /**
     * Represents a search container for a table, which contains a search field and button to perform the actual search.
     *
     * @constructor
     * @name blueriq.models.TableSearchModel
     * @param {Object} model The container model.
     * @param {Object} context The data context.
     * @returns A table search model instance
     */
    blueriq.models.TableSearchModel = function TableSearchModel(model, context) {

        var self = this;
        blueriq.models.BaseModel.call(self, model, context);
        this.type = 'tablesearch';

        this.searchField = context.modelFactory.createViewModel(context.session.getModel(model.children[0]), context);

        this.searchButton = model.children[1];
        this.disabled = context.session.getModel(model.children[1], context).disabled;
        this.searchTerm = ko.observableArray(this.searchField.values());

        /**
         * Performs a search action with the current search terms.
         *
         * @function
         * @name blueriq.models.TableSearchModel#search
         */
        this.search = function () {
            if (self.disabled) {
                return;
            }
            var parameters = {};
            parameters[self.searchField.model.name] = self.searchTerm();
            self.submit(self.searchButton, parameters);
        };

        this.keyPressed = function (data, event) {
            if (event.keyCode === 13) {
                self.search();
            }
            return true;
        };

        this.keyUp = function () {//data, event
            return true;
        };
    };

    /**
     * Representation for file download container.
     *
     * @constructor
     * @name blueriq.models.FileDownloadModel
     * @param {Object} model The container model.
     * @param {Object} context The data context.
     * @returns A FileDownloadModel instance.
     */
    blueriq.models.FileDownloadModel = function FileDownloadModel(model, context) {

        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);

        /**
         * Download button wrapped by this download container
         *
         * @member {blueriq.models.Button}
         */
        this.buttonViewModel = context.modelFactory.createViewModel(context.session.getModel(model.children[0]), context);
        var unauthorizedKey = model.children[1];
        this.buttonViewModel.handleClick = function ButtonModelClick() {
        	$.ajax({
        		type: 'GET',
        		url: bqApp.configuration.baseUri + context.session.id + '/filedownload/' + self.properties().configurationid + '/checkauthorization',
        		success: function() {
        			window.location = bqApp.configuration.baseUri + context.session.id + '/filedownload/' + self.properties().configurationid;
        		}, 
        		error: function() { 
        			self.submit(unauthorizedKey);
        		} 
        	});
        };
    };

    /**
     * Representation for file upload container.
     *
     * @constructor
     * @name blueriq.models.FileUploadModel
     * @param {Object} model The container model.
     * @param {Object} context The data context.
     * @returns A FileUploadModel instance.
     */
    blueriq.models.FileUploadModel = function FileUploadModel(model, context) {
        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);

        this.url = ko.observable(bqApp.configuration.baseUri + context.session.id + '/subscription/' + context.subscriptionId + '/fileupload/' + self.properties().configurationid + '/');
        this.csrfToken = context.session.csrfToken; // observable
        
        this.singleFileMode = self.properties().singlefilemode;
        this.allowedExtensions = self.properties().allowedextensions;
        this.maxFileSize = self.properties().maxfilesize;

        this.singleUploadLabel = self.properties().singleuploadlabel;
        this.multiUploadLabel = self.properties().multiuploadlabel;
        this.fileSizeDescription = self.properties().filesizedescription;
        this.extensionDescription = self.properties().extensiondescription;
        this.fileSizeValidationMessage = self.properties().filesizevalidationmessage;
        this.extensionValidationMessage = self.properties().extensionvalidationmessage;
        this.uploadSuccesMessage = self.properties().uploadsuccesmessage;
        this.uploadFailedMessage = self.properties().uploadfailedmessage;

        this.files = ko.observableArray();
        this.invalidFiles = ko.computed(function () {
            return ko.utils.arrayFilter(self.files(), function (file) {
                return !file.isValid;
            });
        });
        
        this.uploadStartHandler = function uploadStartHandler() { //data
            self.files.removeAll();
        };

        this.uploadDoneHandler = function uploadDoneHandler(data) {
            context.eventHandler.handleEvents(true, data);
            self.uploadProgress(undefined);
        };

        this.uploadProgress = ko.observable(undefined);
        this.progressPercentageHandler = function progressPercentageHandler(data) {
            self.uploadProgress(data + '%');
        };

        this.serverErrorMessagesContainer = ko.observable();        
        this.update = function FileUploadModelUpdate(model) {
        	self.properties(model.properties);
        	self.url(context.configuration.baseUri + context.session.id + '/subscription/' + context.subscriptionId + '/fileupload/' + self.properties().configurationid + '/');
            if (model.children.length === 3) {
                self.serverErrorMessagesContainer(context.modelFactory.createViewModel(context.session.getModel(model.children[0]), context));
            }
        };
    };

    /**
     * Viewmodel that represents field for a filetype.
     *
     * @constructor
     * @name blueriq.models.FileTypeModel
     * @param {Object} model The field model.
     * @param {Object} context The data context.
     * @returns A FileTypeModel instance.
     */
    blueriq.models.FileTypeModel = function FileTypeModel(model, context) {

        var self = this;
        blueriq.models.FieldModel.call(self, model, context);
        this.type = 'filetype';

        this.image = ko.computed(function () {
            var fileType = self.value();
            if (fileType) {
                fileType = fileType.toLowerCase();
                if (fileType.indexOf('application/pdf') > -1) {
                    return 'image-pdf';
                } else if (fileType.indexOf('image/') > -1) {
                    return 'icon-picture';
                }
            }
            return 'icon-file';
        });
    };

    /**
     * Viewmodel that represents instancelist.
     *
     * @constructor
     * @name blueriq.models.InstanceListModel
     * @param {Object} model The container model.
     * @param {Object} context The data context.
     * @returns A TabContainerModel instance.
     */
    blueriq.models.InstanceListModel = function InstanceListModel(model, context) {

        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);
        this.type = 'instancelist';

        this.showSearch = ko.computed(function () {
            if (self.children().length > 3) {
                return true;
            }
            if (model.children.length < 2) {
                return false;
            }
            //also show when there is a search query
            var searchContainerModel = context.session.getModel(model.children[1]);
            if (!searchContainerModel) {
                return false;
            }
            var searchFieldModel = context.session.getModel(searchContainerModel.children[0]);
            return searchFieldModel.values && searchFieldModel.values.length > 0;
        });
    };

    /**
     * Viewmodel that represents a tab container.
     *
     * @constructor
     * @name blueriq.models.TabContainerModel
     * @param {Object} model The container model.
     * @param {Object} context The data context.
     * @returns A TabContainerModel instance.
     */
    blueriq.models.TabContainerModel = function TabContainerModel(model, context) {

        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);

        this.tabContainers = ko.computed(function () {
            return ko.utils.arrayFilter(self.children(), function (child) {
                return child.type === 'container';
            });
        });

        this.tabLabels = ko.computed(function () {
            var labels = [];
            var tabContainers = self.tabContainers();
            for (var i = 0; i < tabContainers.length; i++) {
                labels[i] = {
                    'displayName': tabContainers[i].displayName,
                    'containerKey': tabContainers[i].key
                };
            }

            return labels;
        });
    };

    /**
     * Creates a VisualizationModel to display a single chart
     * @constructor
     * @name blueriq.services.VisualizationModel
     * @param {Object} model - The model for the chart
     * @param {Object} context - The data context
     * @returns {@link blueriq.models.VisualizationModel} single chart-model
     */
    blueriq.models.VisualizationModel = function VisualizationModel(model, context) {
        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);
        this.type = 'visualization';
        this.style = model.styles[0];
        this.displayName = model.displayName;

        this.statistics = [];
        var children = this.children();

        for ( var i = 0; i < children.length; i++ )
        {
            var str = null;
            try
            {
                str = children[i].displayName();
            }
            catch (err)
            {
                str = null;
            }
            str = !(!str || 0 === str.length) ? str : children[i].model.name;
            this.statistics.push([str, children[i].model.properties.value]);
        }
    };

    /**
     * Creates a StatisticModel to display a single statistic
     * @constructor
     * @name blueriq.services.StatisticModel
     * @param {Object} model - The model for the statistic
     * @param {Object} context - The data context
     * @returns {@link blueriq.models.StatisticModel} single statistic-model
     */
    blueriq.models.StatisticModel = function StatisticModel(model, context){
        var self = this;
        blueriq.models.ContainerModel.call(self, model, context);
        this.style = model.styles[0];
        this.statistic = model.properties.value;
    };

}(window.blueriq, window.bqApp, window.ko));