(function () {
    'use strict';
    /* jshint validthis: true */

    /**
     * @directive bbElement
     * @ngInject
     * @example
     *      <bb-element data-key="element.key"></bb-element>
     */
    function bbElementDirective() {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'BBElementController as $ctrl',
            scope: {},
            require: {
                form: '^^bbForm'
            }
        };
    }

    function BBElementController($scope, $element, $attrs, ElementService) {
        var ctrl = this;
        $attrs.$observe('key', updateElement);
        $scope.$watch('element.children', elementChildrenWatch);
        $scope.$watch('element._e', elementWatch);

        function updateElement() {
            //HAPPENS ONCE PER ITEM.
            // It sets the key for the dom object.
            var key = $attrs.key;
            if(!key) return;
            if (key.indexOf("{{") !== -1) return;
            var model = ctrl.form.getElementByKey(key);
            if (model) {
                // ClearsDOM when changing pages or first time
                //--
                $element.html('');
                //--
                $scope.element = model;
                updateComponent();
            }

            // TODO check for memory leaks related to transclusion
        }
        function elementWatch() {
            //if ($scope.element)console.log("BElement:: ElementModelChanged To: ",$scope.element._e);
        }

        function elementChildrenWatch() {
            var key = $attrs.key;
            var model = ctrl.form.getElementByKey(key);
            if (model && model.contentStyle ==="fileupload") {
                $scope.element = model;
                $element.html('');
                if (model) updateComponent();
            }
        }

        function updateComponent() {
            var excludeElements = 'bbControl' in $attrs;
            var model = $scope.element;
            var component = ElementService.findComponentForElement(model, excludeElements);

            if (component) {
                ElementService.runComponent(component, $scope, $element);
            }
        }
    }

    var app = angular.module('forms-ui');
    app.controller('BBElementController', BBElementController);
    app.directive('bbElement', bbElementDirective);
})();
