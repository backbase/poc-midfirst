(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'story-selector',
            condition: function(el) {
                return [
                    el.isContainer,
                    el.styles.indexOf('story_selector') > -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();