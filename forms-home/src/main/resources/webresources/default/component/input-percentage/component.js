(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'input-percentage',
            condition: function(el) {
                return [
                    el.dataType === 'percentage'
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();