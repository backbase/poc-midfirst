#!/usr/bin/env bash

echo "--- Start portals availability checker ---"
for var in "$@"
do
    response=$(curl --user admin:admin --write-out %{http_code} --silent --output /dev/null $var)

    if [ $response -ne 200 ]
    then
        echo "ERROR! Portal isn't available on $var (response code $response)"
        exit 1
    else
        echo "OK on $var"
    fi
done

echo "--- Stop portals availability checker ---"
