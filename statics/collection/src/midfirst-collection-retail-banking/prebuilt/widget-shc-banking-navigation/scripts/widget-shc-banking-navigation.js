/* global b$, $, gadgets */
(function () {
    'use strict';

    const Container = b$.bdom.getNamespace('http://backbase.com/2013/portalView').getClass('container');

    const TRANSACTIONS_PANEL_KEY = 'panel-transactions';
    const TRANSACTIONS_PANEL_URL = 'transactions';

    var selected = null;
    var onClickSet = false;

    const slugify = function (str) {
        return str.toString()
            .toLowerCase()
            .replace(/\s+/g, '-')
            .replace(/[^\w\-]+/g, '')
            .replace(/\-\-+/g, '-')
            .replace(/^-+/, '')
            .replace(/-+$/, '');
    };

    // highlight appropriate link in nav when a panel is selected
    // this way the popstate / back button updates selected link
    gadgets.pubsub.subscribe('panel-selected', function (uuid) {
        const selected = $('.banking-navigation__link--selected');
        selected.removeClass('banking-navigation__link--selected');
        const link = uuid === TRANSACTIONS_PANEL_KEY
            ? $('.banking-navigation__link:first-of-type')
            : $('a[data-uuid="'+ uuid +'"]');
        link.addClass('banking-navigation__link--selected');
    });

    Container.extend(function () {
        Container.apply(this, arguments);
        this.isPossibleDragTarget = true;
    }, {
        localName: 'templateShcSimpleNavigation',
        namespaceURI: 'templates_templateShcSimpleNavigation'
    }, {
        template: function (json) {
            const data = {item: json.model.originalItem};
            return window[this.namespaceURI][this.localName](data);
        },
        handlers: {
            DOMReady: function () {
                const links = document.querySelectorAll('.banking-navigation__link');
                // 1. setup on click for each link
                if (onClickSet === false) {
                    $(links).click(function (event) {
                        const link = event.currentTarget;
                        const uuid = $(link).attr('data-uuid');
                        const slug = slugify($(link).attr('data-title'));
                        gadgets.pubsub.publish('panel-select', { key: uuid, url: slug });
                    });
                    onClickSet = true;
                }
                // 2. load the appropriate panel
                if (selected === null) {
                    const hash = window.location.hash;
                    const emptyHash = hash.length === 0 || !hash;
                    setTimeout(function () {
                        // load first link by default
                        if (emptyHash) $(links[0]).click();
                        // load product summary if hash points to transactions
                        else if (hash.indexOf(TRANSACTIONS_PANEL_URL) > -1) $(links[0]).click();
                        // else load based on URL hash
                        else {
                            const slug = hash.substr(2);
                            const list = [].slice.call(links);
                            const link = list.filter(function (link) {
                                return slugify($(link).attr('data-title')) === slug;
                            })[0];
                            $(link).click();
                        }
                    }, 200);

                }
                // 3. trigger garbage collection of banking panels
                setTimeout(function () {
                    const uuids = [];
                    links.forEach(function(link) {
                        uuids.push($(link).attr('data-uuid'));
                    });
                    gadgets.pubsub.publish('panel-cleanup', uuids);
                }, 250);
            },
            preferencesSaved: function (event) {
                if (event.target === this) {
                    this.refreshHTML(function () {
                        //add code, HTML refreshed
                    });
                }
            }
        }
    });
})();
