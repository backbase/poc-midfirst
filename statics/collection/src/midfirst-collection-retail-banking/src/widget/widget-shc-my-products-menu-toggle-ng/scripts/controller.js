/**
 * @module widget-shc-my-products-menu-toggle-ng
 * @name MyProductsMenuToggleController
 *
 * @description
 * My products menu button
 */

export default function MyProductsMenuToggleController(bus, hooks, widget) {
  const $ctrl = this;

  /**
   * AngularJS Lifecycle hook used to initialize the controller
   *
   * @name MyProductsMenuToggleController#$onInit
   * @returns {void}
   */
  const $onInit = () => {
    $ctrl.items = hooks.itemsFromModel([]);

    bus.publish('cxp.item.loaded', {
      id: widget.getId(),
    });

    bus.publish('bb.item.loaded', {
      id: widget.getId(),
    });
  };

  Object.assign($ctrl, {
    $onInit,

  });
}
