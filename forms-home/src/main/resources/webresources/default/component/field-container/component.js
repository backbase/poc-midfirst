(function () {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerElement({
            name: 'field-container',
            condition: function(el) {
                return [
                    el.isContainer,
                    el.styles.indexOf("concatenated_field") > -1
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();