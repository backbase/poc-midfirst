(function (blueriq, bqApp) {
    'use strict';

    blueriq.cmisModelFactory = function (model, context) {
        if (model && model.name === 'fileDownload') {
            return new blueriq.models.FileDownloadLinkModel(model, context);
        }
    };

    if (bqApp) {
        bqApp.modelFactory.register(blueriq.cmisModelFactory);
    }

    blueriq.models.FileDownloadLinkModel = function(model, context) {
        var self = this;
        blueriq.models.BaseModel.call(self, model, context);

        this.type = 'fileDownloadLink';

        this.url = bqApp.configuration.baseUri + context.session.id + '/api/filedownload/download/' + model.parameters.downloadid;
        this.text = context.session.getModel(model.textRef).nodes[0].text;
    };
})(window.blueriq, window.bqApp);