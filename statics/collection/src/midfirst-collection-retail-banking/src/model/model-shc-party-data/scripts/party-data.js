/**
 * Model factory for model-shc-insurance-party-data
 *
 * @inner
 * @type {function}
 * @param {Object} Promise An ES2015 compatible `Promise` object.
 *
 * @return {Object}
 */
export default function shcPartyModel(Promise,dataModule) {

  /**
   * @name shcPartyModel#getInfo
   * @type {function}
   *
   * @description
   * return party data.
   *
   * @returns {Promise.<Object>} A Promise with some data.
   */
  function getUserInfo() {
      return dataModule.getUserInfo();
  }

  /**
   * @name shcPartyModel
   * @type {Object}
   */
  return {
      getUserInfo,
  };
}
