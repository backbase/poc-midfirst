(function() {

    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'input-checkbox',
            condition: function(el) {
                return [
                    el.dataType === 'boolean'
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();
