/**
 * @name ShcAvatarController
 * @type {object}
 *
 * @description
 * Widget Shc Avatar widget
 */

import Cookies from './cookies';

export default function ShcAvatarController(widget, pPartyDataModule, pLoginModule) {
	const $ctrl = this;
	const portal = window.b$.portal;

	const logoutRedirectPage = widget.getStringPreference('logoutRedirectPage');
	const userDataUrl = widget.getStringPreference('userDataUrl');

	const logoutRedirectUrl =
		`${portal.config.serverRoot}/${portal.portalName}/${logoutRedirectPage}`;


	const cookies = Cookies.getCookies();

	const token = {
		key: 'BBXSRF',
		value: ''
	};


	const $onInit = () => {
		token.value = cookies[token.key];
		//--
		$ctrl.userInfo = null;
		pPartyDataModule.getUserInfo().then(function (pData) {
			$ctrl.userInfo = pData.data;
		});
	};

	const logout = () => {
		pLoginModule.logout(token.value)
			.then(() => {
				window.location.assign(logoutRedirectUrl);
			});
	};


	const loggedUser = () => {
		// for anonymous user portal returns null as a string
		if (!portal.loggedInUserId || portal.loggedInUserId === 'null') {
			return '';
		}

		return portal.loggedInUserId;
	};


	Object.assign($ctrl, {
		$onInit,
		loggedUser,
		logout,
	});
}
