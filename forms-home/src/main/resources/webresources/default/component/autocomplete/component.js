(function () {
    /**
     * @ngInject
     */
    function registerComponent(ComponentRegistry) {
        ComponentRegistry.registerControl({
            name: 'autocomplete',
            controller: 'AutoCompleteController as controller',
            condition: function (el) {
                return [
                    el.styles.indexOf("autocomplete") >= 0,
                    el.hasOptions,
                    !el.multiple
                ];
            }
        });
    }

    angular.module('forms-ui').run(registerComponent);
})();
