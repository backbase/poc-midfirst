(function () {
    angular.module('forms-ui').directive('statementAgree', /*@ngInject*/ function () {
        return {
            restrict: 'A',
            link: function ($scope, element) {
                var context = (element.context) ? element.context : element[0];
                //--
                var scopeValue = $scope.$parent.element.value[0];
                var isBoolean = Boolean(scopeValue);
                if (isBoolean) $scope.checkedVal = (scopeValue == 'true');
                //--
                function emitVal() {
                    $scope.$parent.element.value = ["" + $scope.checkedVal];
                    $scope.$emit('update');
                }

                $scope.inputCheck = function () {
                    emitVal();
                };
                $scope.toggleInput = function () {
                    $scope.checkedVal = !$scope.checkedVal;
                    emitVal();
                };
            }
        };
    });
})();
